-- 4.10
CREATE TABLE workunit_marker(
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` VARCHAR(255) NOT NULL,
  `cid` CHAR(16) NOT NULL DEFAULT 'wumarker5ab4g',
  `workunitId` int NOT NULL,
  `userId` VARCHAR(255) NOT NULL,
  `message` TEXT NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `workunit_marker` ADD UNIQUE (`uid`);
ALTER TABLE `workunit_marker` ADD UNIQUE `WORKUNIT_MARKER_U_WUSUER` (`workunitId`, `userId`);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_UID` (`uid` ASC);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_USERID` (`userId` ASC);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_WUID` (`workunitId` ASC);

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.10',now());
