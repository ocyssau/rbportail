ALTER TABLE `workunit` 
	ADD COLUMN `lastDeliveryDate` Datetime DEFAULT NULL AFTER `deliveryDate`;

UPDATE `workunit` SET `lastDeliveryDate`=`deliveryDate`;
	
-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.16',now());
