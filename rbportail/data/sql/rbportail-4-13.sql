--
-- Table structure for table `acl`
--
CREATE TABLE `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resourceId` varchar(255) NOT NULL,
  `resourceName` varchar(255) DEFAULT NULL,
  `roleId` varchar(255) NOT NULL,
  `roleName` varchar(255) DEFAULT NULL,
  `userId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId` (`userId`,`roleId`,`resourceId`),
  KEY `ACL_RESOURCE` (`resourceId`),
  KEY `ACL_VISIBILITY` (`roleId`),
  KEY `ACL_USERID` (`userId`),
  KEY `ACL_INDEX` (`userId`,`roleId`,`resourceId`),
  KEY `ACL_RESOURCENAME` (`resourceName`),
  KEY `ACL_ROLEID` (`roleId`),
  KEY `ACL_ROLENAME` (`roleName`)
) ENGINE=InnoDB AUTO_INCREMENT=88;

--
-- Temporary table structure for view `anywf`
--

DROP TABLE IF EXISTS `anywf`;
/*!50001 DROP VIEW IF EXISTS `anywf`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `anywf` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `cid`,
 1 AS `name`,
 1 AS `title`,
 1 AS `ownerId`,
 1 AS `parentId`,
 1 AS `parentUid`,
 1 AS `updateById`,
 1 AS `updated`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `anywu`
--

DROP TABLE IF EXISTS `anywu`;
/*!50001 DROP VIEW IF EXISTS `anywu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `anywu` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `cid`,
 1 AS `name`,
 1 AS `ownerId`,
 1 AS `parentId`,
 1 AS `parentUid`,
 1 AS `updateById`,
 1 AS `updated`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `bill`
--
CREATE TABLE `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `emitDate` datetime DEFAULT NULL,
  `orderId` varchar(255) DEFAULT NULL,
  `orderDate` datetime DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `BILL_UID` (`uid`),
  KEY `BILL_NAME` (`name`),
  KEY `BILL_PARENTID` (`parentId`),
  KEY `BILL_PARENTUID` (`parentUid`),
  KEY `BILL_CLASSID` (`cid`),
  KEY `BILL_OWNER` (`ownerId`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `bill_note`
--

CREATE TABLE `bill_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `billId` int(11) NOT NULL,
  `emitDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `BILL_NOTE_UID` (`uid`),
  KEY `BILL_NOTE_NAME` (`name`),
  KEY `BILL_NOTE_PARENTID` (`parentId`),
  KEY `BILL_NOTE_PARENTUID` (`parentUid`),
  KEY `BILL_NOTE_CLASSID` (`cid`),
  KEY `BILL_NOTE_OWNER` (`ownerId`),
  KEY `BILL_NOTE_BILLID` (`billId`)
) ENGINE=InnoDB;

--
-- Table structure for table `component`
--

CREATE TABLE `component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `internalref` varchar(255) NOT NULL,
  `index` int(11) DEFAULT NULL,
  `visibility` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `attributes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `COMPONENT_INDEX` (`index`),
  KEY `COMPONENT_VISIBILITY` (`visibility`),
  KEY `COMPONENT_PARENTID` (`parentId`),
  KEY `COMPONENT_PARENTUID` (`parentUid`),
  KEY `COMPONENT_CLASSID` (`cid`),
  KEY `COMPONENT_INTERNALREF` (`internalref`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `deliverable`
--
CREATE TABLE `deliverable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `circuit` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `projeteur` varchar(255) DEFAULT NULL,
  `indice` varchar(255) DEFAULT NULL,
  `codeFiliere` varchar(255) DEFAULT NULL,
  `codeMer` varchar(255) DEFAULT NULL,
  `map` varchar(255) DEFAULT NULL,
  `ds` varchar(255) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `msn` varchar(255) DEFAULT NULL,
  `protectionCode` varchar(255) DEFAULT NULL,
  `beCode` varchar(255) DEFAULT NULL,
  `functionalClass` varchar(255) DEFAULT NULL,
  `drawingType` varchar(255) DEFAULT NULL,
  `configItem` varchar(255) DEFAULT NULL,
  `layout` varchar(255) DEFAULT NULL,
  `mod` varchar(128) DEFAULT NULL,
  `team` varchar(128) DEFAULT NULL,
  `revision` varchar(128) DEFAULT NULL,
  `gildaStatus` varchar(255) DEFAULT NULL,
  `officialDate` date DEFAULT NULL,
  `compilerDate` date DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DELIVERABLE_UID` (`uid`),
  KEY `DELIVERABLE_NAME` (`name`),
  KEY `DELIVERABLE_PARENTID` (`parentId`),
  KEY `DELIVERABLE_PARENTUID` (`parentUid`),
  KEY `DELIVERABLE_CLASSID` (`cid`),
  KEY `DELIVERABLE_OWNER` (`ownerId`),
  KEY `DELIVERABLE_DS` (`ds`),
  KEY `DELIVERABLE_CI` (`ci`),
  KEY `DELIVERABLE_PROJETEUR` (`projeteur`),
  KEY `DELIVERABLE_CODEF` (`codeFiliere`),
  KEY `DELIVERABLE_CODEM` (`codeMer`),
  KEY `DELIVERABLE_CIRCUIT` (`circuit`),
  KEY `DELIVERABLE_SECTION` (`section`),
  KEY `DELIVERABLE_STATUS` (`status`),
  KEY `DELIVERABLE_MSN` (`msn`),
  KEY `DELIVERABLE_MOD` (`mod`),
  KEY `DELIVERABLE_TEAM` (`team`),
  KEY `DELIVERABLE_REVISION` (`revision`),
  KEY `I_GILDASTATUS` (`gildaStatus`),
  CONSTRAINT `FK_workunit_1` FOREIGN KEY (`parentId`) REFERENCES `workunit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `discussion_comment`
--
CREATE TABLE `discussion_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` char(16) NOT NULL DEFAULT '568be4fc7a0a8',
  `name` varchar(255) DEFAULT NULL,
  `discussionUid` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DISCUSSION_COMMENT_UID` (`uid`),
  KEY `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussionUid`),
  KEY `DISCUSSION_COMMENT_OWNERID` (`ownerId`),
  KEY `DISCUSSION_COMMENT_PARENTID` (`parentId`),
  KEY `DISCUSSION_COMMENT_PARENTUID` (`parentUid`),
  KEY `DISCUSSION_COMMENT_UPDATED` (`updated`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `extendedmodel`
--
CREATE TABLE `extendedmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extendedCid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `asapp` varchar(255) DEFAULT NULL,
  `getter` varchar(255) DEFAULT NULL,
  `setter` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `required` tinyint(4) DEFAULT NULL,
  `min` varchar(255) DEFAULT NULL,
  `max` varchar(255) DEFAULT NULL,
  `step` varchar(255) DEFAULT NULL,
  `list` varchar(255) DEFAULT NULL,
  `multiple` tinyint(4) DEFAULT NULL,
  `return` enum('name','value') DEFAULT NULL,
  `dbtable` varchar(255) DEFAULT NULL,
  `dbFieldForName` varchar(255) DEFAULT NULL,
  `dbFieldForValue` varchar(255) DEFAULT NULL,
  `dbfilter` varchar(255) DEFAULT NULL,
  `dbquery` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `extendedCid` (`extendedCid`,`name`),
  KEY `EXTENDED_EXTENDEDCID` (`extendedCid`),
  KEY `EXTENDED_NAME` (`name`)
) ENGINE=InnoDB;

--
-- Table structure for table `link_workunit_type_context`
--
CREATE TABLE `link_workunit_type_context` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL,
  `parentUid` varchar(255) NOT NULL,
  `childId` int(11) NOT NULL,
  `childUid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_parent_child` (`parentId`,`childId`),
  KEY `WORKUNIT_TYPE_CONTEXT_PARENTID` (`parentId`),
  KEY `WORKUNIT_TYPE_CONTEXT_PARENTUID` (`parentUid`),
  KEY `WORKUNIT_TYPE_CONTEXT_CHILDID` (`childId`),
  KEY `WORKUNIT_TYPE_CONTEXT_CHILDUID` (`childUid`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `schema_version`
--

CREATE TABLE `schema_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(16) DEFAULT NULL,
  `lastModification` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `SITE_CLASSID` (`cid`),
  KEY `SITE_NAME` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5;

--
-- Table structure for table `user`
--
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `login` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `lastlogin` date DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `identity` varchar(255) DEFAULT NULL,
  `memberof` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `uid` (`uid`),
  KEY `USER_UID` (`uid`),
  KEY `USER_CLASSID` (`cid`),
  KEY `USER_LOGIN` (`login`),
  KEY `USER_IDENTITY` (`identity`),
  KEY `USER_MEMBEROF` (`memberof`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wf_activity`
--

CREATE TABLE `wf_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `normalizedName` varchar(256) DEFAULT NULL,
  `isInteractive` tinyint(1) DEFAULT '0',
  `isAutorouted` tinyint(1) DEFAULT '0',
  `isAutomatic` tinyint(1) DEFAULT '0',
  `isComment` tinyint(1) DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `progression` float DEFAULT NULL,
  `expirationTime` datetime DEFAULT NULL,
  `roles` text,
  `attributes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `name` (`name`,`processId`),
  KEY `WFACTIVITY_normalizedName` (`normalizedName`),
  KEY `WFACTIVITY_uid` (`uid`),
  KEY `WFACTIVITY_name` (`name`),
  KEY `WFACTIVITY_parentuid` (`parentUid`),
  KEY `WFACTIVITY_parentid` (`parentId`),
  KEY `WFACTIVITY_cid` (`cid`),
  KEY `WFACTIVITY_ownerId` (`ownerId`),
  KEY `WFACTIVITY_processId` (`processId`),
  KEY `WFACTIVITY_roles` (`roles`(100)),
  KEY `progression` (`progression`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wf_instance`
--
CREATE TABLE `wf_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `nextActivities` text,
  `nextUsers` text,
  `attributes` text,
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFINSTANCE_uid` (`uid`),
  KEY `WFINSTANCE_name` (`name`),
  KEY `WFINSTANCE_parentuid` (`parentUid`),
  KEY `WFINSTANCE_parentid` (`parentId`),
  KEY `WFINSTANCE_cid` (`cid`),
  KEY `WFINSTANCE_ownerId` (`ownerId`),
  KEY `WFINSTANCE_processId` (`processId`),
  KEY `WFINSTANCE_nextActivities` (`nextActivities`(100)),
  KEY `WFINSTANCE_nextUsers` (`nextUsers`(100)),
  KEY `WFINSTANCE_started` (`started`),
  KEY `WFINSTANCE_ended` (`ended`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wf_instance_activity`
--
CREATE TABLE `wf_instance_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `instanceId` int(11) DEFAULT NULL,
  `activityId` int(11) NOT NULL,
  `status` varchar(64) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `attributes` text,
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFINSTACTIVITY_uid` (`uid`),
  KEY `WFINSTACTIVITY_name` (`name`),
  KEY `WFINSTACTIVITY_parentuid` (`parentUid`),
  KEY `WFINSTACTIVITY_parentid` (`parentId`),
  KEY `WFINSTACTIVITY_cid` (`cid`),
  KEY `WFINSTACTIVITY_ownerId` (`ownerId`),
  KEY `WFINSTACTIVITY_instanceId` (`instanceId`),
  KEY `WFINSTACTIVITY_activityId` (`activityId`),
  KEY `WFINSTACTIVITY_type` (`type`),
  KEY `WFINSTACTIVITY_started` (`started`),
  KEY `WFINSTACTIVITY_ended` (`ended`),
  CONSTRAINT `FK_wf_instance_1` FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wf_process`
--
CREATE TABLE `wf_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` varchar(16) DEFAULT NULL,
  `normalizedName` varchar(256) DEFAULT NULL,
  `isValid` tinyint(1) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `normalizedName` (`normalizedName`),
  UNIQUE KEY `name` (`name`,`version`),
  KEY `WFPROCESS_name` (`name`),
  KEY `WFPROCESS_uid` (`uid`),
  KEY `WFPROCESS_parentuid` (`parentUid`),
  KEY `WFPROCESS_parentid` (`parentId`),
  KEY `WFPROCESS_cid` (`cid`),
  KEY `WFPROCESS_ownerId` (`ownerId`),
  KEY `WFPROCESS_normalizedName` (`normalizedName`),
  KEY `WFPROCESS_isActive` (`isActive`),
  KEY `WFPROCESS_isValid` (`isValid`),
  KEY `WFPROCESS_version` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wf_transition`
--
CREATE TABLE `wf_transition` (
  `uid` varchar(32) NOT NULL,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `parentUid` varchar(255) NOT NULL,
  `childUid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lindex` int(11) DEFAULT '0',
  `attributes` text,
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFTRANSITION_uid` (`uid`),
  KEY `WFTRANSITION_name` (`name`),
  KEY `WFTRANSITION_parentId` (`parentId`),
  KEY `WFTRANSITION_childId` (`childId`),
  KEY `WFTRANSITION_parentUid` (`parentUid`),
  KEY `WFTRANSITION_childUid` (`childUid`),
  KEY `WFTRANSITION_lindex` (`lindex`)
) ENGINE=InnoDB;

--
-- Table structure for table `workunit`
--

CREATE TABLE `workunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `version` int(11) DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `tos` varchar(128) DEFAULT NULL,
  `flt` varchar(64) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `siteUid` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `progression` float DEFAULT NULL,
  `ataLeader` varchar(255) DEFAULT NULL,
  `openDate` datetime DEFAULT NULL,
  `planDate` date DEFAULT NULL,
  `deliveryDate` datetime DEFAULT NULL,
  `supplierBillingDate` datetime DEFAULT NULL,
  `sitedeliveryDate` datetime DEFAULT NULL,
  `deliveryMemo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL,
  `visibility` varchar(32) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WORKUNIT_UID` (`uid`),
  KEY `WORKUNIT_NAME` (`name`),
  KEY `WORKUNIT_PARENTID` (`parentId`),
  KEY `WORKUNIT_PARENTUID` (`parentUid`),
  KEY `WORKUNIT_CLASSID` (`cid`),
  KEY `WORKUNIT_OWNER` (`ownerId`),
  KEY `WORKUNIT_SITE` (`siteUid`),
  KEY `WORKUNIT_TYPE` (`typeId`),
  KEY `WORKUNIT_TOS` (`tos`),
  KEY `WORKUNIT_ODATE` (`openDate`),
  KEY `WORKUNIT_PDATE` (`planDate`),
  KEY `WORKUNIT_DDATE` (`deliveryDate`),
  KEY `WORKUNIT_DM` (`deliveryMemo`),
  KEY `progression` (`progression`),
  KEY `visibility` (`visibility`),
  KEY `WORKUNIT_VERSION` (`version`),
  KEY `WORKUNIT_FLT` (`flt`),
  KEY `WORKUNIT_NUMBER` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`192.9.200.%`*/ /*!50003 TRIGGER onWuDelete AFTER DELETE ON workunit FOR EACH ROW 
BEGIN
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM workunit_process WHERE parentId=OLD.id);
	DELETE FROM workunit_process WHERE parentId = OLD.id;
END */;;
DELIMITER ;

CREATE TABLE `workunit_context` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '301',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WORKUNIT_CONTEXT_UID` (`uid`),
  KEY `WORKUNIT_CONTEXT_NAME` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=100;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workunit_marker`
--
CREATE TABLE `workunit_marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` char(16) NOT NULL DEFAULT 'wumarker5ab4g',
  `workunitId` int(11) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `WORKUNIT_MARKER_U_WUSUER` (`workunitId`,`userId`),
  KEY `WORKUNIT_MARKER_UID` (`uid`),
  KEY `WORKUNIT_MARKER_USERID` (`userId`),
  KEY `WORKUNIT_MARKER_WUID` (`workunitId`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `workunit_process`
--

DROP TABLE IF EXISTS `workunit_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workunit_process` (
  `uid` varchar(64) NOT NULL,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `childUid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lindex` int(11) DEFAULT '0',
  `attributes` text,
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `WUPROCESS_CHILDID` (`childId`) COMMENT 'Instance must be link to only one WU',
  KEY `WUPROCESS_UID` (`uid`),
  KEY `WUPROCESS_NAME` (`name`),
  KEY `WUPROCESS_LINDEX` (`lindex`),
  KEY `WUPROCESS_PARENTUID` (`parentUid`),
  KEY `WUPROCESS_CHILDUID` (`childUid`),
  KEY `WUPROCESS_PARENTID` (`parentId`)
) ENGINE=InnoDB;

--
-- Table structure for table `workunit_type`
--
CREATE TABLE `workunit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '301',
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT '2',
  `dimension` tinyint(4) DEFAULT '2',
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `tuProduction` decimal(10,3) DEFAULT NULL,
  `tuProductionTlse` decimal(10,3) DEFAULT NULL,
  `tuManagement` decimal(10,3) DEFAULT NULL,
  `tuManagementTlse` decimal(10,3) DEFAULT NULL,
  `salesRate` decimal(10,3) DEFAULT NULL,
  `costSolintep` float DEFAULT NULL,
  `costGeometric` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WORKUNIT_TYPE_UID` (`uid`),
  KEY `WORKUNIT_TYPE_NAME` (`name`),
  KEY `WORKUNIT_TYPE_PARENTID` (`parentId`),
  KEY `WORKUNIT_TYPE_PARENTUID` (`parentUid`),
  KEY `WORKUNIT_TYPE_CLASSID` (`cid`),
  KEY `WORKUNIT_TYPE_OWNER` (`ownerId`)
) ENGINE=InnoDB AUTO_INCREMENT=100;

--
-- Table structure for table `wumanager_status`
--

CREATE TABLE `wumanager_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `progression` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WUMSTATUS_UID` (`uid`),
  KEY `WUMSTATUS_NAME` (`name`),
  KEY `WUMSTATUS_PARENTID` (`parentId`),
  KEY `STATUS_PARENTUID` (`parentUid`),
  KEY `STATUS_CLASSID` (`cid`),
  KEY `STATUS_OWNER` (`ownerId`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'rbportail'
--

--
-- Dumping routines for database 'rbportail'
--
DELIMITER ;;
CREATE DEFINER=`rbportailuser`@`%` FUNCTION `joursouvres`(date_livraison date, date_planifie date) RETURNS int(11)
BEGIN
	DECLARE duree INT;
 
	SET duree = DATEDIFF(date_livraison,date_planifie);

	if date_livraison > date_planifie 
    THEN
	BEGIN
		WHILE date_planifie <= date_livraison DO
			CASE WEEKDAY(date_planifie)
				WHEN '5' THEN SET duree = duree-1;
				WHEN '6' THEN SET duree = duree-1;
				else SET duree = duree;
			END CASE;
			SET date_planifie = date_planifie + interval 1 day;
		END WHILE;
	END;
    
    ELSE
    BEGIN
		WHILE date_livraison <= date_planifie DO
			CASE WEEKDAY(date_livraison)
				WHEN '5' THEN SET duree = duree+1;
				WHEN '6' THEN SET duree = duree+1;
				else SET duree = duree;
			END CASE;
			SET date_livraison = date_livraison + interval 1 day;
		END WHILE;
    END;
    end if;
	
    RETURN duree;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE DEFINER=`rbportailuser`@`%` FUNCTION `ToDo`(Buget_management decimal, Budget_production decimal, avancement decimal) RETURNS decimal(6,4)
Begin
Declare RAF decimal(6,4);
SET RAF=((Buget_management+Budget_production)*(1-(avancement/100)));
Return RAF;
End ;;
DELIMITER ;

DELIMITER ;;
CREATE DEFINER=`root`@`192.9.200.%` PROCEDURE `deliverableWithoutWorkunit`()
BEGIN
	SELECT deliv.* FROM deliverable as deliv
	LEFT OUTER JOIN workunit as wu ON deliv.parentId=wu.id
	WHERE wu.name is null;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE DEFINER=`root`@`192.9.200.%` PROCEDURE `processInstanceWithoutWu`()
BEGIN
	SELECT wfi.id, wfi.uid, wu.id, wu.name
	FROM wf_instance as wfi
	LEFT OUTER JOIN workunit_process as lnk ON wfi.id=lnk.childId
	LEFT OUTER JOIN workunit as wu ON wu.id=lnk.parentId
	WHERE wu.name is null;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE DEFINER=`root`@`192.9.200.%` PROCEDURE `wuProcessLnkWithoutProcess`()
BEGIN
	SELECT lnk.childId, lnk.parentId, wfi.name, wfi.id, wfi.uid FROM workunit_process as lnk
	LEFT OUTER JOIN wf_instance as wfi ON lnk.childId=wfi.id
	WHERE wfi.name is null;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE DEFINER=`root`@`192.9.200.%` PROCEDURE `wuProcessLnkWithoutWu`()
BEGIN
	-- lien avec wu inexistant
	SELECT lnk.childId, lnk.parentId, wu.name, wu.id, wu.uid FROM workunit_process as lnk
	LEFT OUTER JOIN workunit as wu ON lnk.parentId=wu.id
	WHERE wu.name is null;
END ;;
DELIMITER ;

