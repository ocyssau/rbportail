ALTER TABLE wf_instance_activity ADD COLUMN `comment` TEXT NULL AFTER `ended`;

ALTER TABLE deliverable ADD COLUMN `mod` VARCHAR(128);
ALTER TABLE deliverable ADD COLUMN `team` VARCHAR(128);
ALTER TABLE deliverable ADD COLUMN `revision` VARCHAR(128);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_MOD` (`mod` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_TEAM` (`team` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_REVISION` (`revision` ASC);
