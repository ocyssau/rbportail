-- 4.14
ALTER TABLE `discussion_comment`
	CHANGE COLUMN `ownerId` `ownerUid` VARCHAR(64) NULL,
	ADD COLUMN `ownerId` INT(11) DEFAULT NULL AFTER `ownerUid`;
	
-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.14',now());

