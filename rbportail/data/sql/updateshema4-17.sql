 CREATE TABLE `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(128) NOT NULL,
 `mail` VARCHAR (128) NOT NULL,
 `referenceId` INT(11) NULL,
 `referenceUid` VARCHAR (128) NULL,
 `referenceCid` VARCHAR (128) NULL,
 `ownerId` VARCHAR (128) NULL,
 `ownerUid` VARCHAR (128) NULL,
 `event` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 KEY `INDEX_notifications_2` (`referenceUid`),
 KEY `INDEX_notifications_3` (`event`),
 UNIQUE KEY `UNIQ_notifications_1` (`uid`),
 UNIQUE KEY `UNIQ_notifications_2` (`mail`,`event`,`referenceUid`,`ownerUid`)
 ) ENGINE=MyIsam;

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.17',now());
