-- ======================================================================
-- From file Exception.php
-- ======================================================================
-- ======================================================================
-- From file Bill.php
-- ======================================================================
-- ======================================================================
-- From file BillNote.php
-- ======================================================================
-- ======================================================================
-- From file Factory.php
-- ======================================================================
-- ======================================================================
-- From file Workunit.php
-- ======================================================================
-- ======================================================================
-- From file Site.php
-- ======================================================================
INSERT INTO site (id, uid, cid, name) VALUES(1, 'sierblagnac', 303, 'SIER-BLAGNAC');
INSERT INTO site (id, uid, cid, name) VALUES(2, 'solintep', 303, 'SOLINTEP');
INSERT INTO site (id, uid, cid, name) VALUES(3, 'sierstnaz', 303, 'SIER-STNAZAIRE');
-- ======================================================================
-- From file Status.php
-- ======================================================================
-- ======================================================================
-- From file Any.php
-- ======================================================================
-- ======================================================================
-- From file Deliverable.php
-- ======================================================================
-- ======================================================================
-- From file LinkProcess.php
-- ======================================================================
-- ======================================================================
-- From file Type.php
-- ======================================================================
INSERT INTO workunit_type (id, uid, name, amount, tuProduction, tuManagement, salesRate)
					VALUES
					(1, '3D01', 'Design Solution Creation', 2485, null, null, null),
					(2, '3D02', 'Design Solution Creation', 2015, null, null, null),
					(3, '3D03', 'Design Solution Creation', 7600, null, null, null),
					(4, '3D04', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)', 1920, null, null, null),
					(5, '3D05', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)', 1320, null, null, null),
					(6, '3D06', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)', 2200, null, null, null),
					(7, '3D07', 'Design Solution Modification', 280, null, null, null),
					(8, '3D08', 'Design Solution Modification', 1320, null, null, null),
					(9, '3D09', 'Design Solution Modification & Application of STELIA Aerospace Cheklist for 1st time', 450, null, null, null),
					(10, '3D10', 'DQN implementation', 560, null, null, null),

					(11, '2D01', 'Elementary part or sub-assembly drawing set creation', 310, null, null, null),
					(12, '2D02', 'Complex elementary part drawing set creation', 495, null, null, null),
					(13, '2D03', 'Assembly drawing set creation', 1010, null, null, null),
					(14, '2D04', 'Assembly drawing set creation', 3230, null, null, null),
					(15, '2D05', 'Assembly drawing set creation resulting from', 1890, null, null, null),
					(16, '2D06', 'Elementary par drawing set modification', 125, null, null, null),
					(17, '2D07', 'Assembly drawing set modification', 245, null, null, null),
					(18, '2D08', 'Assembly drawing set modification', 550, null, null, null),
					(19, '2D09', 'Elementary part or sub-assembly drawing set creation', 720, null, null, null),
					(20, '2D10', 'Assembly drawing set creation', 1800, null, null, null),
					(21, '2D11', 'Elementary par drawing set modification', 360, null, null, null),
					(22, '2D12', 'Assembly drawing set creation', 730, null, null, null),
					(23, 'E0', 'Modification of an existing design solution', 160, null, null, null),
					(24, 'E1-1', 'Pre-study', 1100, null, null, null),
					(25, 'E1-2', 'Design solution maturity B development', 1100, null, null, null),
					(26, 'E1-3', 'Design solution maturity C development', 1100, null, null, null),
					(27, 'E2', 'Complete design solution development', 2740, null, null, null),
					(28, 'L1', 'Elementary and equipped part drawing set modification', 160, null, null, null),
					(29, 'L2', 'Installation drawing set modification', 350, null, null, null),
					(30, 'L3', 'Creation of elementary part drawing set', 280, null, null, null),
					(31, 'L4', 'Creation of installation drawing set', 710, null, null, null);
					
					
INSERT INTO workunit_context (id, uid, name)
					VALUES 
					(1, 'a350xwbs1112', 'A350XWB S11/12'),
					(2, 'a350xwbothersection', 'A350XWB OTHER SECTIONS'),
					(3, 'a350xwbhydraulic', 'A350XWB HYDRAULIC'),
					(4, 'a350xwb', 'A350XWB'),
					(5, 'a320neo', 'A320NEO');
					
INSERT INTO link_workunit_type_context (parentId, parentUid, childId, childUid)
					VALUES 
					(1, '3D01', 1, 'a350xwbs1112'),
					(2, '3D02', 2, 'a350xwbothersection'),
					(3, '3D03', 3, 'a350xwbhydraulic'),
					(4, '3D04', 1, 'a350xwbs1112'),
					(5, '3D05', 2, 'a350xwbothersection'),
					(6, '3D06', 3, 'a350xwbhydraulic'),
					(7, '3D07', 1, 'a350xwbs1112'),
					(7, '3D07', 2, 'a350xwbothersection'),
					(8, '3D08', 3, 'a350xwbhydraulic'),
					(9, '3D09', 1, 'a350xwbs1112'),
					(9, '3D09', 2, 'a350xwbothersection'),
					(10, '3D10', 3, 'a350xwbhydraulic'),
					(11, '2D01', 1, 'a350xwbs1112'),
					(11, '2D01', 2, 'a350xwbothersection'),
					(11, '2D01', 3, 'a350xwbhydraulic'),
					(12, '2D02', 1, 'a350xwbs1112'),
					(12, '2D02', 2, 'a350xwbothersection'),
					(12, '2D02', 3, 'a350xwbhydraulic'),
					(13, '2D03', 1, 'a350xwbs1112'),
					(13, '2D03', 2, 'a350xwbothersection'),
					(14, '2D04', 3, 'a350xwbhydraulic'),
					(15, '2D05', 3, 'a350xwbhydraulic'),
					(16, '2D06', 1, 'a350xwbs1112'),
					(16, '2D06', 2, 'a350xwbothersection'),
					(16, '2D06', 3, 'a350xwbhydraulic'),
					(17, '2D07', 1, 'a350xwbs1112'),
					(17, '2D07', 2, 'a350xwbothersection'),
					(18, '2D08', 3, 'a350xwbhydraulic'),
					(19, '2D09', 4, 'a320neo'),
					(20, '2D10', 4, 'a320neo'),
					(21, '2D11', 4, 'a320neo'),
					(22, '2D12', 4, 'a320neo');
					
					
-- ======================================================================
-- From file Error.php
-- ======================================================================
