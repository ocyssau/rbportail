-- ======================================================================
-- From file Exception.php
-- ======================================================================
-- ======================================================================
-- From file Bill.php
-- ======================================================================
ALTER TABLE bill ADD UNIQUE (uid);

ALTER TABLE `bill` ADD INDEX `BILL_UID` (`uid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_NAME` (`name` ASC);

ALTER TABLE `bill` ADD INDEX `BILL_PARENTID` (`parentId` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_PARENTUID` (`parentUid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_CLASSID` (`cid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_OWNER` (`ownerId` ASC);
-- ======================================================================
-- From file BillNote.php
-- ======================================================================

ALTER TABLE bill_note ADD UNIQUE (uid);

ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_UID` (`uid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_NAME` (`name` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_PARENTID` (`parentId` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_PARENTUID` (`parentUid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_CLASSID` (`cid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_OWNER` (`ownerId` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_BILLID` (`billId` ASC);
-- ======================================================================
-- From file Factory.php
-- ======================================================================
-- ======================================================================
-- From file Workunit.php
-- ======================================================================
ALTER TABLE workunit ADD UNIQUE (uid);

ALTER TABLE `workunit` ADD INDEX `WORKUNIT_UID` (`uid` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_NAME` (`name` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_PARENTID` (`parentId` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_PARENTUID` (`parentUid` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_CLASSID` (`cid` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_OWNER` (`ownerId` ASC);

ALTER TABLE `workunit` ADD INDEX `WORKUNIT_SITE` (`siteUid` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_TYPE` (`typeId` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_TOS` (`tos` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_ODATE` (`openDate` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_PDATE` (`planDate` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_DDATE` (`deliveryDate` ASC);
ALTER TABLE `workunit` ADD INDEX `WORKUNIT_DM` (`deliveryMemo` ASC);
-- ======================================================================
-- From file Site.php
-- ======================================================================
ALTER TABLE site ADD UNIQUE (uid);

ALTER TABLE `site` ADD INDEX `SITE_CLASSID` (`cid` ASC);
ALTER TABLE `site` ADD INDEX `SITE_NAME` (`name` ASC);
-- ======================================================================
-- From file Status.php
-- ======================================================================
ALTER TABLE wumanager_status ADD UNIQUE (uid);

ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_UID` (`uid` ASC);
ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_NAME` (`name` ASC);
ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_PARENTID` (`parentId` ASC);
ALTER TABLE `wumanager_status` ADD INDEX `STATUS_PARENTUID` (`parentUid` ASC);
ALTER TABLE `wumanager_status` ADD INDEX `STATUS_CLASSID` (`cid` ASC);
ALTER TABLE `wumanager_status` ADD INDEX `STATUS_OWNER` (`ownerId` ASC);
-- ======================================================================
-- From file Any.php
-- ======================================================================
-- ======================================================================
-- From file Deliverable.php
-- ======================================================================
ALTER TABLE deliverable ADD UNIQUE (uid);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_UID` (`uid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_NAME` (`name` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PARENTID` (`parentId` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PARENTUID` (`parentUid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CLASSID` (`cid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_OWNER` (`ownerId` ASC);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_DS` (`ds` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CI` (`ci` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PROJETEUR` (`projeteur` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CODEF` (`codeFiliere` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CODEM` (`codeMer` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CIRCUIT` (`circuit` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_SECTION` (`section` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_STATUS` (`status` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_MSN` (`msn` ASC);
-- ======================================================================
-- From file LinkProcess.php
-- ======================================================================
ALTER TABLE workunit_process ADD UNIQUE (uid);

ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_UID` (`uid` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_NAME` (`name` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_LINDEX` (`lindex` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_PARENTUID` (`parentUid` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_CHILDUID` (`childUid` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_PARENTID` (`parentId` ASC);
ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_CHILDID` (`childId` ASC);
-- ======================================================================
-- From file Type.php
-- ======================================================================

 ALTER TABLE workunit_type ADD UNIQUE (uid);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_UID` (`uid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_NAME` (`name` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_PARENTID` (`parentId` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_CLASSID` (`cid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_OWNER` (`ownerId` ASC);
 
 ALTER TABLE workunit_context ADD UNIQUE (uid);
 ALTER TABLE `workunit_context` ADD INDEX `WORKUNIT_CONTEXT_UID` (`uid` ASC);
 ALTER TABLE `workunit_context` ADD INDEX `WORKUNIT_CONTEXT_NAME` (`name` ASC);
 
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_PARENTID` (`parentId` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_CHILDID` (`childId` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_CHILDUID` (`childUid` ASC);
-- ======================================================================
-- From file Error.php
-- ======================================================================
