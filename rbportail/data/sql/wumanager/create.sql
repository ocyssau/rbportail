CREATE TABLE bill(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`emitDate` datetime NULL,
	`orderId` varchar(255) NULL,
	`amount` varchar(255) NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE bill_note(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`billId` int NOT NULL,
	`emitDate` datetime NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE workunit(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`typeId` int NULL,
	`siteUid` varchar(255) NULL,
	`processId` INT NULL,
	`tos` varchar(255) NULL,
	`openDate` datetime NULL,
	`planDate` datetime NULL,
	`deliveryDate` datetime NULL,
	`deliveryMemo` varchar(255) NULL,
	`status` varchar(255) NULL,
	`comment` varchar(512) NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE site(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE wumanager_status(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`progression` varchar(255) NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE deliverable(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`status` varchar(255) NULL,
	`section` varchar(255) NULL,
	`circuit` varchar(255) NULL,
	`comment` varchar(255) NULL,
	`projeteur` varchar(255) NULL,
	`indice` varchar(255) NULL,
	`codeFiliere` varchar(255) NULL,
	`codeMer` varchar(255) NULL,
	`map` varchar(255) NULL,
	`ds` varchar(255) NULL,
	`ci` varchar(255) NULL,
	`msn` varchar(255) NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE workunit_process(
	`uid` VARCHAR(64) NOT NULL,
	`parentId` INTEGER NOT NULL,
	`childId` INTEGER NOT NULL,
	`parentUid` VARCHAR(255) NULL,
	`childUid` VARCHAR(255) NULL,
	`name` VARCHAR(255) NULL,
	`lindex` INTEGER DEFAULT 0,
	`attributes` TEXT NULL,
PRIMARY KEY (`parentId`, `childId`)
);
CREATE TABLE workunit_type(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL DEFAULT 301,
	`name` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`context` varchar(255) NULL,
	`amount` float NULL,
	`tuProduction` datetime NULL,
	`tuManagement` datetime NULL,
	`salesRate` varchar(255) NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE workunit_context(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL DEFAULT 301,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);
CREATE TABLE link_workunit_type_context(
	`id` int NOT NULL AUTO_INCREMENT,
	`parentId` int NOT NULL,
	`parentUid` VARCHAR(255) NOT NULL,
	`childId` int NOT NULL,
	`childUid` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);
