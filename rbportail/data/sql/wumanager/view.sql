-- ======================================================================
-- From file Exception.php
-- ======================================================================
-- ======================================================================
-- From file Bill.php
-- ======================================================================
-- ======================================================================
-- From file BillNote.php
-- ======================================================================
-- ======================================================================
-- From file Factory.php
-- ======================================================================
-- ======================================================================
-- From file Workunit.php
-- ======================================================================
-- ======================================================================
-- From file Site.php
-- ======================================================================
-- ======================================================================
-- From file Status.php
-- ======================================================================
-- ======================================================================
-- From file Any.php
-- ======================================================================
CREATE OR REPLACE VIEW anywu AS
	SELECT id, uid, cid, name,  ownerId, parentId, parentUid, updateById, updated FROM workunit
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM deliverable
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM bill
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM bill_note;
-- ======================================================================
-- From file Deliverable.php
-- ======================================================================
-- ======================================================================
-- From file LinkProcess.php
-- ======================================================================
-- ======================================================================
-- From file Type.php
-- ======================================================================
-- ======================================================================
-- From file Error.php
-- ======================================================================
