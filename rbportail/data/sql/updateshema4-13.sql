-- 4.13
ALTER TABLE `workunit_type`
	ADD COLUMN  `costSolintep` float DEFAULT NULL,
	ADD COLUMN  `costGeometric` float DEFAULT NULL;
	
-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.13',now());
