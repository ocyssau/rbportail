ALTER TABLE `workunit_type`
	ADD COLUMN `newCostGeometric` float DEFAULT NULL;

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.15',now());
