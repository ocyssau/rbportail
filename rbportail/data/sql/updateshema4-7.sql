-- 4.7
ALTER TABLE deliverable ADD COLUMN `quantity` INTEGER DEFAULT 1;

-- 4.7.1
ALTER TABLE workunit ADD COLUMN `quantity` INTEGER DEFAULT 1;
CREATE TABLE tempcwu (
	SELECT id,uid,name,count(`name`) as quantity FROM workunit GROUP BY `name`
);
UPDATE workunit SET quantity = (SELECT quantity FROM tempcwu WHERE tempcwu.id=workunit.id);
DROP TABLE tempcwu;

CREATE TABLE tempworkunitbackup (
	SELECT * FROM workunit WHERE quantity IS NULL ORDER BY name ASC
);

-- ADD CONSTRAINT wf_instance_activity -> wf_instance
ALTER TABLE `wf_instance_activity` 
  ADD CONSTRAINT `FK_wf_instance_1`
    FOREIGN KEY (`instanceId`)
    REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Wu-Process link without process
DELIMITER $$
CREATE PROCEDURE `wuProcessLnkWithoutProcess`()
BEGIN
	SELECT lnk.childId, lnk.parentId, wfi.name, wfi.id, wfi.uid FROM workunit_process as lnk
	LEFT OUTER JOIN wf_instance as wfi ON lnk.childId=wfi.id
	WHERE wfi.name is null;
END$$
DELIMITER ;

-- Wu-Process link without Workunit
DELIMITER $$
CREATE PROCEDURE `wuProcessLnkWithoutWu`()
BEGIN
	-- lien avec wu inexistant
	SELECT lnk.childId, lnk.parentId, wu.name, wu.id, wu.uid FROM workunit_process as lnk
	LEFT OUTER JOIN workunit as wu ON lnk.parentId=wu.id
	WHERE wu.name is null;
END$$
DELIMITER ;

-- DELETE wu-process-link without valid workunit
CREATE TABLE tmplnkwithoutwu (
SELECT lnk.parentId,lnk.parentUid, lnk.childId, lnk.childUid FROM workunit_process as lnk
LEFT OUTER JOIN workunit as wu ON lnk.parentId=wu.id
WHERE wu.name is null);
DELETE FROM workunit_process WHERE parentId IN (
	SELECT parentId FROM tmplnkwithoutwu
);
DROP TABLE tmplnkwithoutwu;

-- DELETE wu-process-link without valid processInstance
CREATE TABLE tmplnkwithoutinstance (
SELECT lnk.* FROM workunit_process as lnk
LEFT OUTER JOIN wf_instance as wfi ON lnk.childId=wfi.id
WHERE wfi.name is null
);
DELETE FROM workunit_process WHERE childId IN (
	SELECT childId FROM tmplnkwithoutinstance
);
DROP TABLE tmplnkwithoutinstance;

-- ON DELETE WORKUNIT TRIGGER, delete wu-process link and process instances
delimiter $$
CREATE TRIGGER onWuDelete AFTER DELETE ON workunit FOR EACH ROW 
BEGIN
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM workunit_process WHERE parentId=OLD.id);
	DELETE FROM workunit_process WHERE parentId = OLD.id;
END;$$




-- ************************
CREATE TABLE IF NOT EXISTS `schema_version` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
	`version` varchar(16) NULL,
	`lastModification` DATETIME NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyIsam;
INSERT INTO schema_version(version,lastModification) VALUES('4.7',now());
INSERT INTO schema_version(version,lastModification) VALUES('4.7.1',now());



