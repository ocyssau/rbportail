-- 4.8
ALTER TABLE workunit 
	ADD COLUMN `version` INTEGER DEFAULT 1 AFTER `cid`,
	ADD INDEX `WORKUNIT_VERSION` (`version` ASC),
	ADD UNIQUE `U_WU_NAME_VERSION` (name, version);
	
-- DELETE deliverable without valid workunit
CREATE TABLE tmpdelivwithoutwu (
SELECT deliv.* FROM deliverable as deliv
LEFT OUTER JOIN workunit as wu ON deliv.parentId=wu.id
WHERE wu.name is null
);
DELETE FROM deliverable WHERE id IN (
	SELECT id FROM tmpdelivwithoutwu
);
DROP TABLE tmpdelivwithoutwu;

-- ADD CONSTRAINT workunit -> deliverable
ALTER TABLE `deliverable` 
  ADD CONSTRAINT `FK_workunit_1`
    FOREIGN KEY (`parentId`)
    REFERENCES `workunit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- procedure
CREATE PROCEDURE `deliverableWithoutWorkunit` ()
BEGIN
	SELECT deliv.* FROM deliverable as deliv
	LEFT OUTER JOIN workunit as wu ON deliv.parentId=wu.id
	WHERE wu.name is null;
END
    

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.8',now());

