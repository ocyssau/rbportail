-- ======================================================================
-- From file Process.php
-- ======================================================================
ALTER TABLE wf_process ADD UNIQUE (uid);
ALTER TABLE wf_process ADD UNIQUE (normalizedName);
ALTER TABLE wf_process ADD UNIQUE (name, version);

ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_name` (`name` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_uid` (`uid` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_parentid` (`parentId` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_cid` (`cid` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_normalizedName` (`normalizedName` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_isActive` (`isActive` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_isValid` (`isValid` ASC);
ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_version` (`version` ASC);
-- ======================================================================
-- From file Activity.php
-- ======================================================================
ALTER TABLE `wf_instance_activity` ADD UNIQUE (uid);
ALTER TABLE `wf_instance_activity` ADD UNIQUE (`activityId` ,`instanceId`);

ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_uid` (`uid` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_name` (`name` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_parentid` (`parentId` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_cid` (`cid` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_instanceId` (`instanceId` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_activityId` (`activityId` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_type` (`type` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_started` (`started` ASC);
ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_ended` (`ended` ASC);
-- ======================================================================
-- From file Activity.php
-- ======================================================================
ALTER TABLE `wf_activity` ADD UNIQUE (uid);
ALTER TABLE `wf_activity` ADD UNIQUE (`name` ,`processId`);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_normalizedName` (`normalizedName` ASC);

ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_uid` (`uid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_name` (`name` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_parentid` (`parentId` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_cid` (`cid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_processId` (`processId` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_roles` (roles(100) ASC);

-- ======================================================================
-- From file Instance.php
-- ======================================================================
ALTER TABLE `wf_instance` ADD UNIQUE (uid);
ALTER TABLE `wf_instance` ADD UNIQUE (`name` ,`processId`);

ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_uid` (`uid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_name` (`name` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_parentid` (`parentId` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_cid` (`cid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_processId` (`processId` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_nextActivities` (nextActivities(100) ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_nextUsers` (nextUsers(100) ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_started` (`started` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_ended` (`ended` ASC);
-- ======================================================================
-- From file Transition.php
-- ======================================================================
ALTER TABLE wf_transition ADD UNIQUE (uid);

ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_uid` (`uid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_name` (`name` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_parentId` (`parentId` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_childId` (`childId` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_parentUid` (`parentUid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_childUid` (`childUid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_lindex` (`lindex` ASC);
-- ======================================================================
-- From file End.php
-- ======================================================================
-- ======================================================================
-- From file Join.php
-- ======================================================================
-- ======================================================================
-- From file Aswitch.php
-- ======================================================================
-- ======================================================================
-- From file Split.php
-- ======================================================================
-- ======================================================================
-- From file Activity.php
-- ======================================================================
-- ======================================================================
-- From file Standalone.php
-- ======================================================================
-- ======================================================================
-- From file Start.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file Factory.php
-- ======================================================================
-- ======================================================================
-- From file Any.php
-- ======================================================================
-- ======================================================================
-- From file Link.php
-- ======================================================================
