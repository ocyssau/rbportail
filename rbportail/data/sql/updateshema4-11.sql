-- 4.11
ALTER TABLE workunit 
	DROP INDEX `U_WU_NAME_VERSION`;

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.11',now());

