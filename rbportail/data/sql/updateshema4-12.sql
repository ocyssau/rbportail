-- 4.12
ALTER TABLE `workunit`
	ADD COLUMN `flt` VARCHAR(64) NULL after `name`,
	ADD COLUMN `number` INT(11) NULL after `flt`,
	ADD INDEX `WORKUNIT_FLT` (`flt`),
	ADD INDEX `WORKUNIT_NUMBER` (`number`);

ALTER TABLE workunit CHANGE COLUMN `tos` `tos` varchar(128) AFTER `name`;

UPDATE workunit SET flt=LEFT(`name`, 9) WHERE name NOT LIKE 'MSN%' AND name NOT LIKE 'SNZ%';
UPDATE workunit SET number=RIGHT(`name`, 3);
UPDATE workunit SET version=CAST(SUBSTRING(`name`,11,2) AS SIGNED) WHERE name NOT LIKE 'MSN%' AND name NOT LIKE 'SNZ%';
UPDATE workunit SET flt=tos WHERE name LIKE 'MSN%' or name LIKE 'SNZ%';

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.12',now());
