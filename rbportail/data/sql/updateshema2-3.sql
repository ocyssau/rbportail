ALTER TABLE workunit_type ADD COLUMN `title` VARCHAR(255) NULL DEFAULT 2 AFTER `name`;
ALTER TABLE workunit_type ADD COLUMN `dimension` TINYINT NULL DEFAULT 2 AFTER `title`;

ALTER TABLE `workunit_type` 
	ADD COLUMN `tuProductionTlse` DECIMAL NULL AFTER `tuProduction`,
	ADD COLUMN `tuManagementTlse` DECIMAL NULL AFTER `tuManagement`,
	CHANGE COLUMN `tuProduction` `tuProduction` DECIMAL NULL DEFAULT NULL ,
	CHANGE COLUMN `tuManagement` `tuManagement` DECIMAL NULL DEFAULT NULL ;

UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='1';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='2';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='3';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='4';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='5';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='6';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='7';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='8';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='9';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='10';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='23';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='24';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='25';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='26';
UPDATE `workunit_type` SET `dimension`='3' WHERE `id`='27';
UPDATE `link_workunit_type_context` SET `childId`='5' WHERE `id`='53';
UPDATE `link_workunit_type_context` SET `childId`='5' WHERE `id`='54';
UPDATE `link_workunit_type_context` SET `childId`='5' WHERE `id`='55';
UPDATE `link_workunit_type_context` SET `childId`='5' WHERE `id`='56';

INSERT INTO `workunit_type` (`id`, `uid`, `cid`, `name`, `title`, `ownerId`, `parentId`, `parentUid`, `updateById`, `updated`, `context`, `amount`, `tuProduction`, `tuManagement`, `salesRate`, `dimension`, `tuProductionTlse`, `tuManagementTlse`) VALUES
(23, 'E0', 301, 'Modification of an existing design solution', '2', NULL, NULL, NULL, NULL, NULL, NULL, 160, NULL, NULL, NULL, 3, NULL, NULL),
(24, 'E1-1', 301, 'Pre-study', '2', NULL, NULL, NULL, NULL, NULL, NULL, 1100, NULL, NULL, NULL, 3, NULL, NULL),
(25, 'E1-2', 301, 'Design solution maturity B development', '2', NULL, NULL, NULL, NULL, NULL, NULL, 1100, NULL, NULL, NULL, 3, NULL, NULL),
(26, 'E1-3', 301, 'Design solution maturity C development', '2', NULL, NULL, NULL, NULL, NULL, NULL, 1100, NULL, NULL, NULL, 3, NULL, NULL),
(27, 'E2', 301, 'Complete design solution development', '2', NULL, NULL, NULL, NULL, NULL, NULL, 2740, NULL, NULL, NULL, 3, NULL, NULL),
(28, 'L1', 301, 'Elementary and equipped part drawing set modification', '2', NULL, NULL, NULL, NULL, NULL, NULL, 150, 150, 50, '', 2, 30, 450),
(29, 'L2', 301, 'Installation drawing set modification', '2', NULL, NULL, NULL, NULL, NULL, NULL, 350, 50, 50, '', 2, 500, 1000),
(30, 'L3', 301, 'Creation of elementary part drawing set', '2', NULL, NULL, NULL, NULL, NULL, NULL, 350, 50, 50, '', 2, 500, 1000),
(31, 'L4', 301, 'Creation of installation drawing set', '2', NULL, NULL, NULL, NULL, NULL, NULL, 350, 50, 50, '', 2, 500, 1000);

INSERT INTO link_workunit_type_context (parentId, parentUid, childId, childUid)
	VALUES 
	(23, 'E0', 6 , 'legacy'),
	(24, 'E1-1' ,6 , 'legacy'),
	(25, 'E1-2', 6 , 'legacy'),
	(26, 'E1-3', 6 , 'legacy'),
	(27, 'E2', 6, 'legacy'),
	(28, 'L1', 6 , 'legacy'),
	(29, 'L2', 6 , 'legacy'),
	(30, 'L3', 6 , 'legacy'),
	(31, 'L4', 6 , 'legacy');

ALTER TABLE `link_workunit_type_context` ADD UNIQUE INDEX `u_parent_child` (`parentId` ASC, `childId` ASC);

-- SELECT TIME_FORMAT(SEC_TO_TIME(56999), '%H Heures %i Minutes')
INSERT INTO workunit_context (id, uid, name)
				VALUES 
				(6, 'legacy', 'LEGACY');
