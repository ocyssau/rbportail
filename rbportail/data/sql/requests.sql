-- <SQL REQUETS 
-- Requets parentId from wf_instance_activity tree ordered
SELECT id, name, instanceId, 
(SELECT id FROM wf_instance_activity WHERE id<insta.id AND instanceId=insta.instanceId ORDER BY id DESC LIMIT 0,1) AS parentId 
FROM wf_instance_activity AS insta
-- SQL REQUETS> 

-- <SQL REQUETS 
-- Update parentId and parentUid of wf_instance_activity
DROP TABLE temp_iacttree;
CREATE TABLE temp_iacttree(
	`id` INT NULL,
	`instanceId` INT NULL,
	`parentId` INT NULL,
	`parentUid` VARCHAR(255) NULL
);

INSERT INTO temp_iacttree (id, instanceId) SELECT id,instanceId FROM wf_instance_activity;

UPDATE temp_iacttree SET 
parentId = (SELECT id FROM wf_instance_activity WHERE id<temp_iacttree.id AND instanceId=temp_iacttree.instanceId ORDER BY id DESC LIMIT 0,1),
parentUid = (SELECT uid FROM wf_instance_activity WHERE id<temp_iacttree.id AND instanceId=temp_iacttree.instanceId ORDER BY id DESC LIMIT 0,1)

UPDATE wf_instance_activity SET 
parentId = (SELECT parentId FROM temp_iacttree WHERE id=wf_instance_activity.id),
parentUId = (SELECT parentUid FROM temp_iacttree WHERE id=wf_instance_activity.id)

DROP TABLE temp_iacttree;
-- SQL REQUETS> 


-- Update parentId and parentUid of wf_instance_activity OTHER SOLUTION 
DROP TABLE tempTable1;
CREATE TEMPORARY TABLE tempTable1 (
	id int(11) NOT NULL,
	parentId int(11) NOT NULL
);

DROP TABLE tempTable2;
CREATE TEMPORARY TABLE tempTable2 (
	id int(11) NOT NULL,
	parentUid varchar(256) NOT NULL
);

INSERT INTO tempTable1 (id,parentId) 
SELECT iact.id, MAX(previous.id)
	FROM wf_instance_activity as iact
	JOIN wf_instance_activity as previous ON previous.id < iact.id AND previous.instanceId=iact.instanceId
	GROUP BY iact.id
;

INSERT INTO tempTable2 (id,parentUid) 
SELECT iact.id, previous.uid
	FROM wf_instance_activity as iact
	JOIN tempTable1 ON iact.id=tempTable1.id
	JOIN wf_instance_activity AS previous ON tempTable1.parentId=previous.id
;

SELECT * FROM tempTable1;
SELECT * FROM tempTable2;

UPDATE wf_instance_activity
SET wf_instance_activity.parentId = 
	(SELECT tmp.parentId
	FROM tempTable1 as tmp
	WHERE tmp.id=wf_instance_activity.id
	)
WHERE ISNULL(wf_instance_activity.parentId);

UPDATE wf_instance_activity
SET wf_instance_activity.parentUid = 
	(SELECT tmp.parentUid
	FROM tempTable2 as tmp
	WHERE tmp.id=wf_instance_activity.id
	)
WHERE ISNULL(wf_instance_activity.parentUid);


SELECT * FROM wf_instance_activity WHERE isnull(wf_instance_activity.parentId);


-- <SQL REQUETS 
-- Find instance activities with incoherents start date with previous instance activity end date
SELECT act.*,workunit.name as WuName FROM wf_instance_activity as act 
JOIN workunit_process as proclnk ON proclnk.childId = act.instanceId
JOIN workunit ON proclnk.parentId = workunit.id
WHERE started <> (SELECT ended FROM wf_instance_activity WHERE id  = act.id-1 AND instanceId=act.instanceId) AND act.type<>'start' AND act.`status`='completed';


UPDATE `deliverable` SET `section` = REPLACE(`section`, 'SA, LR & A400M', 'SA, LR & PYLONS') WHERE `section` LIKE 'SA, LR & A400M';
UPDATE `deliverable` SET `section` = REPLACE(`section`, 'A380', 'A380 & A400M') WHERE `section` LIKE 'A380';

