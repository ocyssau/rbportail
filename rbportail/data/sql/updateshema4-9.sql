-- 4.9
ALTER TABLE `wf_instance` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL,
	CHANGE COLUMN `title` `title` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `wf_instance_activity` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL,
	CHANGE COLUMN `title` `title` VARCHAR(255) NULL DEFAULT NULL;

-- SET SCHEMA VERSION
INSERT INTO schema_version(version,lastModification) VALUES('4.9',now());
