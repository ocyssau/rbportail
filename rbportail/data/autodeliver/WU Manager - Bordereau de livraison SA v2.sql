SELECT 
    deliverable.name AS 'N° de plan/DS',
    '' AS Type,
    deliverable.indice AS 'Ind.',
    deliverable.title AS 'Désignation',
    deliverable.ci AS 'Ensemble supérieur',
    deliverable.mod AS 'MOD/PM',
    'X' AS 'Vault in Prog.',
    'na' AS 'Tir. A0',
    'na' AS 'Tir. A3',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'Nom.',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'ECN',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'Request for rev.',
    '' AS 'Dess VD',
    '' AS 'Calc. VC',
    '' AS 'Vault Publish',
    '' AS 'Dess',
    '' AS 'CG',
    '' AS 'RG',
    '' AS 'Calc.',
    '' AS 'LA',
    '' AS 'RS',
    '' AS 'OF',
    '' AS 'Vault release',
    CONCAT(workunit.name,
            '/ Vault ',
            deliverable.team,
            '/ ATA leader ',
            workunit.ataLeader) AS Commentaire
FROM
    deliverable
        JOIN
    workunit ON workunit.id = deliverable.parentId
        JOIN
    workunit_type ON workunit.typeId = workunit_type.id
        JOIN
    link_workunit_type_context ON workunit_type.id = link_workunit_type_context.parentId
        JOIN
    workunit_context ON link_workunit_type_context.childId = workunit_context.id
WHERE
    (workunit_context.name = 'LEGACY')
        AND (deliverable.section <> 'A350')
        AND (workunit.status = 'Ready for delivery')
gROUP BY workunit.name