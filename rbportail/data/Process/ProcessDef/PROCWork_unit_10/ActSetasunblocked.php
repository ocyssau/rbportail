<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;
use Application\Dao\Connexion;

/**
 * A class for activity setasunblocked
 *
 * @package Work_unit_10
 * @generated 2015-11-17T18:34:24+0100
 * @author
 */
class ActSetasunblocked extends ActivityTrigger
{

	public function trigger()
	{
		parent::trigger();

		$piId = $this->workflow->instance->getId();

		//get the transition between the current activity instance and the parent activity instance
		$sql = "SELECT trans.name AS tname FROM wf_transition AS trans WHERE
			trans.parentId=(SELECT activityId FROM wf_instance_activity WHERE wf_instance_activity.instanceId =:instanceId ORDER BY id DESC LIMIT 1 , 1)
			AND
			trans.childId=(SELECT activityId FROM wf_instance_activity WHERE wf_instance_activity.instanceId =:instanceId ORDER BY id DESC LIMIT 0 , 1)
			";

		$stmt = Connexion::get()->prepare($sql);
		$stmt->execute(array(':instanceId'=>$piId));
		$res0 = $stmt->fetch(\PDO::FETCH_ASSOC);
		//var_dump($res0['tname'],$piId);die;
		$previousStatus = $res0['tname'];
		$this->workunit->setStatus($previousStatus);
		$this->workunitDao->save($this->workunit);
	}
}
