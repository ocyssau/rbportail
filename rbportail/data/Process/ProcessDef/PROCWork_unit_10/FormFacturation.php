<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityForm;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/**
 * A class for activity form facturation
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class FormFacturation extends ActivityForm implements InputFilterProviderInterface
{

	/**
	 * @param unknown_type $name
	 */
	public function __construct($workflow)
	{
		Form::__construct(uniqid());

		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;

		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type'  => 'hidden',
				'value'=>'4dcb6a55582ce745a1826edd6126cc0a'
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'orderId',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Order Id',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Order Id',
			),
		));

		$this->add(array(
			'name' => 'orderDate',
			'type'  => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type'  => 'date',
				'placeholder' => 'Order Date',
			),
			'options' => array(
				'label' => 'Order Date',
				'format' => 'd-m-Y',
			),
		));

		$this->add(array(
			'name' => 'next',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Next Status',
				'value_options'=> $this->_getNext(),
				'empty_option'=>'select next status'
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'Int'),
				),
			),
			'orderId' => array(
				'required' => true,
				'filters'  => array(
				),
				'validators' => array(
				),
			),
		);
	}

}

