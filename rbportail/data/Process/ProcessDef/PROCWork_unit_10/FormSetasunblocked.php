<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityForm;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/**
 * A class for activity form setasunblocked
 *
 * @package Work_unit_10
 * @generated 2015-11-17T18:34:24+0100
 * @author
 */
class FormSetasunblocked extends ActivityForm
{
	/**
	 * @param unknown_type $name
	 */
	public function __construct($workflow)
	{
		Form::__construct(uniqid());
		
		// we want to ignore the name passed
		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;

		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type'  => 'hidden',
				'value'=>'4dcb6a55582ce745a1826edd6126cc0a'
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'next',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Next Status',
				'value_options'=> $this->_getNext(),
				'empty_option'=>'select next status'
			),
		));

		$this->add(array(
			'name' => 'comment',
			'attributes' => array(
				'type'  => 'textarea',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Comments',
			),
		));


		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

}

