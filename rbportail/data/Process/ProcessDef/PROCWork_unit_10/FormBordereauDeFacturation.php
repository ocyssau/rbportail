<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityForm;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/**
 * A class for activity form bordereauDeFacturation
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class FormBordereauDeFacturation extends ActivityForm implements InputFilterProviderInterface
{

	/**
	 * @param unknown_type $name
	 */
	public function __construct($workflow)
	{
		Form::__construct(uniqid());
		
		// we want to ignore the name passed
		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;

		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type'  => 'hidden',
				'value'=>'4dcb6a55582ce745a1826edd6126cc0a'
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'name',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Reference',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Reference',
			),
		));
		
		$this->add(array(
			'name' => 'emitDate',
			'type'  => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type'  => 'date',
				'placeholder' => 'Emit Date',
			),
			'options' => array(
				'label' => 'Emit Date',
				'format' => 'd-m-Y',
			),
		));
		
		$this->add(array(
			'name' => 'next',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Next Status',
				'value_options'=> $this->_getNext(),
				'empty_option'=>'select next status'
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification(){
		return array(
			'id' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'Int'),
				),
			),
			'name' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
			'emitDate' => array(
				'required' => true,
				'filters'  => array(
				),
				'validators' => array(
				),
			),
		);
	}

}

