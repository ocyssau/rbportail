<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityForm;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/**
 * A class for activity form delivery
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class FormDelivery extends ActivityForm implements InputFilterProviderInterface
{

	/**
	 */
	public function __construct($workflow)
	{
		Form::__construct(uniqid());

		/* we want to ignore the name passed */
		$this->setHydrator(new Hydrator(false));
		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;

		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type'  => 'hidden',
				'value'=>'4dcb6a55582ce745a1826edd6126cc0a'
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'next',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Next Status',
				'value_options'=> $this->_getNext(),
				'empty_option'=>'select next status'
			),
		));

		$this->add(array(
			'name' => 'deliveryMemo',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Delivery Note',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Delivery note',
			),
		));

		$this->add(array(
			'name' => 'deliveryDate',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Delivery Date',
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => 'Delivery Date',
				'format' => 'd-m-Y',
			),
		));
		
		$this->add(array(
			'name' => 'lastDeliveryDate',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Last Delivery Date',
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => 'Last Delivery Date',
				'format' => 'd-m-Y',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'Int'),
				),
			),
			'deliveryMemo' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
			'deliveryDate' => array(
				'required' => true,
				'filters'  => array(
				),
				'validators' => array(
				),
			),
			'lastDeliveryDate' => array(
				'required' => true,
				'filters'  => array(
				),
				'validators' => array(
				),
			),
		);
	}

}

