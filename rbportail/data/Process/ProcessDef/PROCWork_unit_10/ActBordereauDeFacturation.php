<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;
use Zend\View\Model\ViewModel;

use Wumanager\Model\Bill;
use Application\Dao\Factory as DaoFactory;

/**
 * A class for activity bordereauDeFacturation
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class ActBordereauDeFacturation extends ActivityTrigger
{

	public function trigger()
	{
		parent::trigger();
	}

	/**
	 *
	 * @param unknown_type $form
	 */
	public function runForm($form, $template)
	{
		$view = new ViewModel();

		$request = $this->workflow->serviceManager->get('request');
		$this->form = $form;

		$workunit = $this->workunit;
		$factory = DaoFactory::get();

		$bill = new Bill();
		$billDao = $factory->getDao($bill);
		try{
			$billDao->load($bill, 'parentId='.$workunit->getId());
		}
		catch(\Exception $e){
			$bill = Bill::init();
			$bill->setParent($workunit);
		}
		$form->bind($bill);

		if ($request->isPost() && $request->getPost('submit')){
			$form->setData( $request->getPost() );
			if ( $form->isValid() ){
				$nextTransition = $request->getPost('next');
				$this->workflow->setNextTransition($nextTransition);
				$billDao->save($bill);

				$this->workunit->setVisibility('private');
				$this->workunitDao->save($this->workunit);

				$activityInstance = $this->workflow->lastActivity;
				$attributes = $activityInstance->getAttributes();
				$attributes['bordereau'] = $form->get('name')->getValue();
				$attributes['bordereauEmitDate'] = $form->get('emitDate')->getValue();
				$activityInstance->setAttributes($attributes);
				$factory->getDao($activityInstance)->save($activityInstance);

				return true;
			}
		}

		$view->setTemplate($template);
		$view->title = 'Billing Datas';
		$view->form = $form;
		$this->render($view);
	}

}