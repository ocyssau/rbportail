<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;

/**
 * A class for activity setasblocked
 *
 * @package Legacy_programs_10
 * @generated 2015-06-30T12:02:15+0200
 * @author
 */
class ActSetasblocked extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
        $this->workunit->setStatus('blocked');
        $this->workunitDao->save($this->workunit);
    }
}