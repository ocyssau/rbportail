<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;

/**
 * A class for activity steliaCheck
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class ActSteliaCheck extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
    }
}