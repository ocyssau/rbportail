<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;
use Application\Dao\Factory as DaoFactory;

use Zend\View\Model\ViewModel;

/**
 * A class for activity delivery
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class ActDelivery extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
    }

    /**
     */
    public function runForm($form, $template)
    {
    	$view = new ViewModel();

    	$request = $this->workflow->serviceManager->get('request');
    	$this->form = $form;

    	$workunit = $this->workunit;
    	$form->bind($workunit);
    	
    	if ($request->isPost() && $request->getPost('submit')){
    		$form->setData( $request->getPost() );
    		if ( $form->isValid() ){
    			$workunit->setVisibility('private');
    			$nextTransition = $request->getPost('next');
    			$this->workflow->setNextTransition($nextTransition);
    			$this->workunitDao->save($workunit);
    			
    			$activityInstance = $this->workflow->lastActivity;
    			$attributes = $activityInstance->getAttributes();
    			$attributes['deliveryMemo'] = $form->get('deliveryMemo')->getValue();
    			$attributes['deliveryDate'] = $form->get('deliveryDate')->getValue();
    			$activityInstance->setAttributes($attributes);
    			DaoFactory::get()->getDao($activityInstance)->save($activityInstance);
    			return true;
    		}
    	}

    	$view->setTemplate($template);
    	$view->title = 'Delivery Datas';
    	$view->form = $form;
    	$this->render($view);
    }
}
