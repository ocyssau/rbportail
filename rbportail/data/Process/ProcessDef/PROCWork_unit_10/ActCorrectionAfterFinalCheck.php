<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;

/**
 * A class for activity correctionAfterFinalCheck
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class ActCorrectionAfterFinalCheck extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
    }


}

