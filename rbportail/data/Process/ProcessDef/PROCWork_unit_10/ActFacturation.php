<?php

namespace ProcessDef\PROCWork_unit_10;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;
use Zend\View\Model\ViewModel;
use Wumanager\Model\Bill;
use Application\Dao\Factory as DaoFactory;

/**
 * A class for activity facturation
 *
 * @package Legacy_programs_10
 * @generated 2015-05-11T16:20:53+0200
 * @author
 */
class ActFacturation extends ActivityTrigger
{

	public function trigger()
	{
		parent::trigger();
	}


	/**
	 *
	 * @param unknown_type $form
	 */
	public function runForm($form, $template)
	{
		$view = new ViewModel();

		$request = $this->workflow->serviceManager->get('request');
		$this->form = $form;
		$workunit = $this->workunit;

		$factory = DaoFactory::get();

		$bill = new Bill();
		$dao = $factory->getDao($bill);
		try{
			$dao->load($bill, 'parentId='.$workunit->getId());
		}
		catch(\Exception $e){
			$bill = Bill::init();
			$bill->setParent($workunit);
		}
		$form->bind($bill);

		if ($request->isPost() && $request->getPost('submit')){
			$form->setData( $request->getPost() );
			if ( $form->isValid() ){
				$nextTransition = $request->getPost('next');
				$this->workflow->setNextTransition($nextTransition);
				$dao->save($bill);

				$activityInstance = $this->workflow->lastActivity;
				$attributes = $activityInstance->getAttributes();
				$attributes['facturationOrderId'] = $form->get('orderId')->getValue();
				$attributes['facturationOrderDate'] = $form->get('orderDate')->getValue();
				//$attributes['facturationAmount'] = $form->get('amount')->getValue();
				$activityInstance->setAttributes($attributes);
				$factory->getDao($activityInstance)->save($activityInstance);

				return true;
			}
		}

		$view->setTemplate($template);
		$view->title = 'Billing Datas';
		$view->form = $form;
		$this->render($view);
	}

}

