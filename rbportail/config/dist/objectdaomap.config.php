<?php
return array(
	1=>array('\Application\Dao\Application', 'application'),
	10=>array('\Siergate\Dao\Component\Siergate', 'component', '\Siergate\Model\Component\Siergate'),
	30=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property'),
	90=>array('\Siergate\Dao\Component\Project', 'component', '\Siergate\Model\Component\Template'),
	100=>array('\Siergate\Dao\Component\Project', 'component', '\Siergate\Model\Component\Project'),
	101=>array('\Siergate\Dao\Component\Box', 'component', '\Siergate\Model\Component\Box'),
	102=>array('\Siergate\Dao\Component\Indicator', 'component', '\Siergate\Model\Component\Indicator'),
	103=>array('\Siergate\Dao\Component\Jalon', 'component', '\Siergate\Model\Component\Jalon'),
	104=>array('\Siergate\Dao\Component\Statusbox', 'component', '\Siergate\Model\Component\Statusbox'),
	105=>array('\Siergate\Dao\Component\Tabpanel', 'component', '\Siergate\Model\Component\Tabpanel'),
	106=>array('\Siergate\Dao\Component\Checkbox', 'component', '\Siergate\Model\Component\Checkbox'),
	107=>array('\Siergate\Dao\Component\Simplegrid', 'component', '\Siergate\Model\Component\Simplegrid'),
	108=>array('\Siergate\Dao\Component\Tabpanel\Item', 'component', '\Siergate\Model\Component\Tabpanel\Item'),
	110=>array('\Siergate\Dao\Component\Tinymce', 'component', '\Siergate\Model\Component\Tinymce'),
	111=>array('\Siergate\Dao\Component\Jssor', 'component', '\Siergate\Model\Component\Jssor'),
	112=>array('\Siergate\Dao\Component\Tab', 'component', '\Siergate\Model\Component\Tab'),
	115=>array('\Siergate\Dao\Component\Filelist', 'component', '\Siergate\Model\Component\Filelist'),
	116=>array('\Siergate\Dao\Component\Accordion', 'component', '\Siergate\Model\Component\Accordion'),
	117=>array('\Siergate\Dao\Component\Component', 'component', '\Siergate\Model\Component\AbstractComponent'),

	200=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property'), //Abstract
	201=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\Text'), //Text
	202=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\Number'), //Number
	203=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\Range'), //Range
	204=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\Select'), //Select
	205=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\SelectFromDb'), //SelectFromDb
	206=>array('\Application\Dao\Extended\Property', 'extendedmodel', '\Application\Model\Extended\Property\Date'), //Date

	300=>array('\Wumanager\Dao\Workunit', 'workunit', '\Wumanager\Model\Workunit'),
	301=>array('\Wumanager\Dao\Workunit\Type', 'workunit_type', '\Wumanager\Model\Workunit\Type'),
	302=>array('\Wumanager\Dao\Status', 'wumanager_status', '\Wumanager\Model\Status'),
	303=>array('\Wumanager\Dao\Site', 'site', '\Wumanager\Model\Site'),
	304=>array('\Wumanager\Dao\Deliverable', 'deliverable', '\Wumanager\Model\Deliverable'),
	305=>array('\Wumanager\Dao\Billnote', 'bill_note', '\Wumanager\Model\Billnote'),
	306=>array('\Wumanager\Dao\Bill', 'bill', '\Wumanager\Model\Bill'),
	307=>array('\Wumanager\Dao\Workunit\Context', 'workunit_context', '\Wumanager\Model\Workunit\Context'),
	308=>array('\Wumanager\Dao\Workunit\LinkTypeContext', 'link_workunit_type_context', '\Wumanager\Model\Workunit\LinkTypeContext'),
	309=>array('\Wumanager\Dao\Workunit\LinkProcess', 'workunit_process', '\Wumanager\Model\Workunit\LinkProcess'),
	'wumarker5ab4g'=>array('\Wumanager\Dao\Workunit\Marker', 'workunit_marker', '\Wumanager\Model\Workunit\Marker'),
	'568be4fc7a0a8'=>array('\Discussion\Dao\Comment', 'discussion_comment', '\Discussion\Model\Comment'),
	'479v169a42a96'=>array('\Wumanager\Model\Notification\NotificationDao', 'notifications', '\Wumanager\Model\Notification\Notification'),
	
	420=>array('\Workflow\Dao\Wf\Process', 'wf_process', '\Workflow\Model\Wf\Process'),
	421=>array('\Workflow\Dao\Wf\Activity\Activity', 'wf_activity', '\Workflow\Model\Wf\Activity\Activity'),
	422=>array('\Workflow\Dao\Wf\Activity\Start', 'wf_activity', '\Workflow\Model\Wf\Activity\Start'),
	423=>array('\Workflow\Dao\Wf\Activity\End', 'wf_activity', '\Workflow\Model\Wf\Activity\End'),
	424=>array('\Workflow\Dao\Wf\Activity\Join', 'wf_activity', '\Workflow\Model\Wf\Activity\Join'),
	425=>array('\Workflow\Dao\Wf\Activity\Split', 'wf_activity', '\Workflow\Model\Wf\Activity\Split'),
	426=>array('\Workflow\Dao\Wf\Activity\Aswitch', 'wf_activity', '\Workflow\Model\Wf\Activity\Aswitch'),
	427=>array('\Workflow\Dao\Wf\Activity\Standalone', 'wf_activity', '\Workflow\Model\Wf\Activity\Standalone'),
	428=>array('\Workflow\Dao\Wf\Activity', 'wf_activity', '\Workflow\Model\Wf\Activity'),
	430=>array('\Workflow\Dao\Wf\Transition', 'wf_transition', '\Workflow\Model\Wf\Transition'),
	440=>array('\Workflow\Dao\Wf\Instance', 'wf_instance', '\Workflow\Model\Wf\Instance'),
	441=>array('\Workflow\Dao\Wf\Instance\Activity', 'wf_instance_activity', '\Workflow\Model\Wf\Instance\Activity'),
	442=>array('\Workflow\Dao\Link', '', '\Workflow\Model\Link'),
	450=>array('\Workflow\Dao\Wf\Instance\Standalone', 'wf_instance_activity', '\Workflow\Model\Wf\Instance\Standalone'),

	500=>array('\Application\Dao\Acl\Acl', 'acl', '\Application\Model\Acl\Acl'), //application acl
	501=>array('\Application\Dao\Acl\Acl', 'acl', '\Application\Model\Acl\Application'), //application acl
	505=>array('\Application\Dao\Acl\Acl', 'acl', '\Siergate\Model\Acl\Siergate'), //siergate acl
	510=>array('\Application\Dao\Acl\Acl', 'acl', '\Application\Model\Acl\Project'), //project acl
	550=>array('\Application\Dao\People\User', 'user', '\Application\Model\People\User'),
	700=>array('\Application\Dao\Acl\Acl', 'acl', '\Wumanager\Model\Acl\Wumanager'), //wumanager acl
);
