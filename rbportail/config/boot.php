<?php
//%LICENCE_HEADER%


function rbinit_enableDebug($enable = true)
{
	if ( $enable ) {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.show_error_trace', 1);
		ini_set('xdebug.show_local_vars', 0);
		/* Display undefined var too */
		ini_set('xdebug.dump_undefined', 0);
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.POST', '*');
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.GET', '*');
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 300);
		ini_set('xdebug.var_display_max_data', 3000);
		ini_set('xdebug.var_display_max_depth', 3);
		
		/* tree nested levels of array elements and object relations are displayed */
		ini_set('xdebug.max_nesting_level', 120);
		
		/*
		 * 1: computer readable format / 0: shows a human readable indented trace file with: time index,
		 * memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name,
		 * function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
		 */
		ini_set('xdebug.trace_format', 0);
		
		/*
		 * set_exception_handler(function($e){
		 * echo "Uncaught exception: " , $e->getMessage(), "\n";
		 * //debug_print_backtrace();
		 * });
		 */
	}
	else {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_local_vars', 0);
		ini_set('xdebug.dump_undefined', 0);
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 10);
		ini_set('xdebug.var_display_max_data', 300);
		ini_set('xdebug.var_display_max_depth', 3);
		//ini_set('xdebug.max_nesting_level', 10);
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.trace_format', 0);
	}
}

/**
 * Init constants for web interface
 * 
 * @return void
 */
function rbinit_web()
{
	define('CRLF',"<br />");
}

/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	define('CRLF',"\n");
}

/**
 * Init constants for sessions
 */
function rbinit_usesession()
{
}


/**
 * Init constants for define configs files and set config for Ranchbe and Rbplm
 */
function rbinit_config()
{
}


/** 
 * Init the DAO factory and loader
 */
function rbinit_dao()
{
}

/**
 * Init file system protection and paths
 */
function rbinit_fs()
{
}

/**
 * Init logger
 */
function rbinit_logger()
{
}
