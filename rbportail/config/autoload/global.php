<?php
/**
 * Global Configuration
 *
 * Dont edit this file manually, it will be replaced by distribution version on next upgrade.
 * Put your configuration in local.php
 *
 */

return array(
    'rbp'=>array(
    	'path'=>array('reposit'=>'data/reposits'),
        'db' => array(
            'default'=>array(
                'adapter'=>'mysql',
                'params'=>array(
                    'host'=>'localhost',
                    'dbname'=>'rbportail',
                    'port'=>'3306',
                    'username'=>'rbportail',
                    'password'=>'rbportail',
                ),
            ),
        ),
    	'workflow'=>array(
			'process'=>array(
				'path'=>APPLICATION_PATH.'/data/Process/ProcessDef',
				'namespace'=>'\ProcessDef',
			),
    	),
    	'auth'=>array(
    		'adapter'=>'db', //ldap,db or passall for no authentications
    		'options'=>array(
    			'server1'=>array( //Typical options for Active Directory
    				'host'=>'',
    				'useStartTls'=>true,
    				'accountDomainName'=>'w.net',
    				'accountDomainNameShort'=>'W',
    				'accountCanonicalForm'=>3,
    				'baseDn'=>'CN=Users,DC=w,DC=net',
   				)
   			)
   		),
    	/* Mail */
    	'admin.mail'=>'',
    	/* Smtp options @see Zend\Mail\Transport\SmtpOptions */
    	'message.mail.enable' => true,
    	'mail.smtp' => array(
    		'enable'=>false,
    		'name'              => 'localhost.localdomain',
    		'host'              => '127.0.0.1',
    		/* Only for activate authencation :
			'connection_class'  => 'login',
	    	'connection_config' => array(
	   			'username' => 'user',
	   			'password' => 'pass',
	    	),
    	*/
    	),
    	'autodeliver.xls.template'=>array(
    		'file'=>'data/autodeliver/template1.xlsx',
    	),
    	'autodeliver.sendTo'=>array(
    	),
    	'autodeliver.sendToCc'=>array(
    	),
    	'autodeliver.from'=>array(
    	),
    ),
    'navigation' => array(
	    'default' => array(
	    	array(
		    	'label' => 'Admin',
		    	'route' => 'admin',
		    	'pages' => array(
					array(
						'label' => 'User',
						'route' => 'adminuser',
					),
					array(
						'label' => 'Properties',
						'route' => 'extendedproperties',
						'pages' => array(
							array(
								'label' => 'Add',
								'route' => 'extendedproperties',
								'action' => 'add'
							),
							array(
								'label' => 'Edit',
								'route' => 'extendedproperties',
								'action' => 'edit'
							),
						)
					),
					array(
					    'label' => 'Process',
					    'route' => 'process',
					    'pages' => array(
						    array(
						        'label' => 'Edit',
						        'route' => 'process',
						        'action' => 'edit'
						    ),
						    array(
						        'label' => 'Graph',
						        'route' => 'process',
						        'action' => 'editgraph'
						    ),
						    array(
						        'label' => 'Import',
						        'route' => 'process',
						        'action' => 'import'
						    ),
						    array(
						        'label' => 'Add',
						        'route' => 'process',
						        'action' => 'create'
						    ),
						    array(
						        'label' => 'Instances',
						        'route' => 'instance',
						        'pages' => array(
							        array(
							            'label' => 'Activities',
							            'route' => 'ainstance',
							        ),
						        )
						    ),
				        ),
					),
		    	),
	    	),
		    array(
			    'label' => 'Workunit Manager',
			    'route' => 'wumanager',
			    'pages' => array(
			    	array(
				        'label' => 'types manager',
				        'route' => 'wutype',
				        'pages'=>array(
				        	array(
					        	'label' => 'Edit',
					        	'route' => 'wutype',
					        	'action' => 'edit'
				        	),
				        	array(
				        	    'label' => 'Add',
				        	    'route' => 'wutype',
				        	    'action' => 'add'
				        	),
				        ),
			        ),
			        array(
			            'label' => 'Users roles',
			            'route' => 'wuuser',
			        ),
			        array(
				        'label' => 'Workunit',
				        'route' => 'workunit',
				        'action'=> 'index',
				        'pages' => array(
				        ),
			        ),
			        array(
			            'label' => 'Edit',
			            'route' => 'workunit',
			            'action' => 'edit'
			        ),
			        array(
			            'label' => 'Add',
			            'route' => 'workunit',
			            'action' => 'add'
			        ),
			        array(
			            'label' => 'Billing',
			            'route' => 'workunit',
			            'action' => 'billto'
			        ),
			        array(
			            'label' => 'Delivery',
			            'route' => 'workunit',
			            'action' => 'delivery'
			        ),
			        array(
			            'label' => 'Process',
			            'route' => 'workunit',
			            'action' => 'startprocess'
			        ),
			        array(
			            'label' => 'History',
			            'route' => 'workunit',
			            'action' => 'gethistory'
			        ),
			    ),
		    ),
	    ),
    ),
    'service_manager' => array(
	    'factories' => array(
		    'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
	    )
    ),
    'externalLinks'=>array(
    	'wiki'=>array(
    		'url'=>'/mediawiki/'
    		),
    	'phpmyadmin'=>array(
    		'url'=>'/phpmyadmin/?db=rbportail',
    		)
    )
);
