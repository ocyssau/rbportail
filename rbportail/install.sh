#!/bin/bash

echo "Input the proxy host or let blank if no proxy";
read proxyhost

if [ ! -z "$proxyhost" ]
then
	echo "Input the proxy port"
	read proxyport

	echo "Input the proxy user or let blank if none"
	read proxyuser

	if [ ! -z $proxyuser ]
	then
		export proxyauth=$proxyuser

		echo "Input the proxy passwd or let blank if none"
		read -s proxypasswd
		if [ ! -z $proxypasswd ]
		then
			export proxyauth=$proxyauth:$proxypasswd
		fi
		export proxyauth=$proxyauth@
	fi

	export http_proxy=http://$proxyauth$proxyhost:$proxyport
	export https_proxy=http://$proxyauth$proxyhost:$proxyport
	export HTTP_PROXY=http://$proxyauth$proxyhost:$proxyport
	export HTTPS_PROXY=http://$proxyauth$proxyhost:$proxyport
fi

git config --global credential.helper 'cache --timeout=28800'

//installation of bower
sudo apt-get install nodejs
sudo apt-get install npm
sudo npm install -g bower
sudo ln -s /usr/bin/nodejs /usr/bin/node

sudo apt-get install curl apache2 php php-ldap php-mysql php-mbstring php-xml php-zip php-uuid
#pour ubuntu 16.04 
git config --global credential.helper 'cache --timeout=28800'

sudo apt-get install php-ssh2 libapache2-mod-php
#pour ubuntu 14 : sudo apt-get install libssh2-1-dev libssh2-php libapache2-mod-php
sudo apt-get install libssh2-1-dev libssh2-php

#sudo apt-get install php-xdebug
#sudo phpdismod xdebug
#sudo phpenmod xdebug

sudo phpenmod ldap
sudo a2enmod rewrite
sudo service apache2 restart

curl -s https://getcomposer.org/installer | php --
php composer.phar self-update
php composer.phar install
cp vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist config/autoload/zdt.local.php
sudo php5enmod xdebug

if [ -d /var/www/html ]
then 
	if [ ! -d /var/www/html/rbportail ]
	then 
		sudo ln -s `pwd`/public /var/www/html/rbportail
	fi
else 
	if [ ! -d /var/www/rbportail ]
	then 
		sudo ln -s `pwd`/public /var/www/rbportail
	fi
fi

if [ ! -e /etc/apache2/sites-available/rbportail.conf ]
then 
	sudo cp ./config/dist/apache.conf /etc/apache2/sites-available/rbportail.conf
fi
sudo gedit /etc/apache2/sites-available/rbportail.conf

sudo a2ensite rbportail
sudo a2enmod rewrite
sudo service apache2 restart

#Database init
mysql -u root -p < data/sql/rbportail.sql
mysql -u root -p rbportail < data/sql/updateshema4-6.sql
mysql -u root -p rbportail < data/sql/updateshema4-7.sql
mysql -u root -p rbportail < data/sql/updateshema4-8.sql
mysql -u root -p rbportail < data/sql/updateshema4-9.sql
mysql -u root -p rbportail < data/sql/updateshema4-10.sql
mysql -u root -p rbportail < data/sql/updateshema4-11.sql
mysql -u root -p rbportail < data/sql/updateshema4-12.sql
mysql -u root -p rbportail < data/sql/updateshema4-13.sql
mysql -u root -p rbportail < data/sql/updateshema4-14.sql
mysql -u root -p rbportail < data/sql/updateshema4-15.sql
mysql -u root -p rbportail < data/sql/updateshema4-16.sql

echo "Copier et editer le fichier de config ./config/autoload/local.php.dist -> local.php"
if [ ! -e ./config/autoload/local.php ]
then 
	cp ./config/autoload/local.php.dist ./config/autoload/local.php
fi
gedit ./config/autoload/local.php

echo "Copier et editer le fichier .htaccess"
if [ ! -e ./public/.htaccess ]
then 
	cp ./config/dist/htaccess ./public/.htaccess
	gedit ./public/.htaccess
fi

echo "Installation de zf tool"
php composer.phar require zendframework/zftool:dev-master
ln -s vendor/bin/zf.php zf.php

if [ -d public/js/jquery ]
then
	wget http://code.jquery.com/jquery-2.1.3.min.js
	mkdir public/js/jquery
	mv jquery-2.1.3.min.js public/js/jquery/jquery.min.js
fi

if[ -d public/js/jquery-ui ]
then
	wget http://jqueryui.com/resources/download/jquery-ui-1.11.2.zip
	unzip jquery-ui-1.11.2.zip
	rm jquery-ui-1.11.2.zip
	mkdir public/js/jquery-ui
	cp jquery-ui-1.11.2/jquery-ui.min.js public/js/jquery-ui/.
	cp jquery-ui-1.11.2/jquery-ui.min.css public/css/.
	mv jquery-ui-1.11.2 vendor/.
fi

if[ -d vendor/eternicode-bootstrap-datepicker-809a5c2 ]
then
	bower install bootstrap-datepicker
	
	wget https://github.com/eternicode/bootstrap-datepicker/zipball/1.3.1
	mv 1.3.1 datepicker.zip
	unzip datepicker.zip
	rm datepicker.zip
	cp -r eternicode-bootstrap-datepicker-809a5c2/js public/js/datepicker
	cp -r eternicode-bootstrap-datepicker-809a5c2/css public/css/datepicker
	mv eternicode-bootstrap-datepicker-809a5c2 vendor/.
fi

wget https://github.com/jssor/slider/archive/master.zip
unzip master.zip
if [ -d public/js/jssor ] 
then
	cp -r slider-master/js/*.js public/js/jssor/.
else
	mkdir public/js/jssor
	cp -r slider-master/js/*.js public/js/jssor/.
fi
rm master.zip
mv slider-master vendor/.

if[ -d vendor/codemirror ]
then
	bower install codemirror
	cp -r bower_components/codemirror public/js/codemirror
fi
