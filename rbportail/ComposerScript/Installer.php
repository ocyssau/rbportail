<?php

namespace ComposerScript;

use Composer\Script\Event;

/**
 * 
 *
 */
class Installer
{
	
	public static function postUpdate(Event $event)
	{
		$composer = $event->getComposer();

		if(!is_dir('./public/bootstrap')){
		    mkdir('./public/bootstrap');
		}
		if(!is_dir('./public/bootstrap/js')){
			mkdir('./public/bootstrap/js');
		}
		if(!is_dir('./public/bootstrap/css')){
			mkdir('./public/bootstrap/css');
		}
		if(!is_dir('./public/bootstrap/fonts')){
			mkdir('./public/bootstrap/fonts');
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/css');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/bootstrap/css/'.$Dir->getFileName());
			}
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/js');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/bootstrap/js/'.$Dir->getFileName());
			}
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/fonts');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/bootstrap/fonts/'.$Dir->getFileName());
			}
		}
		
		//copy tinymce in js
		if(!is_dir('./public/js/tinymce')){
			mkdir('./public/js/tinymce');
		}
		
		self::dircopy('vendor/tinymce/tinymce', 'public/js/tinymce', false);
	}

	public static function postPackageInstall(Event $event)
	{
		$installedPackage = $event->getOperation()->getPackage();
		copy('vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist','config/autoload/zdt.local.php');
	}

	public static function warmCache(Event $event)
	{
		// make cache toasty
	}
	
	/**
	 *
	 * @param string $src
	 * @param string $target
	 */
	public static function dircopy($src, $target, $verbose=true)
	{
		$Dir = new \DirectoryIterator($src);
		foreach($Dir as $file){
			if($Dir->isFile()){
				if($verbose) echo 'copy '.$Dir->getPathname() . ' to ' . $target.'/'.$Dir->getFileName() . "\n";
				copy($Dir->getPathname(), $target.'/'.$Dir->getFileName());
			}
			elseif( $Dir->isDir() && !$Dir->isDot() ){
				$dir = $Dir->getPathname();
				$targetDir = $target.'/'.$Dir->getFileName();
				if($verbose) echo "directory ".$dir." copy to $targetDir \n";
				if( !is_dir($targetDir) ){
					mkdir( $targetDir );
				}
				self::dircopy( $dir, $targetDir );
			}
		}
	}
	
}
