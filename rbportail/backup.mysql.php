<?php
/*
Backup scripts for mysql DATABASES
to restore a dump file :
mysql -e "/path-to-backup/backup-file.sql" databaseName
*/
echo date(DATE_RFC822) . "\n";
chdir(__DIR__);

require 'module/Application/src/Application/Model/Sys/Ssh.php';
$ssh = new Ssh();
$ssh->host='daguerre';
$ssh->authuser='o_cyssau';
$ssh->authpass='balzac6';
$ssh->connect();
die;
$localConf = include('config/autoload/local.php');

$host   = $localConf['rbp']['db']['default']['params']['host'];
$user   = $localConf['rbp']['db']['default']['params']['username'];
$pass   = $localConf['rbp']['db']['default']['params']['password'];
$dbname = $localConf['rbp']['db']['default']['params']['dbname'];

echo "Backup of $dbname @ $host \n";

$mysqldumpcmd = 'mysqldump';
$dumpFile = './data/backup/' . $dbname . '.' . date('Y-m-d') . '.sql';

echo 'Backup in file ' . $dumpFile . "\n";

// Utilise les fonctions système : MySQLdump
//Backup in SQL format
$command = "$mysqldumpcmd --opt -h$host -u$user --password=$pass $dbname --result-file=$dumpFile";
$ok = system($command);

//Backup in XML format
//$xml_file = './data/backup/' . $dbname. '_backup.xml';
//$command = "$mysqldumpcmd --xml -h$host -u$user --password=$pass $dbs --result-file=$xml_file";
//system($command);

echo "End of backup at " . date(DATE_RFC822) . "\n";
?>
