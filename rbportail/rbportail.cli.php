#!/usr/bin/php
<?php
namespace rbportail\cli;

use Application\Test\Test;
use Application\Model as Model;
use Application\Dao as Dao;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Connexion;
use Siergate\Model\Component;
use Siergate\Model\Component\Project;
use Siergate\Model\Renderer as Renderer;
use Zend\View\Model\ViewModel;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Application\Model\Acl;
use Application\Model\People;
use Application\Model\Filesystem;
use Zend\Code\Reflection;
use Zend\Code\Generator;

/**
 */
function optionh()
{
	$display = 'Utilisation :

	php tests.php
	[-c <class>]
	[-m <method>]
	[-a]
	[--nodao,]
	[--croot,]
	[-i]
	[-lf]
	[-lc]
	[--doctype]
	[-help, -h, -?]

	Avec:
	-c <class>
	: Le nom de la classe de test a executer. Il peut y avoir plusieur option -c

	-m <method>
	: La methode a executer, ou si omis, execute tous les tests de la classe.

	-a
	: Execute tous les tests, annule l\'option -c

	-t
	: Execute mes tests, annule l\'option -c

	-p
	: Active le profiling des tests

	--nodao
	: Ignore les methodes testDao*

	--croot
	: Creer le noeud root

	-i
	: initialise les données de test.

	--initdb
	: initialise les données de test.

	--lf
	: Affiche la liste des functions de test disponibles

	--lc
	: Affiche la liste des classes de test disponibles

	-s
	: Extrait les scripts sql depuis les classes

	-h
	: Affiche cette aide.

	Examples:
	Execute tous les tests de la classe \Rbplm\AnyObjectTest et Rbplm\Org\Test:
	php tests.php -c \Rbplm\AnyObjectTest -c Rbplm\Org\Test

	Execute la methode \Rbplm\AnyObjectTest::testDao
	php tests.php -c \Rbplm\AnyObjectTest -m testDao

	Avec les options -help, -h vous obtiendrez cette aide

	-t : Execute la function test <fonction> parmis les fonctions suivantes :';
	echo $display;
	displaylisttest();
}

function displaylisttest()
{
	echo $display = '

	';
}

/**
 */
function run()
{
	chdir(__DIR__);

	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	ini_set('xdebug.collect_assignments', 1);
	ini_set('xdebug.collect_params', 4);
	ini_set('xdebug.collect_return', 1);
	ini_set('xdebug.collect_vars', 1);
	ini_set('xdebug.var_display_max_children', 1000);
	ini_set('xdebug.var_display_max_data', 5000);
	ini_set('xdebug.var_display_max_depth', 10);

	$loader = include 'vendor/autoload.php';
	$loader->set('Rbplm', realpath('./vendor/'));
	$loader->set('Rba', realpath('./vendor/'));
	$loader->set('Application', __DIR__ . '/module/Application/src');
	$loader->set('Siergate', __DIR__ . '/module/Siergate/src');
	$loader->set('Wumanager', __DIR__ . '/module/Wumanager/src');
	$loader->set('Workflow', __DIR__ . '/module/Workflow/src');

	/*
	 * ASSERT_ACTIVE assert.active 1 active l'évaluation de la fonction assert()
	 * ASSERT_WARNING assert.warning 1 génére une alerte PHP pour chaque assertion fausse
	 * ASSERT_BAIL assert.bail 0 termine l'exécution en cas d'assertion fausse
	 * ASSERT_QUIET_EVAL assert.quiet_eval 0 désactive le rapport d'erreur durant l'évaluation d'une assertion
	 * ASSERT_CALLBACK assert.callback (NULL) fonction de rappel utilisateur, pour le traitement des assertions fausses
	 */
	assert_options(ASSERT_ACTIVE, 1);
	assert_options(ASSERT_WARNING, 1);
	assert_options(ASSERT_BAIL, 0);
	// assert_options( ASSERT_CALLBACK , 'myAssertCallback');

	/*
	 * ob_start();
	 * phpinfo();
	 * file_put_contents('.phpinfos.out.txt', ob_get_contents());
	 */

	echo 'include path: ' . get_include_path() . PHP_EOL;

	$zver = new \Zend\Version\Version();
	echo 'ZEND VERSION : ' . $zver::VERSION . PHP_EOL;

	// For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
	$i = 1;
	while( $ENV['arg' . $i] ) {
		$argv[$i] = $ENV['arg' . $i];
		$i++;
	}

	$shortopts = '';
	$shortopts .= "c:"; // La classe <class> de test a lancer
	$shortopts .= "m:"; // La methode <method> a executer
	$shortopts .= "a"; // Execute tous les tests
	$shortopts .= "h"; // affiche l'aide
	$shortopts .= "t:"; // call test function <function>
	$shortopts .= "i"; // initialise les données de test
	$shortopts .= "s"; // Extrait les scripts sql depuis les classes
	$shortopts .= "p"; // Active le profiling des tests

	$longopts = array(
		'initdb',
		'nodao',
		'croot',
		'lc', // Affiche la liste des classes de test disponibles
		'lf:'
	) // Affiche la liste des methode de la <classe> de test
;

	$norun = array(
		'nodao',
		'm',
		'p'
	);

	$options = getopt($shortopts, $longopts);

	foreach( array_keys($options) as $o ) {
		if ( !in_array($o, $norun) ) {
			call_user_func(__NAMESPACE__ . '\option' . $o, $options);
		}
	}
	return;
}

/**
 * Execute les test de la classe ou des classes spécifiées
 */
function optionc($options)
{
	boot();

	$method = $options['m'];

	if ( isset($options['nodao']) ) {
		$withDao = false;
	}
	else {
		$withDao = true;
	}

	if ( isset($options['p']) ) {
		$withProfiling = true;
	}
	else {
		$withProfiling = false;
	}

	if ( is_array($options['c']) ) {
		foreach( $options['c'] as $class ) {
			Test::runOne($class, $method, $withDao, $continue = false, $withProfiling);
		}
	}
	else {
		Test::runOne($options['c'], $method, $withDao, $continue = false, $withProfiling);
	}
}

/**
 * execute la fonction test spécifiée
 */
function optiont($options)
{
	boot();

	if ( isset($options['nodao']) ) {
		$withDao = false;
	}
	else {
		$withDao = true;
	}

	$suffix = $options['t'];
	if ( $suffix == '' ) {
		$suffix = 'my';
	}

	$functionName = __NAMESPACE__ . '\test_' . $suffix;
	if ( function_exists($functionName) ) {
		echo "Exec function $functionName :" . PHP_EOL;
		call_user_func($functionName, array());
	}

	if ( $withDao ) {
		$functionName = __NAMESPACE__ . '\testdao_' . $suffix;
		if ( function_exists($functionName) ) {
			echo "Exec function $functionName :" . PHP_EOL;
			call_user_func($functionName, array());
		}
	}
}

/**
 * execute tous les test
 */
function optiona($options)
{
	boot();

	$test = array(
		'date',
		'exception',
		'model',
		'example'
	);
	$functions = get_defined_functions();

	foreach( $functions['user'] as $function ) {
		if ( strpos($function, __NAMESPACE__ . '\test_') === 0 ) {
			echo "Exec functions :$function" . PHP_EOL;
			call_user_func($function, array());
		}
	}
}

/**
 * Initialise la base de données
 */
function optioninitdb($options)
{
	boot();

	$RbplmLibPath = realpath(__DIR__);
	$Schema = new \Rbplm\Dao\Schema(Connexion::get());
	$Schema->sqlInScripts = array(
		'sequence.sql',
		'create.sql',
		'insert.sql'
	);
	$sqlCompiledFile = $Schema->compileSchema($RbplmLibPath);
	$Schema->createBdd(file_get_contents($sqlCompiledFile));
}

/**
 * Extrait les scripts SQL depuis les classes DAO
 */
function options($options)
{
	$Extractor = new \Application\Dao\Schemas\SqlExtractor(__DIR__ . '/data/sql/workflow');
	$path = realpath(__DIR__ . '/module/Workflow/src/Workflow/Dao');
	$Extractor->parse($path);
	echo 'See result in ' . $Extractor->resultPath . CRLF;

	$Extractor = new \Application\Dao\Schemas\SqlExtractor(__DIR__ . '/data/sql/wumanager');
	$path = realpath(__DIR__ . '/module/Wumanager/src/Wumanager/Dao');
	$Extractor->parse($path);
	echo 'See result in ' . $Extractor->resultPath . CRLF;
}

/**
 * Initialize
 */
function boot()
{
	if ( !defined('APPLICATION_PATH') ) {
		define('APPLICATION_PATH', realpath(__DIR__));
	}

	$config = array_merge(include ('config/autoload/global.php'), include ('config/autoload/local.php'));
	$daoMap = include (__DIR__ . '/config/autoload/objectdaomap.local.php');

	var_dump($config, $daoMap);

	Connexion::setConfig($config['rbp']['db']);
	DaoFactory::setMap($daoMap);
	DaoFactory::setConnexion(Connexion::get());
	DaoFactory::setOption('listclass', '\Application\Dao\DaoList');

	Filesystem\Reposit::$basePath = realpath($config['rbp']['path']['reposit']);
	\Wumanager\Service\Workflow\Code::$processPath = realpath($config['rbp']['workflow']['path']['process']);
	People\CurrentUser::get()->setLogin('Test')->setName('TesterFromCli');
}

/**
 */
function test_instanceof()
{
	echo '$object is instance of AnyObject' . "\n";
	$object = new Model\Any();
	assert($object instanceof Model\Any);

	echo '$object is instance of AnyObject' . "\n";
	assert($object instanceof Model\Any);
}

/**
 */
function test_date()
{
	$tz = new \DateTimeZone('Europe/Paris');
	$date = new \DateTime('now', $tz);
	echo $date->format('Y-m-d H:i:s') . "\n";
	echo $date->getTimestamp() . "\n";

	$date = new \DateTime('05 oct 2050', $tz);
	echo $date->format('Y-m-d') . "\n";
	echo $date->getTimestamp() . "\n";

	$date = new \Datetime();
	$date->setTimeStamp(time());
	echo $date->format('Y-m-d') . "\n";
}

function test_exception()
{
	try {
		throw new \Exception('Sys exception message avec un %mot1%');
	}
	catch( \Rbplm\Sys\Exception $e ) {
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	catch( \Exception $e ) {}

	try {
		throw new \Rbplm\Sys\Exception('Sys exception message avec un %mot1%', 5000, array(
			'mot1' => 'remplacepar'
		));
	}
	catch( \Rbplm\Sys\Exception $e ) {
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	catch( \Rbplm\Sys\Exception $e ) {
		var_dump($e->getMessage());
	}

	try {
		throw new \Rbplm\Sys\Exception('Sys exception message avec un %mot1%', 5000, array(
			'mot1' => 'remplacepar'
		));
	}
	catch( \Exception $e ) {
		echo 'attrape SysException depuis \Exception' . CRLF;
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}

	try {
		throw new \Rbplm\Dao\Exception('Sys exception message avec un %mot1%', 5000, array(
			'mot1' => 'remplacepar'
		));
	}
	catch( \Rbplm\Sys\Exception $e ) {
		echo 'attrape DaoException depuis SysException' . CRLF;
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
}

function test_model()
{
	$component = Component\Box::init('BOX1');
	$component->setTitle('toto');
	assert($component->getTitle() == 'toto');

	$component = Component\Box::init('BOX1');
	$component->setWidth('100%');
	$component->setHeight(null);
	$component->isResizable(false);
	$component->isDraggable(false);
	$component->setVisibility('private');

	$Renderer = new Renderer\Box($component, new ViewModel());

	$project = Project::init('PROJECT1');
	$component = new Component\Box($project);
	$component->addChild(new Component\Box());
	$component->addChild(new Component\Box());
	$component->addChild(new Component\Box());

	var_dump($project->getId());

	$Renderer = Renderer\Factory::get($component);
	assert($Renderer instanceof Renderer\Box);

	$Renderer = $component->getRenderer();
	assert($Renderer instanceof Renderer\Box);

	$view = new ViewModel();
	$Renderer->render($view);

	// $dao = new Dao\Component();
	// $Renderer = new Renderer();
}

function test_example()
{
	$project = Project::init('PROJECT1');
	$project->setTitle('PROJET ' . $projectId);

	$component1 = Component\Box::init('BOX1', $project);
	$component1->setWidth('100%');
	$component1->setHeight(null);
	$component1->setTitle('BOX 1');
	$component1->setBody('MY BOX 1 BODY');

	$component2 = Component\Box::init('BOX2', $component1);
	$component2->setTitle('BOX2')->setBody('CONTENU OF THE BOX 2');
	$component3 = Component\Box::init('BOX3', $component1);
	$component3->setTitle('BOX3')->setBody('CONTENU OF THE BOX 3');
	$component4 = Component\Box::init('BOX4', $component3);
	$component4->setTitle('BOX4')->setBody('CONTENU OF THE BOX 4');

	$Jalon = Component\Jalon::init('BOX2', $component1);
	$Jalon->setTitle('JALON 1')
		->setLabel('JALON 1')
		->setStatus('EN COURS')
		->setTargetdate(\DateTime::createFromFormat('d/m/Y', '15/12/2016'))
		->setNotice('Un jalon permet de définir une etape sans durée')
		->setStatuslist(json_encode(array(
		'DRAFT',
		'EN COURS',
		'A VALIDER',
		'VALIDE',
		'ANNULER'
	)));

	$Status = Component\Statusbox::init('BOX2', $component2);
	$Status->setTitle('STATUS 1')
		->setLabel('STATUS 1')
		->setStatus('EN COURS')
		->setNotice('Un status est un état, qu\on se le dise')
		->setStatuslist(json_encode(array(
		'VALIDE',
		'ANNULER'
	)));

	$view = new ViewModel();

	$Renderer = new Renderer\Project($project, new ViewModel());
	$Renderer->bind();

	$view->addChild($Renderer->render(), 'project');
}

function test_statusbox()
{
	$Statusbox = Component\Statusbox::init();
	$uid = $Statusbox->getUid();
	$statusList = json_encode(array(
		'STATUS1',
		'STATUS2',
		'STATUS3'
	));

	$properties = array(
		'name' => 'STATUSBOX_TEST',
		'uid' => $Statusbox->getUid(),
		'parentId' => 1,
		'parentUid' => '54f18059721df',
		'attributes' => array(
			'title' => 'STATUSBOX_TEST',
			'body' => 'BODY TEXT',
			'label' => 'STATUS 1',
			'status' => 'STATUS1',
			'notice' => 'Status box for test only',
			'statuslist' => $statusList
		)
	);

	$Statusbox->hydrate($properties);

	assert($Statusbox->getStatuslist() == $statusList);
	assert($Statusbox->getTitle() == 'STATUSBOX_TEST');
	assert($Statusbox->getBody() == 'BODY TEXT');
	assert($Statusbox->getLabel() == 'STATUS 1');
	assert($Statusbox->getNotice() == 'Status box for test only');

	$Renderer = Renderer\Factory::get($Statusbox);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	assert($Renderer->getView()->statuslist = json_decode($statusList));
}

function testdao_statusbox()
{
	$Statusbox = Component\Statusbox::init();
	$uid = $Statusbox->getUid();
	$statusList = json_encode(array(
		'STATUS1',
		'STATUS2',
		'STATUS3'
	));

	$properties = array(
		'name' => 'STATUSBOX_TEST',
		'uid' => $Statusbox->getUid(),
		'parentId' => 1,
		'parentUid' => '54f18059721df',
		'attributes' => array(
			'title' => 'STATUSBOX_TEST',
			'body' => 'BODY TEXT',
			'label' => 'STATUS 1',
			'status' => 'STATUS1',
			'notice' => 'Status box for test only',
			'statuslist' => $statusList
		)
	);

	$dao = DaoFactory::getDao($Statusbox);
	$Statusbox->hydrate($properties);

	// bind
	// $bind = $dao->bind($Statusbox);

	// Save
	$dao->save($Statusbox);

	// load
	$Statusbox = $dao->loadFromUid(new Component\Statusbox(), $uid);
	var_dump($Statusbox->getStatuslist(), $statusList);
	assert($Statusbox->getStatuslist() == $statusList);
	assert($Statusbox->getTitle() == 'STATUSBOX_TEST');
	assert($Statusbox->getBody() == 'BODY TEXT');
	assert($Statusbox->getLabel() == 'STATUS 1');
	assert($Statusbox->getNotice() == 'Status box for test only');

	$Renderer = Renderer\Factory::get($Statusbox);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	assert($Renderer->getView()->statuslist = json_decode($statusList));
}

function test_jalon()
{
	$Jalon = Component\Jalon::init();
	$uid = $Jalon->getUid();
	$statusList = json_encode(array(
		'STATUS1',
		'STATUS2',
		'STATUS3'
	));

	$properties = array(
		'name' => 'JALON_TEST',
		'uid' => $Jalon->getUid(),
		'parentId' => 1,
		'parentUid' => '54f18059721df',
		'attributes' => array(
			'title' => 'JALON_TEST',
			'body' => 'BODY TEXT',
			'label' => 'JALON 1',
			'status' => 'STATUS1',
			'notice' => 'Jalon for test only',
			'statuslist' => $statusList
		)
	);

	$Jalon->hydrate($properties);

	assert($Jalon->getStatuslist() == $statusList);
	assert($Jalon->getTitle() == 'JALON_TEST');
	assert($Jalon->getBody() == 'BODY TEXT');
	assert($Jalon->getLabel() == 'JALON 1');
	assert($Jalon->getNotice() == 'Jalon for test only');

	$Renderer = Renderer\Factory::get($Jalon);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	assert($Renderer->getView()->statuslist = json_decode($statusList));
}

function testdao_jalon()
{
	$Jalon = Component\Jalon::init();
	$uid = $Jalon->getUid();
	$statusList = json_encode(array(
		'STATUS1',
		'STATUS2',
		'STATUS3'
	));

	$properties = array(
		'name' => 'JALON_TEST',
		'uid' => $Jalon->getUid(),
		'parentId' => 1,
		'parentUid' => '54f18059721df',
		'attributes' => array(
			'title' => 'JALON_TEST',
			'body' => 'BODY TEXT',
			'label' => 'JALON 1',
			'status' => 'STATUS1',
			'notice' => 'Jalon for test only',
			'statuslist' => $statusList
		)
	);

	$dao = DaoFactory::getDao($Jalon);
	$Jalon->hydrate($properties);

	// bind
	// $bind = $dao->bind($Statusbox);

	// Save
	$dao->save($Jalon);

	// load
	$Jalon = $dao->loadFromUid(new Component\Jalon(), $uid);
	var_dump($Jalon->getStatuslist(), $statusList);
	assert($Jalon->getStatuslist() == $statusList);
	assert($Jalon->getTitle() == 'JALON_TEST');
	assert($Jalon->getBody() == 'BODY TEXT');
	assert($Jalon->getLabel() == 'JALON 1');
	assert($Jalon->getNotice() == 'Jalon for test only');

	$Renderer = Renderer\Factory::get($Jalon);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	assert($Renderer->getView()->statuslist = json_decode($statusList));
}

/**
 */
function test_tabpanel()
{
	$tabpanel = Component\Tabpanel::init('Tabpanel1');
	$uid = $tabpanel->getUid();

	$tab1 = new Component\Tabpanel\Item($index = 0, $notice = 'notice 1', $label = 'TAB1');
	$tab2 = new Component\Tabpanel\Item($index = 1, $notice = 'notice 2', $label = 'TAB2');
	$tab3 = new Component\Tabpanel\Item($index = 2, $notice = 'notice 3', $label = 'TAB3');

	$tabpanel->addItem($tab1);
	$tabpanel->addItem($tab2);
	$tabpanel->addItem($tab3);

	$Box1 = Component\Box::init('Box1', $tabpanel)->setIndex(0);
	$Box2 = Component\Box::init('Box2', $tabpanel)->setIndex(1);
	$Box3 = Component\Box::init('Box3', $tabpanel)->setIndex(2);

	$tabpanel->setTitle('TAB PANEL 1');

	$Renderer = Renderer\Factory::get($tabpanel);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	var_dump($Renderer->getView());
}

/**
 */
function testdao_tabpanel()
{
	$project = DaoFactory::getDao(Project::$classId)->loadFromId(new Project(), 1);

	$tabpanel = Component\Tabpanel::init('Tabpanel1');
	$uid = $tabpanel->getUid();
	$tabpanel->setParent($project);

	$tab1 = new Component\Tabpanel\Item($index = 0, $notice = 'notice 1', $label = 'TAB1');
	$tab2 = new Component\Tabpanel\Item($index = 1, $notice = 'notice 2', $label = 'TAB2');
	$tab3 = new Component\Tabpanel\Item($index = 2, $notice = 'notice 3', $label = 'TAB3');

	$tabpanel->addItem($tab1);
	$tabpanel->addItem($tab2);
	$tabpanel->addItem($tab3);

	$Box1 = Component\Box::init('Box1', $tabpanel)->setIndex(0);
	$Box2 = Component\Box::init('Box2', $tabpanel)->setIndex(1);
	$Box3 = Component\Box::init('Box3', $tabpanel)->setIndex(2);

	$tabpanel->setTitle('TAB PANEL 1');

	DaoFactory::getDao($tabpanel)->save($tabpanel);
	DaoFactory::getDao($Box1)->save($Box1);
	DaoFactory::getDao($Box2)->save($Box2);
	DaoFactory::getDao($Box3)->save($Box3);

	// Load
	$tabpanel = DaoFactory::getDao($tabpanel)->loadFromUid(new Component\Tabpanel(), $uid);
	Dao\Loader::loadChildren($tabpanel);

	var_dump($tabpanel->getItems());
	// var_dump($tabpanel->getChildren());

	$Renderer = Renderer\Factory::get($tabpanel);
	$Renderer->setMode($Renderer::MODE_BUILD);
	$Renderer->bind();

	var_dump($Renderer->getView());
}

/**
 */
function test_clonetree()
{
	$project = Project::init('PROJECT1');
	$component1 = Component\Box::init('BOX1');
	$component2 = Component\Box::init('BOX2');
	$component3 = Component\Box::init('BOX3');
	$component4 = Component\Box::init('BOX4');
	$component5 = Component\Box::init('BOX5');
	$component6 = Component\Box::init('BOX6');
	$project->addChild($component1, true);
	$project->addChild($component2, true);
	$project->addChild($component3, true);
	$component3->addChild($component4, true);
	$component3->addChild($component5, true);

	$callback = function ($c) {
		echo $c->getUid() . "\n";
	};

	iterateComponent($project, $callback);

	$Clone = clone $project;

	assert($Clone->getUid() != $project->getUid());
	// var_dump( $Clone->getUid() );
	// var_dump( $project->getUid() );
	// die;

	iterateAndCloneComponent($project);
	iterateComponent($Clone, $callback);
}

/**
 */
function test_process()
{
	$process = new \Workflow\Model\Wf\Process();
	$process->setUid('processUniqId');
	$process->setName('ProcessTest2');

	$aProperties = array(
		'processId' => 8,
		'name' => 'activity001',
		'normalized_name' => 'activity001-0.1',
		'description' => 'Start activity',
		'isInteractive' => true,
		'isAutoRouted' => false,
		'isComment' => false
	);

	$activity001 = \Workflow\Model\Wf\Activity\Activity::init();
	$activity001->hydrate($aProperties);
	$activity001->setExpirationTime(new \DateTime());
	$activity001->setProcess($process);

	$activity002 = \Workflow\Model\Wf\Activity\Activity::init();
	$activity002->hydrate($aProperties);

	$transition = \Workflow\Model\Wf\Transition::factory($activity001, $activity002);
	$transition->setStatus('Status1');
}

/**
 */
function testdao_process()
{
	$process = new \Workflow\Model\Wf\Process();
	$process->setUid('processUniqId');
	$process->setName('ProcessTest2');

	$aProperties = array(
		'processId' => 8,
		'name' => 'activity001',
		'normalized_name' => 'activity001-0.1',
		'description' => 'Start activity',
		'isInteractive' => true,
		'isAutoRouted' => false,
		'isComment' => false
	);

	$activity001 = \Workflow\Model\Wf\Activity\Activity::init();
	$activity001->hydrate($aProperties);
	$activity001->setExpirationTime(new \DateTime());
	$activity001->setProcess($process);
	$dao = DaoFactory::getDao($activity001);
	$dao->save($activity001);

	$activity002 = \Workflow\Model\Wf\Activity\Activity::init();
	$activity002->hydrate($aProperties);
	$dao->save($activity002);

	$transition = \Workflow\Model\Wf\Transition::factory($activity001, $activity002);
	$transition->setStatus('Status1');

	$scripts = $activity001->getScripts();
	var_dump($scripts);
}

/**
 */
function test_wumanager()
{
	$Wu = \Wumanager\Model\Workunit::init();
	$Wu->setSite('BLAGNAC');

	$Deliverable = \Wumanager\Model\Deliverable::init();
	$Deliverable->setParent($Wu);
}

/**
 */
function testdao_wumanager()
{
	$Wu = \Wumanager\Model\Workunit::init();
	$Wu->setSite('BLAGNAC');
	DaoFactory::getDao($Wu)->save($Wu);

	$Deliverable = \Wumanager\Model\Deliverable::init();
	$Deliverable->setParent($Wu);
	DaoFactory::getDao($Deliverable)->save($Deliverable);
}

/**
 */
function test_wucomment()
{
	$comment = \Wumanager\Model\Workunit\Comment::init();
	$comment->setBody('Test');

	$model = DaoFactory::getModel($comment->cid);
	assert($model instanceof \Wumanager\Model\Workunit\Comment);
	$model = DaoFactory::getNewModel(\Wumanager\Model\Workunit\Comment::$classId);
	assert($model instanceof \Wumanager\Model\Workunit\Comment);

	var_dump($comment->cid);
}

/**
 */
function testdao_wucomment()
{
	$wuId = 50;

	$parent = new \Application\Model\Any(array(
		'id' => 50,
		'uid' => uniqid()
	));
	$model = \Wumanager\Model\Workunit\Comment::init(uniqid(), $parent);
	$model->setBody($body);

	$dao = DaoFactory::getDao($model->cid);
	assert($dao instanceof \Wumanager\Dao\Workunit\Comment);

	$model->setBody('Test of comment');
	// $model->ownerId='admin';
	// $model->attachTo($wuId);

	var_dump($model->getParent(true));
	var_dump($model->parentUid);
	die();

	$dao->save($model);
}

/**
 */
function test_reflection()
{
	$fileName = __DIR__ . '/module/Wumanager/src/Wumanager/Service/Workflow/Prototype/ActivityModel.php';
	require ($fileName);
	$file = new Reflection\FileReflection($fileName);
	$class = $file->getClasses()[0];
	var_dump($class);
	var_dump($file->getDocBlock());
	var_dump($file->getDocComment());
	var_dump($file->getNamespace());
	var_dump($file->getStartLine());
	var_dump($file->getUses());

	// var_dump($class->getConstants());
	var_dump($class->getContents());

	// var_dump($class->newInstance());
}

/**
 */
function test_callback()
{
	$project = Project::init('PROJECT1');
	$component1 = Component\Box::init('BOX1');
	$component2 = Component\Box::init('BOX2');
	$component3 = Component\Box::init('BOX3');
	$component4 = Component\Box::init('BOX4');
	$project->addChild($component1, true);
	$project->addChild($component2, true);
	$project->addChild($component3, true);

	$callback = function ($c) {
		echo $c->getUid() . "\n";
	};
	iterateComponent($project, $callback);
}

/**
 */
function test_serialize()
{
	$user = new People\User();
	$user->setLogin('administrateur');

	$appAcl = new Acl\Application();
	$appAcl->loadRole($user);

	$serialize = serialize($appAcl);
	$unserialize = unserialize($serialize);

	var_dump($unserialize);
}

/**
 */
function test_tojson()
{
	echo json_encode(array(
		4
	)) . "\n";

	$a = explode(';', 'o_cyssau');

	var_dump($a);
}

/**
 */
function test_signal()
{}

/**
 */
function test_dircopy()
{
	// copy tinymce in js
	if ( !is_dir('/tmp/test') ) {
		mkdir('/tmp/test');
	}
	Filesystem\Filesystem::dircopy('vendor/tinymce/tinymce', '/tmp/test');
}

/**
 */
function test_filelist()
{
	var_dump(Filesystem\Reposit::$basePath);
	$Filelist = Component\Filelist::init();
	var_dump($Filelist->getReposit());
}

function testdao_connexion()
{
	$conf = include ('config/autoload/local.php');
	Connexion::setConfig($conf['rbp']['db']);
	$connexion = Connexion::get();

	// write
	$connexion->exec("INSERT INTO test (col1) VALUES ('test')");
	$stmt = $connexion->prepare("INSERT INTO test (col1) VALUES (:col1)");
	$stmt->execute(array(
		':col1' => 'fromStmt'
	));

	$bind = array(
		':uid' => 'string',
		':name' => 'name',
		':index' => 4,
		':visibility' => 'public',
		':ownerId' => null,
		':parentId' => null,
		':updateById' => null,
		':updated' => '5/12/2015',
		':serializedAttributes' => 'string'
	);

	$sql = "INSERT INTO component (`uid`,`name`,`index`,`visibility`,`ownerid`,`parentid`,`updatebyid`,`updated`,`attributes`) VALUES (:uid,:name,:index,:visibility,:ownerId,:parentId,:updateById,:updated,:serializedAttributes);";
	$stmt = $connexion->prepare($sql);
	$stmt->execute($bind);

	// get
	$connexion->query("SELECT col1 FROM test WHERE col1='fromStmt'");

	// delete
	$connexion->exec("DELETE FROM test");
}

function testdao_componentdao()
{
	$project = Project::init();
	$project->setTitle('PROJET');
	$project->setUpdated(\DateTime::createFromFormat('Y-m-d H:i:s', '2010-01-25 15:25:12'));

	$dao = DaoFactory::getDao($project);

	$component1 = Component\Box::init('BOX1', $project);
	$component2 = Component\Box::init('BOX2', $component1);
	$component3 = Component\Box::init('BOX3', $component1);

	// bind
	$bind = $dao->bind($project);

	// Save
	$dao->save($project);
	$dao->save($component1);
	$dao->save($component2);
	$dao->save($component3);

	// var_dump($project->getUid(), $project->getName());
	// var_dump($component1->getUid(), $component1->getName());
	// var_dump($component2->getUid(), $component2->getName());
	// var_dump($component3->getUid(), $component3->getName());

	$uid = $project->getUid();
	$id = $project->getId();

	// Load
	$project = $dao->loadFromUid(new Project(), $uid);
	assert($project->getTitle() == 'PROJET');
	assert($project->getId() == $id);

	var_dump($project->getUpdated()->format('Y-m-d H:i:s'));

	$project = $dao->loadFromId(new Project(), $id);
	assert($project->getTitle() == 'PROJET');
	assert($project->getId() == $id);

	// Update
	$project->setTitle('PROJECT MODIFIED');
	$dao->save($project);

	// Delete with children
	$dao->suppress($project->getUid(), true);
}

function testdao_list()
{
	$conf = include ('config/autoload/local.php');
	Connexion::setConfig($conf['rbp']['db']);

	$project = DaoFactory::getDao(Project::$classId)->loadFromId(new Project(), 1);
	_getChildrenAsLiteral($project->getUid());
	_getChildrenAsObject($project);

	$myTree = array();
	Dao\Loader::loadChildrenFromUid($myTree, $project->getUid());
	// var_dump($myTree);
	Dao\Loader::loadChildren($project);
}

/**
 * Recursive function
 * Load from Uid
 */
function _getChildrenAsLiteral($parentUid)
{
	$list = DaoFactory::getList(DaoFactory::GENERIC);
	$filter = "parentUid='$parentUid'";
	$list->load($filter);

	foreach( $list as $item ) {
		// var_dump($item);
		$uid = $item['uid'];
		echo "ParentUid: " . $parentUid . " ChildUid: " . $uid . "\n";
		_getChildrenAsLiteral($uid);
	}
}

/**
 * Recursive function
 * Load from Uid
 */
function _getChildrenAsObject($parent)
{
	$list = DaoFactory::getList($parent::$classId);
	$parentUid = $parent->getUid();
	$filter = "parentUid='$parentUid'";
	$list->load($filter);

	foreach( $list as $item ) {
		$uid = $item['uid'];
		$child = Model\Factory::get($item['cid']);
		DaoFactory::getDao($item['cid'])->loadFromUid($child, $uid);
		$parent->addChild($child);

		echo "ParentId: " . $parent->getId() . " ChildId: " . $child->getId() . "\n";
		_getChildrenAsObject($child);
	}
	return $parent;
}

/**
 */
function test_aclraw()
{
	$user = new People\User();

	$adapter = new \Application\Model\Auth\DbAdapter('projectmanager', 'azerty');
	$result = $adapter->authenticate();
	$identity = $result->getIdentity();
	if ( $identity ) {
		$user->hydrate($identity);
	}
	else {
		$user->hydrate(array(
			'login' => 'user1',
			'memberof' => Acl\Acl::ROLE_GUEST
		));
	}

	$ressourceId = \Application\Model\Application::$id;
	$appResource = new Resource($ressourceId);

	$appAcl = new \Zend\Permissions\Acl\Acl();
	$appAcl->addResource($appResource);
	$appAcl->addRole(new Acl\Role(Acl\Acl::ROLE_GUEST))
		->addRole(new Acl\Role(Acl\Acl::ROLE_COLLABORATOR), array(
		Acl\Acl::ROLE_GUEST
	))
		->addRole(new Acl\Role(Acl\Acl::ROLE_ADMIN), array(
		Acl\Acl::ROLE_COLLABORATOR
	));

	$appAcl->allow(Acl\Acl::ROLE_ADMIN, $appResource)
		 // access to all in $resourceId
		->allow(Acl\Acl::ROLE_COLLABORATOR, $appResource, Acl\Acl::RIGHT_CONSULT)
		->allow(Acl\Acl::ROLE_GUEST, $appResource, Acl\Acl::RIGHT_CONSULT);

	$roles = array(
		(int)$user->memberof
	);
	$appAcl->addRole($user->getLogin(), $roles);

	assert($appAcl->isAllowed($user->getLogin(), $appResource, Acl\Acl::RIGHT_CONSULT) == true);
	assert($appAcl->isAllowed($user->getLogin(), $appResource, Acl\Acl::RIGHT_ADMIN) == false);

	$projectId = 414;
	$projectResource = new \Application\Model\Acl\Resource($projectId);
	$appAcl->addResource($projectResource, $appResource); // project herite de application

	assert($appAcl->isAllowed($user->getLogin(), $projectResource, Acl\Acl::RIGHT_CONSULT) == true);
	assert($appAcl->isAllowed($user->getLogin(), $projectResource, Acl\Acl::RIGHT_ADMIN) == false);
}

/**
 */
function testdao_deletefromfilter()
{
	$instanceDao = DaoFactory::getDao(\Workflow\Model\Wf\Instance::$classId);
	$instanceId = 46;
	$instanceDao->deleteFromId($instanceId);
}

/**
 */
function testdao_acl()
{
	$user = new People\User();

	$adapter = new \Application\Model\Auth\DbAdapter('projectmanager', 'azerty');
	$result = $adapter->authenticate();
	$user->hydrate($result->getIdentity());

	$appAcl = new Acl\Application();
	$appAcl->loadRole($user);

	assert($appAcl->hasRight(Acl\Acl::RIGHT_MANAGE) == true);
	assert($appAcl->hasRight(Acl\Acl::RIGHT_CONSULT) == true);
	assert($appAcl->hasRight(Acl\Acl::RIGHT_DELETE) == true);

	assert($appAcl->hasRight(Acl\Acl::RIGHT_ADMIN) == false);

	$projectAcl = new Acl\Project($projectId = 414, $appAcl);
	$projectAcl->loadRole($user);

	assert($projectAcl->hasRight(Acl\Acl::RIGHT_MANAGE) == true); // inherit
	assert($projectAcl->hasRight(Acl\Acl::RIGHT_ADMIN) == false); // inherit
}

/**
 */
function testdao_initacl()
{
	$appAcl = new Acl\Acl(Component\Application::$id);
	$appAcl->loadRole($userId);
	$appAcl->insertRole($userId, Acl\Acl::ROLE_ADMIN);
}

/**
 */
function test_ldap()
{
	if ( function_exists(ldap_explode_dn) ) {
		$dn = utf8_decode('CN=Cloud,OU=Utilisateurs système,DC=sierbla,DC=int');
		$edn = \ldap_explode_dn($dn, true);
		var_dump($edn);
	}
	else {
		// throw(new \Exception('Support For ldap must be activated.'));
	}
}

/**
 *
 * @param unknown_type $component
 * @param unknown_type $callback
 */
function iterateComponent($component, $callback)
{
	foreach( $component->getChildren() as $c ) {
		$callback($c);
	}
}

/**
 *
 * @param unknown_type $parent
 */
function iterateAndCloneComponent($parent)
{
	foreach( $parent->getChildren() as $child ) {
		$child->newUid();
		$child->setParent($parent, false);
		iterateAndCloneComponent($child);
	}
}

/**
 */
function test_codegenerator()
{
	$method = new Generator\MethodGenerator();
	$method->setName('hello')->setBody('echo \'Hello world!\';');

	$class = new Generator\ClassGenerator();
	$class->setName('World')->addMethodFromGenerator($method);

	$file = new Generator\FileGenerator();
	$file->setClass($class);

	// Render the generated file
	echo $file->generate();

	$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Wumanager\Service\Workflow\Prototype\ActivityModel'));
	$docblock = new Generator\DocBlockGenerator();
	$docblock->setShortDescription('A class for activity ACTNAME');
	$date = new \DateTime();

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('pacakge')
		->setDescription(''));

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('generated')
		->setDescription($date->format(\DateTime::ISO8601)));

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('author')
		->setDescription('rbportail'));

	$class->setName('act1')
		->setDocblock($docblock)
		->setExtendedClass('Workflow\ActivityTrigger')
		->addUse('Application\Dao')
		->setNamespaceName('Workflow\ProcessDef');
	$file = new Generator\FileGenerator();
	echo $file->setClass($class)->generate();

	$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Wumanager\Service\Workflow\Prototype\ActivityForm'));
	$docblock = new Generator\DocBlockGenerator();
	$docblock->setShortDescription('A form for activity ACTNAME');
	$date = new \DateTime();

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('pacakge')
		->setDescription(''));

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('generated')
		->setDescription($date->format(\DateTime::ISO8601)));

	$tag = new Generator\DocBlock\Tag();
	$docblock->setTag($tag->setName('author')
		->setDescription('rbportail'));

	$class->setName('act1')
		->setDocblock($docblock)
		->setExtendedClass('Workflow\ActivityTrigger')
		->addUse('Application\Dao')
		->setNamespaceName('Workflow\ProcessDef')
		->setIndentation(1);

	$file = new Generator\FileGenerator();
	var_dump($file->getIndentation());
	$file->setIndentation("");
	$class->setIndentation("");
	$class->setSourceContent("Hello !");
	var_dump($class->getSourceContent());
	echo $file->setClass($class)->generate();
}

function test_getpreviousactivity()
{}

run();