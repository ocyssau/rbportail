<?php 
/**
 * Example of usage :
* img/05d0c0b2-8774-9530-ca4b-d242870c11bf/55018ae0d3bb9
* img/9d0eef44-4fc7-2597-063c-027802747081/5501c4853a9db.png
*/
chdir(__DIR__.'/../../.');

//Controller
$r = array_reverse(explode('/', $_SERVER['REDIRECT_URL']));
$filename = basename( strtolower($r[0]) );
$repositUid = basename( strtolower($r[1]) );

$conf = include('config/autoload/local.php');
$appPath = 'module/Application/src/';

require_once($appPath.'/Application/Model/Filesystem/Reposit.php');
require_once($appPath.'/Application/Model/Exception.php');
Application\Model\Filesystem\Reposit::$basePath = realpath($conf['rbp']['path']['reposit']);

$reposit = \Application\Model\Filesystem\Reposit::initFromUid($repositUid);
$file = $reposit->getPath()."/$filename";
$mimeType='image';

//var_dump(getcwd());die;
if(is_file($file)){
	$size = filesize($file);
	header("Content-disposition: attachment; filename=$filename");
	header("Content-Type: " . $mimeType);
	header("Content-Transfer-Encoding: $filename\n"); // Surtout ne pas enlever le \n
	header("Content-Length: ".($size));
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
	header("Expires: 0");
	readfile($file);
	die;
}
else{
	http_response_code(404);
	echo 'none founded file';
}
