<?php 

/**
 * Example of usage :
 * get.php?name=55018ae0d3bb9&reposit=05d0c0b2-8774-9530-ca4b-d242870c11bf
 */


chdir(__DIR__.'/../../.');

$conf = include('config/autoload/local.php');

$appPath = 'module/Application/src/';

require_once($appPath.'/Application/Model/Filesystem/Reposit.php');
require_once($appPath.'/Application/Model/Uuid.php');
require_once($appPath.'/Application/Model/Exception.php');


Application\Model\Filesystem\Reposit::$basePath = realpath($conf['rbp']['path']['reposit']);

$filename = basename($_REQUEST['name']);
$repositUid = basename($_REQUEST['reposit']);

$reposit = \Application\Model\Filesystem\Reposit::initFromUid($repositUid);
$file = $reposit->getPath()."/$filename";

$mimeType='';

//var_dump(getcwd());die;
if(is_file($file)){
	$size = filesize($file);
	header("Content-disposition: attachment; filename=$filename");
	header("Content-Type: " . $mimeType);
	header("Content-Transfer-Encoding: $filename\n"); // Surtout ne pas enlever le \n
	header("Content-Length: ".($size));
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
	header("Expires: 0");
	readfile($file);
	die;
}
else{
	http_response_code(404);
}
?>
