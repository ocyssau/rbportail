<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

$env = getenv('APPLICATION_ENV');
if($env=='dev'){
	define('DEBUG', true);
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}
else{
	define('DEBUG', false);
	ini_set('display_errors', 0);
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
}


if (!file_exists('vendor/autoload.php')) {
    throw new RuntimeException(
        'Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.'
    );
}

// Setup autoloading
include 'vendor/autoload.php';

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

$appConfig = include APPLICATION_PATH . '/config/application.config.php';
if (file_exists(APPLICATION_PATH . '/config/development.config.php')) {
    $appConfig = Zend\Stdlib\ArrayUtils::merge($appConfig, include APPLICATION_PATH . '/config/development.config.php');
}

// Setup autoloading
require 'config/boot.php';

//pour changer tous les messages d'erreurs en ErrorException
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler", E_ERROR);

if ( DEBUG === true ) {
	rbinit_enableDebug(true);
	error_reporting(E_ALL);
}
else {
	rbinit_enableDebug(false);
}

//require 'vendor/Rbp/Rbp.php'; //Config of Rbportail application
//var_dump($_SERVER['APPLICATION_ENV']);

set_include_path(get_include_path().':'.realpath(APPLICATION_PATH.'/vendor/PEAR' ));

// Run the application!
Zend\Mvc\Application::init($appConfig)->run();