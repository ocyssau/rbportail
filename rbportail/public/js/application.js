/**
 * 
 */
function initSortableHeader(orderby, order, url)
{
	var th = $('th[data-field="'+orderby+'"]');
	th.data('order', order);
	if(order=='asc'){
		th.children('span').addClass("glyphicon-chevron-down");
	}
	else{
		th.children('span').addClass("glyphicon-chevron-up");
	}

	$('th.sortable').click(function(e){
		var order = $(this).data('order');
		var orderby = $(this).data('field');
		if(order == 'asc'){
			order='desc';
		}
		else{
			order='asc';
		}

		var orderQuery = 'order='+order+'&orderby='+orderby;
		if (url.search(/(\?)./i) == -1) {
			url = url+'?'+orderQuery;
		}
		else{
			url = url+'&'+orderQuery;
		}
		window.open(url,"_self");
	});
}

/**
 * @returns {___anonymous671_715}
 */
function getUrlWithoutSort()
{
	//Url without params
	var url = window.location.origin + window.location.pathname;
	var output = {
		order:null,
		orderby:null,
		url:url
	};

	//put url paramters in object parameters without order and orderby
	if (window.location.search.length > 1) {
	  var aCouples = window.location.search.substr(1).split('&');
	  var aItKey, nKeyId;
	  for (nKeyId = 0; nKeyId < aCouples.length; nKeyId++) {
	    aItKey = aCouples[nKeyId].split('=');
	    if (aItKey[0] == 'order') {
	    	output.order=aItKey[1];
	    }
	    else if (aItKey[0] == 'orderby') {
	    	output.orderby=aItKey[1];
	    }
	    else{
	      if(nKeyId>0){
	    	url = url + "&" + aCouples[nKeyId];
	      }
	      else{
	    	url = url + "?" + aCouples[nKeyId];
	      }
	    }
	  }
	}
	output.url = url;
	return output;
}

/*
 * function to suppress space and other specials characters of a string
 * use as: string.normalize()
 */
String.prototype.normalize = function(){
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
    str = str.replace(" ", ""); //supprime les espaces
    str = str.toLowerCase(); //minuscules
    
    return str;
}


/*
 * function to suppress space and other specials characters of a string
 * use as: string.normalize()
 * @var integer len Fix string to length
 * @var string char Fill with this character  
 */
String.prototype.fixlentgh = function(len, char)
{
	char = char ? char : '0';
    var str = this;
	for(var word = str.toString(); word.length < len; word = char+word){}
    return word;
}

