function wumanager(){
	
	this.baseUrl = "";
	
	/**
	 * 
	 * @param parentJqElement
	 */
	this.init = function(parentJqElement)
	{
		parentJqElement.find(".rb-popup").click(function(e){
			e.preventDefault();
			var width = $(this).data("width");
			var height = $(this).data("height");
			var url = $(this).prop("href");
			var target = $(this).prop("title");
			popupP(url, target , height , width)
			return false;
		});

		parentJqElement.find(".rb-openintop").click(function(e){
			e.preventDefault();
			var href = $(this).attr("href");
			/* the main window must be set with the name defined here by set name as 'window.name="main rb window";' */
			var mainWindow = window.open(href, "main rb window");
			mainWindow.focus();
			return false;
		});

		/* FLASH MESSENGER */
		var rbFlashMessenger = new RbFlashMessenger();
		rbFlashMessenger.init(parentJqElement.find(".rb-feedbacks"));
		
		this.initDate();
	}
	
	/**
	 * 
	 */
	this.initStatusBtn = function(elemts)
	{
		var wumanager = this;
		elemts.each(function(index, item){
			var element = $(item);
			var progression = element.data('progression');
			progression = progression;
			var rgbaColor = wumanager.statusColor(progression);
			var colorTop = "rgba(" + rgbaColor.r + "," + rgbaColor.g + "," + rgbaColor.b + "," + rgbaColor.a + " )";
			//var colorBottom = "rgba(" + (rgbaColor.r - 30) + "," + (rgbaColor.g - 30) + "," + (rgbaColor.b - 30) + "," + rgbaColor.a + " )";
			//element.css("background-image", "linear-gradient(to bottom, " + colorTop + " 0px, " + colorBottom + " 100%)");
			//element.removeClass("btn-success");
			//element.css("color", "#fffff");
			element.css("border", "4px solid "+colorTop);
		});
	}
	
	/**
	 * 
	 */
	this.initDate = function()
	{
		$('[type=date]').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			format: 'dd-mm-yyyy'
		});
		$( '[type=date]' ).attr('type', 'text');
		
		$( ".datepicker" ).datepicker({
			showWeek: true,
			dateFormat:"dd-mm-yy",
			firstDay: 1
		});
		$( ".datepicker" ).datepicker( "option", $.datepicker.regional[ "fr" ] );

		/*
		$( ".datetimepicker" ).datetimepicker({
			showWeek: true,
			formatDate:"dd-mm-yy",
			formatTime:"H:i",
			minDate:0,
			minTime:false,
			maxTime:false,
			//allowTimes: ['12:00','13:00','14:00','15:00','16:00','17:00','18:00','20:00','21:00','00:00'],
			roundTime:'floor',
			firstDay: 1,
			lang: "fr",
			step: 5
		});
		*/
	}
	
	/**
	 * 
	 */
	this.statusColor = function(step)
	{
		/* Green Color */
		var green = {
				r:92,
				g:184,
				b:92,
				a:0.93
		};
		/* End Color */
		var purple = {
				r:163,
				g:73,
				b:153,
				a:0.93
		};
		/* End Color */
		var orange = {
				r:239,
				g:171,
				b:73,
				a:0.93
		};
		
		
		if(step < 33){
			colorRgba = green;
			colorRgba.a = (step / 100) + 0.66;
			colorRgba.r = Math.round(colorRgba.r + (step*10));
		}
		else if(step < 66){
			colorRgba = orange;
			colorRgba.a = (step / 100) + 0.33;
			colorRgba.g = Math.round(colorRgba.g + (step*10));
		}
		else {
			colorRgba = purple;
			colorRgba.a = (step / 100);
			colorRgba.b = Math.round(colorRgba.b + (step*10));
		}
		
		return colorRgba;
	}
	
	/**
	 * 
	 */
	this.addDeliverable = function(){
        var currentCount = $('.rbp-deliverable-list').children().length;
        //var template = $('.rbp-deliverable-item-template').first().html();
        var template = $('form').find("[data-template]").data("template");
        template = template.replace(/__index__/g, currentCount);
        $('.rbp-deliverable-list').last().append(template);
        
    	var fieldset = $(".rbp-deliverable-list").find("fieldset").last();
    	this.formatDeliverableFieldset(fieldset);
        
        return false;
	};
	
	/**
	 * 
	 */
	this.deleteDeliverable = function(button){
		var ok = confirm('Are you sur that you want delete this entry?');
		if(ok==true){
			var item = $(this).parents(".rbp-deliverable-item").first();
			var uid = item.find("#uid").attr("value");
			
			var data = {uid:uid};
			$.ajax({
				type: 'get',
				url:wumanager.baseUrl+"deliverable/delete",
				data: data,
				error: function(jqXHR, textStatus, errorThrown){
					alert('Error durind deletion: ' + textStatus);
				},
				success: function(data, textStatus, jqXHR){
					var form = item.parents("form").first();
					item.parents("form").append('<input type="hidden" name="deleted[]" value="'+uid+'">');
					item.remove(); //remove line in list editor
				}
			});
		}
		return false;
	};
	
	/**
	 */
	this.delFile = function(componentId, fileId, button)
	{
		var myGetterUrl = this.componentGetterBaseUrl+"/delfile";
		$.ajax({
			type: 'get',
			url:myGetterUrl,
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind deletion of file: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				//reload current component
				var comptype = 'filelist';
				var url = rbportail.componentGetterBaseUrl+"/get"+comptype+"/"+componentId;
				$.get( url, function( data ) {
					$(button).parents('div.rbp-component').first().parent().html( data );
				});
			}
		});
	};
	
	/**
	 * 
	 * @param fieldset
	 */
	this.formatDeliverableFieldset = function(fieldset, mode){
		panel = $('<div class="panel panel-default rbp-deliverable-item"><div class="panel-heading"></div><div class="panel-body"></div></div>');
		var col1 = $('<div class="col-md-4"></div>');
		var col2 = $('<div class="col-md-4"></div>');
		var col3 = $('<div class="col-md-4"></div>');
		var row = $('<div class="row"></div>');
		row.append(col1);
		row.append(col2);
		row.append(col3);
		panel.find('.panel-body').append(fieldset);
		panel.find('.panel-body').append(row);
		$(".rbp-deliverable-list").append(panel);
		
		var pcol = col1;
		var c = fieldset.find('label').length;
		var d = c/3;
		var i = 0;
		$.each(fieldset.find('label'), function(index, formControl){
			var fgroup = $('<div class="form-group"></div>');
			var nextElemt = $(formControl).next();
			if(nextElemt.is('ul') ){
				//move error messages ul
				fgroup.append(nextElemt);
			}
			fgroup.prepend(formControl);
			pcol.append(fgroup);
			i++;
			if(i>d){
				pcol = pcol.next();
				i=0;
			}
		});
		
		if(mode=='edit'){
			var minusButton = $('<button></button>');
			minusButton.attr('title', 'Remove');
			minusButton.attr('data-toggle', 'tooltip');
			minusButton.addClass("btn btn-danger btn-xs remove-btn rbp-delete-row pull-right");
			minusButton.css("height", "15px");
			minusButton.css("width", "15px");
			minusButton.append('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
			minusButton.click(wumanager.deleteDeliverable);
			panel.prepend(minusButton);
		}
	};
	
	/**
	 * 
	 */
	this.saveStatus = function(id, status){
		data={};
		data.id = id;
		data.status = status;
		$.ajax({
			type: 'get',
			url:wumanager.baseUrl+"workunit/status",
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus +': '+ errorThrown);
			},
			success: function(data, textStatus, jqXHR){
			}
		});
	};
	
	/**
	 * Get activity from ajax request
	 */
	this.selectActivity = function(url){
		$.ajax({
			type: 'get',
			url:url,
			data:{},
			dataType:'json',
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus + ': ' + errorThrown);
			},
			success: function(activities, textStatus, jqXHR){
				var parent = $('#selectActivityModal').find(".activities-list").first();
				parent.empty();
				
				var wuname = $('#selectActivityModal').find(".wu-name").first();
				wuname.html('');
				
				var instanceId = "";
				var processName = "";
				
				var row = $();
				var col1 = $();
				var col2 = $();
				var col3 = $();

				$.each(activities.activities, function(index,activity){
					if(activity.instanceId != instanceId){
						instanceId = activity.instanceId;
						processNormalName = activity.normalizedName;
						processName = activity.procName;
						processVersion = activity.procVersion;
						row = $('<div class="row"></div>');
						col1 = $('<div class="col-md-4"></div>');
						col2 = $('<div class="col-md-4"></div>');
						col3 = $('<div class="col-md-4"></div>');
						
						parent.append(row);
						row.append(col1);
						row.append(col2);
						row.append(col3);
						
						col1.append(processName+" "+ processVersion +' Act #'+instanceId);
						
						var graph = $('<div class="process-graph"></div>');

						var img = $('<img src="" class="process-img" alt="">');
						img.attr('src', 'img/Graph568ce3c4d4590/'+processNormalName+'/'+processNormalName+'.png');
						img.css('height','100');
						img.hide();
						
						var imgmini = $('<img src="" class="process-img-mini" alt="">');
						imgmini.attr('src', 'img/Graph568ce3c4d4590/'+processNormalName+'/'+processNormalName+'.png');
						imgmini.css('height','100');
						
						graph.append(imgmini);
						graph.append(img);
						col1.append(graph);
					}
					
					var btn = $('<button type="button" class="btn btn-primary btn-run-activity"></button>');
					col2.append(btn);
					btn.html(activity.label);
					btn.data("iaid", activity.id); //instance activity id
					btn.data("aid", activity.activityId); //activity id
					btn.data("iid", activity.instanceId); //process instance id
					btn.data("type", activity.type); //type of activity
					
					var comments = JSON.parse(activity.attributes).comments;
					col3.append(comments);
				});
				
				$.each(activities.standalone, function(index,activity){
					var btn = $('<button type="button" class="btn btn-primary btn-run-activity"></button>');
					col2.append(btn);
					btn.html(activity.label);
					btn.data("iaid", activity.activityId); //instance activity id
					btn.data("aid", activity.activityId); //activity id
					btn.data("iid", activity.procId); //process instance id
					btn.data("type", activity.type); //type of activity

					var comments = JSON.parse(activity.attributes).comments;
					col3.append(comments);
				});
				
				$(".btn-run-activity").click(function(e){
					var activityId = $(this).data("iaid");
					var procInstId = $(this).data("iid");
					var type = $(this).data("type");
					runActivity(activityId, procInstId, type);
				});
				
				$(".process-img").click(
					function(){
						$(".process-img").animate({
							height: 100
						}, 600, function(){
							$(".process-img").hide();
							$(".process-img-mini").show();
						})
					}
				);
				
				$(".process-img-mini").click(
					function(e){
						$( this ).hide();
						$(".process-img").show();
						$(".process-img").animate({
							height: 1000
						}, 600)
					}
				);
			}
		});
		$('#selectActivityModal').modal({});
	};
}

/**
 * 
 */
function RbFlashMessenger()
{
	/**
	 * Include a script only if is not yet loaded
	 */
	this.init = function(baseJqElement)
	{
		this.element = baseJqElement;
		//baseJqElement.alert();

		/* FLASH MESSENGER */
		baseJqElement.find( ".rb-flash-message,.rb-flash-success,.rb-flash-warning" ).delay( 1000 ).fadeOut( 1000 );
		baseJqElement.find( ".btn-redisplay-messages" ).click(function(e){
			$(".rb-flash").show().delay( 10000 ).fadeOut( 1000 );
		});

		/* AJAX FEEDBACKS */
		var t = baseJqElement.find("#rb-ajax-feedback");
		baseJqElement.find("#rb-ajax-error").hide();
		baseJqElement.find("#rb-ajax-feedback").hide();
	};
};

/**
 * 
 */
function RbAjax()
{
	/**
	 * 
	 */
	this.htmlErrorDisplay = function(html)
	{
		var boxPrototype = $("#rb-ajax-error");
		var box = boxPrototype.clone(true); /* with events */
		var index = 1;
		box.attr("id", "rb-ajax-error-"+index);
		box.addClass("rb-ajax-error");
		box.find("#message").html(html);
		boxPrototype.parent().append(box);
		box.show();
		return false;
	};

	/**
	 */
	this.htmlMessageDisplay = function(html)
	{
		var boxPrototype = $("#rb-ajax-feedback");
		var box = boxPrototype.clone(true); /* with events */
		box.attr("id", "rb-ajax-feedback-1");
		box.addClass("rb-ajax-feedback");
		box.find("#message").html(html);
		boxPrototype.parent().append(box);
		box.show().delay( 2000 ).fadeOut( 1000 );
		return false;
	};

}; /* End of object */



function wumanagerClassificationForm()
{
	
	/**
	 * 
	 */
	this.init = function(baseUrl)
	{
		var contextSelect = $("select[name='context']");
		var typeDimensionSelect = $("select[name='typeDimension']");
		var typeSelect = $("select[name='typeId']");

		contextSelect.change(function(e){
	        var val = $(this).val(); // on récupère la valeur
	        var typeDimension = typeDimensionSelect.val();

	        if(val != '' && typeDimension != '') {
	        	typeSelect.empty(); // on vide la liste

	            $.ajax({
	                url: baseUrl + 'wutype/get',
	                data: 'typedim='+typeDimension+'&context='+val,
	                dataType: 'json',
	                success: function(json) {
	                    $.each(json, function(index, value) {
	                    	typeSelect.append('<option value="'+ value.id +'">'+ value.uid + '-' + value.context + '-' + value.name +'</option>');
	                    });
	                }
	            });
	        }
		});

		typeDimensionSelect.change(function(e){
	        var val = $(this).val(); // on récupère la valeur
	        var context = contextSelect.val();

	        if(val != '' && context != '') {
	        	typeSelect.empty(); // on vide la liste

	            $.ajax({
	                url: baseUrl + 'wutype/get',
	                data: 'typedim='+val+'&context='+context,
	                dataType: 'json',
	                success: function(json) {
	                    $.each(json, function(index, value) {
	                    	typeSelect.append('<option value="'+ value.id +'">'+ value.uid + '-' + value.context + '-' + value.name +'</option>');
	                    });
	                }
	            });
	        }
		});

		typeSelect.change(function(e){
		});
	}
};

/**
 * 
 */
function WumNotifier(wumanager, jqTable)
{
	/**
	 * 
	 */
	this.getFromCurrentUser = function()
	{
		var url = wumanager.baseUrl+"notifier/index";
		var notifier = this;
		
		$.ajax({
			type: 'get',
			url: url,
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind notifier get: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				console.log(data);
				$.each(data, function($k,$item){
					var workunitId = $item.workunitId;
					var message = $item.message;
					var trElemt = jqTable.find("tr[data-workunitid='"+workunitId+"']");
					var statusColElemt = trElemt.find('td.status').first();
					statusColElemt.append('<button class="btn btn-danger notifier-dismiss-btn" title="click to dismiss alert"><span class="glyphicon glyphicon-bell badge">ALERT</span> '+message+'</button>');
				});
				notifier.initDismissButtons();
			}
		});
		
		return false;
	};
	
	/**
	 * 
	 */
	this.initDismissButtons = function()
	{
		var url = wumanager.baseUrl+"notifier/delete";
		$(".notifier-dismiss-btn").click(function(e){
			var jqButton = $(this);
			var workunitId = jqButton.parents("tr").first().data("workunitid");

			var data = {
				workunitId: workunitId,
			};
			$.ajax({
				type: 'get',
				url: url,
				data : data,
				dataType: 'json',
				error: function(jqXHR, textStatus, errorThrown){
					alert('Error durind dismiss notification: ' + textStatus);
				},
				success: function(data, textStatus, jqXHR){
					console.log(jqButton,data);
					jqButton.remove();
				}
			});
		});
		return false;
	};
	
}
