<?php
namespace Sandbox\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mail\Message;
use Zend\Mime\Mime;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Application\Service\Mail as MailService;

/**
 * 
 * @author ocyssau
 *
 */
class IndexController extends AbstractActionController
{

	/**
	 * 
	 */
	public function mailAction()
	{
		$mailService = \Application\Service\Mail::get();
		$message = $mailService->messageFactory('Test');
		$subject = 'Sandbox\Controller\IndexController.mailAction()';

		$fromMail = 'olivier.cyssau@sierbla.com';
		$fromName = 'olivier Cyssau';
		$message->setFrom($fromMail, $fromName);
		$message->setSubject($subject);

		$toMail = 'olivier.cyssau@sierbla.com';
		$message->addTo($toMail, $toMail);
		/* */
		if ( $toMail ) {
			$mailService->send($message);
		}
		die('Mail Sended');
	}

	/**
	 *
	 */
	public function multipartAction()
	{
		$mailService = \Application\Service\Mail::get();

		// Au besoin faire appel à l'autoloader Cf. chapitre installation ZF
		// require_once '/chemin/vers/composer/vendor/autoload.php';

		$destinataire = 'olivier.cyssau@sierbla.com';
		$expediteur = 'administrateur@sierbla.com';
		$reponse = $expediteur;

		$msg = new Message();
		$msg->addFrom($destinataire, 'Nom de l\'expéditeur')
			->addReplyTo($reponse)
			->addTo($destinataire)
			->setEncoding('utf-8')
			->setSubject('test avec fichier attaché (façon ZF2/ZF3)');

		$text = new MimePart('Ceci est un mail avec un fichier joint.');
		$text->type = Mime::TYPE_TEXT; // text/plain
		$text->charset = 'utf-8';

		$fichierChemin = 'data/autodeliver/template1.xlsx';
		$fichierMimePart = new MimePart(fopen($fichierChemin, 'r'));
		$fichierMimePart->type = Mime::TYPE_OCTETSTREAM;
		$fichierMimePart->filename = basename($fichierChemin);
		$fichierMimePart->disposition = Mime::DISPOSITION_ATTACHMENT;
		$fichierMimePart->encoding = Mime::ENCODING_BASE64;

		$body = new MimeMessage();
		$body->setParts(array(
			$text,
			$fichierMimePart
		));

		$msg->setBody($body);

		$transport = $mailService->getSmtpTransport();
		$transport->send($msg);
		die('message send');
	}

	/**
	 *
	 */
	public function mailserviceAction()
	{
		$destinataire = 'olivier.cyssau@sierbla.com';
		$expediteur = 'administrateur@sierbla.com';
		$reponse = $expediteur;
		
		$mailService = MailService::get();
		$message = MailService::messageFactory();
		$htmlPart = MailService::htmlPartFactory('<b>For test</b>');
		$filePart = MailService::filePartFactory('data/autodeliver/template1.xlsx');
		$body = MailService::bodyFactory();
		$body->setParts(array(
			$htmlPart,
			$filePart
		));
		$message->setBody($body);

		$message->addFrom($destinataire, 'OLIVIER CYSSAU')
			->addReplyTo($reponse)
			->addTo($destinataire)
			->setSubject('test avec fichier attaché (façon ZF2/ZF3)');
		
		$mailService->send($message);

		die('message send');
	}

	/**
	 * 
	 */
	public function codemirrorAction()
	{}
}
