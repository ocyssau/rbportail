<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'sandbox' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sandbox/[:action]',
					'defaults' => array(
						'controller' => 'Sandbox\Controller\Index',
						'action' => 'index'
					),
				),
			),
		) //end routes section
	), //end router section
	'controllers' => array(
		'invokables' => array(
			'Sandbox\Controller\Index' => 'Sandbox\Controller\IndexController'
		)
	),
	'view_manager' => array(
		'template_map' => array(
			'module_layouts' => array(
				'Sandbox' => 'layout/sandbox'
			),
			'layout/sandbox' => __DIR__ . '/../view/layout/layout.phtml'
		),
		'template_path_stack' => array(
			__DIR__ . '/../view'
		)
	)
);
