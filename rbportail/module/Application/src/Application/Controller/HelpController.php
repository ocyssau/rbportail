<?php

namespace Application\Controller;


/**
 * 
 *
 */
class HelpController extends AbstractController
{

	/**
	 * 
	 */
    public function indexAction()
    {
    	$config =  $this->getEvent()->getApplication()->getServiceManager()->get('Configuration');
    	$url = $config['externalLinks']['wiki']['url'];
    	header('location: ' . $url);
		die;
    }
}
