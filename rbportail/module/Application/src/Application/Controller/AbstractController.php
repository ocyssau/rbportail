<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Application\Model\People;
use Application\Model\Acl;
use Application\Dao;
use Zend\Http\Response as HttpResponse;

/**
 * 
 *
 */
abstract class AbstractController extends AbstractActionController
{
	/**
	 *
	 * @var \Application\Model\Auth\Storage
	 */
	protected $storage;

	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface
	 */
	protected $authservice;

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authservice = $this->getEvent()->getApplication()->getServiceManager()->get('AuthService');
		$this->storage = $this->authservice->getStorage();

		if ( !$this->authservice->hasIdentity() ){
			(isset($this->application)) ? $application = $this->application : $application = null;
			return $this->redirect()->toRoute('auth/login', array('application'=>$application));
		}
		$userProperties = $this->storage->read();

		$user = new People\User();
		$user->hydrate($userProperties);
		People\CurrentUser::set($user);

		$user->setLastLogin(new \DateTime());
		if( $user->isLoaded() ){
			Dao\Factory::get()->getDao($user)->updateLastLogin($user);
		}

		if( !isset($user->appacl) ){
			$appAcl = new Acl\Application();
			$appAcl->loadRole( People\CurrentUser::get() );

			$session =& $_SESSION[$this->storage->getNamespace()][$this->storage->getMember()];
			$session['appacl'] = $appAcl;
			$user->appacl = $appAcl;
		}

		return parent::dispatch($request,$respons);
	}

	/**
	 * @return Acl\Acl
	 */
	public function getAcl()
	{
		$user = People\CurrentUser::get();
		return $user->appacl;
	}

	/**
	 *
	 */
	protected function notauthorized()
	{
		$response = new HttpResponse();
		$response->setStatusCode(HttpResponse::STATUS_CODE_403);

		$view = new viewModel();
		$view->setTemplate('error/403');
		$view->message = "You are not authorized to execute this request. May be its time to presents a coffee to your administrator.";

		$viewRender = $this->getEvent()->getApplication()->getServiceManager()->get('ViewRenderer');
		$html = $viewRender->render($view);

		$response->setContent($html);
		return $response;
	}

	/**
	 * Get the authservice. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		return $this->authservice;
	}

	/**
	 * Get the auth storage object. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Application\Model\Auth\Storage
	 */
	public function getSessionStorage()
	{
		return $this->storage;
	}
}
