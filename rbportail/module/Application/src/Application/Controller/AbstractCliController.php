<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;
use Application\Model\People;
use Application\Model\Acl;


/**
 *
 * @author olivier
 *
 */
abstract class AbstractCliController extends AbstractConsoleController
{
	/**
	 *
	 * @var \Application\Model\Auth\Storage
	 */
	protected $storage;
	
	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface
	 */
	protected $authservice;
	
	/**
	 *
	 * @var string
	 */
	public $resourceCn;

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractConsoleController::dispatch()
	 */
	public function dispatch(RequestInterface $request, ResponseInterface $respons = null)
	{
		$this->init();
		parent::dispatch($request, $respons);
	}

	/**
	 *
	 * @return \Application\Controller\AbstractController
	 */
	abstract public function init();

	/**
	 * @return Acl\Acl
	 */
	public function getAcl()
	{
		$user = People\CurrentUser::get();
		return $user->appacl;
	}
	
	/**
	 *
	 */
	protected function notauthorized()
	{
		throw new \Exception('Not authorized');
	}
	
	/**
	 * Get the authservice. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		return $this->authservice;
	}
	
	/**
	 * Get the auth storage object. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Application\Model\Auth\Storage
	 */
	public function getSessionStorage()
	{
		return $this->storage;
	}
}
