<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Model\Application;

/**
 * 
 *
 */
class IndexController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$application = $this->params()->fromRoute('application', 'wumanager');
		$this->redirect()->toRoute($application,array(), array('force_canonical' => true));
	}

	/**
	 *
	 */
	public function userpreferencesAction()
	{
		$view = new ViewModel();
		return $view;
	}
	
	/**
	 *
	 */
	public function aboutAction()
	{
		return array(
			'version' => array(
				'ver'=>Application::$version,
				'build'=>Application::$version,
				'copyright'=>Application::getCopyright()
				),
		);
	}
}
