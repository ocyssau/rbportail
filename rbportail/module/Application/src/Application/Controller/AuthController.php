<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\Stdlib\RequestInterface as Request;
use Application\Form\Auth as AuthForm;

/**
 *
 *
 */
class AuthController extends AbstractController
{
	protected $form;
	protected $storage;
	protected $authservice;
	protected $successRedirect = 'index';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authservice = $this->getEvent()->getApplication()->getServiceManager()->get('AuthService');
		$this->storage = $this->authservice->getStorage();
		
		return AbstractActionController::dispatch($request,$respons);
	}

	/**
	 * @return AuthForm
	 */
	public function getForm($application=null)
	{
		if (! $this->form) {
			$this->form = new AuthForm();
			$this->form->get('application')->setValue($application);
		}
		$url = $this->url()->fromRoute( 'auth/authenticate' );
		$this->form->setAttribute('action', $url);
		return $this->form;
	}

	/**
	 * Login
	 */
	public function loginAction()
	{
		/* if already login, redirect to success page */
		if ($this->getAuthService()->hasIdentity()){
			return $this->redirect()->toRoute($this->successRedirect);
		}

		$application = $this->params()->fromRoute('application', 'wumanager');
		$form = $this->getForm($application);
		return array(
			'form'      => $form,
			'messages'  => $this->flashmessenger()->getMessages()
		);
	}

	/**
	 * Authenticate
	 */
	public function authenticateAction()
	{
		$form       = $this->getForm();
		$redirect = 'auth/login';

		$request = $this->getRequest();
		if ($request->isPost()){
			$application = $request->getPost()->application;
			$form->setData($request->getPost());
			if ($form->isValid()){
				/* check authentication... */
				$this->getAuthService()->getAdapter()
							->setIdentity($request->getPost('username'))
							->setCredential($request->getPost('password'));
				$result = $this->getAuthService()->authenticate();
				foreach($result->getMessages() as $message)
				{
					/* save message temporary into flashmessenger */
					$this->flashmessenger()->addMessage($message);
				}

				if ($result->isValid()) {
					$redirect = $this->successRedirect;

					/* check if it has rememberMe : */
					if ($request->getPost('rememberme') == 1 ) {
						$this->storage->setRememberMe(1);
					}

					$this->storage->write($result->getIdentity());
				}
			}
		}
		$_SESSION['application'] = $application;
		return $this->redirect()->toRoute($redirect, array('application'=>$application), array('force_canonical' => true));
	}

	/**
	 *
	 */
	public function logoutAction()
	{
		$this->getSessionStorage()->forgetMe();
		$this->getAuthService()->clearIdentity();
		$this->flashmessenger()->addMessage("You've been logged out");
		$application = $this->params()->fromRoute('application', $_SESSION['application']);
		$_SESSION = null;
		return $this->redirect()->toRoute('auth/login', array('application'=>$application));
	}
}
