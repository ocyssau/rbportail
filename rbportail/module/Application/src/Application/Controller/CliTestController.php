<?php
namespace Application\Controller;

use Application\Controller\AbstractCliController as AbstractController;
use Application\Console\Output as Cli;
use Zend\Console\Request as ConsoleRequest;

/**
 * How to use :
 * open terminal.
 * 
 * 		cd [path/to/rbportail/root/directory]
 * 		php public/index.php test <class> <method>
 * 
 * i.e : execute method runTest of class ClassTest :
 * php public/index.php test /Module/Controller/Class run
 * @see  https://framework.zend.com/manual/2.3/en/modules/Zend.console.introduction.html
 * 
 * To run all tests methods of a class
 * 
 * 		php public/index.php test /Module/Controller/Class all
 * 
 * To run all methods of all test classes
 * 
 * 		php public/index.php test all
 * 
 * To display profiling informations
 * 
 * 		php public/index.php test /Module/Controller/Class run -p --loop 10
 * 
 * where 10 is number of loops run on each methods
 * 
 * About tests classes :
 * All tests classes must be called with "Test" suffix and all methods, and only to run, must be named with "Test" suffix.
 * So only tests classes must be named with Test suffix.
 *
 */
class CliTestController extends AbstractController
{

	/**
	 * If true, display profilling informations
	 * 
	 * @var boolean
	 */
	protected $profiling = false;

	/**
	 * Number of loop on each test. Valid only if this::profiling is true.
	 * 
	 * @var integer
	 */
	protected $loop = 1;

	/**
	 */
	public function init()
	{
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	public function testAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		$class = $request->getParam('class', null);
		$method = $request->getParam('method', null);
		$all = $request->getParam('a', false);
		$this->profiling = (bool)$request->getParam('p', $this->profiling);
		$this->loop = $request->getParam('loop', $this->loop);

		if ( $all ) {
			$callbacks = $this->getAllCallback();
		}
		else {
			$class = str_replace('/', '\\', $class);
			$class = $class . 'Test';
			if ( $method == 'all' ) {
				$methods = self::getTestMethods($class);
			}
			else {
				$methods = [
					$method . 'Test'
				];
			}
			$obj = new $class($this);
			foreach( $methods as $method ) {
				if ( !is_callable([
					$obj,
					$method
				]) ) {
					throw new \RuntimeException(sprintf('Method %s of class %s is not callable', $method, $class));
				}
				$callbacks[] = [
					$obj,
					$method
				];
			}
		}

		/**/
		foreach( $callbacks as $callback ) {
			$params = [];
			$obj->init();
			try {
				$this->run($callback, $params);
			}
			catch( \Exception $e ) {
				Cli::exception($e);
				continue;
			}
		}
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	protected function getAllCallback()
	{
		$basepath = realpath('./module');
		$callbacks = [];

		$testFiles = [];
		$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basepath));
		foreach( $directoryIt as $file ) {
			if ( substr($file->getFilename(), -8) == 'Test.php' ) {
				$testFiles[] = $file->getRealPath();
			}
		}

		/**/
		foreach( $testFiles as $path ) {
			$class = substr($path, strlen($basepath));
			$class = trim($class, '/');
			$class = trim($class, '\\');
			$class = trim($class, '.php');
			$class = str_replace('/', '\\', $class);

			$p = strpos($class, '\\src\\');
			$class = substr($class, $p + 5);

			Cli::console($class);

			if ( $class == 'Application\Test\AbstractTest' ) {
				continue;
			}

			$obj = new $class();
			$methods = self::getTestMethods($class);
			foreach( $methods as $method ) {
				$callbacks[] = [
					$obj,
					$method
				];
			}
		}

		return $callbacks;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return array
	 */
	protected static function getTestMethods($class)
	{
		$methods = [];
		foreach( get_class_methods($class) as $method ) {
			if ( substr($method, -4) == 'Test' ) {
				$methods[] = $method;
			}
		}
		return $methods;
	}

	/**
	 * @param string $file
	 * @param integer $line
	 * @param integer $code
	 */
	public static function assertCallback($file, $line, $code, $desc = null)
	{
		$str = 'Assertion failed:';
		$str .= 'File ' . $file . CRLF;
		$str .= 'Line ' . $line . CRLF;
		$str .= 'Code ' . $code . CRLF;
		$str .= 'Description ' . $desc . CRLF;
		echo $str;
	}

	/**
	 *
	 */
	protected function run(array $callback, array $params = [])
	{
		$obj = $callback[0];
		$class = get_class($obj);
		$method = $callback[1];

		Cli::headerLine(sprintf('Start test on %s::%s', $class, $method));

		$obj->setUp();
		if ( $this->profiling == true ) {
			$this->startMem = (memory_get_usage() / 1024);
			$this->startTime = microtime(true);
			$i = 0;
			while( $i < $this->loop ) {
				call_user_func_array($callback, $params);
				$i++;
			}
			$this->endTime = microtime(true);
			$this->endMem = (memory_get_usage() / 1024);
			$this->executionTime = $this->endTime - $this->startTime;
			$this->executionTimeByUnit = $this->executionTime / $this->loop;
			echo "$this->loop loops en $this->executionTime S \n";
			echo "soit $this->executionTimeByUnit S par loop \n";
			echo "Memoire utilisée avant = $this->startMem Ko \n";
			echo "Memoire utilisée après = $this->endMem Ko \n";
			echo "Memoire consommée = " . ($this->endMem - $this->startMem) . "Ko \n";
			echo "Memoire pic = " . (memory_get_peak_usage() / 1024) . " Ko \n";
		}
		else {
			call_user_func_array($callback, $params);
		}
		$obj->tearDown();
	}

	/**
	 *
	 * @param array 		$collection
	 * @param string		$displayProperty	name of property used to display node in tree.
	 * @return void
	 */
	protected static function _displayCollectionTree($collection, $displayProperty = 'name')
	{
		/*Walk along the groups tree relation with a iterator*/
		$it = new \RecursiveIteratorIterator($collection, \RecursiveIteratorIterator::SELF_FIRST);
		$it->setMaxDepth(100);
		foreach( $it as $node ) {
			echo str_repeat('  ', $it->getDepth() + 1) . $node->$displayProperty . CRLF;
		}
	}
}
