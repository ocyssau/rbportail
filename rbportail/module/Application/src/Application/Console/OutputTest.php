<?php
namespace Application\Console;

use Application\Console\Output as Cli;

/**
 *
 * @author olivier
 *
 */
class OutputTest extends \Application\Model\AbstractTest
{

	/**
	 *
	 */
	public function runTest()
	{
		Cli::console("some colored text");
		Cli::red("some colored text");
		Cli::green("some colored text");
		Cli::yellow("some colored text");

		Cli::yellow("Mixed color text part yellow", null);
		Cli::red(" Mixed color text part red", null);
		Cli::green(" Mixed color text part green, END");
	}
}
