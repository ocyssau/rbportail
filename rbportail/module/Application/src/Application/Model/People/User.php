<?php
//%LICENCE_HEADER%

namespace Application\Model\People;

use Rbplm\People\UserInterface;
use Application\Model\Any;

/**
 * @brief user definition.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("User")
 *
 */
class User extends Any implements UserInterface
{
	static $classId = 550;

	const SUPER_USER_UUID 		= '99999999-9999-9999-9999-00000000abcd';
	const ANONYMOUS_USER_UUID 	= '99999999-9999-9999-9999-99999999ffff';

	const SUPER_USER_ID 		= 1;
	const ANONYMOUS_USER_ID 	= 2;

	/**
	 * @var boolean
	 */
	protected $active = null;

	/**
	 * @var \DateTime
	 */
	protected $lastLogin = null;

	/**
	 * @var string
	 */
	protected $login = null;

	/**
	 * @Annotation\Type("Zend\Form\Element\Text")
	 * @Annotation\Required({"required":"true" })
	 * @Annotation\Filter({"name":"StripTags"})
	 * @Annotation\Options({"label":"Username:"})
	 *
	 * @var string
	 */
	protected $firstname = null;

	/**
	 * @var string
	 */
	protected $lastname = null;

	/**
	 * @Annotation\Type("Zend\Form\Element\Password")
	 * @Annotation\Required({"required":"true" })
	 * @Annotation\Filter({"name":"StripTags"})
	 * @Annotation\Options({"label":"Password:"})
	 *
	 * @var
	 */
	protected $password = null;

	/**
	 * @var
	 */
	protected $mail = null;

	/**
	 * array of Group
	 *
	 * @var array
	 */
	protected $groups = array();

	/**
	 * Source of authentication = db|ldap
	 * @var string db|ldap
	 */
	public $authFrom;

	/**
	 * @var string
	 */
	public $fullname;

	/**
	 * @var string
	 */
	public $identity;

	/**
	 * @var string
	 */
	public $dn;

	/**
	 * @var string
	 */
	public $memberof;

	/**
	 * @param array			$properties
	 * @param Any $parent
	 * @return void
	 */
	public function __construct(array $properties = null, $parent = null)
	{
		parent::__construct( $properties, $parent );
		$this->login = $this->name;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return User
	 */
	public function hydrate( array $properties )
	{
		//Any
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		//People
		(isset($properties['firstname'])) ? $this->firstname=$properties['firstname'] : null;
		(isset($properties['lastname'])) ? $this->lastname=$properties['lastname'] : null;
		(isset($properties['lastLogin'])) ? $this->lastLogin=$properties['lastLogin'] : null;
		(isset($properties['mail'])) ? $this->mail=$properties['mail'] : null;
		(isset($properties['password'])) ? $this->password=$properties['password'] : null;
		(isset($properties['login'])) ? $this->login=$properties['login'] : null;

		(isset($properties['loaded'])) ? $this->isLoaded(true) : null;
		(isset($properties['fullname'])) ? $this->fullname=$properties['fullname'] : null;
		(isset($properties['identity'])) ? $this->identity=$properties['identity'] : null;
		(isset($properties['dn'])) ? $this->dn=$properties['dn'] : null;

		(isset($properties['memberof'])) ? $this->memberof = $properties['memberof'] : null;
		(isset($properties['appacl'])) ? $this->appacl = $properties['appacl'] : null;
		(isset($properties['authFrom'])) ? $this->authFrom = $properties['authFrom'] : null;

		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive($bool = null)
	{
		if( is_bool($bool) ){
			return $this->active = $bool;
		}
		else{
			return $this->active;
		}
	}

	/**
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}

	/**
	 * Set the User name
	 *
	 * @param string $Login
	 * @return User
	 */
	public function setLogin($login)
	{
		$this->login = trim($login);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastLogin($format = null)
	{
		return $this->lastLogin;
	}

	/**
	 * @param string
	 * @return User
	 */
	public function setLastLogin(\DateTime $date)
	{
		$this->lastLogin = $date;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 * @return User
	 */
	public function setPassword($password)
	{
		$this->password = trim($password);
		return $this;
	}

	/**
	 * @param Group
	 */
	public function addGroup($group)
	{
		$this->groups[$group->getUid()] = $group;
		return $this;
	}

	/**
	 * Get all groups of the current User
	 * @return array
	 */
	public function getGroups()
	{
		return $this->groups;
	}

	/**
	 * Get mail of user
	 * 
	 * @return string
	 */
	public function getMail()
	{
		return $this->mail;
	}
	
} /* End of class */
