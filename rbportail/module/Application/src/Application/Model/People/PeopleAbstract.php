<?php
namespace Application\Model\People;

/**
 * @brief Base class for all people objects.
 * 
 * @verbatim
 * @endverbatim
 *
 */
abstract class PeopleAbstract extends \Application\Model\Any
{
} //End of class
