<?php
namespace Application\Model\People;

/**
 * Current connected user.
 * Example and tests: Rbplm/People/UserTest.php
 */
class CurrentUser
{
	/**
	 * Singleton instance
	 * 
	 * @var User
	 */
	protected static $user;
	
	/**
	 * 
	 * @param User $user
	 */
	public static function set(User $user)
	{
		self::$user = $user;
	}
	
	/**
	 * @return User
	 */
	public static function get()
	{
		if(!self::$user){
			self::$user = new User();
		}
		return self::$user;
	}
}
