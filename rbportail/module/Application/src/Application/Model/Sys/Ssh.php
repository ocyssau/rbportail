<?php
class Ssh
{
	// SSH Host
	private $host = 'myserver.example.com';

	// SSH Port
	private $port = 22;

	// SSH Server Fingerprint
	private $serverfp = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

	// SSH Username
	private $authuser = 'username';

	// SSH Public Key File
	private $authpub = '/home/username/.ssh/id_rsa.pub';

	// SSH Private Key File
	private $authpriv = '/home/username/.ssh/id_rsa';

	// SSH Private Key Passphrase (null == no passphrase)
	private $authpass;

	// SSH Connection
	private $connection;

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function __set($name, $value)
	{
		$name = strtolower($name);
		$this->$name = $value;
		return $this;
	}

	/**
	 * @param string $name
	 */
	public function __get($name)
	{
		$name = str2lower($name);
		return $this->$name;
	}

	/**
	 *
	 * @throws Exception
	 */
	public function connect()
	{
		if (!($this->connection = ssh2_connect($this->host, $this->port))) {
			throw new Exception('Cannot connect to server');
		}
		$fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
		if (strcmp($this->serverfp, $fingerprint) !== 0) {
			throw new Exception('Unable to verify server identity!');
		}
		if (!ssh2_authpubkey_file($this->connection, $this->authuser, $this->authpub, $this->authpriv, $this->authpass)) {
			throw new Exception('Autentication rejected by server');
		}
	}

	/**
	 *
	 * @param string $cmd
	 * @throws Exception
	 */
	public function exec($cmd)
	{
		if (!($stream = ssh2_exec($this->connection, $cmd))) {
			throw new Exception('SSH command failed');
		}
		stream_set_blocking($stream, true);
		$data = "";
		while ($buf = fread($stream, 4096)) {
			$data .= $buf;
		}
		fclose($stream);
		return $data;
	}

	/**
	 *
	 */
	public function disconnect()
	{
		$this->exec('echo "EXITING" && exit;');
		$this->connection = null;
	}

	/**
	 *
	 */
	public function __destruct()
	{
		$this->disconnect();
	}
}
?>
