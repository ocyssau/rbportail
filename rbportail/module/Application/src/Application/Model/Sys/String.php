<?php 
namespace Application\Model\Sys;

use Zend\Filter\FilterInterface;

class String implements FilterInterface
{
	
	static function normalize($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = strtolower($string);
		//return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
		return $string;
	}
	
	/**
	 * Returns the result of filtering $value
	 *
	 * @param mixed string
	 * @return string
	 */
	public function filter($value){
		return self::normalize($value);
	}
}
