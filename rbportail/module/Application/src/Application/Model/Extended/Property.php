<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended;

/**
 * @brief Entity of a property definition.
 * 
 */
abstract class Property
{

	/**
	 * Must be define in children classes
	 * @var integer
	 */
	static $classId = 200;

	/**
	 * Class Id
	 * @var integer
	 */
	public $cid;

	/**
	 * Commons
	 * @var
	 */
	public $id='';
	public $extendedCid='';
	public $name='';
	public $asapp='';
	public $getter='';
	public $setter='';
	public $label='';
	public $type=null;
	public $size=5;
	public $regex='';
	public $required=false;

	/**
	 * @var boolean
	 */
	protected $saved=false;

	/**
	 * @var boolean
	 */
	protected $loaded=false;

	/**
	 * @param array $properties
	 */
	public function __construct($properties=array())
	{
		foreach($properties as $pname=>$pvalue){
			$this->$pname = $pvalue;
		}
		$this->cid = static::$classId;
	}

	/**
	 * @param string $name
	 * @return Property
	 */
	public static function init($name="")
	{
		$class = get_called_class();
		$obj = new $class();
		if( !$name ){
			$name = uniqid(get_class($obj));
		}
		$obj->name = $name;
		return $obj;
	}

	/**
	 * @return Property
	 */
	public function __get($name)
	{
		(isset($this->$name)) ? $ret = $this->$name : $ret = null;
		return $ret;
	}

	/**
	 */
	public function __set($name, $value)
	{
		$this->$name = $value;
		return $this;
	}

	/**
	 * Alias for hydrate; implement arrayObject interface
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		$return = array();
		foreach($this as $name=>$value){
			$return[$name] = $value;
		}
		return $return;
	}

	/**
	 * Alias for hydrate; implement arrayObject interface
	 * @param array $properties
	 */
	public function populate($properties)
	{
		return $this->hydrate( $properties );
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Property
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['extendedCid'])) ? $this->extendedCid=$properties['extendedCid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['asapp'])) ? $this->asapp=$properties['asapp'] : null;
		(isset($properties['getter'])) ? $this->getter=$properties['getter'] : null;
		(isset($properties['setter'])) ? $this->setter=$properties['setter'] : null;
		(isset($properties['label'])) ? $this->label=$properties['label'] : null;
		(isset($properties['type'])) ? $this->type=$properties['type'] : null;
		(isset($properties['size'])) ? $this->size=$properties['size'] : null;
		(isset($properties['regex'])) ? $this->regex=$properties['regex'] : null;
		(isset($properties['required'])) ? $this->required=$properties['required'] : null;
		(isset($properties['type'])) ? $this->type=$properties['type'] : null;
		return $this;
	}
}
