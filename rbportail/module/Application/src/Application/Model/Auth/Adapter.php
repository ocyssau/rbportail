<?php
namespace Application\Model\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

/**
 *
 *
 */
class Adapter implements AdapterInterface
{

    /**
     * Sets username and password for authentication
     *
     * @return void
     */
    public function __construct($username=null, $password=null)
    {
    	$this->identity = $username;
    	$this->credential = $password;
    	\Application\Model\Acl\Acl::$useLdap = false;
    }

    /**
     *
     * @param string $string
     */
    public function setIdentity($string)
    {
    	$this->identity = $string;
    	return $this;
    }

    /**
     *
     * @param string $string
     */
    public function setCredential($string)
    {
    	$this->credential=$string;
    	return $this;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
     *               If authentication cannot be performed
     */
    public function authenticate()
    {
    	$identity = array(
    		'id'=>$this->identity,
    		'uid'=>$this->identity,
    		'firstname'=>'rbportail',
    		'lastname'=>'rbportail',
    		'mail'=>'',
    		'login'=>$this->identity,
    		'identity'=>$this->identity,
    	);

    	$result = new Result(Result::SUCCESS, $identity);
    	return $result;
    }
}
