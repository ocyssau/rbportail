<?php
namespace Application\Model\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Application\Dao\Connexion;

class DbAdapter implements AdapterInterface
{

    /**
     * Sets username and password for authentication
     *
     * @return void
     */
    public function __construct($username=null, $password=null)
    {
    	$this->identity = $username;
    	$this->credential = $password;
    	\Application\Model\Acl\Acl::$useLdap = false;
    	$this->connexion = Connexion::get();
    }

    /**
     *
     * @param string $string
     */
    public function setIdentity($string)
    {
    	$this->identity = $string;
    	return $this;
    }

    /**
     *
     * @param string $string
     */
    public function setCredential($string)
    {
    	$this->credential=$string;
    	return $this;
    }

    /**
     *
     * @param Dao
     */
    public function setConnexion($conn)
    {
    	$this->connexion = $conn;
    	return $this;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
     *               If authentication cannot be performed
     */
    public function authenticate()
    {
    	$sql = "SELECT * FROM user WHERE login=:login AND password=:password";
    	$stmt = $this->connexion->prepare($sql);
    	$stmt->setFetchMode(\PDO::FETCH_ASSOC);

    	$credential = md5($this->credential);

    	$stmt->execute( array(':login'=>$this->identity, ':password'=>$credential) );
    	$row = $stmt->fetch();

    	if($row){
    		$identity = $row;
    		$identity['loaded'] = true;
    		$identity['authFrom'] = 'db';
    		$result = new Result(Result::SUCCESS, $identity);
    	}
    	else{
    		$identity=array();
    		$result = new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $identity);
    	}
    	return $result;
    }
}
