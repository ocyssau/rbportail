<?php
namespace Application\Model\Auth;

use Zend\Authentication\Result as AuthenticationResult;

/*
object(stdClass)[295]
public 'accountexpires' => string '9223372036854775807' (length=19)
public 'badpasswordtime' => string '130700284306047928' (length=18)
public 'badpwdcount' => string '0' (length=1)
public 'c' => string 'FR' (length=2)
public 'cn' => string 'Olivier CYSSAU' (length=14)
public 'co' => string 'FRANCE' (length=6)
public 'codepage' => string '0' (length=1)
public 'company' => string 'SIER' (length=4)
public 'countrycode' => string '250' (length=3)
public 'department' => string 'CAO' (length=3)
public 'displayname' => string 'Olivier CYSSAU' (length=14)
public 'distinguishedname' => string 'CN=Olivier CYSSAU,OU=Pc_de_bureau,OU=Users SIER,DC=sierbla,DC=int' (length=65)
public 'dn' => string 'CN=Olivier CYSSAU,OU=Pc_de_bureau,OU=Users SIER,DC=sierbla,DC=int' (length=65)
public 'dscorepropagationdata' =>
array (size=3)
0 => string '20141105152837.0Z' (length=17)
1 => string '20140414142017.0Z' (length=17)
2 => string '16010101000417.0Z' (length=17)
public 'givenname' => string 'Olivier' (length=7)
public 'homemdb' => string 'CN=Mailbox Database 1950824684,CN=Databases,CN=Exchange Administrative Group (FYDIBOHF23SPDLT),CN=Administrative Groups,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=196)
public 'homemta' => string 'CN=Microsoft MTA,CN=ADAMS,CN=Servers,CN=Exchange Administrative Group (FYDIBOHF23SPDLT),CN=Administrative Groups,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=189)
public 'instancetype' => string '4' (length=1)
public 'ipphone' => string 'pcrenault.sierbla.int' (length=21)
public 'l' => string 'BLAGNAC' (length=7)
public 'lastlogoff' => string '0' (length=1)
public 'lastlogon' => string '130700284581547460' (length=18)
public 'lastlogontimestamp' => string '130697574093402870' (length=18)
public 'legacyexchangedn' => string '/o=SIER/ou=Exchange Administrative Group (FYDIBOHF23SPDLT)/cn=Recipients/cn=Olivier CYSSAUadf' (length=93)
public 'logoncount' => string '336' (length=3)
public 'mail' => string 'Olivier.CYSSAU@sierbla.com' (length=26)
public 'mailnickname' => string 'o_cyssau' (length=8)
public 'mdbusedefaults' => string 'TRUE' (length=4)
public 'memberof' =>
array (size=11)
0 => string 'CN=Cloud,OU=Utilisateurs systÃ¨me,DC=sierbla,DC=int' (length=51)
1 => string 'CN=informatique,CN=Users,DC=sierbla,DC=int' (length=42)
2 => string 'CN=sier_societe,CN=Users,DC=sierbla,DC=int' (length=42)
3 => string 'CN=sier_entiere,CN=Users,DC=sierbla,DC=int' (length=42)
4 => string 'CN=sier_dp,CN=Users,DC=sierbla,DC=int' (length=37)
5 => string 'CN=sier_cao,CN=Users,DC=sierbla,DC=int' (length=38)
6 => string 'CN=VPN,OU=Users SIER,DC=sierbla,DC=int' (length=38)
7 => string 'CN=GG qualite,OU=Groupes SIER,DC=sierbla,DC=int' (length=47)
8 => string 'CN=GG sys-admin,OU=Groupes SIER,DC=sierbla,DC=int' (length=49)
9 => string 'CN=GG direction,OU=Groupes SIER,DC=sierbla,DC=int' (length=49)
10 => string 'CN=GG caomeca,OU=Groupes SIER,DC=sierbla,DC=int' (length=47)
public 'msexchhomeservername' => string '/o=SIER/ou=Exchange Administrative Group (FYDIBOHF23SPDLT)/cn=Configuration/cn=Servers/cn=ADAMS' (length=95)
public 'msexchmailboxguid' => string 'þÌ@$+EMF¡Q´qly^ó' (length=16)
public 'msexchmailboxsecuritydescriptor' => string '�€��� �������,��������
��������
�����������������
���' (length=72)
public 'msexchpoliciesincluded' =>
array (size=2)
0 => string 'e103ab07-f632-4148-897d-37b7e3a1f01f' (length=36)
1 => string '{26491cfc-9e50-4857-861b-0cb8df22b5d7}' (length=38)
public 'msexchrbacpolicylink' => string 'CN=Default Role Assignment Policy,CN=Policies,CN=RBAC,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=130)
public 'msexchrecipientdisplaytype' => string '1073741824' (length=10)
public 'msexchrecipienttypedetails' => string '1' (length=1)
public 'msexchsafesendershash' => string 'X§²Õ@+G|›Lþ¨ÄXèÊ’]ÙØ“l!+oûøxŒ],Ô—÷Ô§D¸?·�Ð»9ÖjÄøÃRÙ' (length=60)
public 'msexchtextmessagingstate' =>
array (size=2)
0 => string '302120705' (length=9)
1 => string '16842751' (length=8)
public 'msexchumdtmfmap' =>
array (size=4)
0 => string 'emailAddress:6548437297728' (length=26)
1 => string 'reversedPhone:4935811650' (length=24)
2 => string 'lastNameFirstName:2977286548437' (length=31)
3 => string 'firstNameLastName:6548437297728' (length=31)
public 'msexchuseraccountcontrol' => string '0' (length=1)
public 'msexchversion' => string '44220983382016' (length=14)
public 'msexchwhenmailboxcreated' => string '20140415082238.0Z' (length=17)
public 'msnpallowdialin' => string 'FALSE' (length=5)
public 'mssfu30password' => string 'ABCD!efgh12345$67890' (length=20)
public 'name' => string 'Olivier CYSSAU' (length=14)
public 'objectcategory' => string 'CN=Person,CN=Schema,CN=Configuration,DC=sierbla,DC=int' (length=54)
public 'objectclass' =>
array (size=4)
0 => string 'top' (length=3)
1 => string 'person' (length=6)
2 => string 'organizationalPerson' (length=20)
3 => string 'user' (length=4)
public 'objectguid' => string 'Nò`X	·fG•XK(Žoy' (length=16)
public 'objectsid' => string '��������u¹uTwÙtå;+N��' (length=28)
public 'physicaldeliveryofficename' => string 'SIER' (length=4)
public 'postalcode' => string '31700' (length=5)
public 'primarygroupid' => string '513' (length=3)
public 'profilepath' => string '\\niepce\profils$\o_cyssau' (length=26)
public 'proxyaddresses' =>
array (size=3)
0 => string 'smtp:O_CYSSAU@sierbla.com' (length=25)
1 => string 'SMTP:Olivier.CYSSAU@sierbla.com' (length=31)
2 => string 'smtp:CYSSAU.Olivier@sierbla.com' (length=31)
public 'pwdlastset' => string '130675980517758345' (length=18)
public 'samaccountname' => string 'o_cyssau' (length=8)
public 'samaccounttype' => string '805306368' (length=9)
public 'scriptpath' => string 'connect-cao.bat' (length=15)
public 'showinaddressbook' =>
array (size=5)
0 => string 'CN=Mailboxes(VLV),CN=All System Address Lists,CN=Address Lists Container,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=149)
1 => string 'CN=All Mailboxes(VLV),CN=All System Address Lists,CN=Address Lists Container,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=153)
2 => string 'CN=All Recipients(VLV),CN=All System Address Lists,CN=Address Lists Container,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=154)
3 => string 'CN=Tous les utilisateurs,CN=All Address Lists,CN=Address Lists Container,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=149)
4 => string 'CN=Liste d'adresses globale par dÃ©faut,CN=All Global Address Lists,CN=Address Lists Container,CN=SIER,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=sierbla,DC=int' (length=171)
public 'sn' => string 'CYSSAU' (length=6)
public 'st' => string '31' (length=2)
public 'streetaddress' => string '69, rte de Cornebarrieu' (length=23)
public 'telephonenumber' => string '05 61 18 53 94' (length=14)
public 'title' => string 'Administrateur systeme & conception mÃ©canique.' (length=47)
public 'useraccountcontrol' => string '512' (length=3)
public 'userparameters' => string '                                                PCtxCfgPresentã”µæ”±æˆ°ã¢CtxCfgFlags1ã€°ã¦æŒ²ã€¹CtxCallbackã€°ã€°ã€°ã€°CtxShadowã„°ã€°ã€°ã€°(CtxMaxConnectionTimeã€°ã€°ã€°ã€°.CtxMaxDisconnectionTimeã¥ãŒ¹ã°ã€°CtxMaxIdleTimeã¥ãŒ¹ã°ã€°"CtxKeyboardLayoutã€°ã€°ã€°ã€°*CtxMinEncryptionLevelã„° CtxWorkDirectoryã€° CtxNWLogonServerã€°CtxWFHomeDirã€°"CtxWFHomeDirDriveã€° CtxWFProfilePathã€°"CtxInitialProgramã€°"CtxCallbackNumberã€°' (length=469)
	public 'userprincipalname' => string 'o_cyssau@sierbla.int' (length=20)
	public 'usnchanged' => string '8091100' (length=7)
	public 'usncreated' => string '10294' (length=5)
	public 'whenchanged' => string '20150302081009.0Z' (length=17)
	public 'whencreated' => string '20030117154550.0Z' (length=17)
*/
class Ldap extends \Zend\Authentication\Adapter\Ldap
{

	/**
	 *
	 * @param string $sid
	 */
	protected function sidfromLdap($sid)
	{
		// How to unpack all of this in one statement to avoid resorting to hexdec? Is it even possible?
		$t = unpack('H*hex', $sid);
		$sidHex = $t['hex'];
		$subAuths = unpack('H2/H2/n/N/V*', $sid);

		$revLevel = hexdec(substr($sidHex, 0, 2));
		$authIdent = hexdec(substr($sidHex, 4, 12));

		return 'S-'.$revLevel.'-'.$authIdent.'-'.implode('-', $subAuths);
	}

	/**
	 * Performs an authentication attempt
	 *
	 * @return \Zend\Authentication\Result
	 * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
	 *               If authentication cannot be performed
	 */
	public function authenticate()
	{
		/**
		 * @var AuthenticationResult
		 */
		$result = parent::authenticate();
		$messages = $result->getMessages();

		if ($result->isValid()) {
			$adData = $this->getAccountObject();

			$memberof=array();
			if(isset($adData->memberof)){
				if(is_array($adData->memberof)){
					foreach($adData->memberof as $dn){
						$edn = ldap_explode_dn(utf8_decode($dn), true);
						isset($edn[0]) ? $memberof[] = $edn[0] : null;
					}
				}
				else{
					$dn = $adData->memberof;
					$edn = ldap_explode_dn(utf8_decode($dn), true);
					$memberof[] = $edn[0];
				}
			}
			$identity = array(
				'fullname'=>$adData->name,
				'givenname'=>$adData->name,
				'mail'=>$adData->mail,
				'login'=>$this->identity,
				'uid'=>$this->identity,
				'id'=>$this->identity,
				'identity'=>$result->getIdentity(),
				'dn'=>$adData->dn,
				'memberof'=>$memberof,
				'sid'=>$this->sidfromLdap($adData->objectsid),
				'guid'=>$this->sidfromLdap($adData->objectguid),
				'authFrom'=>'ldap'
			);
			$result = new AuthenticationResult(AuthenticationResult::SUCCESS, $identity, $messages);
		}
		/* else try with db */
		else{
			$authAdapter = new DbAdapter();
			$authAdapter
				->setIdentity($this->getIdentity())
				->setCredential($this->getCredential());
			$result = $authAdapter->authenticate();
			$messages = $result->getMessages();
		}
		return $result;
	}
}
