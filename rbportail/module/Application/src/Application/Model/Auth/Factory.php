<?php
namespace Application\Model\Auth;

/**
 * 
 *
 */
class Factory
{

	/**
	 * @return \Zend\Authentication\Adapter\AdapterInterface
	 */
	public static function getAdapter($conf)
	{
		if ( $conf['adapter'] == 'ldap' ) {
			$options = $conf['options'];
			$username = "";
			$password = "";
			$AuthAdapter = new Ldap($options, $username, $password);
		}
		elseif ( $conf['adapter'] == 'passall' ) {
			$AuthAdapter = new Adapter();
		}
		elseif ( $conf['adapter'] == 'db' ) {
			$AuthAdapter = new DbAdapter();
		}
		return $AuthAdapter;
	}
}


