<?php
namespace Application\Model\Acl;

use Application\Dao;
use Application\Model\People\User;

abstract class Acl
{
    static $classId = 500;
    static $rolesMap;
    static $useLdap = true;

    const RIGHT_ADMIN = 'ADMIN';
    const RIGHT_MANAGE = 'MANAGE';
    const RIGHT_CONSULT = 'CONSULT';
    const RIGHT_UPDATE = 'UPDATE';
    const RIGHT_DELETE = 'DELETE';
    const RIGHT_CREATE = 'CREATE';

	const ROLE_ADMIN = 2; //2
	const ROLE_COLLABORATOR = 16; //16
	const ROLE_GUEST = 64; //64

	protected $dao = null;
	protected $userId = null;

	/**
	 * @var \Zend\Permissions\Acl\Acl
	 */
	protected $acl;

	/**
	 * @var Resource
	 */
	protected $mainResource = null;

	/**
	 * The roles defined for the current user.
	 * @var array
	 */
	protected $userRole = array();

	/**
	 * Array ROLE_ID=>array('name', array of parents)
	 * @var array
	 */
	public $roles;

	/**
	 * @var array
	 */
	public $rules;

	/**
	 * @var Acl
	 */
	protected $parentAcl;

	/**
	 *
	 */
	public function __sleep(){
		$this->dao = null;
		foreach($this->acl->getResources() as $resource){
			if($resource != $this->mainResource){
				$this->acl->removeResource($resource);
			}
		}
		$ret = array();
		foreach($this as $name=>$val){
			$ret[]=$name;
		}
		return $ret;
	}

	/**
	 * @param string $rightName
	 */
	public function hasRight($rightName)
	{
		if(!$this->userId){
			throw new \Exception('Acl roles must be load with loadRole before call this method', E_ERROR);
		}
		if($this->userId == 'administrateur'){
			return true;
		}
		return $this->acl->isAllowed($this->userId, $this->mainResource, $rightName);
	}

	/**
	 * @return \Application\Dao\Acl\Acl
	 */
	public function getDao()
	{
		if(!$this->dao){
			$this->dao = Dao\Factory::get()->getDao(static::$classId);
		}
		return $this->dao;
	}

	/**
	 * @return Acl
	 */
	public function getAcl()
	{
		return $this->acl;
	}

	/**
	 * @return Resource
	 */
	public function getMainResource()
	{
		return $this->mainResource;
	}

	/**
	 * $roleId must be an existing role
	 *
	 * @param string $userId
	 * @param string $roleId
	 * @param string $resourceId
	 * @return Acl
	 */
	public function insertRole($userId, $roles, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}
		foreach($roles as $roleId){
			$this->getDao()->insert($userId,$roleId,$resourceId);
			$this->userRole[$roleId] = $roleId;
		}
		$this->acl->addRole($userId, $roles);
		return $this;
	}

	/**
	 * @param User $user
	 * @param string $resourceId
	 */
	abstract public function loadRole(User $user, $resourceId=null);

}
