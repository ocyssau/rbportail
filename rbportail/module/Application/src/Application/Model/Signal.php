<?php

namespace Application\Model;

/**
 * Classe de gestion des signaux.
 *
 * Copyright (C) 2011 Nicolas Joseph
 *
 * LICENSE:
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Rbportail
 * @author     Nicolas Joseph <gege2061@homecomputing.fr>
 * @author     Olivier CYSSAU <ocyssau@free.fr>
 * @copyright  2011 Nicolas Joseph
 * @copyright  2015 Olivier Cyssau
 * @license    http://www.opensource.org/licenses/agpl-3.0.html AGPL v3
 * @filesource
 *
 */
class Signal
{
	/**
	 * @var array
	 */
	protected static $callbackList;
	protected static $eventList;

	/**
	 *
	 * @param $object
	 * @param $signalName
	 * @return string
	 */
	private static function _getSignalHash($object, $signalName)
	{
		return spl_object_hash ($object) . $signalName;
	}


	/**
	 * Connecte une fonction de rappel au signal $name de l'objet $object.
	 *
	 * @param \StdClass $object Object à connecter
	 * @param string $eventName Nom du signal
	 * @param array $callback Fonction de rappel
	 *
	 * @return int L'identifiant unique de la connexion. Voir Signal::disconnect
	 */
	public static function connect($object, $eventName, $callback)
	{
		$id = -1;
		$hash = self::_getSignalHash($object, $eventName);

		if( !isset( self::$eventList[$hash] ) ){
			self::$eventList[$hash] = array ();
		}

		$id = uniqid ();
		self::$eventList[$hash][$id] = new Event($eventName, $callback);

		return $id;
	}

	/**
	 * Déconnecte le signal portant l'identifiant $id de l'object $object.
	 *
	 * @param \StdClass $object Objet connecté au signal
	 * @param string $name Nom du signal
	 * @param int L'indentifiant du signal
	 */
	public static function disconnect($object, $name, $id)
	{
		if (self::has($object, $name)){
			$hash = self::_getSignalHash($object, $name);
			unset(self::$eventList[$hash][$id]);
		}
		else{
			throw new Exception ('UNKNOW_SIGNAL', Error::WARNING, $name);
		}
	}

	/**
	 * Émet le signal $name sur l'object $object.
	 *
	 * @param string $eventName Le nom du signal à envoyé
	 * @param mixed $sender Should be the current object instance
	 * @param mixed ... Liste variable d'arguments passée à la fonction de rappel
	 * @return Event a event object where children property is populate whith sub triggered event
	 *
	 */
	public static function trigger($eventName, $sender = null /*, ...*/)
	{
		$hash = self::_getSignalHash($sender, $eventName);

		$returnEvent = new Event($eventName, null);
		$returnEvent->sender = $sender;

		if(isset(self::$eventList[$hash]) ){
			//get list of callback to be triggered
			$events = self::$eventList[$hash];
			$args = func_get_args();

			foreach ($events as $event){
				if( !empty($event->callback) ){
					$returnEvent->children[] = $event;
					$args[0] = $event;
					$event->sender = $sender;
					call_user_func_array($event->callback, $args);
				}
			}
		}
		return $returnEvent;
	}

} /* End of class */


/**
 *
 */
class Event
{
	public $name=null;
	public $sender=null;
	public $callback=null;
	public $result=null;
	public $error=null;
	public $children=array();

	public function __construct($name, $callback=null)
	{
		$this->name = $name;
		$this->callback = $callback;
	}

	/**
	 *
	 */
	public function hasError()
	{
		if($this->children){
			foreach($this->children as $child){
				if($child->hasError()){
					return true;
				}
			}
		}
		return (!is_null($this->error));
	}

	/**
	 *
	 */
	public function getErrors()
	{
		$errors = array();
		if($this->children){
			foreach($this->children as $child){
				if($child->hasError()){
					$errors = array_merge($child->getErrors(), $errors);
				}
			}
		}
		if(!is_null($this->error)){
			$errors[] = $this->error;
		}
		return $errors;
	}
	
	/**
	 *
	 */
	public function getError()
	{
		return $this->error;
	}
	
	/**
	 *
	 */
	public function getLastError()
	{
		if(!is_null($this->error)){
			return $this->error;
		}
		if($this->children){
			foreach($this->children as $child){
				if($child->hasError()){
					$error = $child->getLastError();
					return $error;
				}
			}
		}
	}


}
