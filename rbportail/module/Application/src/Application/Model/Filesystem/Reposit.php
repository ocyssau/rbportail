<?php
//%LICENCE_HEADER%

namespace Application\Model\Filesystem;

use Application\Model\Uuid;
use Application\Model\Exception;

/**
 *
 */
class Reposit
{
	
	/**
	 * 
	 * @var string
	 */
	public static $basePath = '';
	
	/**
	 * @var string
	 */
	protected $uid;
	
	/**
	 * @var string
	 */
	protected $path;
	
	/**
	 * 
	 */
	public static function init()
	{
		$reposit = new Reposit();
		
		$uid = Uuid::newUid();
		$path = self::$basePath.'/'.$uid;
		if(!is_dir($path)){
			$ok = mkdir($path);
			if(!$ok){
				throw new Exception('Unable to create reposit '.$path.' :Check permissions.');
			}
		}
		$reposit->hydrate(
			array(
				'path'=>$path,
				'uid'=>$uid,
				)
		);
		return $reposit;
	}
	
	/**
	 *
	 * @param string $uid
	 */
	public static function initFromUid($uid)
	{
		$reposit = new Reposit();
		$path = self::$basePath.'/'.basename($uid);
		if(!is_dir($path)){
			mkdir($path);
		}
		$reposit->hydrate(
			array(
				'path'=>$path,
				'uid'=>$uid,
			)
		);
		return $reposit;
	}
	
	/**
	 *
	 * $return Reposit
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		
		return $this;
	}
	
	/**
	 *
	 * $return string $name
	 */
	public function getUid()
	{
		return $this->uid;
	}
	
	/**
	 *
	 * @param string $name
	 */
	public function getPath()
	{
		return $this->path;
	}
	
	/**
	 *
	 * @param string $name
	 */
	public function delete()
	{
		$path = $this->path;
		if(is_dir($path)){
			$objects = scandir($path);
			foreach ($objects as $object) {
				if ($object != '.' && $object != '..') {
					if(is_file($object)){
						unlink("$path/$object");
					}
				}
			}
			$ok = rmdir($path);
			if(!$ok){
				throw new Exception('Unable to delete reposit '.$path.' : Check permissions.');
			}
		}
	}
	
}
