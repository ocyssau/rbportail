<?php
//%LICENCE_HEADER%
namespace Application\Model\Filesystem;

use Application\Model\Error;
use Exception;
use Rbplm\Sys\Trash;

/** 
 * @brief A file on the filesystem
 *
 *
 */
class File extends Filesystem
{

	/**
	 * Path to csv config file for type mimes
	 * @var string
	 */
	private static $_mimes_config_file;

	/**
	 *
	 * @var \SplFileInfo
	 */
	protected $_file = false;

	/**
	 * Name of the File 'File.ext'
	 * @var string
	 */
	protected $name;

	/**
	 * path to the File '/dir/sub_dir'
	 * @var string
	 */
	protected $path;

	/**
	 * extension of the File '.ext'
	 * @var string
	 */
	protected $extension;

	/**
	 * root name of the File 'File'
	 * @var string
	 */
	protected $rootname;

	/**
	 * Real full path and name of file
	 * @var string
	 */
	protected $fullpath;

	/**
	 * Size of the File in octets
	 * @var integer
	 */
	protected $size;

	/**
	 * modification time of the File
	 *
	 * @var integer
	 */
	protected $mtime;

	/**
	 * type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * md5 code of the File.
	 *
	 * @var string
	 */
	protected $md5;

	/**
	 *
	 * @var string
	 */
	protected $mimetype;

	/**
	 *
	 * @var string
	 */
	protected $_mimetype_description;

	/**
	 *
	 * @var boolean
	 */
	protected $_no_read;

	/**
	 *
	 * @var string
	 */
	protected $_viewer_driver;

	public function __construct($file)
	{
		$this->_file = new \SplFileInfo($file);
		$this->type = 'file';
		$this->_init();
	}

	/**
	 * 
	 */
	public function __clone()
	{}

	/**
	 * Re-init properties
	 * @return void
	 */
	private function _init()
	{
		$this->name = $this->_file->getFilename();
		$this->size = $this->_file->getSize();
		$this->mtime = date_timestamp_set(new \DateTime(), $this->_file->getMTime());
		$this->extension = self::sGetExtension($this->name);
		$this->rootname = self::sGetRoot($this->name);
		$this->fullpath = $this->_file->getRealPath();
		$this->path = dirname($this->fullpath);
	}

	/**
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getProperies($displayMd5)
	 *
	 */
	public function getProperties($displayMd5 = true)
	{
		$properties = array(
			'name' => $this->name,
			'size' => $this->size,
			'path' => $this->path,
			'mtime' => $this->mtime,
			'extension' => $this->extension,
			'rootname' => $this->rootname,
			'type' => $this->type,
			'fullpath' => $this->fullpath
		);
		if ( $displayMd5 ) {
			$properties['md5'] = $this->getMd5();
		}
		return $properties;
	}

	/**
	 * Size on filesystem.
	 * Return size in octets.
	 *
	 * @return integer
	 */
	function getSize()
	{
		return $this->size;
	}

	/**
	 * Return data extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * @return string
	 */
	function getExtension()
	{
		return $this->extension;
	}

	/**
	 * Last modification time of data
	 *
	 * @return integer
	 */
	function getMtime()
	{
		return $this->mtime;
	}

	/**
	 *
	 * @return string
	 */
	function getName()
	{
		return $this->name;
	}

	/**
	 *
	 * @return string
	 */
	function getPath()
	{
		return $this->path;
	}

	/**
	 *
	 * @return string
	 */
	function getFullpath()
	{
		return $this->fullpath;
	}

	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 * @return string
	 *
	 */
	function getRootname()
	{
		return $this->rootname;
	}

	/**
	 *
	 * @return string
	 */
	function getType()
	{
		return $this->type;
	}

	/**
	 *
	 * @return string
	 */
	function getMd5()
	{
		if ( !$this->md5 ) {
			$this->md5 = md5_file($this->fullpath);
		}
		return $this->md5;
	}

	/**
	 *
	 * @return string
	 */
	function getMimetype()
	{}

	/**
	 * Move/rename the current file
	 *
	 * @param string	$dst 		fullpath to new file
	 * @param boolean	$replace 	true for replace file
	 * @throws \Rbplm\Sys\Exception
	 */
	function rename($newName, $replace = false)
	{
		$newName = trim($newName);
		
		if ( !Filesystem::limitDir($this->fullpath) ) {
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
		
		if ( !Filesystem::limitDir($newName) ) {
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array(
				'path' => $newName
			));
		}
		
		if ( empty($newName) ) {
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array(
				'path' => $newName
			));
		}
		
		if ( is_file($newName) ) {
			$ok = true;
			if ( $replace ) {
				$ok = unlink($newName);
			}
			else {
				$ok = false;
			}
			if ( $ok == false ) {
				throw new Exception('%path%_IS_EXISTING', Error::ERROR, array(
					'path' => $newName
				));
			}
		}
		
		if ( !is_dir(dirname($newName)) ) {
			throw new Exception('%path%_IS_NOT_EXISTING', Error::ERROR, array(
				'path' => $newName
			));
		}
		
		if ( !is_writeable($this->fullpath) ) {
			throw new Exception('CAN_NOT_RENAME_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
		
		if ( !@rename($this->fullpath, $newName) ) {
			throw new Exception('CAN_NOT_RENAME_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
		
		$this->_file = new \SplFileInfo($newName);
		$this->_init();
		return true;
	}

	/**
	 * Copy the current file
	 *
	 * @param string $toFile fullpath to new file
	 * @param integer $mode mode of the new file
	 * @param boolean $replace true for replace existing file
	 * @throws \Rbplm\Sys\Exception
	 * return Rbplm\Sys\Datatype\File
	 */
	function copy($toFile, $mode = 0755, $replace = true)
	{
		if ( !Filesystem::limitDir($this->fullpath) ) {
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
		
		if ( !Filesystem::limitDir($toFile) ) {
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array(
				'path' => $toFile
			));
		}
		
		if ( $replace == false ) {
			if ( is_file($toFile) ) {
				throw new Exception('%path%_IS_EXISTING', Error::ERROR, array(
					'path' => $toFile
				));
			}
		}
		
		if ( is_dir($toFile) ) {
			throw new Exception('%path%_IS_EXISTING_OR_BAD_TYPE', Error::ERROR, array(
				'path' => $toFile
			));
		}
		
		if ( $this->_file->isFile() ) {
			if ( copy($this->fullpath, $toFile) ) {
				touch($toFile, $this->mtime);
				chmod($toFile, $mode);
			}
			else {
				throw new Exception('COPY_ERROR_FROM_%from%_TO_%to%', Error::ERROR, array(
					'from' => $this->fullpath,
					'to' => $toFile
				));
			}
		}
		else {
			throw new Exception('BAD_TYPE_DATA_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
		
		$fileCopy = clone ($this);
		$fileCopy->_file = new \SplFileInfo($toFile);
		$fileCopy->_init();
		return $fileCopy;
	}

	/**
	 * Suppress the current file
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	function suppress()
	{
		if ( !self::putInTrash($this->fullpath) ) {
			throw new Exception('SUPPRESSION_ERROR_ON_DATA_%path%', Error::ERROR, array(
				'path' => $this->fullpath
			));
		}
	}

	/** 
	 * Put file in the trash dir
	 *
	 * @param boolean	$verbose
	 * @param string	$trashDir
	 * @throws \Rbplm\Sys\Exception
	 */
	function putInTrash($verbose = false, $trashDir = '')
	{
		$dstfile = Trash::getTrashedFilePath($this->_file);
		
		//Copy File to trash
		if ( !$this->copy($dstfile) ) { //copy File to dirtrash
			throw new Exception('ERROR_DURING_COPY_OF_%from%_TO_%to%', Error::ERROR, array(
				'from' => $this->fullpath,
				'to' => $trashDir
			));
		}
		else { //Suppress original File
			chmod($dstfile, 0755);
			if ( !unlink($this->fullpath) ) {
				throw new Exception('ERROR_DURING_SUPPRESS_OF_%file%', Error::ERROR, array(
					'file' => $this->fullpath
				));
			}
		}
		$this->log('File %file% has been put in trash', array(
			'file' => $this->fullpath
		));
		return true;
	}

	/**
	 * Download current data.
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	function download()
	{
		$fileName = $this->name;
		$mimeType = $this->mimetype;
		if ( $mimeType == 'no_read' ) {
			return false;
		}
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . ($this->size));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($this->fullpath);
		return true; //no die, put it in rb_fsdata::downloadFile()
	}

	/**
	 * Return the infos record in the conf/mimes.csv file about the extension of the $filename.
	 *
	 * @return array|false
	 */
	private function _getMimeTypeInfos()
	{
		$mimefile = self::$_mimes_config_file;
		
		$handle = fopen($mimefile, "r");
		if ( !$handle ) {
			throw new Exception('UNABLE_TO_OPEN_%path%', Error::Error, array(
				'path' => $mimefile
			));
		}
		
		while( ($data = fgetcsv($handle, 1000, ";")) !== FALSE ) {
			if ( strstr($data[2], $this->extension) ) {
				$this->mimetype = $data[0];
				$this->_mimetype_description = $data[1];
				$this->_no_read = $data[3];
				$this->_viewer_driver = $data[4]; //Experimental
				//$this->_visu2D_extension = $data[5]; //Experimental
				//$this->_visu3D_extension = $data[6]; //Experimental
				fclose($handle);
				return $data;
			}
		}
		fclose($handle);
		return false;
	}

	/**
	 * Static method version of getExtension()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetExtension($fileName)
	{
		return substr($fileName, strrpos($fileName, '.'));
	}

	/**
	 * Static method version of getRoot()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetRoot($fileName)
	{
		return substr($fileName, 0, strrpos($fileName, '.'));
	}

	/**
	 * Copy a upload File to $dstfile
	 *
	 * @param array $file
	 *		$file[name](string) name of the uploaded File
	 *		$file[type](string) mime type of the File
	 *		$file[tmp_name](string) temp name of the File on the server
	 *		$file[error](integer) error code if error occured during transfert(0=no error)
	 *		$file[size](integer) size of the File in octets
	 *
	 * @param boolean $replace 	if true and if the File exist in the wildspace, it will be replaced by the uploaded File.
	 * @param string $dstdir 	Path of the dir to put the File.
	 * @return boolean
	 */
	public static function upload($file, $replace, $dstdir)
	{
		$dstfile = $dstdir . '/' . $file['name'];
		if ( !$replace ) { //Test if File exist in target dir only if replace=false
			if ( is_file($dstfile) ) {
				throw new Exception('%path%_IS_NOT_EXISTING', Error::ERROR, array(
					'path' => $dstfile
				));
			}
		}
		if ( !is_file($file['tmp_name']) ) {
			throw new Exception('%path%_IS_NOT_FILE', Error::ERROR, array(
				'path' => $file['tmp_name']
			));
		}
		if ( copy($file['tmp_name'], $dstfile) ) {
			chmod($dstfile, 0775);
			return true;
		}
		else {
			throw new Exception('ERROR_DURING_COPY_OF_%from%_TO_%to%', Error::ERROR, array(
				'from' => $file['tmp_name'],
				'to' => $dstfile
			));
		}
	}
} //End of class

