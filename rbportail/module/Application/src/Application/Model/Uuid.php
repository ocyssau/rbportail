<?php
//%LICENCE_HEADER%
namespace Application\Model;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 * 
 * Note you need php-uuid installed.
 * @code
 * //On ubuntu, do simply
 * sudo apt-get install php5-uuid
 * @endcode
 *
 * @see http://fr.wikipedia.org/wiki/Universal_Unique_Identifier
 * @author Marius Karthaus
 * @author Eric COSNARD
 * @author Olivier CYSSAU
 *
 */
class Uuid extends \Rbplm\Uuid
{
}
