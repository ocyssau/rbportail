<?php
namespace Application\Model;

use Application\Model;

class Application
{
	/**
	 * @var integer
	 */
	public static $classId = 1;
	public static $id = 'application';
	public static $version = '0.1';
	public static $build = '07mai2015';
	
	public static function getCopyright()
	{
		$copyright = '&copy; 2015 - '.date('Y').' By Olivier CYSSAU pour SIER - All rights reserved.';
		return $copyright;
	}
	
	public static function getVersion()
	{
		return self::$version;
	}
	
	public static function getId()
	{
		return self::$id;
	}
}
