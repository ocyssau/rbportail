<?php
//%LICENCE_HEADER%
namespace Application\Model;

use Rbplm\Dao\MappedInterface;
use Application\Model\People;
use DateTime;

class Any extends \Rbplm\Any implements MappedInterface, \Serializable, \JsonSerializable
{
	use \Rbplm\Item, \Rbplm\LifeControl, \Rbplm\Owned, \Rbplm\Mapped;

	/**
	 * @param Any $parent
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}
		$this->cid = static::$classId;
	}

	/**
	 * @param string $name
	 * @return Any
	 */
	public static function init($name = "", $parent = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();

		if ( !$name ) {
			$name = uniqid();
		}

		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());

		if ( $parent ) {
			$obj->setParent($parent);
		}

		return $obj;
	}

	/**
	 * Redefine with uniqid
	 * @param string $name
	 * @return Any
	 */
	public function newUid()
	{
		$this->id = null;
		$this->uid = uniqid();
		$this->saved = false;
		return $this;
	}

	/**
	 * Redefine with uniqid
	 * Set the uid
	 *
	 * @param string
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		return $this;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['ownerId'])) ? $this->ownerUid = $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['ownerUid'])) ? $this->ownerUid = $properties['ownerUid'] : null;
		(isset($properties['updateById'])) ? $this->updateById = $properties['updateById'] : null;
		(isset($properties['updated'])) ? $this->setUpdated(new DateTime($properties['updated'])) : null;
		return $this;
	}

	/**
	 * method of Serializable interface
	 * Used By ZendForm to exchange data with form. So the objects must be return as Object and not be serialized as Uid as is doing in \Rplm\Any
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		$return = array();
		foreach( $this as $name => $value ) {
			if ( $name == '_plugins' || $name == 'children' || $name == 'links' || $name == 'dao' || $name == 'factory' ) {
				continue;
			}
			elseif ( is_scalar($value) ) {
				$return[$name] = $value;
			}
			elseif ( is_array($value) ) {
				$return[$name] = array();
				foreach( $value as $subvalue ) {
					if ( $subvalue instanceof Any ) {
						$return[$name][] = $subvalue;
					}
					elseif ( $subvalue instanceof DateTime ) {
						$return[$name][] = $value->format('d-m-Y');
					}
					elseif ( is_scalar($subvalue) ) {
						$return[$name][] = $subvalue;
					}
				}
			}
			elseif ( $value instanceof DateTime ) {
				$return[$name] = $value->format('d-m-Y');
			}
			elseif ( $value instanceof Any ) {
				$return[$name] = $value;
			}
			elseif ( $value === null ) {
				$return[$name] = null;
			}
		}
		return $return;
	}

	/**
	 * Method of Serializable interface.
	 * 
	 * @return string
	 */
	public function serialize()
	{
		$return = array();
		foreach( $this as $name => $value ) {
			if ( $name == '_plugins' || $name == 'dao' || $name == 'factory' ) {
				continue;
			}
			else {
				$return[$name] = $value;
			}
		}
		return serialize($return);
	}

	/**
	 * Method of JsonSerializable interface.
	 * Return array of all properties to include in Serialized datas.
	 * 
	 * @return array
	 */
	public function jsonSerialize()
	{
		return $this->getArrayCopy();
	}

	/**
	 * Method of Serializable interface.
	 * @return void
	 */
	public function unserialize($data)
	{
		foreach( $data as $name => $value ) {
			$this->$name = $value;
		}
	}
}

