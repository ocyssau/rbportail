<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Form\Exception;

/**
 * 
 *
 */
class ProgressBar extends AbstractHelper
{

	protected $validTagAttributes = array(
		'name'           => true,
		'max'            => true,
		'min'            => true,
		'step'           => true,
		'type'           => true,
		'value'          => true,
		'width'          => true,
		'aria-valuenow'  => true,
		'aria-valuemin'  => true,
		'aria-valuemax'  => true,
		'aria-valuemax'  => true,

	);

    /**
     * Invoke helper as functor
     *
     * Proxies to {@link render()}.
     *
     * @param  \stdClass $element
     * @return string
     */
    public function __invoke($element = null)
    {
        if (!$element) {
            return $this;
        }

        return $this->render($element);
    }

    /**
     * Render a form <input> element from the provided $element
     *
     * @param  \stdClass $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(\Zend\Form\Element $element)
    {
    	$name = $element->getName();
    	if ($name === null || $name === '') {
    		throw new Exception\DomainException(sprintf(
    			'%s requires that the element has an assigned name; none discovered',
    			__METHOD__
    			));
    	}

    	$value = $element->getValue();
    	$attributes          = $element->getAttributes();
    	$attributes['name']  = $name;
    	$attributes['style']  = 'width:'.$value.'%;';

    	(!isset($attributes['aria-valuenow'])) ? $attributes['aria-valuenow']=$value:null;
    	(!isset($attributes['aria-valuemin'])) ? $attributes['aria-valuemin']=0:null;
    	(!isset($attributes['aria-valuemax'])) ? $attributes['aria-valuemax']=100:null;

    	return sprintf('<div %s', $this->createAttributesString($attributes)).'>'.$value.'%</div>';
    }
}
