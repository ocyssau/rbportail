<?php
namespace Application\Form\Extended;

/**
 * 
 *
 */
class SelectForm extends PropertyForm
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = 'propertyFieldset')
	{
		parent::__construct($name);
		//$this->template = 'application/extendedproperties/selectform.phtml';
		
		$this->additionalsElements = array(
			'size',
			'list',
			'multiple'
		);
		
		$this->add(array(
			'name' => 'size',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'number',
				'step' => '1',
				'min' => '5',
				'placeholder' => 'Size'
			),
			'options' => array(
				'label' => 'Size'
			)
		));
		
		$this->add(array(
			'name' => 'list',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Items',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Items'
			)
		));
		
		$this->add(array(
			'name' => 'multiple',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'type' => 'checkbox',
				'placeholder' => 'Multiple',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Multiple'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'options' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			)
		);
	}
}
