<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 *
 *
 */
class PaginatorForm extends Form //implements InputFilterAwareInterface
{
	public $maxLimit = 1000;
	public $limit = 500;
	public $page = 1;
	public $maxPage = 2;
	public $offset = 0;
	public $order = null;
	public $orderby = 'asc';

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('paginator');
		$this->setAttribute('id', 'paginator');
		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'form-inline');
		$this->nameSpace = $name;

		$inputFilter = new InputFilter();
		$this->setInputFilter($inputFilter);

		$this->add(array(
			'name' => 'orderby',
			'type'  => 'Zend\Form\Element\Hidden',
		));
		$inputFilter->add(array(
			'name' => 'orderby',
			'required' => false,
		));

		$this->add(array(
			'name' => 'order',
			'type'  => 'Zend\Form\Element\Hidden',
		));
		$inputFilter->add(array(
			'name' => 'order',
			'required' => false,
		));

		$this->add(array(
			'name' => 'paginator-limit',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Result per page',
			),
			'attributes' => array(
				'onChange'=>'$(\'#paginator\').submit();',
				'options'=>array(
					10=>10,
					20=>20,
					50=>50,
					100=>100,
					500=>500,
					1000=>1000
				),
				'value' => 50,
				'class'=>'form-control form-control-paginator',
			)
		));
		$inputFilter->add(array(
			'name' => 'paginator-limit',
			'required' => false,
		));


		$this->add(array(
			'name' => 'paginator-page',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Page',
			),
			'attributes' => array(
				'id'=>'paginator-page',
				'onChange'=>"$('#paginator').submit();",
				'options'=>array(
					1=>1,
				),
				'value' => 1,
				'class'=>'form-control form-control-paginator',
			)
		));
		$inputFilter->add(array(
			'name' => 'paginator-page',
			'required' => false,
		));


		$this->add(array(
			'name' => 'paginator-next',
			'type'  => 'Zend\Form\Element\Button',
			'options' => array(
				'label' => '>',
				'value' => 'next',
			),
			'attributes' => array(
				'onClick'=>"var page=$('#paginator-page').val();page++;$('#paginator-page').val(page);$('#paginator').submit();",
				'class'=>'btn btn-default btn-paginator-next',
			)
		));

		$this->add(array(
			'name' => 'paginator-prev',
			'type'  => 'Zend\Form\Element\Button',
			'options' => array(
				'label' => '<',
				'value' => 'prev',
			),
			'attributes' => array(
				'onClick'=>"var page=$('#paginator-page').val();page--;$('#paginator-page').val(page);$('#paginator').submit();",
				'class'=>'btn btn-default btn-paginator-prev',
			)
		));
	}

	/**
	 * @param integer $maxLimit
	 */
	public function setMaxLimit($maxLimit)
	{
		$this->maxLimit = $maxLimit;
	}

	/**
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare()
	{
		parent::prepare();
		$pPageElmt = $this->get('paginator-page');

		$this->page = $pPageElmt->getValue();
		$this->limit = $this->get('paginator-limit')->getValue();
		if(!$this->limit){
			$this->limit=50;
		}
		$this->maxPage = ceil($this->maxLimit / $this->limit);

		$options = array(1=>1);
		for($i=2;$i<=$this->maxPage;$i++){
			$options[$i]=$i;
		}
		$pPageElmt->setValueOptions($options);

		$this->offset = ($this->page-1)*$this->limit;
		$this->order = $this->get('order')->getValue();
		$this->orderby = $this->get('orderby')->getValue();
		return $this;
	}

	/**
	 * @param $view
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= $view->formRow($this->get('orderby'));
		$html .= $view->formRow($this->get('order'));
		$html .= '<div class="form-group">' . $view->formRow($this->get('paginator-limit')) . '</div>';
		$html .= $view->formRow($this->get('paginator-prev')) ;
		$html .= '<div class="form-group">' . $view->formRow($this->get('paginator-page')) . '</div>';
		$html .= $view->formRow($this->get('paginator-next'));
		$html .= $view->form()->closeTag();
		return $html;
	}

	/**
	 * Set view variables from paginator datas
	 *
	 * @param $view
	 */
	public function bindToView($view)
	{
		$this->isValid();
		$view->paginator = $this;
		$view->orderby = $this->orderby;
		$view->order = $this->order;
		return $this;
	}

	/**
	 *
	 */
	public function bindToFilter(\Rbplm\Dao\FilterInterface $filter)
	{
		if(!$this->isPrepared){
			$this->prepare();
		}

		if($this->isValid()){
			$filter->page($this->page, $this->limit);
			$filter->sort($this->orderby, $this->order);
			$this->bindFilter = $filter;
		}
		return $this;
	}


	/**
	 *
	 */
	public function toSql()
	{
		$sql = "";
		$offset = ($this->page-1) * $this->limit;
		if($offset < 0){
			$offset = 0;
		}
		if($this->orderby){
			$sql .= " ORDER BY $this->orderby $this->order";
		}
		$sql .= " LIMIT $this->limit OFFSET $offset";
		return $sql;
	}

	/**
	 *
	 */
	public function save()
	{
		$_SESSION['paginator'][$this->nameSpace] = $this->getData();
		return $this;
	}

	/**
	 * @param \Zend\Http\Request
	 */
	public function load($request)
	{
		if($request->getPost()->resetf OR $request->getQuery()->resetf){
			$this->reset();
			return $this;
		}

		(isset($_SESSION['paginator'][$this->nameSpace])) ? $data1 = $_SESSION['paginator'][$this->nameSpace] : $data1 = array();
		$data2 = array_merge($request->getPost()->getArrayCopy(),$request->getQuery()->getArrayCopy());
		$data = array_merge($data1, $data2);

		$this->setData($data);
		return $this;
	}

	/**
	 */
	public function reset()
	{
		$_SESSION['paginator'][$this->nameSpace] = null;
		return $this;
	}


}
