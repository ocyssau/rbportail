<?php
namespace Application\Form;

use Zend\Form\Form;

class StdFilterForm extends Form //implements InputFilterAwareInterface
{
	public $where;
	public $passThrough = true;
	public $key = 'name';
	public $bind = array();

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('stdfilter');
		$this->setAttribute('id', 'stdfilter');
		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'form-inline');

		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type'  => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => '',
			),
			'attributes' => array(
				'onChange'=>'',
				'class'=>'form-control',
			)
		));

		$this->add(array(
			'type'  => 'Zend\Form\Element\Submit',
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Filter',
				'id' => 'stdfilter-submit',
				'class'=>'btn btn-default'
			),

		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare(){
		parent::prepare();
		$search = $this->get('stdfilter-searchInput')->getValue();
		if($search){
			$this->where = $this->key . " LIKE :search";
			if($this->passThrough){
				$search = str_replace('*','%',$search);
				$this->bind[':search'] = '%'.$search.'%';
			}
			else{
				$search = str_replace('*','%',$search);
				$this->bind[':search'] = $search;
			}
		}
	}

	/**
	 *
	 * @param \Zend\View\View $view
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-searchInput')) . '</div>';
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-submit')) . '</div>';
		$html .= $view->form()->closeTag();
		return $html;
	}
}

