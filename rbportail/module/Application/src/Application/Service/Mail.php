<?php
namespace Application\Service;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Exception;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Message;
use Zend\Mime\Mime;


/**
 */
class Mail
{
	
	/**
	 * Default mail adress to used.
	 * @var string
	 */
	public $adminMail;

	/**
	 * Instance of singleton mail service
	 *
	 * @var Mail
	 */
	protected static $instance;

	/**
	 *
	 * @var \Zend\Mail\Transport\Smtp
	 */
	protected $smtpTransport;

	/**
	 */
	public function __construct($config)
	{
		if ( $config['enable'] == true ) {
			unset($config['enable']);
			$this->smtpTransport = new SmtpTransport(new SmtpOptions($config));
		}
		self::$instance = $this;
	}

	/**
	 * Singleton getter with factory
	 * 
	 * @return Mail
	 */
	public static function get()
	{
		if ( !self::$instance ) {
			$instance = new self();
			self::$instance = $instance;
		}
		return self::$instance;
	}
	
	/**
	 * Factory method for message
	 *
	 * @return Message
	 */
	public static function messageFactory()
	{
		$message = new Message();
		$message->setEncoding('utf-8');
		return $message;
	}
	
	/**
	 * Factory method for mime part
	 * @param string $text
	 * @return MimePart
	 *
	 */
	public static function htmlPartFactory($html)
	{
		$part = new MimePart($html);
		$part->type = Mime::TYPE_HTML;
		$part->charset = 'utf-8';
		return $part;
	}
	
	/**
	 * Factory method for mime part
	 * $type = text or file
	 *
	 * @param string $file
	 * @return MimePart
	 *
	 */
	public static function filePartFactory($file)
	{
		$part = new MimePart(fopen($file, 'r'));
		$part->type = Mime::TYPE_OCTETSTREAM;
		$part->filename = basename($file);
		$part->disposition = Mime::DISPOSITION_ATTACHMENT;
		$part->encoding    = Mime::ENCODING_BASE64;
		return $part;
	}
	
	/**
	 * 
	 * @return \Zend\Mime\Message
	 */
	public static function bodyFactory()
	{
		$body = new MimeMessage();
		return $body;
	}

	/**
	 *
	 */
	public function send($message)
	{
		try {
			if ( $this->smtpTransport ) {
				$this->smtpTransport->send($message);
			}
		}
		catch( \Exception $e ) {
			throw new Exception('Send of mail failed with reason : ' . $e->getMessage(), 6000, $e);
		}
	}

	/**
	 *
	 * @return SmtpTransport
	 */
	public function getSmtpTransport()
	{
		return $this->smtpTransport;
	}

	/**
	 *
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}
}
