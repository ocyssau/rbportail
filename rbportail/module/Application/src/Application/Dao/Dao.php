<?php
//%LICENCE_HEADER%
namespace Application\Dao;

use Rbplm\Dao\Mysql;
use Rbplm\Signal as Signal;
use Application\Dao\Exception as Exception;
use DateTime;
use Rbplm\Sys\Date;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 */
abstract class Dao extends Mysql
{

	/**
	 * Define filter to use to convert property value from db to model and from model to db.
	 * Simply, this map define name of property in db semantic and associate a filter
	 * The filter must be the prefix of methodes as named prefixToApp and prefixToSys.
	 *
	 * Example :
	 * 	array('sysname'=>'json');
	 * and the current class define methods jsonToApp and jsonToSys
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'attributes' => 'json'
	);

	/**
	 * Plugins for extends model
	 * @var \Application\Dao\Extended\DaoPlugin
	 */
	public $extended;
	
	/**
	 * 
	 * @var \PDOStatement
	 */
	public $selectToDeleteFromFilterStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	public $deleteFromFilterStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	public $deleteStmt;
	
	/**
	 *
	 * @var \PDOStatement
	 */
	public $selectToDeleteStmt;
	

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
		
		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_ltable = static::$ltable;
		$this->_lvtable = static::$lvtable;
		
		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;
		
		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return void
	 * @throws Exception
	 *
	 */
	protected function _update($mapped, $select = null)
	{
		$sysToApp = $this->metaModel;
		$bind = $this->bind($mapped);
		if ( !$this->updateStmt ) {
			$set = [];
			foreach( $sysToApp as $asSys => $asApp ) {
				$set[] = '`' . $asSys . '`' . '=:' . $asApp;
			}
			$sql = 'UPDATE ' . static::$table . ' SET ' . implode(',', $set) . ' WHERE `id`=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}
		$this->updateStmt->execute($bind);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return void
	 * @throws Exception
	 */
	protected function _insert($mapped, $select = null)
	{
		$sysToApp = $this->metaModel;
		$bind = $this->bind($mapped);
		$table = static::$table;
		unset($sysToApp['id']);
		unset($bind[':id']);
		
		//Init statement
		if ( !$this->insertStmt ) {
			$sysSet = [];
			$appSet = [];
			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = '`' . $asSys . '`';
				$appSet[] = ':' . $asApp;
			}
			
			if ( $this->_sequenceName ) {
				$sql = 'UPDATE ' . $this->_sequenceName . ' SET ' . $this->_sequenceKey . ' = LAST_INSERT_ID(' . $this->_sequenceKey . ' + 1) LIMIT 1;';
				$this->seqStmt = $this->connexion->prepare($sql);
			}
			
			$sql = 'INSERT INTO ' . $table . ' (' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}
		
		if ( $this->_sequenceName ) {
			$this->seqStmt->execute();
			$mapped->setId($this->connexion->lastInsertId($this->_sequenceName));
			$this->insertStmt->execute($bind);
			$this->connexion->exec('ALTER TABLE ' . $this->_sequenceName . ' AUTO_INCREMENT = ' . $mapped->getId());
		}
		else {
			$this->insertStmt->execute($bind);
			$id = $this->connexion->lastInsertId(static::$table);
			$mapped->setId($id);
		}
	}

	/**
	 * @return \Rbplm\Dao\MetamodelTranslator
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 * Recursive function
	 * @return Dao
	 */
	public function deleteFromId($id, $withChildren = true, $withTrans = null)
	{
		$filter = 'id=:id';
		$bind = array(
			':id' => $id
		);
		return $this->deleteFromFilter($filter, $bind, $withChildren, $withTrans);
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.DaoInterface::delete()
	 *
	 * Suppress current record in database
	 *
	 * @param string  $uid
	 * @param boolean $withChildren	If true, suppress all childs and childs of childs
	 * @param boolean $withTrans	If true or false, force the transaction set to off or on.
	 * 								Use for control transactions with children recursive function
	 * @throws Exception
	 * @return Dao
	 *
	 */
	public function delete($uid, $withChildren = true, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		
		if ( $withTrans ) $this->connexion->beginTransaction();
		if ( !$this->deleteStmt ) {
			$sql = 'DELETE FROM ' . static::$table . ' WHERE `uid`=:uid';
			$this->deleteStmt = $this->connexion->prepare($sql);
			
			$sql = 'SELECT `uid` FROM ' . static::$table . ' WHERE `parentUid`=:uid';
			$this->selectToDeleteStmt = $this->connexion->prepare($sql);
		}
		
		try {
			Signal::trigger(self::SIGNAL_PRE_DELETE, $this);
			
			if ( $withChildren ) {
				$bind = array(
					':uid' => $uid
				);
				
				$this->selectToDeleteStmt->execute($bind);
				$result = $this->selectToDeleteStmt->fetchAll(\PDO::FETCH_COLUMN);
				
				foreach( $result as $childUid ) {
					$this->delete($childUid, true, false);
				}
				
				$this->deleteStmt->execute($bind);
			}
			else {
				throw new Exception('the mode withchilds=false is not implemented');
			}
			
			Signal::trigger(self::SIGNAL_POST_DELETE, $this);
			
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}

	/**
	 * @throws Exception
	 * @return Dao
	 */
	public function setFromId($id, $propertyName, $propertyValue, $withTrans=null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		$table = static::$table;
		$sql = "UPDATE $table SET $propertyName=:value WHERE id=:id;";
		$stmt = $this->connexion->prepare($sql);
		
		$bind = array(
			':id' => $id,
			':value' => $propertyValue
		);
		
		try {
			$stmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 *
	 * @param string $filter
	 * @param array $bind
	 * @param boolean $withChildren
	 * @param boolean $withTrans
	 * @throws Exception
	 * @return \Application\Dao\Dao
	 */
	public function deleteFromFilter($filter, $bind, $withChildren = true, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		if ( !$this->deleteStmt || $this->deleteFilter != $filter ) {
			$table = static::$table;
			
			$sql = 'DELETE FROM ' . $table . ' WHERE ' . $filter;
			$this->deleteFromFilterStmt = $this->connexion->prepare($sql);
			
			$sql = "SELECT uid FROM $table WHERE parentUid=(SELECT uid FROM $table WHERE $filter)";
			$this->selectToDeleteFromFilterStmt = $this->connexion->prepare($sql);
			
			$this->deleteFilter == $filter;
		}
		
		try {
			Signal::trigger(self::SIGNAL_PRE_DELETE, $this);
			
			if ( $withChildren ) {
				$this->selectToDeleteFromFilterStmt->execute($bind);
				$result = $this->selectToDeleteFromFilterStmt->fetchAll(\PDO::FETCH_COLUMN);
				
				foreach( $result as $childUid ) {
					$this->delete($childUid, true, false);
				}
				
				$this->deleteFromFilterStmt->execute($bind);
			}
			else {
				$this->deleteFromFilterStmt->execute($bind);
			}
			Signal::trigger(self::SIGNAL_POST_DELETE, $this);
			
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}

	/**
	 * get properties from mapped object to put in db.
	 * Return a array with properties of mapped object to save in db.
	 *
	 * @param \Rbplm\Any $mapped
	 * @return Dao
	 */
	abstract public function bind($mapped);

	/**
	 * @param integer $id
	 * @param array $andFilter
	 *
	 * @todo A TESTER
	 *
	 */
	public function getListFromParentId($id, $andFilter = null)
	{
		$table = static::$table;
		
		$sysName = array_search('parentId', $this->metaModel);
		$filter = $sysName . '=' . $id;
		
		if ( $andFilter ) {
			foreach( $this->metaModel as $asSys => $asApp ) {
				$andFilter = str_replace($asApp, $asSys, $filter);
			}
			$filter = $filter . ' AND ' . $andFilter;
		}
		
		$sql = "SELECT * FROM $table WHERE $filter";
		$list = $this->newList();
		$list->loadFromSql($sql);
		return $list;
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function dateToApp($date)
	{
		if ( !$date or substr($date, 0, 4) == '0000' ) {
			return null;
		}
		elseif ( $date instanceof DateTime ) {
			return $date;
		}
		else {
			return new Date((int)$date);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function dateToSys($in)
	{
		if ( $in instanceof DateTime ) {
			return $in->format(self::DATE_FORMAT);
		}
		elseif ( is_null($in) ) {
			return null;
		}
		elseif ( is_string($in) ) {
			if ( substr($in, 0, 10) == '0000-00-00' ) {
				return null;
			}
			else {
				return $in;
			}
		}
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToApp($json)
	{
		return json_decode($json, true);
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToSys($array)
	{
		return json_encode($array, true);
	}
}
