<?php
//%LICENCE_HEADER%
namespace Application\Dao;

use Rbplm\Dao\DaoListAbstract;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error as Error;

/** SQL_VIEW>>
 <<*/

/**
 * 
 */
class DaoList extends DaoListAbstract
{
	
	/**
	 * @see Rbplm\Dao.ListInterface::toApp()
	 *
	 * Translate current row from system notation to application notation.
	 *
	 * Class variable $this->dao may be set the dao object.
	 * If $dao is a object, assume that is a type with a method toApp().
	 * If $dao is null, try to use the cid property from record to get the dao
	 * In this last case, require an attribut cid in current statement.
	 *
	 * @param string $daoClassOrObjectOrCid	[OPTIONAL]	Set a Dao class or any other class with a method toApp(), to use for convert attributs.
	 * @return array
	 */
	public function toApp($daoClassOrObjectOrCid = null)
	{
		$current = $this->_current;
		
		if ( is_string($daoClassOrObjectOrCid) || is_object($daoClassOrObjectOrCid) ) {
			$dao = $daoClassOrObjectOrCid;
		}
		elseif ( is_integer($daoClassOrObjectOrCid) ) {
			$classId = $daoClassOrObjectOrCid;
			$dao = $this->factory->getDaoClass($classId);
		}
		elseif ( isset($this->options['dao']) ) {
			$dao = $this->options['dao'];
		}
		elseif ( isset($current['cid']) ) {
			$classId = $current['cid'];
			$dao = $this->factory->getDaoClass($classId);
		}
		else {
			throw new Exception('cid_IS_NOT_SET', Error::WARNING, $current);
		}
		
		return call_user_func(array(
			$dao,
			'toApp'
		), $current);
	}
}
