<?php
namespace Application\Dao;

use Application\Dao\Exception as Exception;
use Application\Dao\Error as Error;

/** MODEL OF TABLE
 CREATE TABLE [tablename](
 `uid` VARCHAR(32) NOT NULL,
 `parentId` INTEGER NOT NULL,
 `childId` INTEGER NOT NULL,
 `parentUid` VARCHAR(255) NOT NULL,
 `childUid` VARCHAR(255) NOT NULL,
 `name` VARCHAR(255) NULL,
 `lindex` INTEGER DEFAULT 0,
 `attributes` TEXT NULL,
 PRIMARY KEY (`parentId`, `childId`)
 );

 ALTER TABLE [tablename] ADD UNIQUE (uid);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_name` (`name` ASC);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_parentId` (`parentId` ASC);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_childId` (`childId` ASC);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_parentUid` (`parentUid` ASC);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_childUid` (`childUid` ASC);
 ALTER TABLE `[tablename]` ADD INDEX `[tablename]_lindex` (`lindex` ASC);
*/

class Link
{
	/**
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var string
	 */
	public $_table;

	/**
	 *
	 * @var string
	 */
	public $_vtable;


	/**
	 *
	 * @var MetamodelTranslator
	 */
	protected $translator;

	/**
	 * @var array
	 */
	protected $metaModel;

	/**
	 * @var array
	 */
	protected $metaModelFilters;

	/**
	 * @var \PDOStatement
	 */
	protected $insertStmt;

	/**
	 * @var \PDOStatement
	 */
	protected $updateStmt;

	/**
	 * @var \PDOStatement
	 */
	protected $deleteStmt;

	
	/**
	 * @var \PDOStatement
	 */
	protected $loadparentStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $loadChildrenStmt;

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'anyobject_links';
	public static $vtable = '';
	public static $childTable = null;
	public static $childForeignKey = 'id';
	public static $parentTable = null;
	public static $parentForeignKey = 'id';
	public static $sequenceName = '';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'uid'=>'uid',
		'parentId'=>'parentId',
		'childId'=>'childId',
		'parentUid'=>'parentUid',
		'childUid'=>'childUid',
		'name'=>'name',
		'index'=>'index',
		'attributes'=>'attributes',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
			'attributes'=>'json',
	);

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct( $conn = null )
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;
	}

	/**
	 */
	public function bind($mapped)
	{
		return array(
			':uid'=>$mapped->uid,
			':parentId'=>(int)$mapped->parentId,
			':childId'=>(int)$mapped->childId,
			':parentUid'=>$mapped->parentUid,
			':childUid'=>$mapped->childUid,
			':name'=>$mapped->name,
			':index'=>(int)$mapped->index,
			':attributes'=>\json_encode($mapped->attributes)
		);
	}

	/**
	 *
	 * @return \PDO
	 */
	public function getConnexion()
	{
	    if( !$this->connexion ){
	        throw new Exception('CONNEXION_IS_NOT_SET', Error::ERROR);
	    }
	    return $this->connexion;
	}

	/**
	 * @return MetamodelTranslator
	 */
	public function getTranslator()
	{
		if(!isset($this->translator)){
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 * @todo Escape sql injection on select
	 * $mapped \Rbh\Link or array of \Rbh\Link
	 */
	public function insert($mapped, array $select = null)
	{
		$table = static::$table;

		if(!$this->insertStmt){
			$sql = 'INSERT INTO '.$table.' (uid, parentId, childId, parentUid, childUid, name, lindex, attributes)';
			$sql .= ' VALUES (:uid, :parentId, :childId, :parentUid, :childUid, :name, :index, :attributes)';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$this->insertStmt->execute($this->bind($map));
				}
			}
			else{
				$this->insertStmt->execute($this->bind($mapped));
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * @todo Escape sql injection on select
	 *
	 * @param \Rbplm\Any $mapped
	 * @param array $select
	 * @throws Exception
	 * @return Link
	 */
	public function update($mapped, array $select = null )
	{
		$table = static::$table;

		if(!$this->updateStmt){
			$sql = 'UPDATE '.$table.' SET
			parentId=:parentId,
			childId=:childId,
			parentUid=:parentUid,
			childUid=:childUid,
			name=:name,
			lindex=:lindex,
			attributes=:attributes
			WHERE uid=:uid;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$this->updateStmt->execute($map->bind());
				}
			}
			else{
				$this->updateStmt->execute($mapped->bind());
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * To do a partial update.
	 * Properties must be selected in array $properties as key=property name, value=property value.
	 *
	 * @todo Escape sql injection
	 *
	 * @param string $filter
	 * @param array $properties
	 * @return Link
	 */
	public function partialUpdate($filter, $properties)
	{
		$table = static::$table;

		$set='';
		foreach($properties as $pname=>$pvalue){
			$set .= "$pname=:$pname,";
			$bind[":$pname"] = $pvalue;
		}
		$set=trim($set, ',');

		$sql = 'UPDATE '.$table.' SET '.$set.' WHERE '.$filter;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $this;
	}


	/**
	 * Alias for delete
	 */
	public function suppress($filter)
	{
		return $this->delete($filter);
	}

	/**
	 * @todo Escape sql injection
	 */
	public function delete($filter)
	{
	    $table = static::$table;
	    $sql = 'DELETE FROM '.$table . " WHERE $filter";
	    $deleteStmt = $this->connexion->prepare($sql);
	    $deleteStmt->execute();
	    return $this;
	}

	/**
	 * Suppress all links without child or parent
	 */
	public function cleanOrpheanLinks()
	{
		throw new Exception('Not implemented method');

		$table = static::$table;
		$sql = "DELETE FROM $table as l WHERE l.childId NOT IN(SELECT id FROM anyobject) OR l.parentId NOT IN (SELECT id FROM anyobject)";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $this;
	}

	/**
	 * @return \PDOStatement
	 */
	public function getChildren($parentId)
	{
		$table = static::$table;
		$leftKey = 'childId';
		$joinTable = static::$childTable;
		$rightKey = static::$childForeignKey;

		if(!$this->loadChildrenStmt){
			$sql="SELECT lt.id AS linkId, lt.parentId, lt.parentUid, lt.childId, lt.childUid, jt.* FROM $table AS lt";
			if($joinTable && $rightKey){
				$sql.=" JOIN $joinTable AS jt ON lt.$leftKey=jt.$rightKey";
			}
			$sql.=" WHERE lt.parentId=:parentId";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}
		$this->loadChildrenStmt->execute(array(':parentId'=>$parentId));
		return $this->loadChildrenStmt;
	}

	/**
	 * @return \PDOStatement
	 */
	public function getParent($childId)
	{
	    $table = static::$table;
	    $leftKey = 'parentId';
	    $joinTable = static::$parentTable;
	    $rightKey = static::$parentForeignKey;

	    if(!$this->loadparentStmt){
	    	$sql="SELECT lt.uid AS linkId, lt.parentId, lt.parentUid, lt.childId, lt.childUid, jt.* FROM $table AS lt";
	    	if($joinTable && $rightKey){
	    	    $sql.=" JOIN $joinTable AS jt ON lt.$leftKey=jt.$rightKey";
	    	}
	    	$sql.=" WHERE lt.childId=:childId";

	        $stmt = $this->connexion->prepare($sql);
	        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
	        $this->loadparentStmt = $stmt;
	    }
	    $this->loadparentStmt->execute(array(':childId'=>$childId));
	    return $this->loadparentStmt;
	}


} //End of class
