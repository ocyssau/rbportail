<?php

// %LICENCE_HEADER%
namespace Application\Dao\People;

use Application\Dao\Dao;

/**
 * SQL_SCRIPT>>
 * CREATE TABLE user(
 * `id` int NOT NULL AUTO_INCREMENT,
 * `uid` VARCHAR(255) NOT NULL,
 * `cid` int NOT NULL,
 * `lastname` VARCHAR(255) NULL,
 * `firstname` VARCHAR(255) NULL,
 * `login` VARCHAR(255) NOT NULL,
 * `mail` VARCHAR(255) NOT NULL,
 * `lastlogin` date NULL,
 * `password` VARCHAR(255) NOT NULL,
 * `fullname` VARCHAR(255) NULL,
 * `identity` VARCHAR(255) NULL,
 * `memberof` VARCHAR(255) NOT NULL,
 * PRIMARY KEY (`id`)
 * );
 * <<
 */

/**
 * SQL_ALTER>>
 * ALTER TABLE user ADD UNIQUE (login);
 * ALTER TABLE user ADD UNIQUE (uid);
 *
 * ALTER TABLE `user` ADD INDEX `USER_UID` (`uid` ASC);
 * ALTER TABLE `user` ADD INDEX `USER_CLASSID` (`cid` ASC);
 * ALTER TABLE `user` ADD INDEX `USER_LOGIN` (`login` ASC);
 * ALTER TABLE `user` ADD INDEX `USER_IDENTITY` (`identity` ASC);
 * ALTER TABLE `user` ADD INDEX `USER_MEMBEROF` (`memberof` ASC);
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * DROP TABLE user;
 * <<
 */
class User extends Dao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'user';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'firstname' => 'firstname',
		'lastname' => 'lastname',
		'login' => 'login',
		'password' => 'password',
		'lastlogin' => 'lastlogin',
		'fullname' => 'fullname',
		'identity' => 'identity',
		'memberof' => 'memberof',
		'mail' => 'mail'
	);

	/**
	 * Constructor
	 * 
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	}

	/**
	 *
	 * @param \Rbplm\Any $mapped        	
	 * @return array
	 */
	public function bind($mapped)
	{
		$properties = $mapped->getArrayCopy();
		foreach( $this->metaModel as $asApp ) {
			$ret[':' . $asApp] = $properties[$asApp];
		}
		return $ret;
	}

	/**
	 * Recursive function
	 * 
	 * @return Dao
	 */
	public function deleteFromLogin($login, $withChilds = true, $withTrans = true)
	{
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		if ( !$this->deleteFromLoginStmt ) {
			$sql = 'DELETE FROM ' . static::$table . ' WHERE `login`=:login';
			$this->deleteFromLoginStmt = $this->connexion->prepare($sql);
		}
		$bind = array(
			':login' => $login
		);
		$this->deleteFromLoginStmt->execute($bind);
		
		if ( $withTrans ) $this->connexion->commit();
		
		return $this;
	}

	/**
	 *
	 * @param \Application\Model\People\User $mapped        	
	 * @return void
	 *
	 */
	public function updateLastLogin($mapped)
	{
		$sql = 'UPDATE ' . static::$table . ' SET lastlogin=:lastlogin WHERE `login`=:login;';
		$bind = array(
			':lastlogin' => $mapped->getLastLogin()->format(self::DATE_FORMAT),
			':login' => $mapped->getLogin()
		);
		$updateStmt = $this->connexion->prepare($sql);
		$updateStmt->execute($bind);
	}
}
