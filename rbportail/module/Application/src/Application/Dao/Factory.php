<?php
//%LICENCE_HEADER%

namespace Application\Dao;

use Rbplm\Dao\FactoryAbstract;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory extends FactoryAbstract
{
	const GENERIC = 1;
}
