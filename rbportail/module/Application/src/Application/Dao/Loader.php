<?php

namespace Application\Dao;

class Loader extends \Rbplm\Dao\Loader
{
	/**
	 * @param string $uid	Uuid of the object to load.
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function loadFromUid($uid, $classId)
	{
		if(!$uid || !$classId){
			throw new Exception( Error::BAD_PARAMETER_OR_EMPTY, Error::WARNING);
		}

		$options = $this->options;
		$useRegistry = $options['useregistry'];

		if($useRegistry){
			$object = Registry::singleton()->getFromUid($uid);
			if($object){
				return $object;
			}
		}

		//use the factory for get instance of dao from registry:
		$factory = $this->factory;

		$object = $factory::getModel($classId);
		$dao = $factory->getDao($classId);
		$dao->loadFromUid($object, $uid, $options);

		if($useRegistry){
			Registry::singleton()->add($object);
		}
		return $object;
	}

	/**
	 * @param string $uid	Uuid of the object to load.
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function loadFromId($id, $classId)
	{
		if(!$id || !$classId){
			throw new Exception( Error::BAD_PARAMETER_OR_EMPTY, Error::WARNING);
		}

		$options = $this->options;
		$useRegistry = $options['useregistry'];

		if($useRegistry){
			$object = Registry::singleton()->getFromId($id, $classId);
			if($object){
				return $object;
			}
		}

		/* use the factory for get instance of dao from registry: */
		$factory = $this->factory;
		$object = $factory->getModel($classId);
		$dao = $factory->getDao($classId);
		$dao->loadFromId($object, $id);

		if($useRegistry){
			Registry::singleton()->add($object);
		}
		return $object;
	}

    /**
     * Recursive function
     * Load from Uid
     */
    public static function loadChildrenFromUid(&$treeList, $parentUid)
    {
    	$factory = $this->factory;

        $list = $factory->getList(Factory::GENERIC);
        $filter = "parentUid='$parentUid'";
        $list->load($filter);

        foreach($list as $item){
            $treeList[] = $item;
            $uid = $item['uid'];
            self::loadChildrenFromUid($treeList, $uid);
        }
        return $treeList;
    }

    /**
     * Recursive function
     * Load from Uid
     */
    public function loadChildren($parent, $filter=null, $recursive=true)
    {
        $factory = $this->factory;

        $list = $factory->getList($parent::$classId);
        $parentUid = $parent->getUid();
        if($filter){
            $filter = "$filter AND parentUid='$parentUid' ORDER BY `id` ASC";
        }
        else{
            $filter = "parentUid='$parentUid' ORDER BY `id` ASC";
        }
        $list->load($filter);

        foreach($list as $item){
            $uid = $item['uid'];
            $child = $factory->getModel($item['cid']);
            $factory->getDao($item['cid'])->loadFromUid($child, $uid);
            $parent->addChild($child);
            if($recursive) self::loadChildren($child);
        }
        return $parent;
    }

}
