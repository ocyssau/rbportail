<?php 
namespace Application\Dao\Acl;

use Exception;
use Application\Dao\Connexion;
use Application\Model\Error;

/** SQL_SCRIPT>>

CREATE TABLE acl(
	`id` int NOT NULL AUTO_INCREMENT,
	`resourceId` VARCHAR(255) NOT NULL,
	`resourceName` VARCHAR(255) NOT NULL,
	`roleId` VARCHAR(255) NOT NULL,
	`roleName` VARCHAR(255) NOT NULL,
	`userId` VARCHAR(255) NOT NULL,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
ALTER TABLE acl ADD UNIQUE (userId, roleId, resourceId);
ALTER TABLE `acl` ADD INDEX `ACL_RESOURCE` (`resourceId` ASC);
ALTER TABLE `acl` ADD INDEX `ACL_RESOURCENAME` (`resourceName` ASC);
ALTER TABLE `acl` ADD INDEX `ACL_ROLEID` (`roleId` ASC);
ALTER TABLE `acl` ADD INDEX `ACL_ROLENAME` (`roleName` ASC);
ALTER TABLE `acl` ADD INDEX `ACL_USERID` (`userId` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class Acl
{
	/**
	 * @var string
	 */
	public static $table='acl';
	protected $_table;
	
	/**
	 * 
	 * @var \Application\Model\Acl\Acl;
	 */
	public $acl = null;

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
	    if( $conn ){
	        $this->connexion = $conn;
	    }
	    else{
	        $this->connexion = Connexion::get();
	    }
	    $this->_table = static::$table;
	}
	
	/**
	 * @param integer $userId
	 * @param integer $roleId
	 * @param integer $resourceId
	 * @param Acl $acl
	 */
	public function insert($userId,$roleId,$resourceId,$acl)
	{
		if(!$this->insertStmt){
			$table = static::$table;
			$sql = "INSERT INTO $table (userId, roleId, roleName, resourceId, resourceName) VALUES (:userId, :roleId, :roleName, :resourceId, :resourceName);";
			$this->insertStmt = $this->connexion->prepare($sql);
		}
		
		$roleMap = $acl->roles;
		$roleName = $roleMap[$roleId][0];
		$resourceName='';
		
		$bind = array(
			':userId'=>$userId,
			':roleId'=>$roleId,
			':roleName'=>$roleName,
			':resourceId'=>$resourceId,
			':resourceName'=>$resourceName
		);
		
		$this->connexion->beginTransaction();
		
		try{
			$this->insertStmt->execute($bind);
			$id = $this->connexion->lastInsertId(static::$table);
			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $id;
	}
	
	/**
	 * 
	 * @param integer $userId
	 * @param integer $roleId
	 * @param integer $resourceId
	 */
	public function delete($userId,$roleId,$resourceId)
	{
		if(!$this->deleteStmt){
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE `userId`=:userId AND `roleId`=:roleId AND `resourceId`=:resourceId;";
			$this->deleteStmt = $this->connexion->prepare($sql);
		}
		
		$bind = array(
			':userId'=>$userId,
			':roleId'=>$roleId,
			':resourceId'=>$resourceId
		);
		
		$this->connexion->beginTransaction();
		
		try{
			$this->deleteStmt->execute($bind);
			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param string $filter
	 * @param array $bind
	 */
	public function deleteAll($filter, $bind=null)
	{
		$table = static::$table;
		$sql = "DELETE FROM $table WHERE $filter";
		$stmt = $this->connexion->prepare($sql);
		
		$this->connexion->beginTransaction();
		
		try{
			$stmt->execute($bind);
			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param integer $resourceId
	 * @param string $filter
	 * @throws Exception
	 */
	public function getRoleFromResourceId($resourceId, $filter=null, $bind=null)
	{
		$table = static::$table;
		$sql = "SELECT * FROM $table WHERE resourceId='$resourceId'";
		
		if($filter){
			$sql .= " $filter";
		}
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute();
		$row = $stmt->fetch();
		
		if($stmt->execute($bind)){
			return $stmt->fetchAll();
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING);
		}
	}
	
	/**
	 * 
	 * @param string $filter
	 * @param string $select
	 * @return \PDOStatement
	 * @throws Exception
	 */
	public function getRole($filter=null, $select=null)
	{
		$table = static::$table;
		
		if($select){
			$select ='*';
		}
		
		$sql = "SELECT $select FROM $table";
		
		if($filter){
			$sql .= " WHERE $filter";
		}
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		if($stmt->execute()){
			return $stmt;
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING);
		}
	}
} //End of class

