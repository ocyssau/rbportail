<?php
//%LICENCE_HEADER%
namespace Application\Dao\Extended;

use Application\Dao\Connexion;
use Application\Dao\Exception as Exception;
use Application\Dao\Error as Error;
use Application\Dao;
use Application\Model;

/** SQL_SCRIPT>>
 CREATE TABLE extendedmodel(
 `id` int NOT NULL AUTO_INCREMENT,
 `extendedCid` int NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `asapp` VARCHAR(255) NULL,
 `getter` VARCHAR(255) NULL,
 `setter` VARCHAR(255) NULL,
 `label` VARCHAR(255) NULL,
 `type` VARCHAR(255) NULL,
 `size` VARCHAR(255) NULL,
 `regex` VARCHAR(255) NULL,
 `required` tinyint NULL,
 `min` VARCHAR(255) NULL,
 `max` VARCHAR(255) NULL,
 `step` VARCHAR(255) NULL,
 `list` VARCHAR(255) NULL,
 `multiple` tinyint NULL,
 `return` ENUM('name', 'value'),
 `dbtable` VARCHAR(255) NULL,
 `dbFieldForName` VARCHAR(255) NULL,
 `dbFieldForValue` VARCHAR(255) NULL,
 `dbfilter` VARCHAR(255) NULL,
 `dbquery` VARCHAR(255) NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `extendedmodel` ADD INDEX `EXTENDED_EXTENDEDCID` (`extendedCid` ASC);
 ALTER TABLE `extendedmodel` ADD INDEX `EXTENDED_NAME` (`name` ASC);
 ALTER TABLE `extendedmodel` ADD UNIQUE (`extendedCid`,`name`);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE extendedmodel;
 <<*/

/**
 * Entity of a property definition.
 */
class Property
{

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'extendedmodel';

	/**
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 * Class id use by postgresql dao to map rbplm class to entry in db.
	 * @var integer
	 */
	protected static $classId;

	/**
	 *
	 * @var \PDOStatement
	 */
	private $insertStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	private $updateStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	private $seqStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	private $deleteStmt;

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
		
		$this->metaModel = array(
			'id' => 'id',
			'extendedCid' => 'extendedCid',
			'name' => 'name',
			'asapp' => 'asapp',
			'getter' => 'getter',
			'setter' => 'setter',
			'label' => 'label',
			'type' => 'type',
			'size' => 'size',
			'regex' => 'regex',
			'required' => 'required',
			'min' => 'min',
			'max' => 'max',
			'step' => 'step',
			'list' => 'list',
			'multiple' => 'multiple',
			'return' => 'return',
			'dbtable' => 'dbtable',
			'dbFieldForName' => 'dbFieldForName',
			'dbFieldForValue' => 'dbFieldForValue',
			'dbfilter' => 'dbfilter',
			'dbquery' => 'dbquery'
		);
	}
	
	/**
	 */
	public function bind($mapped)
	{
		$bind = array();
		foreach( $this->metaModel as $name ) {
			$bind[':' . $name] = $mapped->$name;
		}
		return $bind;
	}

	/**
	 */
	public function bindType($appType)
	{
		switch ($appType) {
			case 'text':
				$sysType = 'VARCHAR(255)';
				break;
			case 'longtext':
				$sysType = 'text';
				break;
			case 'date':
				$sysType = 'DATETIME';
				break;
			case 'select':
				$sysType = 'text';
				break;
			case 'selectFromDb':
				$sysType = 'text';
				break;
			case 'number':
				$sysType = 'decimal';
				break;
			case 'range':
				$sysType = 'decimal';
				break;
			default:
				$sysType = 'text';
		}
		return $sysType;
	}

	/**
	 *
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if ( !$this->connexion ) {
			throw new Exception('CONNEXION_IS_NOT_SET', Error::ERROR);
		}
		return $this->connexion;
	}
	
	/**
	 * @todo Escape sql injection on select
	 * $mapped \Application\Model\Extended\Property
	 */
	public function insert($mapped, array $select = null)
	{
		$table = static::$table;
		$this->connexion->beginTransaction();
		
		if ( !$this->insertStmt ) {
			foreach( $this->metaModel as $name ) {
				$set[] = '`' . $name . '`';
				$values[] = ':' . $name;
			}
			
			$sql = 'INSERT INTO ' . $table . ' (' . implode(',', $set) . ')';
			$sql .= ' VALUES(' . implode(',', $values) . ')';
			$this->insertStmt = $this->connexion->prepare($sql);
		}
		
		//insert property request
		$alterClassId = $mapped->extendedCid;
		$alterTable = Dao\Factory::get()->getTable($alterClassId);
		$column = $mapped->name;
		$type = $this->bindType($mapped->type);
		$alterSql = "ALTER TABLE $alterTable ADD COLUMN `$column` $type NULL";
		
		try {
			$this->connexion->query($alterSql);
			
			if ( is_array($mapped) ) {
				foreach( $mapped as $map ) {
					$this->insertStmt->execute($this->bind($map));
				}
			}
			else {
				$this->insertStmt->execute($this->bind($mapped));
			}
			
			$id = $this->connexion->lastInsertId($table);
			$mapped->id = $id;
			
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * @todo Escape sql injection on select
	 *
	 * @param \Rbplm\Any $mapped
	 * @param array $select
	 * @throws Exception
	 * @return Property
	 */
	public function update($mapped, array $select = null)
	{
		$table = static::$table;
		
		if ( !$this->updateStmt ) {
			foreach( $this->metaModel as $name ) {
				$set[$name] = '`' . $name . '`=:' . $name;
			}
			unset($set['id']);
			
			$sql = 'UPDATE ' . $table . ' SET ' . implode(',', $set) . ' WHERE id=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}
		
		try {
			$this->connexion->beginTransaction();
			if ( is_array($mapped) ) {
				foreach( $mapped as $map ) {
					$this->updateStmt->execute($this->bind($map));
				}
			}
			else {
				$this->updateStmt->execute($this->bind($mapped));
			}
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			//var_dump($this->updateStmt->queryString);
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}
	
	/**
	 * To do a partial update.
	 * Properties must be selected in array $properties as key=property name, value=property value.
	 *
	 * @todo Escape sql injection
	 *
	 * @param string $filter
	 * @param array $properties
	 * @return Property
	 */
	public function partialUpdate($filter, $properties)
	{
		$table = static::$table;
		
		$set = '';
		foreach( $properties as $pname => $pvalue ) {
			$set .= "$pname=:$pname,";
			$bind[":$pname"] = $pvalue;
		}
		$set = trim($set, ',');
		
		$sql = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $filter;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $this;
	}
	
	/**
	 * @todo Escape sql injection
	 *
	 * @param \Rbplm\Any $mapped
	 * @param string $filter
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function load($mapped, $filter = null, $bind = null)
	{
		$table = static::$table;
		$select = 'obj.*';
		$sql = "SELECT $select FROM $table AS obj";
		
		if ( $filter ) {
			$sql .= " WHERE $filter";
		}
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();
		
		if ( $row ) {
			if ( isset($row['attributes']) ) {
				$row['attributes'] = json_decode($row['attributes'], true);
			}
			$mapped->hydrate($row);
		}
		else {
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING);
		}
		return $mapped;
	}
	
	/**
	 * @todo Escape sql injection
	 */
	public function deleteFromId($id)
	{
		$table = static::$table;
		
		try {
			$this->connexion->beginTransaction();
			
			//get extended table
			$sql = "SELECT * FROM $table AS obj WHERE id=:id";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$stmt->execute(array(
				':id' => $id
			));
			$row = $stmt->fetch();
			$alterClassId = $row['extendedCid'];
			$column = $row['name'];
			$alterTable = Dao\Factory::get()->getTable($alterClassId);
			
			//delete property
			$sql = 'DELETE FROM ' . $table . " WHERE id=:id";
			$suppressStmt = $this->connexion->prepare($sql);
			$suppressStmt->execute(array(
				':id' => $id
			));
			
			//delete column
			$alterSql = "ALTER TABLE $alterTable DROP `$column`";
			$this->connexion->query($alterSql);
			
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * @todo Escape sql injection
	 */
	public function deleteFromUid($uid)
	{
		$filter = 'uid=:uid';
		$table = static::$table;
		
		try {
			$this->connexion->beginTransaction();
			
			//get extended table
			$sql = "SELECT * FROM $table AS obj WHERE uid=:id";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$stmt->execute(array(
				':id' => $id
			));
			$row = $stmt->fetch();
			$alterClassId = $row['extendedCid'];
			$column = $row['name'];
			$alterTable = Dao\Factory::get()->getTable($alterClassId);
			
			//delete property
			$sql = 'DELETE FROM ' . $table . " WHERE id=:id";
			$suppressStmt = $this->connexion->prepare($sql);
			$suppressStmt->execute(array(
				':id' => $id
			));
			
			//delete column
			$alterSql = "ALTER TABLE $alterTable DROP `$column`";
			$this->connexion->query($alterSql);
			
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			$this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
}
