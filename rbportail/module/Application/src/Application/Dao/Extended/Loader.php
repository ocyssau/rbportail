<?php

namespace Application\Dao\Extended;

use Application\Model\Any;
use Application\Dao\Connexion;

/**
 * @brief Load the extended properties and configure the Dao.
 * 
 * Complete Dao metamodel
 * Set the plugin to Dao
 * 
 * @author olivier
 *
 */
class Loader
{
	public static $loadStmt=null;
	
	/**
	 * 
	 * @param string|Any $anyOrId
	 * @param \Application\Dao\Dao $dao
	 * @return array
	 */
	public static function load($anyOrId, $dao)
	{
		if($anyOrId instanceof Any){
			$cid = $anyOrId->cid;
		}
		else{
			$cid = $anyOrId;
		}

		if(!self::$loadStmt){
			$connexion = Connexion::get();
			$sql = "SELECT id,name,asapp,getter,setter,type FROM extendedmodel WHERE extendedCid=:classId";
			$stmt = $connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			self::$loadStmt = $stmt;
		}
		else{
			$stmt = self::$loadStmt;
		}

		$stmt->execute(array(':classId'=>$cid));
		$row = $stmt->fetchAll();
		$extended = new DaoPlugin($row);
		$dao->metaModel = array_merge($dao->metaModel, $extended->getMetamodel());
		$dao->extended = $extended;

		return $extended;
	}
}
