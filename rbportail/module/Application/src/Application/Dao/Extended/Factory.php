<?php
//%LICENCE_HEADER%

namespace Application\Dao\Extended;

use Application\Model\Any;

/**
 * @brief Use to build a Dao with extended properties capability
 *
 */
class Factory
{
	const GENERIC = 200;

	/**
	 * Registry of instanciated DAO.
	 *
	 * @var array
	 */
	private static $_registry = array();

	/**
	 * Return a dao object for the mapped object.
	 *
	 * @param Any|integer $anyOrId
	 * @return \Application\Dao\Dao
	 */
	public static function getDao($anyOrId, $factory)
	{
		$dao = $factory::getDao($anyOrId);
		Loader::load($anyOrId, $dao);
		return $dao;
	}
}
