<?php
//%LICENCE_HEADER%

namespace Application\Dao\Extended;

/**
 * @brief Plugins for Dao
 * This is used by Dao to load/save extended properties in model
 * @author olivier
 */
class DaoPlugin
{
	/**
	 * @var array
	 */
	public $properties = array();

	/**
	 * @var array
	 */
	public $metamodel;

	function __construct($properties=null)
	{
		if($properties){
			$this->properties = $properties;
		}
		$this->metamodel = array();
	}

	/**
	 *
	 */
	public function getMetamodel()
	{
		if(!$this->metamodel){
			foreach($this->properties as $property){
				$this->metamodel[$property['name']] = $property['name'];
			}
		}
		return $this->metamodel;
	}

	/**
	 *
	 */
	public function bind($mapped)
	{
		foreach($this->properties as $property){
			$pname = $property['name'];
			$getter = $property['getter'];
			$bind[':'.$pname] = $mapped::$getter;
		}
		return $bind;
	}

	/**
	 *
	 */
	public function hydrate($mapped, $values)
	{
		foreach($this->properties as $property){
			$pname = $property['name'];
			$setter = $property['setter'];
			$value = $values[$pname];
			if($setter){
				call_user_func(array($mapped,$setter), $value);
			}
			else{
				$mapped->$pname = $value;
			}
		}
		return $mapped;
	}


}
