<?php
namespace Application\Dao;

/**
 *
 *
 */
class MetamodelTranslator
{

	/**
	 * Date format use by db
	 * @var string
	 */
	const DATE_FORMAT = 'Y-m-d H:i:s';

	public function __construct($metaModel, $metaModelFilters)
	{
		$this->sysToAppMetaModel = $metaModel;
		$this->metamodelFilters = $metaModelFilters;
		$this->appToSysMetaModel = array_flip($metaModel);
	}

	/**
	 * @see Rbplm\Dao.DaoInterface::toApp()
	 *
	 * Translate property name and value to model semantic
	 *
	 * @param array|string		$in		Db properties name or array (key as db column name => value)
	 * @return array			Model properties
	 */
	public function toApp($in)
	{
		$translator = $this->sysToAppMetaModel;
		$sysToAppFilter = $this->metamodelFilters;

		if( is_array($in) ){
			$out = array();
			foreach($in as $asSys=>$value){
				if( isset($translator[$asSys]) ){
					if(isset($sysToAppFilter[$asSys])){
						$filterMethod = $sysToAppFilter[$asSys].'ToApp';
						$value = static::$filterMethod($value);
					}
					$out[$translator[$asSys]] = $value;
				}
				else{
					$out[$asSys] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$out = $translator[$in];
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * @see Rbplm\Dao.DaoInterface::toSys()
	 *
	 * Translate current row data from model to db semantic
	 *
	 * @param array|string		 $in	Application property name or array (key as model property name => value)
	 * @return array					System attributs
	 */
	public function toSys($in)
	{
		$out = array();

		$translator = $this->appToSysMetaModel;
		$sysToAppFilter = $this->metamodelFilters;

		if( is_array($in) ){
			foreach( $in as $appName=>$value ){
				if( isset($translator[$appName]) ){
					$out[$translator[$appName]] = $value;
				}
				else{
					$out[$appName] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$out = $translator[$in];
				$filterMethod = $sysToAppFilter[$in].'ToSys';
				$bind[':'.$appName] = static::$filterMethod($in);
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * Get select with AS applicationName
	 * @return array
	 */
	public function getSelectAsApp()
	{
		$select = array();
		$translator = $this->sysToAppMetaModel;
		foreach($translator as $asSys=>$asApp){
			$select[] = "`$asSys` AS `$asApp`";
		}
		return $select;
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToApp($json)
	{
		if($json[0]=='{' || $json[0]=='['){
			return json_decode($json, true);
		}
		else{
			return array($json);
		}
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToSys($array)
	{
		return json_encode($array, true);
	}

	/**
	 *
	 * @param string $yesOrNo
	 * @return boolean
	 */
	protected static function yesOrNoToApp($yesOrNo)
	{
		return ($yesOrNo == 'y') ? true : false;
	}

	/**
	 * @param boolean $yesOrNo
	 * @return string
	 */
	protected static function yesOrNoToSys($yesOrNo)
	{
		return ($yesOrNo == true) ? 'y' : 'n';
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function dateToApp($timestamp)
	{
		if($timestamp == 0){
			return new \Rbplm\Sys\Date();
		}
		else{
			return new \Rbplm\Sys\Date((int)$timestamp);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function dateToSys($in)
	{
		if($in instanceof \DateTime){
			return $in->getTimestamp();
		}
		elseif($in == 0){
			$date = new \DateTime();
			return $date->getTimestamp();
		}
		elseif(is_string($in)){
			$date = new \DateTime($in);
			return $date->getTimestamp();
		}
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function datetimeToApp($string)
	{
		if($string == ''){
			return new \Rbplm\Sys\Date();
		}
		else{
			return new \Rbplm\Sys\Date($string);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function datetimeToSys($in)
	{
		if($in instanceof \DateTime){
			return $in->format();
		}
		elseif($in == 0){
			return null;
		}
		elseif(is_string($in)){
			$date = new \Rbplm\Sys\Date($in);
			return $date->format(self::DATE_FORMAT);
		}
	}

}
