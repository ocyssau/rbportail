namespace Application\Test;

use Application\Dao\Loader;
use Application\Dao\Factory as DaoFactory;

/**
 *
 * @author olivier
 *
 */
class DaoTest extends Test
{

	/**
	 *
	 */
	function Test_Loader()
	{
		$classId = \Workflow\Model\Wf\Process::$classId;
		$uid = '560e4f0b43286';

		Loader::get()->setFactory(new DaoFactory());
		$process = Loader::get()->loadFromUid($uid, $classId);

		Loader::get()->loadChildren($process);

		var_dump($process->getChildren());
	}
}
