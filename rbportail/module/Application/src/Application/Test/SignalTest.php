namespace Application\Test;

use Application\Model\Any;
use Application\Model\Signal;

/**
 * @author olivier
 *
 */
class Sender
{
	public function sendSignal($eventName)
	{
		$e = Signal::trigger($eventName,$this,'arg2','arg3');
	}
	
	public function setName($name)
	{
		$event = Signal::trigger('setname',$this,$name);
		var_dump($event);
	}
}

/**
 * @author olivier
 *
 */
class Listener
{
	public function onInit($event, $sender, $arg1, $arg2)
	{
		echo "onInit is trigger".CRLF;
		//var_dump( $event, $arg2, $arg3 );
	}
	
	public function onRun($event, $sender, $arg2, $arg3)
	{
		echo "onRun is trigger".CRLF;
	}
	
	public function onSetname($event, $sender, $name)
	{
		echo "onSetname is trigger".CRLF;
		$sender->name = $name;
		$event->result = "Super ouais!";
	}
	
	public function onSetname2($event, $sender, $name)
	{
		echo "onSetname2 is trigger".CRLF;
		$sender->lastname = 'lastname-'.$name;
		$event->result2 = "Super";
	}
	
}

/**
 * 
 * @author olivier
 *
 */
class SignalTest extends Test
{
	function Test_Tutorial()
	{
		$sender = new Sender();
		$listener = new Listener();
		
		Signal::connect($sender, 'init', array($listener, 'onInit'));
		Signal::connect($sender, 'run', array($listener, 'onRun'));
		Signal::connect($sender, 'setname', array($listener, 'onSetname'));
		Signal::connect($sender, 'setname', array($listener, 'onSetname2'));
		
		$sender->sendSignal('init');
		$sender->sendSignal('nothing');
		$sender->sendSignal('run');
		$sender->setName('Marcel');
		var_dump($sender->name);
		var_dump($sender->lastname);
		
	}
}
