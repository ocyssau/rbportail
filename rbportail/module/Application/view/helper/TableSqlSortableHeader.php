<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class TableSqlSortableHeader extends AbstractHelper
{

    public function __invoke($title, $urlOrderby)
    {
        $order=$_REQUEST['order'];
        $orderby=$_REQUEST['orderby'];
        $urlOrder = $order == 'asc' ? 'desc' : 'asc';
        
        $request=$this->getView()->getRequest();
        var_dump($request->getQuery());
        $url = $this->getView()->url('admin', array('controller'=>'doctype')).'?&orderby=doctype_number&order='.$urlOrder;
        
        $out = '<a title="Click me to sort" href="'.$url.'">'.$title.'<span class="'.$glyphicon.'"></span></a>';
        return $out;
    }
}
