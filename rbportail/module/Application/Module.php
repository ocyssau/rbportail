<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\EventManager\EventInterface as Event;
use Application\Dao\Connexion;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Loader as DaoLoader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\AuthenticationService;
use Application\Model\Auth;

/**
 */
class Module implements AutoloaderProviderInterface
{

	/**
	 *
	 * @param Event $e
	 * @throws \Exception
	 */
	public function onBootstrap(Event $e)
	{
		$config = $e->getApplication()
			->getServiceManager()
			->get('Configuration');
		
		if ( !isset($config['rbp']) ) {
			throw new \Exception('Config for Portail is not set', E_ERROR);
		}
		
		\Application\Model\Uuid::init('UniqId');
		
		Connexion::setConfig($config['rbp']['db']);
		\Rbplm\Dao\Connexion::add(\Rbplm\Dao\Connexion::getDefault(), Connexion::get());
		
		$daoMap = include ('config/dist/objectdaomap.config.php');
		
		$factory = new DaoFactory();
		$factory->setMap($daoMap)
			->setOption('listclass', '\Application\Dao\DaoList')
			->setOption('filterclass', '\Application\Dao\Filter')
			->setConnexion(Connexion::get());
		$factory->setLoader(new DaoLoader($factory));
		$e->daoFactory = $factory;
		
		$eventManager = $e->getApplication()->getEventManager();
		$eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) {
			$controller = $e->getTarget();
			$controllerClass = get_class($controller);
			$moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
			$config = $e->getApplication()
				->getServiceManager()
				->get('config');
			if ( isset($config['view_manager']['template_map']['module_layouts'][$moduleNamespace]) ) {
				$controller->layout($config['view_manager']['template_map']['module_layouts'][$moduleNamespace]);
			}
		}, 100);
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	}

	/**
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 */
	public function getViewHelperConfig()
	{
		return array(
			'invokables' => array(
				'TableSqlSortableHeader' => 'Application\View\Helper\TableSqlSortableHeader'
			)
		);
	}

	/**
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'Application\Model\AuthStorage' => function ($sm) {
					return new \Application\Model\Auth\Storage('rbportail');
				},
				'AuthService' => function ($sm) {
					/* @var \Zend\ServiceManager\ServiceManager $sm */
					$config = $sm->get('Configuration');
					$authAdapter = Auth\Factory::getAdapter($config['rbp']['auth']); // = Application\Model\Auth\Ldap or DbAdapter
					$authService = new AuthenticationService();
					$authService->setAdapter($authAdapter);
					$authService->setStorage($sm->get('Application\Model\Auth\Storage'));
					
					return $authService;
				},
				'Workflow' => function ($sm) {
					$config = $sm->get('Configuration');
					\Wumanager\Service\Workflow\Code::$processPath = $config['rbp']['workflow']['process']['path'];
					\Wumanager\Service\Workflow\Code::$processNamespace = $config['rbp']['workflow']['process']['namespace'];
					$workflow = new \Wumanager\Service\Workflow();
					$workflow->serviceManager = $sm;
					return $workflow;
				},
				'Mail' => function ($sm) {
					$config = $sm->get('Configuration');
					$mail = new \Application\Service\Mail($config['rbp']['mail.smtp']);
					$mail->adminMail = $config['rbp']['admin.mail'];
					$mail->serviceManager = $sm;
					return $mail;
				}
			),
			'aliases' => array(),
			'abstract_factories' => array(),
			'invokables' => array(),
			'services' => array(),
			'shared' => array()
		);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\ModuleManager\Feature.AutoloaderProviderInterface::getAutoloaderConfig()
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
				)
			)
		);
	}
}
