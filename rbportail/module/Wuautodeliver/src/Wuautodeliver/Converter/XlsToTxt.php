<?php
namespace Wuautodeliver\Converter;

//use \Rbplm\Sys\Datatype\File as FileTool;

/**
 * abiword is require
 * apt install abiword
 *
 */
class XlsToTxt extends AbstractConverter
{

	/**
	 *
	 * {@inheritDoc}
	 * @see AbstractConverter::convert()
	 */
	public function convert($file, $toFile)
	{
		if ( !is_file($file) ) {
			throw new \Exception("file $file is not existing");
		}
		
		$return = null;
		$outputs = null;
		
		$execcmd = sprintf("abiword --to=txt --to-name=%s '%s'", $toFile, $file);
		exec($execcmd, $outputs, $return);
		if ( $return != 0 ) {
			$this->getResult()->error($outputs);
			throw new \RuntimeException("Error during run $execcmd");
		}
		
		return $this;
	}
} /* End of class */
