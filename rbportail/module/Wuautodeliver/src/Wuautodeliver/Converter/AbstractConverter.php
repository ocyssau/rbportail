<?php
namespace Wuautodeliver\Converter;

/**
 *
 *
 */
abstract class AbstractConverter implements ConverterInterface
{

	/**
	 * Data to convert
	 *
	 * @var string
	 */
	protected $data;

	/**
	 * Resulted datas
	 *
	 * @var Result
	 */
	protected $result;
	
	/**
	 * @var array
	 */
	protected $options;

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
	}
	
	/**
	 *
	 * @param array $options
	 * @return AbstractConverter
	 */
	public function setOptions(array $options)
	{
		$this->options = $options;
		return $this;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see ConverterInterface::convert()
	 */
	public abstract function convert($file, $toFile);

	/**
	 *
	 * @return Result
	 */
	public function getResult()
	{
		if(!isset($this->result)){
			$this->result = new Result();
		}
		return $this->result;
	}
}


