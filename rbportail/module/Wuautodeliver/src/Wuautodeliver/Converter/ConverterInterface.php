<?php
namespace Wuautodeliver\Converter;

/**
 * 
 *
 */
interface ConverterInterface
{

	/**
	 * @param string $file
	 * @param string $toFile
	 * @return ConverterInterface
	 * @throws \RuntimeException \Exception
	 */
	public function convert($file, $toFile);

	/**
	 *
	 * @return Result
	 */
	public function getResult();
	
	/**
	 *
	 * @param array $options
	 * @return ConverterInterface
	 */
	public function setOptions(array $options);
}
