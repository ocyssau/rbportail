<?php
namespace Wuautodeliver\Converter;

//use \Rbplm\Sys\Datatype\File as FileTool;

/**
 * Libreoffice is require
 * >apt install libreoffice
 * 
 * And you must create home for apache2 run user
 * >mkhomedir_helper www-data
 *
 */
class XlsToPdf extends AbstractConverter
{
	
	/**
	 * 
	 * @var string
	 */
	public $outdir = '/tmp';

	/**
	 * 
	 * {@inheritDoc}
	 * @see AbstractConverter::convert()
	 */
	public function convert($file, $toFile)
	{
		if ( !is_file($file) ) {
			throw new \Exception("file $file is not existing");
		}
		
		$outputs = null;
		$return = null;
		
		/* Capture pdf */
		$execcmd = sprintf('libreoffice --headless --convert-to pdf --outdir ' . $this->outdir . ' %s', $file);
		exec($execcmd, $outputs, $return);
		if ( $return != 0 ) {
			$this->getResult()->error($outputs);
			throw new \RuntimeException("Error during run $execcmd");
		}
		else {
			/*
			$tf = $this->outdir . '/' . basename($file);
			$tf = FileTool::sGetRoot($tf).'.pdf';
			if ( is_file($tf) && $tf<>$toFile) {
				rename($tf, $toFile);
			}
			else{
				throw new \RuntimeException("Output file $tf is not founded");
			}
			*/
		}
		return $this;
	}
} /* End of class */

