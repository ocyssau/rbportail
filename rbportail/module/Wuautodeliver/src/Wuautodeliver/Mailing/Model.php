<?php
namespace Wuautodeliver\Mailing;

use Application\Model\People;
use Application\Service\Mail as MailService;

/**
 De: Rodolphe David <Rodolphe.David@sierbla.com>
 Objet: Legacy programs WU, livraison SIER, BL-TP_STELIA-SIER-499
 Date: 17 juillet 2018 à 21:10:08 UTC+2
 À: ROLLAND Nicolas <nicolas.rolland@stelia-aerospace.com>, "LAUMET Jean-Charles" <jean-charles.laumet@external.stelia-aerospace.com>, "BARDET Aurelien" <aurelien.bardet@stelia-aerospace.com>, BARON Christophe <christophe.baron@stelia-aerospace.com>, POUJADE Vincent <Vincent.POUJADE@external.stelia-aerospace.com>, AUGUY Fabrice <fabrice.auguy@stelia-aerospace.com>, AMBERT Fabien <fabien.ambert@stelia-aerospace.com>, CZARNECKI Michel <michel.czarnecki@stelia-aerospace.com>, DANIEL Cyril <cyril.daniel@stelia-aerospace.com>, DAMINATO Gilles <gilles.daminato@stelia-aerospace.com>, VACHER Marc <Marc.VACHER@stelia-aerospace.com>
 Cc: LAGUENY Christophe <christophe.lagueny@external.stelia-aerospace.com>, CALVO Pascal <pascal.calvo@external.stelia-aerospace.com>

 Bonjour,

 Veuillez trouver ci-joint le bordereau de livraison BL-TP_STELIA-SIER-499.

 Cordialement. 
 */

/**
 * 
 * @author ocyssau
 *
 */
class Model
{
	public $reference;
	public $subject;
	public $body;
	public $sendTo = [];
	public $sendToCc = [];
	public $from = [];
	public $file;
	public $filetype;
	
	/**
	 * 
	 */
	public function __construct($params = [])
	{
		$this->reference = '';
		$this->subject = 'Legacy programs WU, livraison SIER, %s';
		$this->body = 'Bonjour,';
		$this->body .= '<br>Veuillez trouver ci-joint le bordereau de livraison %s.';
		$this->body .= '<br>Cordialement.';
		
		isset($params['reference']) ? $this->reference = $params['reference'] : null;
		isset($params['subject']) ? $this->subject = $params['subject'] : null;
		isset($params['body']) ? $this->body = $params['body'] : null;
		isset($params['sendTo']) ? $this->sendTo = $params['sendTo'] : null;
		isset($params['sendToCc']) ? $this->sendToCc = $params['sendToCc'] : null;
		isset($params['from']) ? $this->from = $params['from'] : null;
		isset($params['file']) ? $this->file = $params['file'] : null;
		isset($params['filetype']) ? $this->filetype = $params['filetype'] : null;
		
		$this->subject = sprintf($this->subject, $this->reference);
		$this->body = sprintf($this->body, $this->reference);
	}
	
	/**
	 * @param array $receipt
	 * @param People\User $from
	 * @param string $subject
	 * @param string $body
	 * @return Model
	 */
	public function send()
	{
		$mailService = MailService::get();
		$message = MailService::messageFactory();
		$htmlPart = MailService::htmlPartFactory($this->body);
		$filePart = MailService::filePartFactory($this->file);
		$body = MailService::bodyFactory();
		$body->setParts(array(
			$htmlPart,
			$filePart
		));
		$message->setBody($body);
		
		$message->addFrom($this->from[1], $this->from[0])
			->addReplyTo($this->from[1])
			->setSubject($this->subject);
		
		foreach( $this->sendTo as $receipt ) {
			$message->addTo($receipt[1], $receipt[0]);
		}
		foreach( $this->sendToCc as $receipt ) {
			$message->addCc($receipt[1], $receipt[0]);
		}
		$mailService->send($message);
		return $this;
	}
}
