<?php
//%LICENCE_HEADER%

namespace Wuautodeliver\Dao;

use Rbplm\Dao\Connexion;

/** SQL_SCRIPT>>
CREATE FUNCTION `getMemoSequence`(_schema VARCHAR(64)) RETURNS int(11)
BEGIN
	DECLARE lastId INT(11);
    SELECT MAX(RIGHT(`deliveryMemo`, 4)) INTO lastId FROM `workunit` WHERE `status` IN ('Delivered','Attente facturation', 'Facturée') AND `deliveryMemo` LIKE _schema;

    set @ret = lastId;
    RETURN @ret;
END

<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
<<*/

/** SQL_DROP>>
<<*/

/**
 * 
 * @author ocyssau
 *
 */
class MemoSequence
{
	/**
	 * 
	 * @var integer
	 */
	public $minValue = 300;
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
	}
	
	/**
	 * search max of sequence from $schema and replace in $schema the % by sequence found
	 * 
	 * @param string $schema
	 */
	public function getNextReference($schema = 'BL-TP_STELIA-SIER-%')
	{
		$sql = "select getMemoSequence(:schema)";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':schema'=>$schema));
		
		$last = current($stmt->fetch(\PDO::FETCH_NUM));
		if(!$last){
			$last = $this->minValue;
		}
		else{
			$last++;
		}
		$reference = str_replace('%', $last, $schema);
		return $reference;
	}
}

