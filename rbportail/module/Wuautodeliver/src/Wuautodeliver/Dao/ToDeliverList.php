<?php
//%LICENCE_HEADER%

namespace Wuautodeliver\Dao;

use Rbplm\Dao\Connexion;

/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
DROP VIEW toDeliverLegacy;
CREATE VIEW toDeliverLegacy AS
SELECT 
    `workunit`.`id` AS 'workunitId',
    `deliverable`.`id` AS 'deliverableId',
    `deliverable`.`name` AS 'number',
    '' AS Type,
    `deliverable`.`indice` AS 'indice',
    `deliverable`.`title` AS 'designation',
    `deliverable`.`ci` AS 'parentAssy',
    `deliverable`.`mod` AS 'modpm',
    'X' AS 'vaultInProgram.',
    'na' AS 'TirA0',
    'na' AS 'TirA3',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'Nom.',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'ECN',
    CASE
        WHEN LEFT(deliverable.title, 1) = 'P' THEN 'X'
        ELSE ''
    END AS 'Request for rev.',
    '' AS 'DessVD',
    '' AS 'CalcVC',
    '' AS 'VaultPublish',
    '' AS 'Dess',
    '' AS 'CG',
    '' AS 'RG',
    '' AS 'Calc.',
    '' AS 'LA',
    '' AS 'RS',
    '' AS 'OF',
    '' AS 'Vaultrelease',
    CONCAT(`workunit`.`name`,
            '/ Vault ',
            `deliverable`.`team`,
            '/ ATA leader ',
            `workunit`.`ataLeader`) AS Commentaire
FROM
    `deliverable`
        JOIN
    `workunit` ON `workunit`.`id` = `deliverable`.`parentId`
        JOIN
   `workunit_type` ON `workunit`.`typeId` = `workunit_type`.`id`
        JOIN
    `link_workunit_type_context` ON `workunit_type`.`id` = `link_workunit_type_context`.`parentId`
        JOIN
    `workunit_context` ON `link_workunit_type_context`.`childId` = `workunit_context`.`id`
WHERE `workunit`.`status` = 'Ready for delivery'
AND (`deliverable`.`section` <> 'A350')
GROUP BY `workunit`.`name`, `workunit`.`id`, `deliverable`.`id`;
<<*/

/** SQL_DROP>>
<<*/

/**
 * 
 * @author ocyssau
 *
 */
class ToDeliverList extends \Application\Dao\DaoList
{
	/**
	 * @var string
	 */
	public $table = 'toDeliverLegacy';
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		$this->options['table'] = $this->table;
		
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
	}
	
}
