<?php
namespace Wuautodeliver\Form;

use Zend\Hydrator\HydratorInterface;

/**
 * 
 * @author ocyssau
 *
 */
class Hydrator implements HydratorInterface
{
	
	/**
	 * 
	 */
	public function extract($object)
	{
		$ret = array(
			'reference'=>$object->reference,
			'subject'=>$object->subject,
			'body'=>$object->body,
			'from'=>self::serializeContact($object->from),
			'sendTo'=>self::mailingToForm($object->sendTo),
			'sendToCc'=>self::mailingToForm($object->sendToCc),
			'file'=>$object->file,
			'filetype'=>$object->filetype
		);
		return $ret;
	}

	/**
	 * 
	 */
	public function hydrate(array $data, $object)
	{
		//$object->reference = $data['reference'];
		$object->subject = $data['subject'];
		$object->body = $data['body'];
		//$object->sendTo = $data['sendTo'];
		//$object->sendToCc = $data['sendToCc'];
		//$object->from = $data['from'];
		//$object->template = $data['template'];
	}
	
	/**
	 * 
	 */
	public static function mailingToForm($list)
	{
		$ret = [];
		foreach($list as $k=>$contact){
			$value = $k;
			$label = self::serializeContact($contact);
			$ret[$value] = $label;
		}
		return $ret;
	}
	
	/**
	 *
	 */
	public static function serializeContact($contact)
	{
		return $contact[0].'('.$contact[1].')';
	}
	
	/**
	 * 
	 */
	protected static function mailingToModel()
	{
		
	}
}
