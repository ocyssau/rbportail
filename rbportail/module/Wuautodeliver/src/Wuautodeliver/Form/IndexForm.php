<?php
namespace Wuautodeliver\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Wuautodeliver\Form\Hydrator as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 * @author ocyssau
 *
 */
class IndexForm extends Form implements InputFilterProviderInterface
{
	
	/**
	 *
	 */
	public function __construct($mailModel)
	{
		parent::__construct('Autodeliver');
		$this->template = 'wuautodeliver/autodeliver/indexform.phtml';

		$this
			->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());


		$this->add(array(
		    'name' => 'template',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'text',
		        'placeholder' => 'Template',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'Template File',
		    ),
		));

		$this->add(array(
		    'name' => 'reference',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'text',
		        'placeholder' => 'Reference',
		        'class'=>'form-control',
		    	'disabled'=>'disabled',
		    	'size'=>50
		    ),
		    'options' => array(
		        'label' => 'Reference',
		    ),
		));

		$this->add(array(
		    'name' => 'sendTo',
		    'type'  => 'Zend\Form\Element\MultiCheckbox',
		    'attributes' => array(
		        'placeholder' => 'sendTo',
		    	'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'sendTo',
		    	'value_options'=>Hydrator::mailingToForm($mailModel->sendTo)
		    ),
		));

		$this->add(array(
			'name' => 'sendToCc',
			'type'  => 'Zend\Form\Element\MultiCheckbox',
			'attributes' => array(
				'placeholder' => 'sendToCc',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'sendToCc',
				'value_options'=>Hydrator::mailingToForm($mailModel->sendToCc)
				
			),
		));
		
		$this->add(array(
			'name' => 'from',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'from',
				'class'=>'form-control',
				'disabled'=>'disabled',
				'size'=>50
			),
			'options' => array(
				'label' => 'from',
			),
		));
		
		$this->add(array(
			'name' => 'subject',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'subject',
				'class'=>'form-control',
				'disabled'=>'disabled',
				'size'=>50
			),
			'options' => array(
				'label' => 'subject',
			),
		));
		
		$this->add(array(
			'name' => 'body',
			'type'  => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'cols'=>100,
				'rows'=>10,
				'class'=>'form-control',
				'disabled'=>'disabled',
			),
			'options' => array(
				'label' => 'Body',
			),
		));
		
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Send',
				'id' => 'submitbutton',
				'class'=>'btn btn-success',
				'title'=>'To send BL to each users of receipts'
			),
		));
		
		$this->add(array(
			'name' => 'download',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Download',
				'id' => 'downloadbutton',
				'class'=>'btn btn-primary',
				'title'=>'To Download the BL without send to receipts'
			),
		));
		
		$this->add(array(
			'name' => 'quit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Quit',
				'id' => 'quitbutton',
				'class'=>'btn btn-default',
				'title'=>'Return to workunits manager'
			),
		));
		
		$this->bind($mailModel);
		//var_dump($this->get('sendTo'));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
	    return array(
	    	'template' => array(
	    	    'required' => false,
	    	),
	    	'reference' => array(
	    		'required' => false,
	    	),
	    	'sendTo' => array(
	    		'required' => false,
	    	),
	    	'sendToCc' => array(
	    	    'required' => false,
	    	),
	    	'from' => array(
	    	    'required' => false,
	    	),
	    	'subject' => array(
	    	    'required' => false,
	    	),
	    	'body' => array(
	    	    'required' => false,
	    	),
	    );
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
	    $ret = array();
	    return $ret;
	}
}
