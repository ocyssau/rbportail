<?php
namespace Wuautodeliver\Excel;

/**
 * Template for Callback to run on Docfile
 */
class Populator
{
	
	/**
	 * List of workunit entries
	 * @var array
	 */
	public $list = [];

	/**
	 * 
	 */
	public function __construct()
	{}

	/**
	 * Assume that column of $list = column of target template.
	 * Start populate from row of temple = $firstBodyRow
	 * 
	 * 
	 * @param \Wuautodeliver\Dao\ToDeliverList $list
	 * @return Populator
	 */
	public function populate(\Wuautodeliver\Excel\Template $template, \Wuautodeliver\Dao\ToDeliverList $list, $reference)
	{
		$stmt = $list->getStmt();
		$activeSheet = $template->activeSheet;

		$firstBodyRow = 9;
		$row = $firstBodyRow;
		while( $entry = $stmt->fetch(\PDO::FETCH_NUM) ) {
			$this->list[] = $entry;
			/* count column in db view fetch */
			$c = count($entry);
			
			/* init excel column first number */
			$col = 1;
			
			/* ignore 2 first column of db view = workitemId and deliverableId */
			for($i=2; $i < $c; $i++){
				$val = $entry[$i];
				$activeSheet->setCellValueByColumnAndRow($col, $row, $val);
				$col++;
			}
			$row++;
		}

		$now = new \DateTime();
		
		$dateRange = [
			4,
			2
		];
		$refRange = [
			4,
			3
		];
		
		$activeSheet->setCellValueByColumnAndRow($refRange[0], $refRange[1], $reference);
		$activeSheet->setCellValueByColumnAndRow($dateRange[0], $dateRange[1], $now->format('d/m/Y'));

		return $this;
	}
}

