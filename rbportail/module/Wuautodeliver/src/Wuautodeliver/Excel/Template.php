<?php
namespace Wuautodeliver\Excel;

/**/
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Template for Callback to run on Docfile
 */
class Template
{
	
	/**
	 * 
	 * @param \PhpOffice\PhpSpreadsheet\Spreadsheet
	 */
	public $spreadsheet;
	
	/**
	 *
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
	 */
	public $activeSheet;

	/**
	 *
	 * @param string
	 */
	public $template;

	/**
	 * 
	 * @param string $template
	 * @return Template
	 */
	public function load($template)
	{
		$this->template = $template;
		$this->spreadsheet = IOFactory::load($template);
		$this->activeSheet = $this->spreadsheet->getActiveSheet();
		return $this;
	}

	/**
	 * Redirect output to a client’s web browser (Xls)
	 */
	public function download($filename = 'delivery', $as='xlsx')
	{
		$spreadsheet = $this->spreadsheet;
		
		if($as=='xlsx'){
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		}
		elseif($as=='xls'){
			header('Content-Type: application/vnd.ms-excel');
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
		}
		
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		//header("Content-Transfer-Encoding: $filename\n"); // Surtout ne pas enlever le \n
		header('Cache-Control: max-age=0');
		/* If you're serving to IE 9, then the following may be needed */
		header('Cache-Control: max-age=1');
		/* If you're serving to IE over SSL, then the following may be needed */
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); /* Date in the past */
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); /* always modified */
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer->save('php://output');
		die();
	}
	
	/**
	 * Redirect output to a client’s web browser (Xls)
	 */
	public function downloadPdf($filename = 'delivery')
	{
		$spreadsheet = $this->spreadsheet;
		
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$converter = new \Wuautodeliver\Converter\XlsToPdf();
		
		$file = $converter->outdir.'/'.$filename . '.xlsx';
		$writer->save($file);
		
		$toFile = $filename.'.pdf';
		$converter->convert($file, $toFile);
		$filename = $converter->outdir.'/'.$toFile;
		
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment;filename="' . basename($filename) . '"');
		//header("Content-Transfer-Encoding: $filename\n"); // Surtout ne pas enlever le \n
		header('Cache-Control: max-age=0');
		/* If you're serving to IE 9, then the following may be needed */
		header('Cache-Control: max-age=1');
		/* If you're serving to IE over SSL, then the following may be needed */
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); /* Date in the past */
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); /* always modified */
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		readfile($filename);
		die();
	}
	
	/**
	 * Redirect output to a client’s web browser (Xls)
	 */
	public function savePdf($filename = 'delivery')
	{
		$spreadsheet = $this->spreadsheet;
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save($filename);
		
		$converter = new \Wuautodeliver\Converter\XlsToPdf();
		$file = $converter->outdir.'/'.$filename;
		$toFile = $converter->outdir.'/'.$filename.'.pdf';
		$converter->convert($file, $toFile);
		//var_dump($file, $filename);die;
	}
	
	/**
	 * Redirect output to a client’s web browser (Xls)
	 */
	public function save($filename = 'delivery', $as='xlsx')
	{
		$spreadsheet = $this->spreadsheet;
		
		if($as=='xlsx'){
			$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		}
		elseif($as=='xls'){
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
		}
		elseif($as=='pdf'){
			$writer = IOFactory::createWriter($spreadsheet, 'DomPdf');
		}
		$writer->save($filename);
	}
}
	
