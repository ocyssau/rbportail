<?php
namespace Wuautodeliver\Controller;

use Wumanager\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Wuautodeliver\Excel\Populator;
use Wuautodeliver\Excel\Template;
use Wuautodeliver\Dao\ToDeliverList;
use Wuautodeliver\Dao\MemoSequence;
use Wuautodeliver\Mailing;
use Wumanager\Model\Workunit;
use Application\Dao\Factory as DaoFactory;

/**
 * 
 *
 */
class AutodeliverController extends AbstractController
{

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = new ViewModel();
		$request = $this->getRequest();

		/* Check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_WFPUSH) ) {
			return $this->notauthorized();
		}

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Configuration');

		/* get wu to deliver */
		$toDeliverList = new ToDeliverList();
		$toDeliverList->load();

		/* get last sequence */
		$seq = new MemoSequence();
		$reference = $seq->getNextReference('BL-TP_STELIA-SIER-%');

		/* generate pdf and excel with PhpOffice */
		/* load template */
		$templateFile = $config['rbp']['autodeliver.xls.template']['file'];
		$sendTo = $config['rbp']['autodeliver.sendTo'];
		$sendToCc = $config['rbp']['autodeliver.sendToCc'];
		$from = $config['rbp']['autodeliver.from'];

		$tempfile = 'data/autodeliver/temp/' . $reference . '.xlsx';

		/* mailing */
		$mailModel = new Mailing\Model(array(
			'reference' => $reference,
			'template' => $templateFile,
			'file' => $tempfile,
			'filetype' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'sendTo' => $sendTo,
			'sendToCc' => $sendToCc,
			'from' => $from
		));
		$mailModel->templateFile = $templateFile;

		$form = new \Wuautodeliver\Form\IndexForm($mailModel);

		/**/
		if ( $request->isPost() ) {
			/* download */
			$download = $request->getPost('download');
			$run = $request->getPost('submit');

			/* init Template object */
			$template = new Template();
			$template->load($templateFile);

			/* init populator and populate template */
			$populator = new Populator();
			$populator->populate($template, $toDeliverList, $reference);
			$form->workunits = $populator->list;

			if ( $download ) {
				$template->download($reference, 'xlsx');
			}
			elseif ( $run ) {
				try {
					$template->save($tempfile, 'xlsx');
					$mailModel->send();
					$this->pushDeliveryActivity($populator, $reference);
					/* cleanup temp files */
					unlink($tempfile);
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
		}
		else {
			$form->workunits = $toDeliverList->stmt->fetchAll(\PDO::FETCH_ASSOC);
			$view->setTemplate($form->template);
		}

		/* Show view */
		$view->form = $form;
		$view->templateFile = $templateFile;
		$view->reference = $reference;
		$view->mailing = $mailModel;
		return $view;
	}

	/**
	 * Push WORKFLOW
	 * 
	 * @param Populator $populator
	 * @param string $reference
	 * @throws \Exception
	 */
	protected function pushDeliveryActivity($populator, $reference)
	{
		/* ********** Push WORKFLOW ****************** */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Workunit::$classId);

		/* @var \Wumanager\Dao\Workunit\LinkProcess $dao */
		$linkprocessDao = $factory->getDao(Workunit\LinkProcess::$classId);

		/**/
		$lastDeliveryDate = new \DateTime();
		$deliveryDate = $lastDeliveryDate;
		$deliveryMemo = $reference;

		/* @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->getRequest();
		$request->setMethod('post');
		$post = $request->getPost();
		$query = $request->getQuery();

		/**/
		foreach( $populator->list as $entry ) {
			$workunit = new Workunit();
			$workunitId = $entry[0];
			$dao->loadFromId($workunit, $workunitId);

			/* its not the first delivery ?*/
			if ( $workunit->getDeliverymemo() != "" ) {
				$deliveryDate = $workunit->getDeliveryDate();
			}
			else {
				$deliveryDate = $lastDeliveryDate;
			}

			/* get delivery activity */
			$nextActivities = $linkprocessDao->getNextactivities($workunitId)->fetchall(\PDO::FETCH_ASSOC);
			$actInstanceId = null;
			$instanceId = null;

			foreach( $nextActivities as $na ) {
				if ( $na['name'] == 'delivery' ) {
					$actInstanceId = $na['id'];
					$instanceId = $na['instanceId'];
					break;
				}
			}
			if ( !$actInstanceId ) {
				throw new \Exception(sprintf('This workunit %s (id=%s) is not to deliver', $workunit->getName(), $workunitId), 5000);
			}

			/* push workflow POST method */
			$post->fromArray([
				'id' => $workunitId,
				'4dcb6a55582ce745a1826edd6126cc0a' => '4dcb6a55582ce745a1826edd6126cc0a',
				'deliveryMemo' => $deliveryMemo,
				'deliveryDate' => $deliveryDate->format('d-m-Y'),
				'lastDeliveryDate' => $lastDeliveryDate->format('d-m-Y'),
				'next' => 'Delivered',
				'submit' => 'Save'
			]);
			$query->fromArray([
				'aid' => $actInstanceId,
				'pid' => $instanceId,
				'redirectTo' => ''
			]);

			/* Instanciate Workflow service */
			$workflow = $this->getEvent()
				->getApplication()
				->getServiceManager()
				->get('Workflow')
				->connect($this);

			/* RUN ACTIVITY: */
			$workflow->init();
			$workflow->runActivity($actInstanceId)->lastActivity;

			/*
			 $this->forward()->dispatch('Wumanager\Controller\Process', [
			 'action' => 'runactivity',
			 'id' => $actInstanceId
			 ]);
			 */
		}
	}
}
