<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'autodeliver' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/wumanager/autodeliver',
					'defaults' => array(
						'controller' => 'Wuautodeliver\Controller\Autodeliver',
						'action'=> 'index',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Wuautodeliver\Controller\Autodeliver'=>'Wuautodeliver\Controller\AutodeliverController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);
