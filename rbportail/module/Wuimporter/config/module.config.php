<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'wuimporter' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/wuimporter',
					'defaults' => array(
						'__NAMESPACE__' => 'Wuimporter\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
			),
			'flt-wuimporter-start' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/wuimporter/flt/start',
					'defaults' => array(
						'__NAMESPACE__' => 'Wuimporter\Controller',
						'controller'=> 'Flt',
						'action'=> 'start',
					),
				),
			),
			'flt-wuimporter-confirm' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/wuimporter/flt/confirm',
					'defaults' => array(
						'__NAMESPACE__' => 'Wuimporter\Controller',
						'controller'=> 'Flt',
						'action'=> 'confirm',
					),
				),
			),
			'flt-wuimporter-run' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/wuimporter/flt/run',
					'defaults' => array(
						'__NAMESPACE__' => 'Wuimporter\Controller',
						'controller'=> 'Flt',
						'action'=> 'run',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Wuimporter\Controller\Index' => 'Wuimporter\Controller\IndexController',
			'Wuimporter\Controller\Flt' => 'Wuimporter\Controller\FltController',
		),
	),
	'view_manager' => array(
		'template_map' => array(
			'layout/wuimporter'   => __DIR__ . '/../view/layout/layout.phtml',
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);
