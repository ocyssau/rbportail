<?php
namespace Wuimporter\Controller;

use Application\Dao\Factory as DaoFactory;
use Wuimporter\Form;
use Wumanager\Controller\AbstractController;
use Zend\View\Model\ViewModel;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = "wuimporter_index";

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{

		$acl = $this->getAcl();

		if($acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->startAction();
		}
		elseif($acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->startAction();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function startAction()
	{
		/* switch page */
		$view = new ViewModel();
		$request = $this->getRequest();

		/* init model */
		$form = new Form\FltImporter\StartForm();

		if ( $request->isPost()){
			$form->setData($request->getPost());
			if ($form->isValid()) {
				return $this->redirect()->toRoute('wumanager');
			}
		}

		$view->setTemplate($form->template);
		$view->workunit = $model;
		$view->title = 'Add new Workunit';
		$view->form = $form;

		return $view;
	}
}
