<?php
namespace Wuimporter\Controller;

use Application\Dao\Factory as DaoFactory;
use Wuimporter\Form;
use Wumanager\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Application\Model\People;

/**
 */
class FltController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = "wuimporter_flt";

	/**
	 */
	public function indexAction()
	{
		$acl = $this->getAcl();

		if ( $acl->hasRight($acl::RIGHT_ADMIN) ) {
			return $this->startAction();
		}
		elseif ( $acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->startAction();
		}
		else {
			return $this->notauthorized();
		}
	}

	/**
	 */
	public function startAction()
	{
		/* switch page */
		$view = new ViewModel();
		$request = $this->getRequest();

		/* init model */
		$form = new Form\FltImporter\StartForm();
		$form->setAttribute('action', $this->url()
			->fromRoute('flt-wuimporter-start'));

		if ( $request->isPost() ) {
			$post = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
			$form->setData($post);
			if ( $form->isValid() ) {
				$data = $form->getData();

				/* open file */
				$filename = $data['file']['tmp_name'];
				$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);

				/* extract datas from excel */
				$mask = new \Wuimporter\Excel\Flt\Mask();
				$extractor = new \Wuimporter\Excel\Extractor($spreadsheet, $mask);
				$extractModel = new \Wuimporter\Excel\Extractor\Model();
				$extractor->extract($extractModel);

				/* save data in session */
				$_SESSION['lastflt'] = serialize($extractModel);

				/* goto next */
				return $this->forward()->dispatch('Wuimporter\Controller\Flt', array(
					'action' => 'confirm',
					'model' => $extractModel
				));
			}
		}

		/* */
		$view->setTemplate($form->template);
		$view->title = 'Add new Workunit';
		$view->form = $form;

		return $view;
	}

	/**
	 */
	public function confirmAction()
	{
		/* switch page */
		$view = new ViewModel();
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		if ( $request->isPost() ) {
			$next = $request->getPost('run', null);
		}

		/* get forwarded extracted model */
		$extractModel = $this->params()->fromRoute('model');
		if ( !$extractModel ) {
			$extractModel = unserialize($_SESSION['lastflt']);
		}

		/* Translate datas in application */
		$collection = new \Wuimporter\Excel\Flt\WuCollection();
		$translator = new \Wuimporter\Excel\Flt\Translator($factory);
		$checker = new \Wuimporter\Excel\Flt\Checker($factory);

		/* */
		foreach( $extractModel->collections['deliverables'] as $fltDelivModel ) {
			$wu = \Wumanager\Model\Workunit::init();
			$deliverable = \Wumanager\Model\Deliverable::init();
			try {
				$translator->translate($extractModel, $fltDelivModel, $wu, $deliverable);
			}
			catch( \Exception $e ) {
				$msg = 'Unable to translate Flt to workunit with error: %s';
				$msg = sprintf($msg, $e->getMessage());
				$checker->setResult($wu, 'errors', $msg);
				$checker->setResult($wu, 'todo', 'ignore');
				$collection->add($wu);
				continue;
			}

			/* check data of model, result is put in $wu->checkerResult */
			$checker->check($wu);
			if ( isset($wu->checkerResult['existingModel']) ) {
				$existingModel = $wu->checkerResult['existingModel'];
				$existingModel->hashBefore = \Wumanager\Model\Workunit\Hasher::hash($existingModel);
				$existingModel->hydrate($translator->translatedDatas['workunit']);
				$existingModel->checkerResult = $wu->checkerResult;
				$wu = $existingModel;
				/* load associated deliverable */
				$factory->getDao($wu)->loadDeliverables($wu);
				$deliverable = current($wu->getDeliverables());
				$deliverable->hydrate($translator->translatedDatas['deliverable']);
			}
			else {
				/* set links wu->deliverable */
				$deliverable->setParent($wu);
				$wu->setDeliverables(array(
					$deliverable
				));
			}

			$collection->add($wu);
		}

		/* check deleted */
		// $checker->checkDeleted($collection, $extractModel->refFltAer);

		/* init model */
		$form = new Form\FltImporter\ConfirmForm();
		$form->setAttribute('action', $this->url()
			->fromRoute('flt-wuimporter-confirm'));
		$form->bind($collection);

		if ( $request->isPost() && $next ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* goto next */
				$_SESSION['lastworkunits'] = serialize($collection);
				$_SESSION['lastflt'] = null;
				return $this->forward()->dispatch('Wuimporter\Controller\Flt', array(
					'action' => 'run',
					'workunits' => $collection
				));
			}
		}

		$view->setTemplate($form->template);
		$view->title = 'Import Workunit Confirmation';
		$view->form = $form;

		return $view;
	}

	/**
	 */
	public function runAction()
	{
		/* switch page */
		$factory = DaoFactory::get();
		$wuDao = $factory->getDao(\Wumanager\Model\Workunit::$classId);
		$delivDao = $factory->getDao(\Wumanager\Model\Deliverable::$classId);

		/* get forwarded extracted model */
		$collection = $this->params()->fromRoute('workunits');
		if ( !$collection ) {
			$collection = unserialize($_SESSION['lastworkunits']);
		}

		/** @var \Zend\EventManager $eventManager */
		$eventManager = $this->getEvent()->getApplication()->getEventManager();

		foreach( $collection->workunits as $wu ) {
			$todo = $wu->checkerResult['todo'];
			switch ($todo) {
				case 'ignore':
					continue;
					break;
				case 'version':
					$wu = clone ($wu);
					$wu->setUpdated(new \DateTime());
					$wu->setOwner(People\CurrentUser::get());
					$wu->setVersion($wuDao->getLastVersion($wu->getName()) + 1);
					$wu->setStatus('init');
					$wu->setProgression(0);
					$wuDao->save($wu);
					foreach( $wu->getDeliverables() as $deliverable ) {
						$deliverable = clone ($deliverable);
						$deliverable->setParent($wu);
						$delivDao->save($deliverable);
					}
					$eventManager->trigger('edit.post', $wu, array());
					break;
				case 'edit':
				case 'downgrade':
					$wuDao->save($wu);
					foreach( $wu->getDeliverables() as $deliverable ) {
						if ( $deliverable->getUid() == '' ) {
							$deliverable->newUid();
						}
						$deliverable->setParent($wu);
						$delivDao->save($deliverable);
					}
					$eventManager->trigger('edit.post', $wu, array());
					break;
				default:
					$wuDao->save($wu);
					foreach( $wu->getDeliverables() as $deliverable ) {
						$deliverable->newUid();
						$deliverable->setParent($wu);
						$delivDao->save($deliverable);
					}
					break;
			}
		}

		$_SESSION['lastworkunits'] = null;
		return $this->redirect()->toRoute('wumanager');
	}
}
