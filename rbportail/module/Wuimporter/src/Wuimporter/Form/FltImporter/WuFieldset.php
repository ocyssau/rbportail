<?php
namespace Wuimporter\Form\FltImporter;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Wumanager\Model\Workunit;
use Wumanager\Model\Site;
use Application\Dao\Factory as DaoFactory;

/**
 */
class WuFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 * 
	 * @var string
	 */
	public $template;

	/**
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new Workunit());

		$this->template = 'wuimporter/fltimporter/wufieldset.phtml';

		/* HIDDEN */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* DELIVERABLE COLLECTION */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'deliverables',
			'options' => array(
				'label' => 'Deliverable',
				'count' => 0,
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Wuimporter\Form\FltImporter\DeliverableFieldset',
				),
			),
		));

		/* NAME */
		$this->add(array(
			'name' => 'name',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name',
			),
		));

		/* TITLE */
		$this->add(array(
			'name' => 'title',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Description',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Description',
			),
		));

		/* TOS */
		$this->add(array(
			'name' => 'tos',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Tos',
			),
		));

		/* SITE UID */
		$this->add(array(
			'name' => 'siteUid',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Site',
				'value_options'=> $this->_getSiteOptions(),
				'empty_option'=>'Please, select a site'
			),
		));

		/* VISIBILITY */
		$this->add(array(
			'name' => 'visibility',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Visibility',
				'value_options'=> array('internal'=>'internal', 'public'=>'public'),
				'empty_option'=>'Please, select a diffusion level'
			),
		));

		/* QUANTITY */
		$this->add(array(
			'name' => 'quantity',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '1',
				'min'=> '0',
				'placeholder' => 'Quantity',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Quantity',
			),
		));

	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => false
			),
			'title' => array(
				'required' => false
			),
			'tos' => array(
				'required' => false
			),
			'visibility' => array(
				'required' => false,
			),
			'siteUid' => array(
				'required' => false,
			),
			'quantity' => array(
				'required' => false,
			),
		);
	}

	/**
	 *
	 */
	protected function _getSiteOptions()
	{
		$ret=array();
		$list = DaoFactory::get()->getList(Site::$classId);
		foreach($list->load() as $item){
			$ret[$item['uid']] = $item['name'];
		}
		return $ret;
	}
}
