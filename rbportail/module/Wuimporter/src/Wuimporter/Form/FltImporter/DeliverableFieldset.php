<?php
namespace Wuimporter\Form\FltImporter;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Wumanager\Model\Deliverable;

/*
 * @see http://framework.zend.com/manual/2.3/en/modules/zend.form.collections.html
 */
class DeliverableFieldset extends Fieldset implements InputFilterProviderInterface
{

	protected $inputFilter;

	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new Deliverable());

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'id'
			)
		));

		$this->add(array(
			'name' => 'uid',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'uid'
			)
		));

		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Reference',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Reference'
			)
		));

		$this->add(array(
			'name' => 'section',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Section',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Section'
			)
		));

		$this->add(array(
			'name' => 'indice',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Indice',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Indice'
			)
		));

		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => 'Comments',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Comments'
			)
		));

		$this->add(array(
			'name' => 'team',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Team/Data Path',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Team/Data Path'
			)
		));

	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'uid' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			),
			'section' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			),
			'indice' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 10
						)
					)
				)
			),
			'comment' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
			),
			'team' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
			),
		);
	}

	/**
	 *
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function render(\Zend\View\Model\ViewModel $view)
	{
		$view->form = $this;
		$view->setTemplate($this->template);
		return $view;
	}
}
