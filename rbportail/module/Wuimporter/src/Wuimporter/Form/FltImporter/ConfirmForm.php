<?php
namespace Wuimporter\Form\FltImporter;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/**
 */
class ConfirmForm extends Form implements InputFilterProviderInterface
{

	public $template;

	/**
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('flt_import_start');
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->template = 'wuimporter/fltimporter/confirmform.phtml';

		/* WU COLLECTION */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'workunits',
			'options' => array(
				'label' => 'Workunits',
				'count' => 0,
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Wuimporter\Form\FltImporter\WuFieldset',
				),
			),
		));

		/* BUTTONS */
		$this->add(array(
			'name' => 'run',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Next',
				'id' => 'nextbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}
}
