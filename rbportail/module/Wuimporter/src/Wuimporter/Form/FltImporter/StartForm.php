<?php
namespace Wuimporter\Form\FltImporter;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\InputFilter\InputFilter;
//use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 */
class StartForm extends Form implements InputFilterProviderInterface
{

	public $template;

	/**
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('flt_import_start');
		$this->setAttribute('method', 'post')
			//->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->template = 'wuimporter/fltimporter/startform.phtml';

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'file',
			'type' => 'Zend\Form\Element\File',
			'attributes' => array(
				'placeholder' => 'Flt'
			),
			'options' => array(
				'label' => 'Select Flt File'
			)
		));

		$this->add(array(
			'name' => 'next',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Next',
				'id' => 'nextbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'file' => array(
				'required' => true
			)
		);
	}
}
