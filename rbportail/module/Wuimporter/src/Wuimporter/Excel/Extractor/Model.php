<?php
// %LICENCE_HEADER%
namespace Wuimporter\Excel\Extractor;

/**
 */
class Model
{

	/**
	 * Set to true if at least one property is not null
	 */
	public $isNotEmpty = false;

	/**
	 *
	 * @var array
	 */
	public $properties = array();

	/**
	 *
	 * @var array
	 */
	public $collections = array();


	/**
	 *
	 * @var string $name
	 * @var string $value
	 * @return Model
	 */
	public function __get($name)
	{
		if(isset($this->properties[$name])){
			return $this->properties[$name];
		}
	}


	/**
	 *
	 * @var string $name
	 * @var string $value
	 * @return Model
	 */
	public function set($name, $value)
	{
		if (!empty($value)) {
			$this->isNotEmpty = true;
		}
		$this->properties[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @var string $name
	 * @var array $collection
	 * @return Model
	 */
	public function addCollection($name, $collection)
	{
		$this->isNotEmpty = true;
		$this->collections[$name] = $collection;
		return $this;
	}


} /* End of class */
