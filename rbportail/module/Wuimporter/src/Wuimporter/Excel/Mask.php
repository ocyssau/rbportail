<?php
// %LICENCE_HEADER%

namespace Wuimporter\Excel;

/**
 *
 *
 */
class Mask
{

	/**
	 * @var array
	 */
	protected $_map;

	/**
	 * @var array
	 *
	 * array(
	 * 	sheetName=>[propertyName=>array(col, row, filter)]
	 * )
	 *
	 */
	public static $map = array();

	/**
	 *
	 */
	public function __construct()
	{
		$this->_map = static::$map;
	}

	/**
	 *
	 */
	public function getMap()
	{
		return $this->_map;
	}

	/**
	 *
	 */
	public function setMap($map)
	{
		$this->_map = $map;
		return $this;
	}
} /* End of class */
