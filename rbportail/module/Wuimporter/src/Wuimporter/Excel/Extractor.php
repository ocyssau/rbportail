<?php
// %LICENCE_HEADER%
namespace Wuimporter\Excel;

/**
 */
class Extractor
{

	/**
	 *
	 * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
	 */
	protected $excelWorkbook;

	/**
	 *
	 * @var \Wuimporter\Excel\Flt\Mask
	 */
	protected $mask;


	/**
	 *
	 * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $excelWorkbook
	 * @param \Wuimporter\Excel\Flt\Mask $mask
	 */
	public function __construct($excelWorkbook, $mask)
	{
		$this->excelWorkbook = $excelWorkbook;
		$this->mask = $mask;
	}

	/**
	 *
	 * @param \Wuimporter\Excel\Extractor\Model $model
	 * @return \Wuimporter\Excel\Extractor
	 */
	public function extract($model)
	{
		$excelWorkbook = $this->excelWorkbook;
		$mask = $this->mask;

		foreach( $mask->getMap() as $sheetMap ) {
			$excelSheet = $excelWorkbook->getSheetByName($sheetMap['name']);
			if ( isset($sheetMap['properties']) ) {
				$this->extractProperties($excelSheet, $sheetMap['properties'], $model);
			}
			if ( isset($sheetMap['collections']) ) {
				foreach( $sheetMap['collections'] as $collectionMap ) {
					$this->extractCollection($excelSheet, $collectionMap, $model);
				}
			}
		}
		return $this;
	}

	/**
	 *
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $excelSheet
	 * @param array $propertyMap
	 * @param \Wuimporter\Excel\Extractor\Model $model
	 * @return \Wuimporter\Excel\Extractor
	 */
	protected function extractProperty($excelSheet, $propertyMap, $model)
	{
		$row = $propertyMap['row'];
		$col = $propertyMap['col'];
		$name = $propertyMap['name'];
		$model->set($name, $excelSheet->getCellByColumnAndRow($col, $row)->getFormattedValue());
		return $this;
	}

	/**
	 *
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $excelSheet
	 * @param array $propertiesMap
	 * @param \Wuimporter\Excel\Extractor\Model $model
	 * @return \Wuimporter\Excel\Extractor
	 */
	protected function extractProperties($excelSheet, $propertiesMap, $model)
	{
		foreach( $propertiesMap as $propertyMap ) {
			$this->extractProperty($excelSheet, $propertyMap, $model);
		}
		return $this;
	}

	/**
	 *
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $excelSheet
	 * @param array $collectionMap
	 * @param \Wuimporter\Excel\Extractor\Model $model
	 * @return \Wuimporter\Excel\Extractor
	 */
	protected function extractCollection($excelSheet, $collectionMap, $model)
	{
		$firstRow = $collectionMap['rows'][0];
		$lastRow = $collectionMap['rows'][1];
		$name = $collectionMap['name'];
		$propertiesMap = $collectionMap['properties'];
		$item = new \Wuimporter\Excel\Extractor\Model();

		$row = $firstRow;
		$collection = array();
		while( $row < $lastRow ) {
			$item = new \Wuimporter\Excel\Extractor\Model();
			foreach( $propertiesMap as $propertyMap ) {
				$propertyMap['row'] = $row;
				$this->extractProperty($excelSheet, $propertyMap, $item);
			}
			if($item->isNotEmpty){
				$collection[] = $item;
			}
			$row++;
		}

		$model->addCollection($name,$collection);

		return $this;
	}
} /* End of class */
