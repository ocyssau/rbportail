<?php
// %LICENCE_HEADER%
namespace Wuimporter\Excel\Flt;

use Application\Model\Any;

/**
 * 
 *
 */
class WuCollection extends Any implements \Countable
{

	/**
	 * 
	 * @var string
	 */
	static $classId = 'wucollection';

	/**
	 *
	 * @var array
	 */
	public $workunits = array();

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['workunits'])) ? $this->workunits = $properties['workunits'] : null;
		return $this;
	}

	/**
	 *
	 * @param \Wumanager\Model\Workunit $wu
	 */
	public function add($wu)
	{
		$this->workunits[$wu->getUid()] = $wu;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->workunits);
	}

	/**
	 * @return array
	 */
	public function getIterator()
	{
		return $this->workunits;
	}
}
