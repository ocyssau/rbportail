<?php
// %LICENCE_HEADER%
namespace Wuimporter\Excel\Flt;

/**
 */
class Mask extends \Wuimporter\Excel\Mask
{

	/**
	 *
	 * @var array array(
	 *      array col, row, filter
	 *      )
	 *
	 */
	public static $map = array(
		'FLT' => array(
			'name' => 'FLT',
			'index' => 0,
			'properties' => array(
				'refFltAer' => array(
					'name' => 'refFltAer',
					'col' => 2,
					'row' => 2,
					'filter' => 'string'
				),
				'issue' => array(
					'name' => 'issue',
					'col' => 4,
					'row' => 2,
					'filter' => 'string'
				),
				'redactor' => array(
					'name' => 'redactor',
					'col' => 8,
					'row' => 2,
					'filter' => 'string'
				),
				'pm' => array(
					'name' => 'pm',
					'col' => 2,
					'row' => 4,
					'filter' => 'string'
				),
				'modif' => array(
					'name' => 'modif',
					'col' => 2,
					'row' => 5,
					'filter' => 'string'
				),
				'version' => array(
					'name' => 'version',
					'col' => 2,
					'row' => 6,
					'filter' => 'string'
				),
				'type' => array(
					'name' => 'type',
					'col' => 2,
					'row' => 8,
					'filter' => 'string'
				),
				'description' => array(
					'name' => 'description',
					'col' => 4,
					'row' => 4,
					'filter' => 'string'
				),
				'dateMajFlt' => array(
					'name' => 'dateMajFlt',
					'col' => 4,
					'row' => 3,
					'filter' => 'string'
				),
				'dateCreationFlt' => array(
					'name' => 'dateCreationFlt',
					'col' => 2,
					'row' => 3,
					'filter' => 'string'
				)
			)
		),
		'FLT_LIV' => array(
			'name' => 'FLT_LIV',
			'index' => 1,
			'properties' => array(),
			'collections' => array(
				array(
					'name' => 'deliverables',
					'rows' => array(
						5,
						300
					),
					'properties' => array(
						'issue' => array(
							'name' => 'issue',
							'col' => 1,
							'row' => null,
							'filter' => 'string'
						),
						'number' => array(
							'name' => 'number',
							'col' => 2,
							'row' => null,
							'filter' => 'string'
						),
						'description' => array(
							'name' => 'description',
							'col' => 4,
							'row' => null,
							'filter' => 'string'
						),
						'reference' => array(
							'name' => 'reference',
							'col' => 6,
							'row' => null,
							'filter' => 'string'
						),
						'uo' => array(
							'name' => 'uo',
							'col' => 7,
							'row' => null,
							'filter' => 'string'
						),
						'quantity' => array(
							'name' => 'quantity',
							'col' => 8,
							'row' => null,
							'filter' => 'string'
						),
						'besoinAer' => array(
							'name' => 'besoinAer',
							'col' => 9,
							'row' => null,
							'filter' => 'string'
						),
						'codesRsCgVault' => array(
							'name' => 'codesRsCgVault',
							'col' => 10,
							'row' => null,
							'filter' => 'string'
						)
					)
				)
			)
		)
	);
} /* End of class */
