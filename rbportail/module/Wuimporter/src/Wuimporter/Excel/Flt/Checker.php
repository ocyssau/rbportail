<?php
namespace Wuimporter\Excel\Flt;

use Wumanager\Model\Workunit;
use Wumanager\Model\Wumanager;
use Wumanager\Helper\Issue;

/**
 * Helper to check existance of a entity
 */
class Checker
{

	/**
	 *
	 * @param \Application\Dao\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * ref is existing ?
	 *
	 * If exisiting, populate property $model->checkerResult['existingModel'].
	 * Try to load from name as "AA-Z1234A-%-123" where % replace the issue number.
	 * Get only the last version.
	 *
	 *
	 * @param \Wumanager\Model\Workunit $wu
	 */
	public function notExisting($model)
	{
		try {
			$class = get_class($model);
			$existingModel = new $class();
			$name = $model->getName();
			$t = explode('-', $name);
			$found = $t[0] . '-' . $t[1] . '-%-' . $t[3];
			$this->factory->getDao($model)->load($existingModel, "name LIKE '" . $found . "' ORDER BY version,id DESC LIMIT 1");
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			return true;
		}
		catch( \Exception $e ) {
			throw $e;
		}
		$model->checkerResult['existingModel'] = $existingModel;
		throw new \Rbplm\Dao\ExistingException('data is existing');
	}

	/**
	 * Check workunit
	 * Populate property $model->checkerResult with result
	 *
	 * @param \Wumanager\Model\Workunit $model
	 *
	 */
	public function check($model)
	{
		try {
			$this->notExisting($model);
			$model->checkerResult['todo'] = 'create';
		}
		catch( \Rbplm\Dao\ExistingException $e ) {
			$this->setResult($model, 'feedbacks', 'Data is existing with the same ref in db');
			$this->setResult($model, 'todo', 'edit');
		}

		/* */
		if ( $model->checkerResult['todo'] == 'edit' ) {
			$existingModel = $model->checkerResult['existingModel'];
			$previousIssue = Issue::extractFromName($existingModel->getName());
			$nextIssue = Issue::extractFromName($model->getName());
			if ( $model->getQuantity() == 0 ) {
				$this->setResult($model, 'todo', 'ignore');
				$this->setResult($model, 'alerts', 'Quantity 0');
			}
			elseif ( $nextIssue == $previousIssue ) {
				$this->setResult($model, 'todo', 'ignore');
				$this->setResult($model, 'alert', 'Same issue');
			}
			elseif ( $nextIssue < $previousIssue ) {
				$this->setResult($model, 'todo', 'ignore');
				$this->setResult($model, 'alert', 'the imported version is inferior to existing version');
			}
			else {
				$this->setResult($model, 'todo', 'edit');
				if( $this->toVersioned($existingModel) ){
					$this->setResult($model, 'alert', 'The current Workunit beyond the deliverable status. A new version must be created');
					$this->setResult($model, 'todo', 'version');
				}
				if ( $model->getQuantity() == 0 ) {
					$this->setResult($model, 'alert', 'Quantity is null, this workunit must be manually deleted after import');
				}
			}
		}
		elseif ( $model->checkerResult['todo'] == 'create' ) {
			$model->setVisibility('public');
			$model->setSite('sierblagnac');
			if ( $model->getQuantity() == 0 ) {
				$this->setResult($model, 'alerts', 'Try to create with quantity 0');
				$this->setResult($model, 'todo', 'ignore');
			}
		}
	}

	/**
	 *
	 * @param \Wumanager\Model\Workunit $model
	 * @param string $type
	 *        	errors|feedbacks|alerts|todo
	 * @param string $msg
	 * @return Checker
	 */
	public function setResult($model, $type, $msg)
	{
		if ( !isset($model->checkerResult) ) {
			$model->checkerResult = array(
				'errors' => array(),
				'feedbacks' => array(),
				'alerts' => array(),
				'todo' => ''
			);
		}

		switch ($type) {
			case 'error':
			case 'errors':
				$model->checkerResult['errors'][] = $msg;
				break;
			case 'feedback':
			case 'feedbacks':
				$model->checkerResult['feedbacks'][] = $msg;
				break;
			case 'alert':
			case 'alerts':
				$model->checkerResult['alerts'][] = $msg;
				break;
			case 'todo':
				$model->checkerResult['todo'] = $msg;
				break;
		}
		return $this;
	}

	/**
	 * Check workunit
	 * Populate property $model->checkerResult with result
	 *
	 * @param \Wuimporter\Excel\Flt\WuCollection $collection
	 *
	 */
	public function checkDeleted($collection, $fltNumber)
	{
		throw new \Exception('Function is not implemented');

		$deleted = false;
		$c = 0;

		/* get before workunits of flt */
		$list = $this->factory->getList(\Wumanager\Model\Workunit::$classId);
		$k = $list->countAll('name LIKE :fltNumber', array(
			':fltNumber' => $fltNumber . '%'
		));

		$c = $collection->count();

		if ( $k != $c ) {
			$deleted = true;
		}

		$collection->deleted = $deleted;
		$collection->deletedCount = $c + $k;

		return $deleted;
	}

	/**
	 *
	 * @param Workunit $model
	 * @return boolean
	 */
	public function toVersioned($model)
	{
		$status = $model->getStatus();
		if ( $status == 'Delivered' || $status == 'Accepted' || $status == 'Attente facturation' || $status == 'Facturee'){
			return true;
		}
		else{
			return false;
		}
	}
} /* End of class */
