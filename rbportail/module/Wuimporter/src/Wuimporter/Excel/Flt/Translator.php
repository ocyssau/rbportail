<?php
namespace Wuimporter\Excel\Flt;

use DateTime;
use DateInterval;

/**
 */
class Translator
{

	/**
	 *
	 * @var \Application\Dao\Factory
	 */
	protected $daoFactory;

	/**
	 * Array['workunit'=>array, 'deliverable'=>array]
	 *
	 * @var array
	 */
	public $translatedDatas;

	/**
	 *
	 * @param \Application\Dao\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->daoFactory = $factory;
	}

	/**
	 * Date format use by db
	 *
	 * @var string
	 */
	const DATE_FORMAT = 'n#j#Y';

	/**
	 * @param \Wuimporter\Excel\Extractor\Model $wuFltModel
	 * @param \Wuimporter\Excel\Extractor\Model $delivFltModel
	 * @param \Wumanager\Model\Workunit $wu
	 * @param \Wumanager\Model\Deliverable $deliverable
	 * @return Translator
	 */
	public function translate($wuFltModel, $delivFltModel, $wu, $deliverable)
	{
		$wudata = array();
		$delivdata = array();

		$wudata['title'] = $wuFltModel->description;
		$wudata['tos'] = $wuFltModel->refFltAer . '-' . $delivFltModel->issue;
		$wudata['name'] = $wudata['tos'] . '-' . self::formatDeliverableNum($delivFltModel->number);
		$wudata['version'] = $delivFltModel->issue;
		$wudata['number'] = (int)$delivFltModel->number;
		$wudata['flt'] = $wuFltModel->refFltAer;
		$wudata['ataLeader'] = $wuFltModel->redactor;
		$wudata['openDate'] = $this->dateToApp($wuFltModel->dateMajFlt);
		$wudata['planDate'] = $this->dateToApp($delivFltModel->besoinAer);
		if ( $wudata['planDate'] ) {
			$deliveryDate = clone ($wudata['planDate']);
			$deliveryDate->sub(new DateInterval('P1W'));
			$wudata['sitedeliveryDate'] = $deliveryDate;
		}

		/* Deliverable reference */
		$d = self::extractDeliverableRef($delivFltModel->reference, $delivFltModel->uo);
		$delivdata['name'] = $d['name'];
		$delivdata['indice'] = $d['indice'];

		/**/
		$delivdata['comment'] = $this->extractCodeRsCg($delivFltModel->codesRsCgVault);
		$delivdata['projeteur'] = 'TBC';
		$delivdata['description'] = $delivFltModel->description;

		/* Layout */
		if ( $wuFltModel->version ) {
			$delivdata['layout'] = $wuFltModel->version;
		}
		else {
			$delivdata['layout'] = $wuFltModel->type;
		}

		/* Mod/Pm */
		if ( $wuFltModel->pm ) {
			$delivdata['mod'] = $wuFltModel->modif . '/' . $wuFltModel->pm;
		}
		else {
			$delivdata['mod'] = $wuFltModel->modif;
		}

		/* Team */
		$delivdata['team'] = $this->extractTeam($delivFltModel->codesRsCgVault);

		/* Quantity */
		$delivdata['quantity'] = $delivFltModel->quantity;
		$wudata['quantity'] = $delivFltModel->quantity;

		/* Section */
		$delivdata['section'] = 'LEGACY';

		/* Hydrate the application models */
		$wu->hydrate($wudata);
		$deliverable->hydrate($delivdata);

		/* Set Type of wu */
		$type = $this->getType($delivFltModel->uo);
		$wu->setType($type);

		$this->translatedDatas = array(
			'workunit' => $wudata,
			'deliverable' => $delivdata
		);

		return $this;
	}

	/**
	 *
	 * @param string $comment
	 */
	public static function extractCodeRsCg($comment)
	{
		return "";
	}

	/**
	 *
	 * @param string $comment
	 */
	public static function extractTeam($comment)
	{
		return "";
	}

	/**
	 *
	 * @param string $uo
	 */
	public static function extractSiteFromUo($uo)
	{
		return 'sierblagnac';
	}

	/**
	 *
	 * @param string $uo
	 */
	public function getType($uo)
	{
		$factory = $this->daoFactory;
		//$uo = 'UO ' . strtoupper($uo);
		//$uo = 'WU ' . strtoupper($uo);
		$uo = 'WoUn ' . strtoupper($uo); //2022
		$type = new \Wumanager\Model\Workunit\Type();
		$dao = $factory->getDao(\Wumanager\Model\Workunit\Type::$classId);
		$dao->loadFromUid($type, $uo);
		return $type;
	}
	
	/**
	 *
	 * @param string $ref
	 * @param string $uo
	 * @return string
	 */
	public static function extractDeliverableRef($ref, $uo)
	{
		$type = substr($uo, 0, 1);
		if ( $type == 'L' ) {
			return array(
				'name' => substr($ref, 0, 9),
				'indice' => substr($ref, -3, 3)
			);
		}
		else {
			return array(
				'name' => $ref,
				'indice' => null
			);
		}
	}

	/**
	 *
	 * @param string $num
	 * @return string
	 */
	public static function formatDeliverableNum($num)
	{
		if ( strlen($num) < 4 ) {
			return sprintf("%'03d", $num);
		}
		else {
			return $num;
		}
	}

	/**
	 *
	 * @param integer|DateTime $date
	 * @return \DateTime
	 */
	public static function dateToApp($date)
	{
		if ( !$date or substr($date, 0, 4) == '0000' ) {
			return null;
		}
		elseif ( $date instanceof DateTime ) {
			return $date;
		}
		else {
			$date = str_replace('/', '-', $date);
			return DateTime::createFromFormat('n-j-Y|', $date);
		}
	}
} /* End of class */
