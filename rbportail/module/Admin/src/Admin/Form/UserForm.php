<?php
namespace Admin\Form;

use Zend\XmlRpc\Value\ArrayValue;

use Zend\Form\Form;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilter;

use Application\Model\Acl\Acl;

class UserForm extends Form implements InputFilterProviderInterface
{
	public function __construct(Acl $acl)
	{
		// we want to ignore the name passed
		parent::__construct('user');
		
		$this->setAttribute('method', 'post')
				->setAttribute('autocomplete', 'off')
				->setHydrator(new Hydrator(false))
				->setInputFilter(new InputFilter());
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'firstname',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder'=>'First Name',
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'First Name',
			),
		));
		$this->add(array(
			'name' => 'lastname',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder'=>'Last Name',
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Last Name',
			),
		));
		$this->add(array(
			'name' => 'login',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder'=>'Login',
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Connexion Name',
			),
		));
		$this->add(array(
			'name' => 'mail',
			'type'  => 'Zend\Form\Element\Email',
			'attributes' => array(
				'placeholder'=>'Mail'
			),
			'options' => array(
				'label' => 'Mail',
			),
		));
		$this->add(array(
			'name' => 'password1',
			'type'  => 'Zend\Form\Element\Password',
			'attributes' => array(
				'placeholder'=>'Password',
				'autocomplete'=>'off',
			),
			'options' => array(
				'label' => 'Password',
			),
		));
		$this->add(array(
			'name' => 'password2',
			'type'  => 'Zend\Form\Element\Password',
			'attributes' => array(
				'placeholder'=>'Confirm Password',
				'autocomplete'=>'off',
			),
			'options' => array(
				'label' => 'Retype Password',
			),
		));
		$this->add(array(
			'name' => 'password',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'memberof',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder'=>'Role',
			),
			'options' => array(
				'label' => 'Role',
				'value_options'=> $this->_getRoleOptions($acl),
				'empty_option'=>'Please, select a role'
			),
		));
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}
	
	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			),
			'firstname' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
			'lastname' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
			'login' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
			'mail' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			),
			'password1' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			),
			'password2' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			),
			'memberof' => array(
				'required' => true,
			),
				
		);
	}
	
	/**
	 * @param Acl
	 * @return Array
	 */
	protected function _getRoleOptions($acl)
	{
		$ret = array();
		foreach($acl->roles as $id=>$def){
			$ret[$id]=$def[0];
		}
		return $ret;
	}
	
}
