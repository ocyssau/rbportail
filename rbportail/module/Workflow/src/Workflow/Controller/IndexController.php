<?php

namespace Workflow\Controller;

use Zend\View\Model\ViewModel;

use Application\Model\People;
use Application\Dao\Filter;
use Application\Dao\Factory as DaoFactory;

use Application\Form\PaginatorForm;
use Application\Form\StdFilterForm;

use Workflow\Model;
use Workflow\Model\Wf;
use Workflow\Form;
use Exception;


class IndexController extends AbstractController
{
	/**
	 * Display instance of all process
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
    	$view = new ViewModel();
    	$request = $this->getRequest();

    	$userId = People\CurrentUser::get()->getLogin();
    	$userLogin = People\CurrentUser::get()->getLogin();

    	$filter = new StdFilterForm();
    	$filter->setData($request->getPost());
    	$filter->key = 'CONCAT(procInst.name,procInst.pId)';
    	$filter->passThrough = true;
    	$filter->prepare();

    	//search from header search area :
    	$bind = array();
    	$list = DaoFactory::get()->getList(Wf\Instance::$classId);

    	$paginator = new PaginatorForm();
    	$paginator->setMaxLimit($list->countAll(""));
    	$paginator->setData($request->getPost());
    	$paginator->setData($request->getQuery());
    	$paginator->prepare()->bindToView($view);

    	$sql  = "SELECT * FROM galaxia_instances AS procInst";
    	$sql .= " LEFT OUTER JOIN galaxia_processes AS proc ON proc.pId=procInst.pId";
    	$sql .= " WHERE (procInst.owner=:ownerId)";

    	$bind[':ownerId'] = '99999999-9999-9999-9999-00000000abcd';

    	if($filter->where){
    		$sql .= ' AND ' . $filter->where;
    		$bind = array_merge($bind, $filter->bind);
    	}
    	$sql .= $paginator->toSql();

    	$list->loadFromSql($sql, $bind);
    	$view->myProcessInstance = $list;

    	$view->headers = array(
    		'#'=>'id',
    		'Name'=>'name',
    		'Started'=>'started',
    		'Ended'=>'ended',
    	);

    	$view->filter = $filter;
    	$view->title = 'My process instances';
    	return $view;
    }

    //ajax method
    public function getnextAction()
    {
    	$id = $this->params()->fromRoute('id');
    	$request = $this->getRequest();
    	$view = new ViewModel();

    	if ($this->getRequest()->isXmlHttpRequest()) {
    		$view->setTerminal(true);
    	}

    	//check authorization
    	//@todo

    	$process = new Wf\Process();
    	$factory = DaoFactory::get();
    	$factory->getDao($process)->loadFromId($process, $id);

    	$processInstance = new Wf\Instance();
    	$factory->getDao($processInstance)->loadFromId($processInstance, $id);

    	//Get next candidates activities
    	$runningList = $factory->getDao($processInstance)->getRunningActivities($processInstance->getId());
    	foreach($runningList as $properties){
    		$return[] = $properties;
    	}

    	$view->activities = $return;
    	return $view;
    }

}

