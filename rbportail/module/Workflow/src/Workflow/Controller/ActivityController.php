<?php

namespace Workflow\Controller;

use Zend\View\Model\ViewModel;

use Application\Model\People;
use Application\Dao\Filter;

use Workflow\Model;
use Workflow\Model\Wf;
use Workflow\Dao;
use Workflow\Form;

class ActivityController extends AbstractController
{

	const SIGNAL_DELETEACTIVITY_PRE = 'deleteactivity.pre';
	const SIGNAL_DELETEACTIVITY_POST = 'deleteactivity.post';
	
	
    public function indexAction()
    {
        return new ViewModel();
    }

    public function editAction()
    {
    	$appAcl = People\CurrentUser::get()->appacl;
    	if(!$appAcl->hasRight($appAcl::RIGHT_ADMIN)){
    		return $this->notauthorized();
    	}
        return new ViewModel();
    }

    public function createAction()
    {
    	$appAcl = People\CurrentUser::get()->appacl;
    	if(!$appAcl->hasRight($appAcl::RIGHT_ADMIN)){
    		return $this->notauthorized();
    	}
        return new ViewModel();
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function deleteAction()
    {
    	$view = new ViewModel();
    	if ($this->getRequest()->isXmlHttpRequest()) {
    		$view->setTerminal(true);
    	}

    	$activityId = $this->params()->fromRoute('id');
    	
    	$appAcl = People\CurrentUser::get()->appacl;
    	if(!$appAcl->hasRight($appAcl::RIGHT_ADMIN)){
    		return $this->notauthorized();
    	}
		
    	$workflow = $this->getEvent()->getApplication()->getServiceManager()->get('Workflow')->connect($this);
    	$activity = $workflow->deleteActivity($activityId);
    	
    	$view->object = $activity;
    	return $view;
    }
    
    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function runAction()
    {
    	$appAcl = People\CurrentUser::get()->appacl;
    	if(!$appAcl->hasRight($appAcl::RIGHT_CONSULT)){
    		return $this->notauthorized();
    	}

    	$view = new ViewModel();
    	if ($this->getRequest()->isXmlHttpRequest()) {
    	    $view->setTerminal(true);
    	}
    	$actInstId = $this->params()->fromRoute('id');
    	
    	//instanciate Workflow service
    	$Workflow = new \Workflow\Service\Workflow();

    	//RUN ACTIVITY:
    	$act = $Workflow->runActivity($actInstId)->lastActivity;
    	if($act->getType() == 'end'){
    		$Workflow->complete($Workflow->instance->getId());
    	}
    	else{
    		//TRANSLATE :
    		$Workflow->translateActivity($actInstId);
    	}
    	
    	return $this->redirect()->toRoute('workflow');
    }
    
}
