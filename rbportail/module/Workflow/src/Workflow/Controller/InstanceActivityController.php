<?php
namespace Workflow\Controller;

use Zend\View\Model\ViewModel;

use Application\Model\People;
use Application\Form\PaginatorForm;
use Application\Dao\Factory as DaoFactory;

use Workflow\Model\Wf;
use Workflow\Form\InstanceFilterForm as Filter;
use Exception;

class InstanceActivityController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_CONSULT)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();

		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();

		$instanceId = $this->params()->fromRoute('id');

		$filter = $this->_getFilter('workflow/instanceActivity/filter');
		$filter->get('stdfilter-id')->setValue($instanceId);
		$filter->prepare();
		$filter->saveToSession('workflow/instanceActivity/filter');

		$bind = array();
		$list = DaoFactory::get()->getList(Wf\Instance\Activity::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$table = DaoFactory::get()->getTable(Wf\Instance\Activity::$classId);
		$sql = "SELECT * FROM $table";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->list = $list;
		$view->headers = array(
			'#'=>'id',
			'Name'=>'name',
			'Description'=>'title',
			'Owner'=>'ownerId',
			'Started'=>'started',
			'Ended'=>'ended',
			'Status'=>'status',
			'Process Instance'=>'instanceId',
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$aInstanceId = $this->params()->fromRoute('id');
		$aInstanceDao = DaoFactory::get()->getDao(Wf\Instance\Activity::$classId);
		$aInstanceDao->deleteFromId($aInstanceId);
		return $this->redirect()->toRoute('ainstance');
	}

	/**
	 *
	 */
	public function editAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$actInstId = $this->params()->fromRoute('id');
		$workflow = $this->getEvent()->getApplication()->getServiceManager()->get('Workflow')->connect($this);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new Filter();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = 'CONCAT_WS(name, title, status, id, ownerId)';
		$filter->key2 = 'instanceId';
		return $filter;
	}

}
