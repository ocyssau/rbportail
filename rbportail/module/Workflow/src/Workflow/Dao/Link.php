<?php
// %LICENCE_HEADER%
namespace Workflow\Dao;

use Application\Dao\Connexion;

class Link extends \Application\Dao\Link
{

	/**
	 */
	public function loadChildren($mapped)
	{
		$table = static::$table;
		if ( !$this->loadChildrenStmt ) {
			$sql = "SELECT * FROM $table WHERE lparent=:parentId";
			$this->loadChildrenStmt = $this->connexion->prepare($sql);
			$this->loadChildrenStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		$this->loadChildrenStmt->execute(array(
			':parentId' => $mapped->id
		));
		while( $row = $this->loadChildrenStmt->fetch() ) {
			$lnk = new Link();
			$mapped->links[$lnk->uid] = $lnk;
		}
		return $mapped;
	}
} //End of class
