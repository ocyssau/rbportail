<?php
//%LICENCE_HEADER%

namespace Workflow\Dao;

use Application\Dao\Dao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW anywf AS
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_process
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_activity
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_instance
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_instance_activity;
 <<*/

/** SQL_DROP>>
 <<*/

class Any extends Dao
{

	/**
	 * @var string
	 */
	public static $table='wf_any';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updateById'=>'updateById',
		'updated'=>'updated',
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge($this->metaModel, self::$sysToApp);
		$this->metaModelFilters = array_merge($this->metaModelFilters, self::$sysToAppFilter);
	}


	/**
	 * @param Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
			':ownerId'=>$mapped->getOwner(true),
			':parentId'=>$mapped->getParent(true),
			':parentUid'=>$mapped->getParentUid(),
			':updateById'=>$mapped->getUpdateBy(true),
			':updated'=>$mapped->getUpdated(self::DATE_FORMAT),
		);
	}

	/**
	 * Force $witchild to true
	 * @return Dao
	 */
	public function delete($uid, $withChildren=true, $withTrans=null)
	{
		return parent::delete($uid, true, $withTrans);
	}

	/**
	 * Recursive function
	 * @return Dao
	 */
	public function deleteFromId($id, $withChildren=true, $withTrans=null)
	{
		$filter = 'id=:id';
		$bind = array(':id'=>$id);
		return $this->deleteFromFilter($filter, $bind, $withChildren, $withTrans);
	}

}
