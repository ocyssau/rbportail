<?php
//%LICENCE_HEADER%

namespace Workflow\Dao\Wf;

use Workflow\Dao\Any;

/** SQL_SCRIPT>>
CREATE TABLE wf_activity(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
		`normalizedName` varchar(256) NULL,
		`isInteractive` boolean default 0,
		`isAutorouted` boolean default 0,
		`isAutomatic` boolean default 0,
		`isComment` boolean default 0,
		`type` varchar(32) NULL,
		`processId` integer NULL,
		`progression` float NULL,
		`expirationTime` datetime NULL,
		`roles` text NULL,
		`attributes` text NULL,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE `wf_activity` ADD UNIQUE (uid);
ALTER TABLE `wf_activity` ADD UNIQUE (`name` ,`processId`);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_normalizedName` (`normalizedName` ASC);

ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_uid` (`uid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_name` (`name` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_parentid` (`parentId` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_cid` (`cid` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_processId` (`processId` ASC);
ALTER TABLE `wf_activity` ADD INDEX `WFACTIVITY_roles` (roles(100) ASC);


ALTER TABLE `wf_activity` ADD progression FLOAT NULL DEFAULT NULL AFTER processId, ADD INDEX ( progression );

<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE wf_activity CASCADE;
 <<*/

/**
 *
 */
class Activity extends Any
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'wf_activity';

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'title'=>'title',
		'normalizedName'=>'normalizedName',
		'processId'=>'processId',
		'progression'=>'progression',
		'isInteractive'=>'isInteractive',
		'isAutorouted'=>'isAutorouted',
		'isAutomatic'=>'isAutomatic',
		'isComment'=>'isComment',
		'type'=>'type',
		'expirationTime'=>'expirationTime',
		'attributes'=>'attributes',
		'roles'=>'roles'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Workflow\Model\Wf\Activity $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':title'=>$mapped->getTitle(),
			':normalizedName'=>$mapped->getNormalizedName(),
			':processId'=>(integer) $mapped->getProcess(true),
			':progression'=>$mapped->getProgression(),
			':isInteractive'=>(integer) $mapped->isInteractive(),
			':isAutorouted'=>(integer) $mapped->isAutorouted(),
			':isAutomatic'=>(integer) $mapped->isAutomatic(),
			':isComment'=>(integer) $mapped->isComment(),
			':type'=>$mapped->getType(),
			':expirationTime'=>$mapped->getExpirationTime(self::DATE_FORMAT),
			':attributes'=>json_encode($mapped->getAttributes()),
			':roles'=>json_encode($mapped->getRoles())
		));
	}

	/**
	 * Delete all activities of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId, $withTrans=null)
	{
		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;
		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE processId=:processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
			if($withTrans) $this->connexion->commit();

		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

} //End of class

