<?php
//%LICENCE_HEADER%

namespace Workflow\Dao\Wf;

use Rbplm\Dao\MappedInterface;
use Workflow\Dao\Any;
use Workflow\Model;
use Application\Model\Signal;
use Application\Model\Error;
use Exception;

/** SQL_SCRIPT>>
CREATE TABLE wf_instance(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NULL,
	`title` VARCHAR(255) NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
		`processId` int NULL,
		`status` VARCHAR(64) NULL,
		`nextActivities` TEXT NULL,
		`nextUsers` TEXT NULL,
		`attributes` TEXT NULL,
		`started` datetime NULL,
		`ended` datetime NULL,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE `wf_instance` ADD UNIQUE (uid);

ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_uid` (`uid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_name` (`name` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_parentuid` (`parentUid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_parentid` (`parentId` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_cid` (`cid` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_ownerId` (`ownerId` ASC);

ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_processId` (`processId` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_nextActivities` (nextActivities(100) ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_nextUsers` (nextUsers(100) ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_started` (`started` ASC);
ALTER TABLE `wf_instance` ADD INDEX `WFINSTANCE_ended` (`ended` ASC);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class for \Workflow\Dao\Wf\Instance
 *
 * See the examples: Rbplm/Wf/InstanceTest.php
 *
 * @see \Workflow\Model\Dao\Pg
 * @see \Workflow\Dao\Wf\InstanceTest
 *
 */
class Instance extends Any
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'wf_instance';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'processId' => 'processId',
		'status' => 'status',
		'nextActivities' => 'nextActivities',
		'nextUsers' => 'nextUsers',
		'attributes' => 'attributes',
		'started' => 'started',
		'ended' => 'ended'
	);

	/**
	 * Load a instance from Instance JOIN with Activity
	 *
	 * @todo Escape sql injection
	 *
	 * @param Any $mapped
	 * @param string $filter
	 * @return Any
	 * @throws Exception
	 */
	public function load(MappedInterface $mapped, $filter = null, $bind = null)
	{
		if ( $mapped->isLoaded() == true && $force == false ) {
			return;
		}

		Signal::trigger(self::SIGNAL_PRE_LOAD, $this);

		$table = $this->_table;

		/* Select field of activity */
		$select = implode(',', array(
			'obj.*',
			'proc.isActive',
			'proc.isValid',
			'proc.normalizedName',
			'proc.version'
		));
		$sql = "SELECT $select FROM $table AS obj JOIN wf_process AS proc ON obj.processId=proc.id";

		if ( $filter ) {
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();

		if ( $row ) {
			$mapped->hydrate($row);
		}
		else {
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING);
		}
		$mapped->isLoaded(true);
		Signal::trigger(self::SIGNAL_POST_LOAD, $this);
		return $mapped;
	}

	/**
	 *
	 * @param Model\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':processId' => $mapped->getProcess(true),
			':status' => $mapped->getStatus(),
			':nextActivities' => \json_encode($mapped->getNextActivities()),
			':nextUsers' => \json_encode($mapped->getNextUsers()),
			':attributes' => \json_encode($mapped->getAttributes()),
			':started' => $mapped->getStarted(self::DATE_FORMAT),
			':ended' => $mapped->getEnded(self::DATE_FORMAT)
		));
	}

	/**
	 * Getter for activity instances of this process instance.
	 * Return a PDOStatement
	 *
	 * @param integer $instanceId
	 * @return \PDOStatement
	 */
	public function getActivities($instanceId, $filter='1=1', $bind=array())
	{
		if ( !$this->getActivitiesStmt || $this->getActivitiesFilter != $filter ) {
			$sql = "SELECT instAct.* FROM wf_instance_activity AS instAct WHERE instAct.instanceId = :instanceId";
			$sql .= ' AND '.$filter;
			$this->getActivitiesStmt = $this->connexion->prepare($sql);
			$this->getActivitiesFilter = $filter;
		}

		$bind[':instanceId'] = $instanceId;
		$this->getActivitiesStmt->execute($bind);
		return $this->getActivitiesStmt;
	}

	/**
	 * Get Activities children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * Return all properties of Wf\Activity object
	 * with nextStatus attach to transition
	 * and process instance id as instanceId
	 *
	 * $selectActivities may contain a list of id of Activity to load
	 * $selectTransition may contain a list of name of transitions
	 *
	 * @param integer $activityId
	 * @param array $selectActivities
	 * @param array $selectTransition
	 * @return array
	 */
	public function getNextCandidates($actInstId, $selectActivities = null, $selectTransition = null)
	{
		$filter = null;

		if ( $selectActivities ) {
			$filter = ' AND act.id IN (' . implode(',', $selectActivities) . ')';
		}

		if ( $selectTransition ) {
			$filter = ' AND trans.name IN (' . implode(',', $selectTransition) . ')';
		}

		$sql = "SELECT act.*, trans.name AS nextStatus, actinst.instanceId AS instanceId
		    	FROM wf_activity AS act
		    	JOIN wf_transition AS trans ON act.id = trans.childId
		    	JOIN (SELECT activityId, instanceId FROM wf_instance_activity WHERE id=:activityId) AS actinst
		    	WHERE trans.parentId = actinst.activityId" . $filter;

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':activityId' => $actInstId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Get Activities children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * Return all properties of Wf\Activity object
	 * with nextStatus attach to transition
	 * and process instance id as instanceId
	 *
	 * $selectActivities may contain a list of id of Activity to load
	 * $selectTransition may contain a list of name of transitions
	 *
	 * @param integer $activityId
	 * @param array $selectActivities
	 * @param array $selectTransition
	 * @return array
	 */
	public function getNextTransitions($actInstId)
	{
		$sql = "SELECT trans.*, actinst.instanceId AS instanceId, act.name as actName
		    	FROM wf_activity AS act
		    	JOIN wf_transition AS trans ON act.id = trans.childId
		    	JOIN (SELECT activityId, instanceId FROM wf_instance_activity WHERE id=:activityId) AS actinst
		    	WHERE trans.parentId = actinst.activityId" . $filter;

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':activityId' => $actInstId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Getter for runningActivities.
	 * Return also properties of activity object as :
	 * type, isComment, isInteractive, isAutomatic
	 *
	 * @param integer $processInstanceId
	 * @return array
	 */
	public function getRunningActivities($processInstanceId)
	{
		$status = Model\Wf\Instance\Activity::STATUS_RUNNING;

		$sql = "SELECT inst.*, act.type, act.isComment, act.isInteractive, act.isAutomatic
		    	FROM wf_instance_activity AS inst
		    	JOIN wf_activity AS act ON inst.activityId=act.id
		    	WHERE inst.status=:status AND inst.instanceId = :instanceId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':status' => $status,
			':instanceId' => $processInstanceId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Delete all transitions of a process
	 *
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;
		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->deleteFromProcessStmt ) {
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE processId=:processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}

		try {
			$this->deleteFromProcessStmt->execute(array(
				':processId' => $processId
			));
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}
} /* End of class */
