//%LICENCE_HEADER%

namespace Workflow\Dao\Wf;

use Workflow\Model\Wf;
use Application\Dao\Connexion;
use Application\Dao\Factory as DaoFactory;

/**
 * @brief __test class for Wf librairy.
 * @include Rbplm/Wf/__test.php
 *
 */
class Test extends \Application\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a __test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		define(GALAXIA_COMPILER_SOURCE, RBPLM_LIB_PATH . '/Rbplm/Wf/Compiler');
	}

	/**
	 * Load the full process in a data strutured
	 */
	public function Test_SavegraphService()
	{
		$processUid = "553be9e330834";
		$dao = DaoFactory::get()->getDao(Wf\Process::$classId);
		$process = $dao->loadFromUid(new Wf\Process(), $processUid);
		$workflow = new \Wumanager\Service\Workflow\Code($process);
		$workflow->init();
		$workflow->saveGraph();
		die;
	}

	/**
	 * Load the full process in a data strutured
	 */
	public function Test_Savegraph()
	{
		$processUid = "553be9e330834";
		$dao = DaoFactory::get()->getDao(Wf\Process::$classId);

		$graphViz = new \Wumanager\Service\Workflow\GraphViz();

		/*Set name of graph*/
		$graphViz->setPid($processUid);

		$interactiveFontColor = 'pink';
		$notinteractiveFontColor = 'black';
		$automaticColor = 'blue';
		$notautomaticColor = 'black';
		$nodebgColor = 'white';
		$nodeEdgesColor = 'black';

		//load activitities with relations
		$filter = 'act.processId=:processUid';
		$bind = array(':processUid'=>$processUid);
		$list = $dao->getGraphWithTransitions($filter, $bind);

		foreach($list as $activity){
			if( $activity['isInteractive'] ){
				$fontcolor = $interactiveFontColor;
			}
			else{
				$fontcolor = $notinteractiveFontColor;
			}

			$nodeId = $activity['uid'];
			$label = $activity['name'];
			$label = $activity['title'];
			$shape = Shape::getShapeFromType($activity['type']);
			$url = '#';
			$graphViz->addNode ($nodeId,
					array (
							'URL'=>$url,
							'label'=>$label,
							'shape'=>$shape,
							'fontcolor'=>$fontcolor,
							'style'=>'filled',
							'fillcolor'=>$nodebgColor,
							'color'=>$nodeEdgesColor
					)
			);

			$nodeParentId = $activity['parentUid'];
			if($nodeParentId){
				if( $activity['isAutomatic'] ){
					$transitionColor = $automaticColor;
				}
				else{
					$transitionColor = $notautomaticColor;
				}
				$graphViz->addEdge(
						array($nodeParentId => $nodeId ),
						array ('color' => $transitionColor )
				);
			}
		}

		$toDirectory = '/tmp';
		$graphViz->imageAndMap($toDirectory, $graphViz::$imageType);
	}


	/**
	 * Load the full process in a data strutured
	 */
	public function Test_GraphAsArray()
	{
		$processUid = "553be9e330834";
		$process = new Wf\Process();
		$dao = DaoFactory::get()->getDao($process);
		$dao->loadFromUid($process, $processUid);

		$array = $dao->getGraphAsArray($process->getId());
		var_dump($array);
	}

	/**
	 *
	 */
	public function _Test_()
	{
		$processName = uniqid('Process__test');
		$process = new Wf\Process( array('name'=>$processName) );

		$activity001 = new Wf\Activity\Start( array('name'=>uniqid('activity001')), $actOu);
		$activity002 = new Wf\Activity\Activity( array('name'=>uniqid('activity002')), $actOu);
		$activity012 = new Wf\Activity\Activity( array('name'=>uniqid('activity012')), $actOu);
		$activity022 = new Wf\Activity\Split( array('name'=>uniqid('activity022')), $actOu);
		$activity023 = new Wf\Activity\Activity( array('name'=>uniqid('activity023')), $actOu);
		$activity024 = new Wf\Activity\Activity( array('name'=>uniqid('activity024')), $actOu );
		$activity025 = new Wf\Activity\Join( array('name'=>uniqid('activity025')), $actOu );
		$activity003 = new Wf\Activity\End( array('name'=>uniqid('activity003')), $actOu );
		$activity001->setProcess($process);
		$activity002->setProcess($process);
		$activity012->setProcess($process);
		$activity022->setProcess($process);
		$activity023->setProcess($process);
		$activity024->setProcess($process);
		$activity025->setProcess($process);
		$activity003->setProcess($process);
		$activity001->getChild()->add($activity002);
		$activity002->getChild()->add($activity012);
		$activity012->getChild()->add($activity022);
		$activity022->getChild()->add($activity023);
		$activity022->getChild()->add($activity024);
		$activity023->getChild()->add($activity025);
		$activity024->getChild()->add($activity025);
		$activity025->getChild()->add($activity003);
		$activity025->getParents()->add($activity023);
		$activity025->getParents()->add($activity024);
		$process->getChild()->add($activity001);

		$processDao = DaoFactory::get()->getDao($process->cid);
		$activityDao = DaoFactory::get()->getDao($activity001);

		$processDao->save( $process );

		foreach( $process->getLinks() as $link ){
			var_dump($link->getName(), get_class($link) );
			if( $link instanceof Collection ) {
				foreach($link as $subLink){
					var_dump($subLink->getName(), get_class($subLink));
					$activityDao->save($subLink);
				}
			}
			else{
				$activityDao->save($link);
			}
		}
		$uid = $process->getUid();

		/***************************** __test LOAD ********************************/
		$processDao->setConnexion( Connexion::get() );

		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid, array(), true);
		assert( $process->getName() == $processName );
		assert( $process->isLoaded() == true );
		assert( Uuid::compare($process->getUid(), $uid) );
		assert( Uuid::compare( $process->parentId, $RootOu->getUid() ) );

		/** LOAD THE PARENT **/
		\Workflow\Model\Dao\Pg\Loader::loadParent($process);
		assert( $process->getParent()->getName() == $RootOu->getName() );

		/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/
		$list = DaoFactory::get()->getList()->setOption('table', 'view_wf_Activity_links');
		$list->load("lparent='$uid'");

		//convert list to collection
		//$collection = new Collection();
		//$list->loadInCollection( $process->getActivities() );

		foreach($process->getActivities() as $a){
			var_dump( $a->getName() );
		}

		/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid, array(), true);
		Loader::get()->loadParent($process);
		$list = $processDao->getActivities($process);

		$activityDao = new Wf\ProcessDaoPg( array(), Connexion::get() );

		foreach($list as $a){
			Loader::get()->loadParent($a);
			var_dump( $a->getName() , $a->getParent()->getName() );
		}
	}

	/**
	 *
	 */
	public function Test_Tutorial()
	{
		$processName = "Work unit";
		$process = new Wf\Process();
		DaoFactory::get()->getDao($process)->loadFromName($process, $processName);

		//Create instance of process
		$processInstance = Wf\Instance::start($process);
		DaoFactory::get()->getDao($processInstance)->save($processInstance);

		//Find Start activity and run it
		$start = new Activity\Start();
		DaoFactory::get()->getDao($start)->load($start, "type='start' AND processId=".$process->getId());
		$activityInstance = $processInstance->execute($start);
		DaoFactory::get()->getDao($activityInstance)->save($activityInstance);

		//add a listener to this instance
		//Signal::connect($activityInstance, 'runactivity.pre', array($code, 'onRun'));

		//Get next candidates activities
		$nextList = DaoFactory::get()->getDao($processInstance)->getNextCandidates($start->getId());

		//Create activities instance and set running status on next activities
		foreach($nextList as $properties){
			$nextActivity = new Activity($properties);
			$nextStatus = $properties['nextStatus'];
			$nextInstance = Instance\Activity::start($nextActivity, $processInstance);
			DaoFactory::get()->getDao($nextInstance)->save($nextInstance);
		}

		//now get the runnings activities and execute it
		$runningList = DaoFactory::get()->getDao($processInstance)->getRunningActivities($processInstance->getId());
		foreach($runningList as $properties){
			$runningInstanceActivity = new Instance\Activity($properties);
			$type = $properties['type'];

			//set the source activity object
			$activity = Activity::factory($type, false);
			DaoFactory::get()->getDao($activity)->loadFromId($activity, $properties['activityId']);
			$runningInstanceActivity->setActivity($activity);
			$runningInstanceActivity->execute();

			//save the state of instance
			DaoFactory::get()->getDao($runningInstanceActivity)->save($runningInstanceActivity);

			var_dump($runningInstanceActivity);
		}

		/***************************** __test LOAD ********************************/
		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid);
		assert( $process->getName() == $processName );
		assert( $process->isLoaded() == true );
		assert( Uuid::compare($process->getUid(), $uid) );
		assert( Uuid::compare( $process->parentId, $RootOu->getUid() ) );

		/** LOAD THE PARENT **/
		Loader::get()->loadParent($process);
		assert( $process->getParent()->getName() == $RootOu->getName() );

		/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/
		$list = new DaoList( array('table'=>'view_wf_activity_links') );
		$list->setConnexion( Connexion::get() );
		$list->load("lparent='$uid'");

		//convert list to collection
		$collection = new \Workflow\Model\Model\Collection();
		$list->loadInCollection( $process->getActivities() );

		foreach($process->getActivities() as $a){
			var_dump( $a->getName() );
		}

		/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid, array(), true);
		Loader::get()->loadParent($process);
		$list = $processDao->getActivities($process);

		$activityDao = DaoFactory::get()->getDao(Wf\Process::$classId);

		$collectionListBridge = new \Workflow\Model\Model\CollectionListBridge($process->getActivities(), $list);
		foreach($collectionListBridge as $a){
			Loader::get()->loadParent($a);
			var_dump( $a->getName() , $a->getParent()->getName() );
		}
	}




} //End of class

