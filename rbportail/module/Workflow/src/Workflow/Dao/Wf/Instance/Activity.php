<?php
// %LICENCE_HEADER%
namespace Workflow\Dao\Wf\Instance;

use Rbplm\Dao\MappedInterface;
use Workflow\Dao\Any;
use Application\Model\Signal;
use Workflow\Dao\Wf;
use Exception;
use Application\Dao\Error;

/**
 * SQL_SCRIPT>>
 * CREATE TABLE wf_instance_activity(
 * `id` int NOT NULL AUTO_INCREMENT,
 * `uid` VARCHAR(255) NOT NULL,
 * `cid` int NOT NULL,
 * `name` VARCHAR(255) NOT NULL,
 * `title` VARCHAR(255) NOT NULL,
 * `ownerId` varchar(255) NULL,
 * `parentId` int NULL,
 * `parentUid` varchar(255) NULL,
 * `updateById` varchar(255) NULL,
 * `updated` datetime NULL,
 * `instanceId` INT NULL,
 * `activityId` INT NOT NULL,
 * `status` VARCHAR(64) NULL,
 * `type` VARCHAR(16) NOT NULL,
 * `attributes` TEXT NULL,
 * `started` datetime NULL,
 * `ended` datetime NULL,
 * `comment` TEXT NULL,
 * PRIMARY KEY (`id`)
 * );
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * ALTER TABLE `wf_instance_activity` ADD UNIQUE (uid);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_uid` (`uid` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_name` (`name` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_parentuid` (`parentUid` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_parentid` (`parentId` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_cid` (`cid` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_ownerId` (`ownerId` ASC);
 *
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_instanceId` (`instanceId` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_activityId` (`activityId` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_type` (`type` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_started` (`started` ASC);
 * ALTER TABLE `wf_instance_activity` ADD INDEX `WFINSTACTIVITY_ended` (`ended` ASC);
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * @brief Dao class for \Workflow\Dao\Wf\Instance\Activity
 *
 * @see \Workflow\Model\Dao\Pg
 *
 */
class Activity extends Any
{
	
	/**
	 *
	 * @var string
	 */
	public static $table = 'wf_instance_activity';
	
	/**
	 * @var \PDOStatement
	 */
	protected $deleteFromInstanceStmt;

	/**
	 * @var \PDOStatement
	 */
	protected $deleteFromProcessStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $getpreviousStmt;
	
	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'instanceId' => 'instanceId',
		'activityId' => 'activityId',
		'status' => 'status',
		'type' => 'type',
		'started' => 'started',
		'ended' => 'ended',
		'attributes' => 'attributes',
		'comment' => 'comment'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge($this->metaModel, self::$sysToApp);
		$this->metaModelFilters = array_merge($this->metaModelFilters, self::$sysToAppFilter);
	}

	/**
	 * Rewrite load to define the Instance JOIN Activity
	 *
	 * Load a instance from Instance JOIN with Activity
	 *
	 * @todo Escape sql injection
	 *
	 * @param Any $mapped
	 * @param string $filter
	 * @return Any
	 * @throws Exception
	 */
	public function load(MappedInterface $mapped, $filter = null, $bind = null, $force = false)
	{
		if ( $mapped->isLoaded() == true && $force == false ) {
			return;
		}

		$table = $this->_table;

		Signal::trigger(self::SIGNAL_PRE_LOAD, $this);

		/* Select field of activity */
		$select = implode(',', array(
			'obj.*',
			'act.processId',
			'act.isInteractive',
			'act.isAutorouted',
			'act.isAutomatic',
			'act.isComment',
			'act.progression',
			'act.expirationTime',
			'act.roles',
			'act.title',
			'act.normalizedName',
			'act.attributes AS actAttributes'
		));
		$sql = "SELECT $select FROM $table AS obj JOIN wf_activity AS act ON obj.activityId=act.id";

		if ( $filter ) {
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();
		if ( $row ) {
			$mapped->hydrate($row);
		}
		else {
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING);
		}
		$mapped->isLoaded(true);
		Signal::trigger(self::SIGNAL_POST_LOAD, $this);
		return $mapped;
	}

	/**
	 *
	 * @param \Workflow\Model\Wf\Activity $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':instanceId' => $mapped->getInstance(true),
			':activityId' => $mapped->getActivity(true),
			':status' => $mapped->getStatus(),
			':type' => $mapped->getType(),
			':attributes' => \json_encode($mapped->getAttributes()),
			':started' => $mapped->getStarted(self::DATE_FORMAT),
			':ended' => $mapped->getEnded(self::DATE_FORMAT),
			':comment' => $mapped->getComment()
		));
	}

	/**
	 * Getter for previous.
	 * Return a PDOStatment.
	 *
	 * @param integer $activityInstanceId
	 * @return \PDOStatement
	 */
	public function getPrevious($activityInstanceId)
	{
		if ( !$this->getpreviousStmt ) {
			/* get activity instance of parent activity of the instance activity */
			$sql = "SELECT parentIA.*,prevtrans.name AS previousTransitionName
			FROM wf_instance_activity AS parentIA
			JOIN wf_transition AS trans ON trans.parentId = parentIA.activityId
			JOIN wf_instance_activity AS childIA ON trans.childId = childIA.activityId
			JOIN wf_transition AS prevtrans ON prevtrans.childId = parentIA.activityId
			WHERE childIA.id = :actInstanceId AND parentIA.instanceId=childIA.instanceId;";
			$this->getpreviousStmt = $this->connexion->prepare($sql);
		}

		$bind = array(
			':actInstanceId' => $activityInstanceId
		);
		$this->getpreviousStmt->execute($bind);
		return $this->getpreviousStmt;
	}

	/**
	 * Getter for comments.
	 * Return a list.
	 *
	 * @param
	 *        	Any
	 * @return array
	 */
	public function getComments($mapped)
	{}

	/**
	 * Delete all transitions of a process
	 *
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;
		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->deleteFromProcessStmt ) {
			$table = static::$table;
			$activityTable = Wf\Activity::$table;
			$sql = "DELETE FROM actInst USING $table AS actInst";
			$sql .= " LEFT JOIN $activityTable AS act ON (actInst.activityId = act.id)";
			$sql .= " WHERE act.processId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}

		try {
			$this->deleteFromProcessStmt->execute(array(
				':processId' => $processId
			));
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * Delete all transitions of a process
	 *
	 * @param integer $processId
	 */
	public function deleteFromInstance($instanceId, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;
		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->deleteFromInstanceStmt ) {
			$table = static::$table;
			$sql = "DELETE FROM  $table WHERE instanceId = :instanceId";
			$this->deleteFromInstanceStmt = $this->connexion->prepare($sql);
		}

		try {
			Signal::trigger(self::SIGNAL_PRE_DELETE, $this);
			$this->deleteFromInstanceStmt->execute(array(
				':instanceId' => $instanceId
			));
			Signal::trigger(self::SIGNAL_POST_DELETE, $this);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

} /* End of class */
