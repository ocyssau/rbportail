<?php
//%LICENCE_HEADER%
namespace Workflow\Dao\Wf;

use Workflow\Dao\Any;
use Workflow\Model\Wf;

/** SQL_SCRIPT>>
 CREATE TABLE wf_process(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` int NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `title` VARCHAR(255) NOT NULL,
 `ownerId` varchar(255) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updateById` varchar(255) NULL,
 `updated` datetime NULL,
 `version` varchar(16),
 `normalizedName` varchar(256),
 `isValid` boolean,
 `isActive` boolean,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_ALTER>>
 ALTER TABLE wf_process ADD UNIQUE (uid);
 ALTER TABLE wf_process ADD UNIQUE (normalizedName);
 ALTER TABLE wf_process ADD UNIQUE (name, version);
 
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_name` (`name` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_uid` (`uid` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_parentuid` (`parentUid` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_parentid` (`parentId` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_cid` (`cid` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_ownerId` (`ownerId` ASC);
 
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_normalizedName` (`normalizedName` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_isActive` (`isActive` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_isValid` (`isValid` ASC);
 ALTER TABLE `wf_process` ADD INDEX `WFPROCESS_version` (`version` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP table wf_process;
 <<*/

/**
 * @brief Dao class for \Workflow\Dao\Wf\Process
 *
 * @see \Workflow\Model\Dao
 * @see \Workflow\Dao\Wf\Test
 *
 */
class Process extends Any
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'wf_process';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'title' => 'title',
		'version' => 'version',
		'normalizedName' => 'normalizedName',
		'isValid' => 'isValid',
		'isActive' => 'isActive'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Dao\MappedInterface $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':title' => $mapped->getTitle(),
			':version' => $mapped->getVersion(),
			':normalizedName' => $mapped->getNormalizedName(),
			':isValid' => (integer)$mapped->isValid(),
			':isActive' => (integer)$mapped->isActive()
		));
	}

	/**
	 * Getter for activities. Return a list.
	 *
	 * @param integer $processId
	 * @return array
	 */
	public function getTransitions($processId)
	{
		$sql = "SELECT trans.* FROM wf_transition AS trans";
		$sql .= " JOIN wf_activity AS act ON act.id = trans.parentId";
		$sql .= " WHERE act.processId=:processId";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':processId' => $processId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Getter for activities. Return a list.
	 *
	 * @param Model\Any
	 * @return Dao\DaoList
	 */
	/*
	 public function getActivities($mapped)
	 {
	 $List = new \Workflow\Model\Dao\Pg\DaoList( array('table'=>'view_wf_activity_links') );
	 $List->setConnexion( $this->getConnexion() );
	 $uid = $mapped->getUid();
	 $List->load("lparent='$uid'");
	 return $List;
	 }
	 */
	
	/**
	 * Getter for activities. Return a list.
	 *
	 * @param string $processName
	 * @return array
	 */
	public function getVersions($processName)
	{
		$table = self::$table;
		
		$sql = "SELECT version FROM $table WHERE name=:processName ORDER BY version DESC";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':processName' => $processName
		));
		return $stmt->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * Return the activities setted with parentId and parentUid from transitions
	 * @param integer
	 * @return \Rbplm\Dao\ListInterface
	 */
	public function getGraphWithTransitions($filter, $bind)
	{
		$daoFactory = $this->factory;
		if ( !$daoFactory ) {
			throw new \Exception('the factory public property must be set before');
		}
		
		//populate activities with parentId property
		$sql = "SELECT
		trans.parentId,
		trans.parentUid,
		trans.name as status,
		act.id, act.uid, act.cid, act.name, act.normalizedName,
		act.title, act.ownerId,
		act.updateById, act.updated,
		act.isInteractive, act.isAutorouted, act.isAutomatic, act.isComment,
		act.type, act.processId, act.progression, act.expirationTime, act.roles,
		act.attributes
		FROM wf_activity AS act
		LEFT OUTER JOIN wf_transition as trans ON act.id=trans.childId
		WHERE $filter";
		$list = $daoFactory->getList(Wf\Activity\Activity::$classId)->loadFromSql($sql, $bind);
		return $list;
	}

	/**
	 *
	 */
	public function getGraphAsArray($processId)
	{
		$daoFactory = $this->factory;
		if ( !$daoFactory ) {
			throw new \Exception('the factory public property must be set before');
		}
		
		//load activitities
		$activities = $daoFactory->getList(Wf\Activity\Activity::$classId);
		$activities->load('processId=:processId', array(
			':processId' => $processId
		));
		$activities->setOption('asapp', true);
		$activities->dao = $daoFactory->getDao(Wf\Activity\Activity::$classId);
		
		//load transitions
		$sql = "SELECT trans.parentId, trans.childId, trans.name, trans.attributes
		FROM wf_transition AS trans
		JOIN wf_activity AS act ON act.id = trans.parentId";
		$sql .= " WHERE act.processId=:processId";
		
		$transitions = $daoFactory->getList(Wf\Transition::$classId);
		$transitions->loadFromSql(trim($sql), array(
			':processId' => $processId
		));
		
		foreach( $transitions as $transition ) {
			$attributes = json_decode($transition['attributes'], true);
			$return['transitions'][] = array(
				'name' => $transition['name'],
				'fromid' => $transition['parentId'],
				'toid' => $transition['childId'],
				'attributes' => $attributes
			);
		}
		
		foreach( $activities as $activity ) {
			$attributes = $activity['attributes'];
			$roles = $activity['roles'];
			
			if ( !is_array($attributes) ) {
				$attributes = json_decode($attributes, true);
			}
			
			if ( !is_array($roles) ) {
				$roles = json_decode($roles, true);
			}
			
			$return['activities'][] = array(
				'id' => $activity['id'],
				'name' => $activity['name'],
				'title' => $activity['title'],
				'type' => $activity['type'],
				'isAutomatic' => $activity['isAutomatic'],
				'isComment' => $activity['isComment'],
				'isInteractive' => $activity['isInteractive'],
				'progression' => $activity['progression'],
				'attributes' => $attributes,
				'roles' => $roles
			);
		}
		
		return $return;
	}

/**
 * Getter for activities. Return a list.
 *
 * @param Model\Any
 * @return Dao\DaoList
 */
	/*
	 public function loadStartActivity(Wf\Instance $mapped)
	 {
	 $table = Activity::$table;
	 $processId = $mapped->getProcess(true);
	 $sql = "SELECT * FROM $table WHERE type='start' AND processId='$processId'";
	 
	 $stmt = $this->connexion->prepare($sql);
	 $stmt->setFetchMode(\PDO::FETCH_ASSOC);
	 $stmt->execute();
	 $row = $stmt->fetch();
	 return $row;
	 }
	 */

/**
 */
	/*
	 public function _delete($pId, $withChilds = true, $withTrans = true)
	 {
	 // Remove process roles
	 $query = "DELETE FROM " . Role::$table . " WHERE pId = $pId;";
	 $query = "DELETE FROM " . Role::$table . " WHERE pId = $pId;";
	 
	 // Remove all instance data
	 $query .= "DELETE FROM " . Workitem::$table . " USING ";
	 $query .= Workitem::$table . " gw , " . Instance::$table . " gi ";
	 $query .= "WHERE gw.instanceId = gi.instanceId AND gi.pId = $pId;";
	 
	 $query .= "DELETE FROM " . Instance\Activity::$table . " USING ";
	 $query .= Instance\Activity::$table . " gia , " . Instance::$table . " gi ";
	 $query .= "WHERE gia.instanceId = gi.instanceId AND gi.pId = $pId;";
	 
	 $query .= "DELETE FROM " . Instance\Comment::$table . " USING ";
	 $query .= Instance\Comment::$table . " gic , " . Instance::$table . " gi ";
	 $query .= "WHERE gic.instanceId = gi.instanceId AND gi.pId = $pId;";
	 
	 $query = "DELETE FROM " . Instance::$table . " WHERE pId = $pId;";
	 
	 // And finally remove the proc
	 $query = "delete from " . self::$table . " where pId=$pId";
	 
	 if ($this->query ( $query )) {
	 // Remove process activities
	 $query = "select activityId from " . Activity\Activity::$table . " where pId=$pId";
	 $result = $this->query ( $query );
	 while( $res = $result->fetchRow () ){
	 $aM->remove_activity ( $pId, $res ['activityId'] );
	 }
	 
	 return true;
	 }
	 else{
	 $this->db->CompleteTrans ( false );
	 }
	 
	 return false;
	 }
	 */
} //End of class
