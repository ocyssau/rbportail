<?php
//%LICENCE_HEADER%

namespace Workflow\Model;


/**
 *
 *
 */
class Any extends \Application\Model\Any
{
	/**
	 * @var string
	 */
	protected $title='';

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);
		(isset($properties['title'])) ? $this->title=$properties['title'] : null;
		return $this;
	}

	/**
	 * @param string $property
	 * @return mixed
	 */
	public function get( $property )
	{
		if(isset($this->$property)){
			return $this->$property;
		}
	}

	/**
	 * @param string $property
	 * @param string $value
	 * @return Any
	 */
	public function set( $property, $value )
	{
		$this->$property = $value;
		return $this;
	}

	/**
	 * @param string $string
	 * @return Any
	 */
	public function setTitle($string)
	{
		$this->title = $string;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
}
