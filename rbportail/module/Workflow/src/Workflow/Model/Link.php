<?php
//%LICENCE_HEADER%

namespace Workflow\Model;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Link extends \Application\Model\Link
{
	public static $classId = 442;
	
	/**
	 * 
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
	}
}
