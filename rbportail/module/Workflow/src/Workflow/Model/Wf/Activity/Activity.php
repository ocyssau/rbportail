<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * This class handles activities of type 'activity'
 *
 */
class Activity extends Wf\Activity
{
	
    /**
     * @var string
     */
    protected $type = Wf\Activity::TYPE_ACTIVITY;
    
    /**
     * @var integer
     */
    public static $classId = 421;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'box';
	
}