<?php
//%LICENCE_HEADER%

namespace Workflow\Model\Wf\Activity;

/**
 * @brief Collection of \Rbplm\LinkableInterface object.
 * 
 * See Php \SplObjectStorage documentation:
 * @link http://php.net/manual/en/class.splobjectstorage.php
 * 
 * Collection implements RecursiveIterator interface and may be iterate on all COMPOSITE OBJECTS tree.
 * Collection implements \Rbplm\LinkableInterface to be link to a \Rbplm\AnyObject.
 * Each item of this collection must be a \Rbplm\LinkableInterface object.
 * 
 * A collection is a \Rbplm\Model\CompositComponentInterface, so it may be add to a other collection and be integrated to COMPOSIT OBJECTS tree.
 * 
 * Example and tests: Rbplm/Model/CollectionTest.php
 * 
 * @see \Rbplm\Model\CompositComponentInterface
 * @see \Rbplm\LinkableInterface
 * 
 * @link http://php.net/manual/en/class.recursiveiterator.php
 * 
 */
class Collection extends \SplObjectStorage implements \RecursiveIterator
{
	
	/**
	 * Constructor.
	 * 
	 * @param array	$properties
	 * @param \Rbplm\Any $parent
	 */
	public function __construct($datas=null, $parent=null)
	{
		if(is_array($datas)){
			foreach($datas as $object){
				parent::attach($object, null);
			}
		}
	    if($parent){
	        $this->setParent($parent);
	    }
	}
	
	/**
	 * Add a object to collection.
	 * Alias for attach.
	 * 
	 * @param LinkableInterface
	 * @param mixed			Additionals datas to associate to object in collection
	 * @return Collection
	 */
	public function add(\Rbplm\LinkableInterface $object, $data = null)
	{
		parent::attach($object, $data);
		return $this;
	}
	
	/**
	 * Returns if an iterator can be created fot the current entry
	 * @see boolean
	 */
	public function hasChildren()
	{
		return ( count($this->current()->getChildren()) > 0 );
	}
	
	/**
	 * Returns an iterator for the current entry
	 * @see Collection
	 */
	public function getChildren()
	{
		return new Collection($this->current()->getChildren());
		return false;
	}
	
	/**
	 * Overload of \SplObjectStorage::attach
	 */
	/*
	public function attach($object, $data)
	{
		$this->_indexUid[ $object->getUid() ] = $object;
		$this->_indexName[ $object->getName() ] = $object;
		parent::attach($object, $data);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::detach
	 */
	/*
	public function detach($object)
	{
		unset( $this->_indexUid[ $object->getUid() ] );
		unset( $this->_indexName[ $object->getName() ] );
		return parent::detach($object);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::offsetUnset
	 */
	/*
	public function offsetUnset($object)
	{
		$this->detach($object);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::offsetSet
	 */
	/*
	public function offsetSet($object, $data = null)
	{
		$this->attach($object, $data);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::removeAll
	 */
	/*
	public function removeAll($storage)
	{
		$this->_indexName = array();
		$this->_indexUid = array();
		parent::removeAll($storage);
		foreach($this as $obj){
			$this->_indexName[$obj->getName()] = $obj;
			$this->_indexUid[$obj->getUid()] = $obj;
		}
	}
	*/
	
	/**
	 * Get item object of collection from his uid.
	 * 
	 * @param string $uid
	 * @return \Rbplm\Any | void
	 */
	public function getByUid($uid)
	{
		foreach($this as $current){
			if( $current->getUid() == $uid ){
				return $current;
				break;
			}
		}
		return;
		
		/* 3x plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getUid() == $uid ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	/**
	 * Get item object of collection from his name.
	 * 
	 * @param string $name
	 * @return \Rbplm\Any | void
	 */
	public function getByName($name)
	{
		foreach($this as $current){
			if( $current->getName() == $name ){
				return $current;
				break;
			}
		}
		return;
		/* 3x Plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getName() == $name ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	/**
	 * Get item object of collection from his index.
	 * 
	 * @param string $name
	 * @return \Rbplm\Any | void
	 */
	public function getByIndex($index)
	{
		$this->rewind();
		while($this->key() != $index){
			$this->next();
		}
		return $this->current();
	}
	
	/**
	 * @see CompositComponentInterface#getParent()
	 */
    public function getParent()
    {
    	return $this->_parent;
    }
	
    /**
     * @see CompositComponentInterface#setParent($object)
     * @return Collection
     */
    public function setParent($object)
    {
    	$this->_parent = $object;
		return $this;
    }
	
}
