<?php

namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;


/**
 * 
 * This class handles activities of type 'join'
 *
 */
class Join extends Wf\Activity 
{
	
    /**
     * @var string
     */
    protected $type = Wf\Activity::TYPE_JOIN;
    
    /**
     * @var integer
     */
    public static $classId = 424;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'invtriangle';
    
}
