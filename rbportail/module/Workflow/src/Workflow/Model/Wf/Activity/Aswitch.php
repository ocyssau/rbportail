<?php

namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;


/**
 * 
 * This class handles activities of type 'switch'
 *
 */
class Aswitch extends Wf\Activity
{

	/**
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_SWITCH;
	
	public static $classId = 426;
	
	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'diamond';

}
