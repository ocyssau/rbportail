<?php

namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 *
 * This class handles activities of type 'standalone'
 *
 */
class Standalone extends Wf\Activity 
{

	/**
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_STANDALONE;
	
	/**
	 * @var integer
	 */
	public static $classId = 427;
	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'hexagon';
}


