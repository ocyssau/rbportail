<?php

namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * 
 * This class handles activities of type 'split'
 *
 */
class Split extends Wf\Activity {
	
    /**
     * @var string
     */
    protected $type = Wf\Activity::TYPE_SPLIT;
    
    /**
     * @var integer
     */
    public static $classId = 425;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'triangle';
	
}

