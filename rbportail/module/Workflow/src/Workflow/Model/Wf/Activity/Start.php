<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * 
 * This class handles activities of type 'start'
 *
 */
class Start extends Wf\Activity {

	/**
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_START;
	
	/**
	 * @var integer
	 */
	public static $classId = 422;

	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'circle';
}


