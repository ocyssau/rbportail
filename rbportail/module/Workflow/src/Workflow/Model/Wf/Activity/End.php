<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 *
 * This class handles activities of type 'end'
 *
 */
class End extends Wf\Activity
{

    /**
     * @var string
     */
    protected $type = Wf\Activity::TYPE_END;

    /**
     * @var integer
     */
    public static $classId = 423;

	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    public $shape = 'doublecircle';

}


