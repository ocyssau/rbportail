<?php

namespace Workflow\Model\Wf;

use Workflow\Model\Wf\Activity;

class Shape
{
	const TYPE_ABSTRACT = 'box';
	const TYPE_ACTIVITY = 'box';
	const TYPE_END = 'doublecircle';
	const TYPE_JOIN = 'invtriangle';
	const TYPE_SPLIT = 'triangle';
	const TYPE_STANDALONE = 'hexagon';
	const TYPE_START = 'circle';
	const TYPE_SWITCH = 'diamond';
	
	public static function getShapeFromType($type)
	{
		switch($type){
			case Activity::TYPE_ACTIVITY:
				return self::TYPE_ACTIVITY;
				break;
			case Activity::TYPE_END:
				return self::TYPE_END;
				break;
			case Activity::TYPE_JOIN:
				return self::TYPE_JOIN;
				break;
			case Activity::TYPE_SPLIT:
				return self::TYPE_SPLIT;
				break;
			case Activity::TYPE_STANDALONE:
				return self::TYPE_STANDALONE;
				break;
			case Activity::TYPE_START:
				return self::TYPE_START;
				break;
			case Activity::TYPE_SWITCH:
				return self::TYPE_SWITCH;
				break;
			default:
				return self::TYPE_ABSTRACT;
				break;
		}
	}
}
