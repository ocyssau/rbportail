<?php
//%LICENCE_HEADER%

namespace Workflow\Model\Wf;

use DateTime;
use Workflow\Model\Any;
use Application\Model\People;
use Application\Model\Error;
use Exception;

/**
 * @brief Auto generated class Instance
 *
 * This is a class generated with \Workflow\Model\Model\\Generator.
 *
 * @see Rbplm/Wf/InstanceTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Instance extends Process
{
	const STATUS_RUNNING = 'running';
	const STATUS_ABORTED = 'aborted';
	const STATUS_ACTIVE = 'active';
	const STATUS_COMPLETED = 'completed';
	const STATUS_EXCEPTION = 'exception';

	/**
	 * @var integer
	 */
	public static $classId = 440;

    /**
     * @var DateTime
     */
    protected $started;

    /**
     * @var DateTime
     */
    protected $ended;

    /**
     * @var Process
     */
    protected $process = null;
    protected $processId = null;

    /**
     * @var array
     */
    protected $attributes = null;

    /**
     * @var string
     */
    protected $status = null;

    /**
     * Array with value = id of Wf\Activity to execute
     *
     * @var array
     */
    protected $nextActivities = null;

    /**
     * @var array
     */
    protected $nextUsers = null;

    /**
     * Collection of Instance\Activity
     *
     * @var array
     */
    protected $activities = array();

    /**
     * Collection of Activity
     *
     * @var array
     */
    protected $runningActivities = array();

    /**
     * Hydrator.
     * Load the properties in the mapped object.
     *
     * @param array $properties \PDO fetch result to load
     * @return Instance
     */
    public function hydrate( array $properties )
    {
    	parent::hydrate($properties);
    	(isset($properties['processId'])) ? $this->processId=$properties['processId'] : null;
    	(isset($properties['processUid'])) ? $this->processUid=$properties['processUid'] : null;

    	(isset($properties['status'])) ? $this->status=$properties['status'] : null;
    	(isset($properties['nextActivities'])) ? $this->nextActivities=json_decode($properties['nextActivities']) : null;
    	(isset($properties['nextUsers'])) ? $this->nextUsers=json_decode($properties['nextUsers']) : null;
    	(isset($properties['attributes'])) ? $this->attributes=json_decode($properties['attributes']) : null;

    	if(isset($properties['started'])){
    		if($properties['started'] != null){
    			$this->started = new DateTime($properties['started']);
    		}
    	}

    	if(isset($properties['ended'])){
    		if($properties['ended'] != null){
    			$this->ended = new DateTime($properties['ended']);
    		}
    	}

    	return $this;
    }


    /**
     * Start a new instance.
     * @param Process 		$process
     * @param Any	$anyobject
     * @return Instance
     */
    public static function start(Process $process)
    {
    	$instance = Instance::init();
    	$instance->setProcess($process);
    	$instance->status = self::STATUS_RUNNING;
    	$instance->started = new DateTime();
    	$instance->name = $process->getName();
    	$instance->setOwner(People\CurrentUser::get());
    	return $instance;
    }

    /**
     * Run the activity
     * @param Activity	$Activity
     * @return Instance\Activity
     */
    public function execute(Activity $activity)
    {
    	if( $this->status != self::STATUS_RUNNING ){
    		throw new Exception('NOT_RUNNING_PROCESS', Error::WARNING);
    	}

    	$instanceActivity = Instance\Activity::start($activity, $this);
    	$instanceActivity->execute();
    	return $instanceActivity;
    }

    /**
     * @return DateTime
     */
    public function getStarted($format=null)
    {
    	if($this->started && $format){
    		return $this->started->format($format);
    	}
    	elseif($this->started){
    		return $this->started;
    	}
    }

    /**
     * @return DateTime
     */
    public function getEnded($format=null)
    {
    	if($this->ended && $format){
    		return $this->ended->format($format);
    	}
    	elseif($this->ended){
    		return $this->ended;
    	}
    }

    /**
     * @param DateTime
     * @return Instance
     */
    public function setEnded($date)
    {
    	$this->ended = $date;
    	return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
    	return $this->status;
    }

    /**
     * @param string $string
     * @return Instance
     */
    public function setStatus($string)
    {
    	$this->status = $string;
    	return $this;
    }

    /**
     * @return Process
     */
    public function getProcess($asId=false)
    {
    	if($asId){
    		return $this->processId;
    	}
    	else{
    		return $this->process;
    	}
    }

    /**
     * @param Process $process
     * @return void
     */
    public function setProcess(Process $process)
    {
        $this->process = $process;
        $this->processId = $this->process->getId();
        $this->processUid = $this->process->getUid();
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $properties
     * @return Instance
     */
    public function setAttributes(array $properties)
    {
        $this->attributes = $properties;
        return $this;
    }

    /**
     * @return People\User
     */
    public function getNextUsers()
    {
        return $this->nextUsers;
    }

    /**
     * @param array $array
     * @return Instance
     */
    public function setNextUsers($array)
    {
        $this->nextUsers = $array;
        return $this;
    }

    /**
     * Collection of Instance\Activity
     *
     * @return array
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * @param Instance\Activity
     * @return Instance
     */
    public function addActivity( $activity, $bidirectionnal=true)
    {
    	if($activity instanceof Instance\Activity){
    	    throw new Exception('BAD PARAMETER: $activity must be of type Instance\Activity');
    	}
    	$this->activities[$activity->getUid()] = $activity;
    	if($bidirectionnal){
    	    $activity->setInstance($this, false);
    	}
    	return $this;
    }

    /**
     * Return a array with value = id of Wf\Activity to execute
     *
     * @return array
     */
    public function getNextActivities()
    {
        return $this->nextActivities;
    }

    /**
     * @return Instance
     */
    public function setNextActivities($array)
    {
        $this->nextActivities = $array;
        return $this;
    }

}
