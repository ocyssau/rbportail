//%LICENCE_HEADER%

namespace Workflow\Model\Wf;

use Workflow\Model\Wf;
use Workflow\Dao;
use Application\Dao\Connexion;
use Application\Dao\Factory as DaoFactory;

/**
 * @brief __test class for Wf librairy.
 * @include Rbplm/Wf/__test.php
 * 
 */
class Test extends \Application\Test\Test
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a __test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	define(GALAXIA_COMPILER_SOURCE, RBPLM_LIB_PATH . '/Rbplm/Wf/Compiler');
    }
    
    /**
     */
    function testRaw()
    {
    	/*Construct a new process*/
    	$process = new Wf\Process();
    	$process->setName('Process__test');
    	$pId = $process->getUid();
		assert( !empty($pId) );
    	
    	/*Activities definition*/
		$aProperties = array( 
						'name'=>'activity001',
						'normalized_name'=>'activity001-0.1', 
						'description'=>'Start activity', 
						'isInteractive'=>true, 
						'isAutoRouted'=>false, 
						'isComment'=>false);
    	
    	$activity001 = new Wf\Activity\Start($aProperties);
    	$activity002 = new Wf\Activity\Activity( array('name'=>'activity002') );
    	$activity012 = new Wf\Activity\Activity( array('name'=>'activity012') );
    	$activity022 = new Wf\Activity\Split( array('name'=>'activity022') );
    	$activity023 = new Wf\Activity\Activity( array('name'=>'activity023') );
    	$activity024 = new Wf\Activity\Activity( array('name'=>'activity024') );
    	$activity025 = new Wf\Activity\Join( array('name'=>'activity025') );
    	$activity003 = new Wf\Activity\End( array('name'=>'activity003') );
    	
    	/*Create bi-directionnal agregation relation*/
    	echo "Create bi-directionnal agregation relation".CRLF;
    	$activity001->setProcess($process);
    	$activity002->setProcess($process);
    	$activity012->setProcess($process);
    	$activity022->setProcess($process);
    	$activity023->setProcess($process);
    	$activity024->setProcess($process);
    	$activity025->setProcess($process);
    	$activity003->setProcess($process);
    	
    	/*Create transitions*/
    	echo "Create transitions".CRLF;
    	$transtion1to2 = new Wf\Transition( $activity001, $activity002 );
    	$transtion2to12 = new Wf\Transition( $activity002, $activity012 );
    	$transtion12to22 = new Wf\Transition( $activity012, $activity022 );
    	$transtion22to23 = new Wf\Transition( $activity022, $activity023 );
    	$transtion22to24 = new Wf\Transition( $activity022, $activity024 );
    	$transtion23to25 = new Wf\Transition( $activity023, $activity025 );
    	$transtion24to25 = new Wf\Transition( $activity024, $activity025 );
    	$transtion25to3 = new Wf\Transition( $activity025, $activity003 );
    	
    	/*with use of transitions
    	$process->getTransitions()->add($transtion1to2);
    	$process->getTransitions()->add($transtion2to12);
    	$process->getTransitions()->add($transtion12to22);
    	$process->getTransitions()->add($transtion22to23);
    	$process->getTransitions()->add($transtion22to24);
    	$process->getTransitions()->add($transtion23to25);
    	$process->getTransitions()->add($transtion24to25);
    	$process->getTransitions()->add($transtion25to3);
    	*/

    	/*Create needed folders for store scripts files*/
    	$process->initFolders();
    	$activity001->compile(true);
		$recursiveIterator = new \RecursiveIteratorIterator( $activity001->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . "\n";
    		$script = $child->getScripts('compiledCode');
    		echo 'Code: ' . $script . "\n";
    		$child->compile();
		}
		
    	/*Create instance from instance constructor*/
    	$Instance = Wf\Instance::start( $process, new \Workflow\Model\Ged\Document\Version() );
    	$activity002->isAutomatic(true);
    	$Instance->execute($activity001);
    	
		$recursiveIterator = new \RecursiveIteratorIterator( $Instance->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
    	
		/*Get runnings activities*/
    	foreach( $Instance->getActivities() as $ra){
			if( $ra->status == Wf\Instance_Activity::STATUS_RUNNING ){
				$running = $ra;
				break;
			}
		}
		$running->execute();
		
		/*Get runnings activities*/
		/*
		$startTime = microtime(true);
		foreach( $Instance->getActivities() as $ra){
			if( $ra->status == Wf\Instance_Activity::STATUS_RUNNING ){
				//echo 'Runnings activity: ' . $ra->getName() . "\n";
				$running = $ra;
			}
		}
		echo microtime(true) - $startTime . " S \n";
		*/
		//$startTime = microtime(true);
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$ra = $queue->dequeue();
    		echo 'Runnings activity: ' . $ra->getName() . "\n";
			$ra->execute();
		}
		//echo microtime(true) - $startTime . " S \n";
		
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$a = $queue->dequeue();
			echo 'Runnings activity: ' . $a->getName() . "\n";
		}
		
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity003')->execute();
		
		var_dump( $Instance->status, $Instance->getUid() );
		
		try{
			$Instance->execute( $activity001 );
		}catch(\Exception $e){
			echo $e->getMessage() . "\n";
		}
		
    } //End of method
    
    
    /**
     */
    function _Test_Tutorial(){
    	
    	/*
    	 * First, construct a new process.
    	 */
    	$process = Wf\Process::init();
    	$process->setName('Process__test');
    	$process->initFolders();
    	
    	/* And add some activities.
    	 * Activities, as other Rbplm objects, have type \Workflow\Model\AnyObject, and as anyobject, take array of properties in first parameter
    	 * and parent on second.
    	 * But, basicaly, parent is not used, and may be use to put workflow anyobject in organizational unit for example.
    	 */
    	$activity001 = Wf\Activity\Start::init( 'activity001' );
    	$activity002 = Wf\Activity\Activity::init( 'activity002' );
    	$activity012 = Wf\Activity\Activity::init( 'activity012' );
    	$activity022 = Wf\Activity\Split::init( 'activity022' );
    	$activity023 = Wf\Activity\Activity::init( 'activity023' );
    	$activity024 = Wf\Activity\Activity::init( 'activity024' );
    	$activity025 = Wf\Activity\Join::init( 'activity025' );
    	$activity003 = Wf\Activity\End::init( 'activity003' );
    	
    	/*
    	 * Create bi-directionnal agregation relation.
    	 * The activities must be attached to a process.
    	 * The method setProcess create a relation from activities to process but too from
    	 * process to activity.
    	 * You may retrieve activities from a process with method Wf\Process::getActivities()
    	 * 
    	 */
    	echo "Create bi-directionnal agregation relation".CRLF;
    	$activity001->setProcess($process)->compile();
    	$activity002->setProcess($process)->compile();
    	$activity012->setProcess($process)->compile();
    	$activity022->setProcess($process)->compile();
    	$activity023->setProcess($process)->compile();
    	$activity024->setProcess($process)->compile();
    	$activity025->setProcess($process)->compile();
    	$activity003->setProcess($process)->compile();
    	
    	/*Create transitions*/
    	echo "Create transitions".CRLF;
    	$transtion1to2 = Wf\Transition::factory( $activity001, $activity002 );
    	$transtion2to12 = Wf\Transition::factory( $activity002, $activity012 );
    	$transtion12to22 = Wf\Transition::factory( $activity012, $activity022 );
    	$transtion22to23 = Wf\Transition::factory( $activity022, $activity023 );
    	$transtion22to24 = Wf\Transition::factory( $activity022, $activity024 );
    	$transtion23to25 = Wf\Transition::factory( $activity023, $activity025 );
    	$transtion24to25 = Wf\Transition::factory( $activity024, $activity025 );
    	$transtion25to3 = Wf\Transition::factory( $activity025, $activity003 );
    	
    	/*The activities list may be retrieve from process : */
    	foreach($process->getActivities() as $activity){
    		var_dump($activity->getName());
    	}
    	
    	echo '------------------------------------------' . "\n";
		
		/*
		 * HOW TO RUN THE PROCESS.
		 */
		
    	/* First create a process instance.
    	 * The process instance is attach to a process definition.
    	 * It add informations about execution, like start date, owner, and running activities.
    	 * To create a new instance, use start method. Dont call directly the constructor.
    	 * Here the instance is attached to a document.
    	 * 
    	 * A instance of workflow may be attach to any object with type \Workflow\Model\AnyObject.
    	 * To get a attach anyobject use method getanyobject.
    	 * 
    	 */
    	$Instance = Wf\Instance::start($process);
    	$attachedanyobject = $Instance->getanyobject();
    	$attachedanyobject->setName('my attached document');
    	
    	/* A new process instance has name set to "noname".
    	 * You must set a name.
    	 */
    	echo 'Initiale name of a new process: ' . $Instance->getName() . "\n";
    	echo '------------------------------------------' . "\n";
    	
    	
    	$Instance->setName( $attachedanyobject->getName() );
    	
    	/*
    	 * Activity may be set as automatic. In this case, when previous activity is completed, the activity is automaticly executed.
    	 */
    	$activity002->isAutomatic(true);
    	
    	/*
    	 * To execute a activity:
    	 * Execute Activity001 and automatic activity Activity002
    	 */
    	$activity001Instance = $Instance->execute($activity001);
    	
    	/*
    	 * Add a comment to the activity instance.
    	 * Comment provide a fluent interface to chain setters.
    	 */
    	$activity001Instance->getComment()->setTitle('A new comment')->setBody('My comment body');
    	
    	echo '------------------------------------------' . "\n";
    	echo 'Comment title: ' . $activity001Instance->getComment()->getTitle() . "\n";
    	echo 'Comment body: ' . $activity001Instance->getComment()->getBody() . "\n";
    	
    	/*
    	 * Each executed activity is added to a second structure in process instance.
    	 * Show it:
    	 */
    	echo '------------------------------------------' . "\n";
    	echo 'Tree of process activities instances execution:' . "\n";
		$recursiveIterator = new \RecursiveIteratorIterator( $Instance->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
		
    	echo '------------------------------------------' . "\n";
		/*
		 * To get runnings activities, simply do a foreach.
		 * The runnings activities are put in a queue
		 * 
		 */
		$queue = $Instance->getRunningActivities();
    	foreach( $queue as $a){
    		echo 'Is running: '. $a->getName() . "\n";
		}
		
		/*
		 * Its easy to execute all activities that must be.
		 */
		while( !$queue->isEmpty() ){
			echo 'Execute :' . "\n";
			$queue->dequeue()->execute();
		}
		
		echo '------------------------------------------' . "\n";
		/* 
		 * In followings code, execute each activity one to one.
		 */
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		/*
		 * If try to re-execute a completed activity, no effects!
		 */
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		
		/*
		 * The end activity set process to completed and set the ended date
		 */
		$Instance->getActivities()->getByName('activity003')->execute();
		
		echo '------------------------------------------' . "\n";
		echo 'status of process after end activity :' . $Instance->status . "\n";
		echo 'Ended date of process :' . \Workflow\Model\Sys\Date::format($Instance->ended) . "\n";
		
		/*
		 * If try to execute activity on completed instance, a exception is throws.
		 */
		try{
			echo '------------------------------------------' . "\n";
			echo 'Try to execute activity on completed process:' . "\n";
			$Instance->execute( $activity001 );
		}catch(\Exception $e){
			echo 'Error message:' . "\n";
			echo $e->getMessage() . "\n";
		}
    } //End of method
    
    
    /**
     * How to define roles and resource ACLs on activities.
     * 
     */
    public function __testRoleAcl(){
    	/*Process definition*/
    	/*For use acl it is require to define a tree attached to the root node \Workflow\Model\Org\Root*/
		$RootOu = \Workflow\Model\Org\Root::singleton();
    	$process = new Wf\Process( array('name'=>'Process__test'), $RootOu );
    	$activity001 = new Wf\Activity\Start( array('name'=>'activity001'), $process);
    	$activity002 = new Wf\Activity\Activity( array('name'=>'activity002'), $process);
    	$activity012 = new Wf\Activity\Activity( array('name'=>'activity012'), $process);
    	$activity022 = new Wf\Activity\Split( array('name'=>'activity022'), $process);
    	$activity023 = new Wf\Activity\Activity( array('name'=>'activity023'), $process);
    	$activity024 = new Wf\Activity\Activity( array('name'=>'activity024'), $process );
    	$activity025 = new Wf\Activity\Join( array('name'=>'activity025'), $process );
    	$activity003 = new Wf\Activity\End( array('name'=>'activity003'), $process );
    	$activity001->setProcess($process);
    	$activity002->setProcess($process);
    	$activity012->setProcess($process);
    	$activity022->setProcess($process);
    	$activity023->setProcess($process);
    	$activity024->setProcess($process);
    	$activity025->setProcess($process);
    	$activity003->setProcess($process);
    	$activity001->getChild()->add($activity002);
    	$activity002->getChild()->add($activity012);
    	$activity012->getChild()->add($activity022);
    	$activity022->getChild()->add($activity023);
    	$activity022->getChild()->add($activity024);
    	$activity023->getChild()->add($activity025);
    	$activity024->getChild()->add($activity025);
    	$activity025->getChild()->add($activity003);
    	$activity025->getParents()->add($activity023);
    	$activity025->getParents()->add($activity024);
    	$process->getChild()->add($activity001);
    	
    	/*Init roles and resources*/
    	$CurrentUser = \Workflow\Model\People\CurrentUser::get();
    	\Workflow\Model\Acl\Initializer::initRole( $CurrentUser );
    	\Workflow\Model\Acl\Initializer::initResource( $activity001 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity002 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity012 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity022 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity023 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity024 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity025 );
    	\Workflow\Model\Acl\Initializer::initResource( $activity003 );
    	
		//Get ACL object from singleton to get rules define internaly to People objects
		$Acl = \Workflow\Model\Acl\Acl::singleton();
		
		/* __test acl on inherit.
		 * Note that, because $process is parent of Activities, it may be use as a resource too without need to init it.
		 */
		$Acl->deny($CurrentUser->getUid(), array($process->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $activity001->getUid(), 'exec') == false);
		
		$Acl->allow($CurrentUser->getUid(), array($activity001->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $activity001->getUid(), 'exec') == true);
    }
    
    
    
	/**
	 * HOW TO GENERATE A GRAPH WITH GRAPHVIZ.
	 * 
	 */
    public function Test_TutorialGraphviz()
    {
    	/*Process definition*/
    	$process = new Wf\Process( array('name'=>'Process_test') );
    	$activity001 = new Wf\Activity\Start( array('name'=>'activity001'), $process);
    	$activity002 = new Wf\Activity\Activity( array('name'=>'activity002'), $activity001);
    	$activity012 = new Wf\Activity\Activity( array('name'=>'activity012'), $activity002);
    	$activity022 = new Wf\Activity\Split( array('name'=>'activity022'), $activity012);
    	$activity023 = new Wf\Activity\Activity( array('name'=>'activity023'), $activity022);
    	$activity024 = new Wf\Activity\Activity( array('name'=>'activity024'), $activity022 );
    	$activity025 = new Wf\Activity\Join( array('name'=>'activity025'), $activity023 );
    	$activity003 = new Wf\Activity\End( array('name'=>'activity003'), $activity025 );
    	$activity001->setProcess($process);
    	$activity002->setProcess($process);
    	$activity012->setProcess($process);
    	$activity022->setProcess($process);
    	$activity023->setProcess($process);
    	$activity024->setProcess($process);
    	$activity025->setProcess($process);
    	$activity003->setProcess($process);
    	$activity001->addChild($activity002);
    	$activity002->addChild($activity012);
    	$activity012->addChild($activity022);
    	$activity022->addChild($activity023);
    	$activity022->addChild($activity024);
    	$activity023->addChild($activity025);
    	$activity024->addChild($activity025);
    	$activity025->addChild($activity003);
    	$process->addChild($activity001);
    	
    	$activity023->isInteractive(true);
    	$activity002->isAutomatic(true);
    	
    	/*Instanciate a new graph*/
		if(defined('GRAPHVIZ_BIN_DIR') && GRAPHVIZ_BIN_DIR){
	    	$graphViz = new Wf\GraphViz(array(), GRAPHVIZ_BIN_DIR);
		}
		else{
	    	$graphViz = new \Workflow\Service\GraphViz();
		}
    	$graphViz->setPid ( $process->getNormalizedName() );
    	
    	$interactiveFontColor = 'pink';
    	$notinteractiveFontColor = 'black';
    	$automaticColor = 'blue';
    	$notautomaticColor = 'black';
    	$nodebgColor = 'white';
    	$nodeEdgesColor = 'black';
    	$bgColor = 'transparent';

    	$attributes = array(
    		'bgcolor'=>'transparent',
    	);

    	$graphViz->addAttributes(array('bgcolor'=>$bgColor));

    	/*Create an new iterator to parse the graph of activities*/
		//$recursiveIterator = new \RecursiveIteratorIterator( $process->getChildren(), \RecursiveIteratorIterator::SELF_FIRST );
    	$iterator = new Dao\Wf\Activity\RecursiveList();
    	$recursiveIterator = new \RecursiveIteratorIterator( $iterator, \RecursiveIteratorIterator::SELF_FIRST );

		//$recursiveIterator = $process->getChildren();
		$parents = array();
		foreach($recursiveIterator as $activity){
			if( $activity->isInteractive() ){
				$fontcolor = $interactiveFontColor;
			}
			else{
				$fontcolor = $notinteractiveFontColor;
			}
			
			$name = $activity->getName();
			$shape = $activity->shape;
			$url = 'foourl?uid=' . $activity->getUid();
			$graphViz->addNode ( $name, array ('URL'=>$url, 'label'=>$name, 'shape'=>$shape, 'fontcolor'=>$fontcolor, 'style'=>'filled', 'fillcolor'=>$nodebgColor, 'color'=>$nodeEdgesColor) );
			
			$level = $recursiveIterator->getDepth();
			if( $parents[$level - 1] ){
				$parentName = $parents[$level - 1];
				if( $activity->isAutomatic() ){
					$color = $automaticColor;
				}
				else{
					$color = $notautomaticColor;
				}
				$graphViz->addEdge( array( $parentName => $activity->getName() ), array ('color' => $color ) );
			}
			$parents[$level] = $activity->getName();
			echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName()  . ' Parent: ' . $parentName ."\n";
		}
    	
		$toDirectory = '/tmp';
		$graphViz->imageAndMap($toDirectory, 'png');
		
		die;
    }
    
    /**
     * 
     */
    public function __testSerialize(){
		$RootOu = \Workflow\Model\Org\Root::singleton();
    	$process = new Wf\Process( array('name'=>'Process__test'), $RootOu );
    	$actOu = new \Workflow\Model\Org\Unit( array('name'=>'activities'), $process);
    	$activity001 = new Wf\Activity\Start( array('name'=>'activity001'), $actOu);
    	$activity002 = new Wf\Activity\Activity( array('name'=>'activity002'), $actOu);
    	$activity012 = new Wf\Activity\Activity( array('name'=>'activity012'), $actOu);
    	$activity022 = new Wf\Activity\Split( array('name'=>'activity022'), $actOu);
    	$activity023 = new Wf\Activity\Activity( array('name'=>'activity023'), $actOu);
    	$activity024 = new Wf\Activity\Activity( array('name'=>'activity024'), $actOu );
    	$activity025 = new Wf\Activity\Join( array('name'=>'activity025'), $actOu );
    	$activity003 = new Wf\Activity\End( array('name'=>'activity003'), $actOu );
    	$activity001->setProcess($process);
    	$activity002->setProcess($process);
    	$activity012->setProcess($process);
    	$activity022->setProcess($process);
    	$activity023->setProcess($process);
    	$activity024->setProcess($process);
    	$activity025->setProcess($process);
    	$activity003->setProcess($process);
    	$activity001->getChild()->add($activity002);
    	$activity002->getChild()->add($activity012);
    	$activity012->getChild()->add($activity022);
    	$activity022->getChild()->add($activity023);
    	$activity022->getChild()->add($activity024);
    	$activity023->getChild()->add($activity025);
    	$activity024->getChild()->add($activity025);
    	$activity025->getChild()->add($activity003);
    	$activity025->getParents()->add($activity023);
    	$activity025->getParents()->add($activity024);
    	$process->getChild()->add($activity001);
    	
    	//var_dump( serialize($activity025) );
    	//var_dump( serialize($process) );
    	
    	Wf\Serializer::export($process, './tmp/serialize__test.xml');
    	$process2 = Wf\Serializer::import( './tmp/serialize__test.xml', $actOu );
    	
    	assert( $process2->getUid() ==  $process->getUid() );
    	assert( $process2->getName() ==  $process->getName() );
    	assert( $process2->getActivities()->count() ==  $process->getActivities()->count() );
    	assert( $process2->getActivities()->getByIndex(0)->getName() ==  $process->getActivities()->getByIndex(0)->getName() );
    	assert( $process2->getActivities()->getByIndex(7)->getUid() ==  $process->getActivities()->getByIndex(7)->getUid() );
    	
		$recursiveIterator = new \RecursiveIteratorIterator( $process2->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $activity){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName() . "\n";
		}
    	
		Wf\Graph\Generator::generate($process2);
    	
    }
    
    
    public function __testDao(){
    	\Workflow\Model\Dao\Pg\ClassDao::singleton()->setConnexion( Connexion::get() );
    	
		$RootOu = \Workflow\Model\Org\Root::singleton();
		
		$processName = uniqid('Process__test');
    	$process = new Wf\Process( array('name'=>$processName), $RootOu );
    	$actOu = new \Workflow\Model\Org\Unit( array('name'=>uniqid('activities')), $RootOu);
    	
    	$activity001 = new Wf\Activity\Start( array('name'=>uniqid('activity001')), $actOu);
    	$activity002 = new Wf\Activity\Activity( array('name'=>uniqid('activity002')), $actOu);
    	$activity012 = new Wf\Activity\Activity( array('name'=>uniqid('activity012')), $actOu);
    	$activity022 = new Wf\Activity\Split( array('name'=>uniqid('activity022')), $actOu);
    	$activity023 = new Wf\Activity\Activity( array('name'=>uniqid('activity023')), $actOu);
    	$activity024 = new Wf\Activity\Activity( array('name'=>uniqid('activity024')), $actOu );
    	$activity025 = new Wf\Activity\Join( array('name'=>uniqid('activity025')), $actOu );
    	$activity003 = new Wf\Activity\End( array('name'=>uniqid('activity003')), $actOu );
    	$activity001->setProcess($process);
    	$activity002->setProcess($process);
    	$activity012->setProcess($process);
    	$activity022->setProcess($process);
    	$activity023->setProcess($process);
    	$activity024->setProcess($process);
    	$activity025->setProcess($process);
    	$activity003->setProcess($process);
    	$activity001->getChild()->add($activity002);
    	$activity002->getChild()->add($activity012);
    	$activity012->getChild()->add($activity022);
    	$activity022->getChild()->add($activity023);
    	$activity022->getChild()->add($activity024);
    	$activity023->getChild()->add($activity025);
    	$activity024->getChild()->add($activity025);
    	$activity025->getChild()->add($activity003);
    	$activity025->getParents()->add($activity023);
    	$activity025->getParents()->add($activity024);
    	$process->getChild()->add($activity001);
    	
    	/*BE CAREFUL to order of save. Parents must be always save first*/
    	
    	try{
	    	$RootOuDao = new \Workflow\Model\Org\RootDaoPg();
			$RootOuDao->setConnexion( Connexion::get() );
			$RootOuDao->save( $RootOu );
    	}
    	catch(\Exception $e){
    		//nothing
    	}
    	
    	$OuDao = new \Workflow\Model\Org\UnitDaoPg( array(), Connexion::get() );
    	
    	/*Clean previous activities*/
    	$List = $OuDao->newList();
    	$List->suppress("path::ltree <@ '" . \Workflow\Model\Dao\Pg::pathToSys($actOu->getPath()) . "'");
    	
    	/*Save OU*/
		$OuDao->save( $actOu );
    	
    	$processDao = new Wf\ProcessDaoPg();
		$processDao->setConnexion( Connexion::get() );
		$processDao->save( $process );
    	
    	foreach( $process->getLinks() as $link ){
    		var_dump($link->getName(), get_class($link) );
    		if( is_a($link, '\Workflow\Model\Model\Collection') ){
    			foreach($link as $subLink){
		    		var_dump($subLink->getName(), get_class($subLink));
		    		$this->_saveActivity($subLink);
    			}
    		}
    		else{
    			$this->_saveActivity($link);
    		}
    	}
		$uid = $process->getUid();
		
		/***************************** __test LOAD ********************************/
    	$processDao = new Wf\ProcessDaoPg();
		$processDao->setConnexion( Connexion::get() );
    	
		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid, array(), true);
		assert( $process->getName() == $processName );
		assert( $process->isLoaded() == true );
		assert( \Workflow\Model\Uuid::compare($process->getUid(), $uid) );
		assert( \Workflow\Model\Uuid::compare( $process->parentId, $RootOu->getUid() ) );
		
		/** LOAD THE PARENT **/
		\Workflow\Model\Dao\Pg\Loader::loadParent($process);
		assert( $process->getParent()->getName() == $RootOu->getName() );
		
		/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/		
		$List = new \Workflow\Model\Dao\Pg\DaoList( array('table'=>'view_wf_Activity\links') );
		$List->setConnexion( Connexion::get() );
		$List->load("lparent='$uid'");
		
		//convert list to collection
		$collection = new \Workflow\Model\Model\Collection();
		$List->loadInCollection( $process->getActivities() );
		
		foreach($process->getActivities() as $A){
			var_dump( $A->getName() );
		}
		
		/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
		$process = new Wf\Process();
		$processDao->loadFromUid($process, $uid, array(), true);
		\Workflow\Model\Dao\Pg\Loader::loadParent($process);
		$List = $processDao->getActivities($process);
		
		$activityDao = new Wf\ProcessDaoPg( array(), Connexion::get() );
		
		$CollectionListBridge = new \Workflow\Model\Model\CollectionListBridge($process->getActivities(), $List);
		foreach($CollectionListBridge as $A){
			\Workflow\Model\Dao\Pg\Loader::loadParent($A);
			var_dump( $A->getName() , $A->getParent()->getName() );
		}
    }
    
    protected function _saveActivity( $activity )
    {
    	$activityDao = new Wf\ActivityDaoPg();
		$activityDao->setConnexion( Connexion::get() );
		$activityDao->save( $activity );
    }
    
    public function Test_Tutorial()
    {
    	$processName = "Work unit";
    	$process = new Wf\Process();
    	DaoFactory::get()->getDao($process)->loadFromName($process, $processName);
    	
    	//Create instance of process
    	$processInstance = Wf\Instance::start($process);
    	DaoFactory::get()->getDao($processInstance)->save($processInstance);
    	
    	//Find Start activity and run it
    	$Start = new Activity\Start();
    	DaoFactory::get()->getDao($Start)->load($Start, "type='start' AND processId=".$process->getId());
    	$activityInstance = $processInstance->execute($Start);
    	DaoFactory::get()->getDao($activityInstance)->save($activityInstance);

    	//add a listener to this instance
    	//Signal::connect($activityInstance, 'runactivity.pre', array($code, 'onRun'));
    	
    	//Get next candidates activities
    	$nextList = DaoFactory::get()->getDao($processInstance)->getNextCandidates($Start->getId());

    	//Create activities instance and set running status on next activities
    	foreach($nextList as $properties){
    		$nextActivity = new Activity($properties);
    		$nextStatus = $properties['nextStatus'];
    		$nextInstance = Instance\Activity::start($nextActivity, $processInstance);
    		DaoFactory::get()->getDao($nextInstance)->save($nextInstance);
    	}
    	
    	//now get the runnings activities and execute it
    	$runningList = DaoFactory::get()->getDao($processInstance)->getRunningActivities($processInstance->getId());
    	foreach($runningList as $properties){
    		$runningInstanceActivity = new Instance\Activity($properties);
    		$type = $properties['type'];
    		
    		//set the source activity object
    		$activity = Activity::factory($type, false);
    		DaoFactory::get()->getDao($activity)->loadFromId($activity, $properties['activityId']);
    		$runningInstanceActivity->setActivity($activity);
    		$runningInstanceActivity->execute();
    		
    		//save the state of instance
    		DaoFactory::get()->getDao($runningInstanceActivity)->save($runningInstanceActivity);
    		
    		var_dump($runningInstanceActivity);
    	}
    	
    	
    	die;
    	 
    	/*BE CAREFUL to order of save. Parents must be always save first*/
    	 
    	/***************************** __test LOAD ********************************/
    	$processDao = new Wf\ProcessDaoPg();
    	$processDao->setConnexion( Connexion::get() );
    	 
    	$process = new Wf\Process();
    	$processDao->loadFromUid($process, $uid, array(), true);
    	assert( $process->getName() == $processName );
    	assert( $process->isLoaded() == true );
    	assert( \Workflow\Model\Uuid::compare($process->getUid(), $uid) );
    	assert( \Workflow\Model\Uuid::compare( $process->parentId, $RootOu->getUid() ) );
    
    	/** LOAD THE PARENT **/
    	\Workflow\Model\Dao\Pg\Loader::loadParent($process);
    	assert( $process->getParent()->getName() == $RootOu->getName() );
    
    	/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/
    	$List = new \Workflow\Model\Dao\Pg\DaoList( array('table'=>'view_wf_Activity\links') );
    	$List->setConnexion( Connexion::get() );
    	$List->load("lparent='$uid'");
    
    	//convert list to collection
    	$collection = new \Workflow\Model\Model\Collection();
    	$List->loadInCollection( $process->getActivities() );
    
    	foreach($process->getActivities() as $A){
    		var_dump( $A->getName() );
    	}
    
    	/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
    	$process = new Wf\Process();
    	$processDao->loadFromUid($process, $uid, array(), true);
    	\Workflow\Model\Dao\Pg\Loader::loadParent($process);
    	$List = $processDao->getActivities($process);
    
    	$activityDao = new Wf\ProcessDaoPg( array(), Connexion::get() );
    
    	$CollectionListBridge = new \Workflow\Model\Model\CollectionListBridge($process->getActivities(), $List);
    	foreach($CollectionListBridge as $A){
    		\Workflow\Model\Dao\Pg\Loader::loadParent($A);
    		var_dump( $A->getName() , $A->getParent()->getName() );
    	}
    }
    
    
    
    	
} //End of class

