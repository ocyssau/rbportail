<?php
//%LICENCE_HEADER%

namespace Workflow\Model\Wf;

/**
 * @brief \Exception for Rbplm.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Exception extends \Exception
{
}
