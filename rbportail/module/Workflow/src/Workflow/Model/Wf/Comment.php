<?php
//%LICENCE_HEADER%

namespace Workflow\Model\Wf;
use Application\Model\Uuid;

/**
 *
 */
class Comment
{
	
    /**
     * Uuid
     * @var string
     */
    protected $_uid = null;
	
    /**
     * @var string
     */
    protected $_title = null;

    /**
     * @var string
     */
    protected $_body = null;
    
    
    /**
     * 
     * @param string	$title
     * @param string	$body
     * @return void
     */
    public function __construct($title='', $body='')
    {
    	$this->uid = Uuid::newUid();
    	$this->title = $title;
    	$this->body = $body;
    }
    
    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }
    
    /**
     * @param string
     */
    public function setUid( $uid )
    {
        $this->uid = $uid;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Comment	Fluent interface
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $comment
     * @return Comment	Fluent interface
     */
    public function setBody($comment)
    {
        $this->body = $comment;
        return $this;
    }

}

