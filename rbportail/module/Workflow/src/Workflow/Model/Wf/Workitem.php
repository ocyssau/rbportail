<?php
namespace Workflow\Model\Wf;

use Application\Model\Any;

/**
 * A class representing workitems
 * Not yet implemented
 *
 */
class Workitem extends Any
{
	/**
	 * 
	 * @var \Workflow\Model\Wf\Instance
	 */
	public $instance;
	
	/**
	 * 
	 * @var array
	 */
	public $properties = array();
	
	/**
	 * 
	 * @var integer
	 */
	public $started;
	
	/**
	 * 
	 * @var integer
	 */
	public $ended;
	
	/**
	 * 
	 * @var \Workflow\Model\Wf\Activity
	 */
	public $activity;
}
