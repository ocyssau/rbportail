<?php


namespace Workflow\Model\Wf;

/**
 * @brief Export/Import a process and his anyobjects.
 * 
 * Serializer class to save full process definition (process, activities, codes, templates) into xml file.
 *
 */
class Serializer{
	

	/**
	 * Creates an XML representation of a process.
	 * 
	 * @param \Workflow\Model\Wf\Process $process
	 */
	public static function export( \Workflow\Model\Wf\Process $process, $file ) 
	{
		$xmldoc = new \DOMDocument('1.0');
		$xmldoc->formatOutput = true;
		$processNode = $xmldoc->appendChild( $xmldoc->createElement( 'process', $process->getUid() ) );

		/*Serialize process*/
		$sxmlNode = dom_import_simplexml( new \SimpleXMLElement( $process->xmlSerialize() ) );
		$importSxmlNode = $xmldoc->importNode($sxmlNode, true);
		$importSxmlNode = $processNode->appendChild( $importSxmlNode );
		
		/*Include shared code*/
		$sharedCodeNode = $processNode->appendChild( $xmldoc->createElement( 'sharedCode', htmlspecialchars($process->description) ) );
		
		/*Include activities*/
		$Activities = $process->getActivities();
		$Activity = $Activities->getByIndex(0);
		
		/**/
		$sharedCodeFileName = $Activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_SHARED);
		$sharedCode = file_get_contents($sharedCodeFileName);
		$sharedCdata = $xmldoc->createCDATASection( $sharedCode );
		$sharedCodeNode->appendChild($sharedCdata);
		
		$transitionsNode = $processNode->appendChild( $xmldoc->createElement( 'transitions' ) );
		
		/* Get all activities and serialize it */
		foreach($Activities as $activity){
			$activityNode = $processNode->appendChild( $xmldoc->createElement( 'activity', $activity->getUid() ) );
			
			$codeNode = $activityNode->appendChild( $xmldoc->createElement( 'code' ) );
			$codeFileName = $activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_CODE_SOURCE);
			$codeCode = file_get_contents($codeFileName);
			$codeNode->appendChild( $xmldoc->createCDATASection( $codeCode ) );
			
			if( $activity->isInteractive() ){
				$tplNode = $activityNode->appendChild( $xmldoc->createElement( 'template' ) );
				$tplFileName = $activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_TEMPLATE_SOURCE);
				$tplCode = file_get_contents($tplFileName);
				$tplNode->appendChild( $xmldoc->createCDATASection( $tplCode ) );
			}
			
			/*Serialize activity*/
			$sxmlNode = dom_import_simplexml( new \SimpleXMLElement( $activity->xmlSerialize() ) );
			$importSxmlNode = $xmldoc->importNode($sxmlNode, true);
			$importSxmlNode = $activityNode->appendChild( $importSxmlNode );
			
			/*Include transitions*/
			foreach( $activity->getChild() as $child ){
				$transitionNode = $transitionsNode->appendChild( $xmldoc->createElement( 'transition' ) );
				$transitionNode->appendChild( $xmldoc->createElement( 'from', $activity->getUid() ) );
				$transitionNode->appendChild( $xmldoc->createElement( 'to', $child->getUid() ) );
			}
		}
		
		return $xmldoc->save($file);
		
	}

	/**
	 * Creates an XML representation of a process.
	 * 
	 * @param string				$file	Input Xml file
	 * @param \Rbplm\Any			$parent	Parent anyobject container where put the process and activities.
	 */
	public static function import( $file, \Rbplm\Any $parent=null ) {
		$xmldoc = new \DOMDocument();
		$xmldoc->load( $file );
		$xpath = new \DOMXPath($xmldoc);
		
		/* Build the process */
		$entries = $xpath->query( '//process/properties/*' );
		$properties = array();
		foreach($entries as $property){
			$properties[$property->tagName] = $property->nodeValue;
		}
		$process = new \Workflow\Model\Wf\Process($properties, $parent);
		
		/* Build each activity */
		$activities = $xpath->query( '//process/activity' );
		for ($i = 1; $i <= $activities->length; $i++) {
			$properties = array();
			$property = null;
			foreach( $xpath->query( "//process/activity[$i]/properties/*" ) as $property ){
				$properties[$property->tagName] = $property->nodeValue;
			}

			$Activity = \Workflow\Model\Wf\Activity::factory($properties, $parent);
			$Activity->setProcess($process);
			
			if( $Activity->type == \Workflow\Model\Wf\Activity::TYPE_JOIN ){
		        foreach( $process->getActivities() as $act ){
		        	if($act->getChild()->contains($Activity)){
		        		$Activity->getParents()->add($act);
		        	}
		        }
			}
		}
		
		$transitions = $xpath->query( '//process/transitions/transition' );
		for ($i = 1; $i <= $transitions->length; $i++) {
			$properties = array();
			$property = null;
			foreach( $xpath->query( "//process/transitions/transition[$i]/*" ) as $property ){
				$properties[$property->tagName] = $property->nodeValue;
			}
			
			$from = $properties['from'];
			$to = $properties['to'];
			
			$fromActivity = $process->getActivities()->getByUid($from);
			$toActivity = $process->getActivities()->getByUid($to);
			$fromActivity->getChild()->add($toActivity);
		}
		
		return $process;
	}
	
	
} //End of class


