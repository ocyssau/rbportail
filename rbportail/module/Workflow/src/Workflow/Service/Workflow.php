<?php
namespace Workflow\Service;

use Workflow\Model\Wf;
use Application\Dao\Registry;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Loader;
use Exception;
use Application\Model\Signal;
use Application\Model\People;

use Application\Dao\Filter\Op;

class Workflow
{

	const SIGNAL_START_POST = 'start.post';

	const SIGNAL_RUNACTIVITY_PRE = 'runactivity.pre';

	const SIGNAL_RUNACTIVITY_POST = 'runactivity.post';

	const SIGNAL_TRANSLATE_PRE = 'translate.pre';

	const SIGNAL_TRANSLATE_POST = 'translate.post';

	const SIGNAL_COMPLET_POST = 'complete.post';

	const SIGNAL_SAVEPROCESS_PRE = 'saveprocess.pre';
	
	const SIGNAL_SAVEPROCESS_POST = 'saveprocess.post';

	const SIGNAL_DELETEPROCESS_PRE = 'deleteprocess.pre';
	
	const SIGNAL_DELETEPROCESS_POST = 'deleteprocess.post';

	const SIGNAL_SAVEACTIVITY_PRE = 'saveactivity.pre';
	
	const SIGNAL_SAVEACTIVITY_POST = 'saveactivity.post';

	const SIGNAL_DELETEACTIVITY_PRE = 'deleteactivity.pre';
	
	const SIGNAL_DELETEACTIVITY_POST = 'deleteactivity.post';

	/* send by activity controller */
	const SIGNAL_DELETEINSTANCE_POST = 'deleteinstance.post';

	/**
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 * @var Wf\Process
	 */
	public $process;

	/**
	 * @var Wf\Instance
	 */
	public $instance;

	/**
	 * @var Wf\Activity\Start
	 */
	public $startActivity;

	/**
	 * @var Wf\Instance\Activity
	 */
	public $startInstance;

	/**
	 * @var Wf\Instance\Activity
	 */
	public $lastActivity;
	
	/**
	 * @var array
	 */
	protected $nextTransitions = [];

	/**
	 * @var string
	 */
	public $nextStatus;

	/**
	 * @var Wf\Instance\Activity
	 */
	public $nextActivity;

	/**
	 *
	 */
	public function __construct()
	{
		$this->factory = DaoFactory::get();
	}

	/**
	 * @param DaoFactory $factory
	 */
	public function setFactory($factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 * @return DaoFactory
	 */
	public function getFactory()
	{
		return $this->factory;
	}

	/**
	 *
	 * query graph =
	 * array (size=3)
	 * 'activities' =>
	 * array (size=3)
	 * 0 =>
	 * array (size=8)
	 * 'id' => string 'sample' (length=6)
	 * 'name' => string 'sample' (length=6)
	 * 'title' => string 'SAMPLE' (length=6)
	 * 'type' => string 'activity' (length=8)
	 * 'isAutomatic' => string 'false' (length=5)
	 * 'isComment' => string 'false' (length=5)
	 * 'isInteractive' => string 'false' (length=5)
	 * 'attributes' =>
	 * array (size=2)
	 * 'positionx' => string '630px' (length=5)
	 * 'positiony' => string '90.5px' (length=6)
	 * 1 =>
	 * 'transitions' =>
	 * array (size=2)
	 * 0 =>
	 * array (size=3)
	 * 'id' => string 'con_12' (length=6)
	 * 'fromid' => string 'sample' (length=6)
	 * 'toid' => string '50' (length=2)
	 * 'processid' => string '8' (length=1)
	 *
	 *
	 * @param \Workflow\Model\Wf\Process $process
	 * @param array $activities
	 * @param array $transitions
	 */
	public function saveProcess($process, $activities, $transitions)
	{
		$this->process = $process;
		$processId = $process->getId();

		/* create activitities */
		$registry = array();
		$return = array(
			'activities' => array(),
			'transitions' => array()
		);
		
		/* signal is send before save */
		$e = Signal::trigger(self::SIGNAL_SAVEPROCESS_PRE, $this);
		if ( $e->hasError() ) {
			throw new Exception(implode(' - ', $e->getErrors()));
		}

		if ( $activities ) {
			foreach( $activities as $properties ) {
				if ( $properties['loaded'] == true ) {
					$aid = $properties['id'];
					if ( !$aid ) {
						throw new Exception("Activity Id is not set");
					}
					$activity = Wf\Activity::factory($properties['type'], false);
					$this->factory->getDao($activity)->loadFromId($activity, $aid);
				}
				else {
					$activity = Wf\Activity::factory($properties['type'], true);
					$properties['processId'] = $processId;
				}

				if ( isset($properties['roles']) ) {
					if ( !is_array($properties['roles']) ) {
						$properties['roles'] = explode(',', $properties['roles']);
					}
				}

				if ( isset($properties['name']) ) {
					$properties['name'] = \Workflow\Helper\Normalizer::normalize($properties['name']);
				}

				$domId = $properties['attributes']['id']; /* id of element as record in dom */
				$activity->hydrate($properties);

				$this->factory->getDao($activity::$classId)->save($activity);

				$e = Signal::trigger(self::SIGNAL_SAVEACTIVITY_POST, $this, $activity);
				$registry[$domId] = $activity;
				$properties['id'] = $activity->getId();
				$return['activities'][] = $properties;
			}
		}

		/* delete transitions... */
		$dao = $this->factory->getDao(Wf\Transition::$classId);
		$dao->deleteFromProcess($processId);

		/* ...and re-create transitions */
		if ( $transitions ) {
			foreach( $transitions as $properties ) {
				$fromActivity = $registry[$properties['fromid']];
				$toActivity = $registry[$properties['toid']];

				$transition = Wf\Transition::factory($fromActivity, $toActivity);
				$transition->setStatus($properties['status']);
				$transition->setAttributes(array(
					'fromAnchor' => $properties['fromAnchor'],
					'toAnchor' => $properties['toAnchor']
				));
				$dao->insert($transition);
				$properties['id'] = $transition->id;
				$return['transitions'][] = $properties;
			}
		}

		/* signal is send after save */
		$e = Signal::trigger(self::SIGNAL_SAVEPROCESS_POST, $this);
		if ( $e->hasError() ) {
			throw new Exception(implode(' - ', $e->getError()));
		}

		$this->process->isValid(true);
		$this->factory->getDao($this->process)->save($this->process);

		return $return;
	}

	/**
	 */
	public function deleteProcess($processId)
	{
		/* load */
		$dao = $this->factory->getDao(Wf\Process::$classId);

		$process = new Wf\Process();
		$dao->loadFromId($process, $processId);
		$this->process = $process;

		$connexion = $dao->getConnexion();
		$connexion->beginTransaction();

		try {
			/* 1>delete activity instance */
			$this->factory->getDao(Wf\Instance\Activity::$classId)->deleteFromProcess($processId);

			/* 2>delete instances */
			$this->factory->getDao(Wf\Instance::$classId)->deleteFromProcess($processId);

			/* 3>delete transitions */
			$this->factory->getDao(Wf\Transition::$classId)->deleteFromProcess($processId);

			/* 4>delete activity */
			$this->factory->getDao(Wf\Activity::$classId)->deleteFromProcess($processId);

			/* 5>delete process */
			$dao->deleteFromId($processId, $withchild = true, $withtrans = false);

			$connexion->commit();

			$e = Signal::trigger(self::SIGNAL_DELETEPROCESS_POST, $this, $process);
			if ( $e->hasError() ) {
				throw new Exception($e->getError());
			}
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw $e;
		}

		return $process;
	}

	/**
	 */
	public function deleteActivity($activityId)
	{
		/* load activity */
		$activity = new Wf\Activity();
		$dao = $this->factory->getDao($activity);
		$dao->loadFromId($activity, $activityId);

		$ok = $dao->deleteFromId($activityId, $withChild=true, $withTrans=true);
		if ( $ok ) {
			$this->activity = $activity;
			$e = Signal::trigger(self::SIGNAL_DELETEACTIVITY_POST, $this, $activity);
			if ( $e->hasError() ) {
				throw new Exception($e->getError());
			}
		}

		return $activity;
	}

	/**
	 */
	public function deleteInstance($instanceId)
	{
		/* load instance */
		$instance = new Wf\Instance();

		try {
			$instanceDao = $this->factory->getDao(Wf\Instance::$classId);
			$connexion = $instanceDao->getConnexion();
			$connexion->beginTransaction();

			/* 1>delete activity instance */
			$this->factory->getDao(Wf\Instance\Activity::$classId)->deleteFromInstance($instanceId, false, false);

			/* 2>delete instances */
			$instanceDao->deleteFromId($instanceId, true, false);

			$connexion->commit();

			$e = Signal::trigger(self::SIGNAL_DELETEINSTANCE_POST, $this, $instance);
			if ( $e->hasError() ) {
				throw new Exception($e->getError());
			}
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * Start a process from his id and return an instance with the start activity instanciate
	 * Return a start activity
	 *
	 * @param integer $processId
	 * @return Workflow
	 */
	public function startProcess($processId=null)
	{
		/* set DAOs */
		$procDao = $this->factory->getDao(Wf\Process::$classId);
		$procInstDao = $this->factory->getDao(Wf\Instance::$classId);
		$actDao = $this->factory->getDao(Wf\Activity::$classId);

		/* Load the process definition */
		if ( !$this->process && $processId) {
			$this->process = new Wf\Process();
			$procDao->loadFromId($this->process, $processId);
		}
		else{
			$processId = $this->process->getId();
		}
		$proc = $this->process;

		if ( $proc->isActive() == false ) {
			throw new Exception('this process is not active');
		}

		/* Instanciate process */
		$procInst = Wf\Instance::start($proc);

		/* Find Start activity... */
		$startAct = new Wf\Activity\Start();

		$filter = $this->factory->getFilter($startAct->cid)->setOption('asapp', true);
		$filter->andfind($processId, $actDao->toSys('processId'), Op::EQUAL);
		$filter->andfind('start', 'type', Op::EQUAL);
		$actDao->load($startAct, $filter);

		/* Execute start activity and save it */
		$startActInst = $procInst->execute($startAct);

		$registry = Registry::singleton();
		$registry->add($proc);
		$registry->add($procInst);
		$registry->add($startAct);

		$this->lastActivity = $startActInst;
		$this->startInstance = $startActInst;
		$this->startActivity = $startAct;
		$this->instance = $procInst;

		/* signal is send before save */
		$e = Signal::trigger(self::SIGNAL_START_POST, $this);
		if ( $e->hasError() ) {
			throw new Exception($e->getError());
		}

		/* save process instance */
		$procInstDao->save($procInst);

		/* now proc instance id is set, set process instance to activity instance here */
		$startActInst->setInstance($procInst);

		/* ...and save it */
		$this->factory->getDao($startActInst)->save($startActInst);

		return $this;
	}

	/**
	 * Run the instance of an activity
	 * Return Lazy interface
	 *
	 * @param integer $activityId
	 *        	Id of activity instance to run
	 * @return Workflow
	 */
	public function runActivity($actInstOrId)
	{
		$loader = $this->factory->getLoader();

		/* load activity instance */
		if ( $actInstOrId instanceof Wf\Instance\Activity ) {
			$actInst = $actInstOrId;
		}
		else {
			$actInst = $loader->loadFromId($actInstOrId, Wf\Instance\Activity::$classId);
		}
		$this->lastActivity = $actInst;
		$actInstId = $actInst->getId();

		/* Load instance of process */
		$procInstId = $actInst->getInstance(true);
		if ( !$this->instance ) {
			$this->instance = $loader->loadFromId($procInstId, Wf\Instance::$classId);
		}

		$procInst = $this->instance;

		/* create link, need by execute method */
		$actInst->setInstance($procInst);

		/* signal is send before save  */
		$e = Signal::trigger(self::SIGNAL_RUNACTIVITY_PRE, $this, $actInst);
		if ( $e->hasError() ) {
			$error = $e->getLastError();
			throw new Exception($error);
		}

		/* execute activity */
		$actInst->execute();
		$type = $actInst->getType();

		/* END activity */
		if ( $type == Wf\Activity::TYPE_END ) {
			/* close the current process */
			$this->complete($this->instance->getId());
		}
		/* JOIN activity */
		elseif ( $type == Wf\Activity::TYPE_JOIN ) {
			/* check that all previous acitivies are completed before translate this activity */
		}
		/* SWITCH activity */
		elseif ( $type == Wf\Activity::TYPE_SWITCH ) {
			if ( !$this->nextTransitions ) {
				throw (new Exception('None transitions specified'));
			}
			$this->translateActivity($actInstId);
		}
		/* STANDALONE activity */
		elseif ( $type == Wf\Activity::TYPE_STANDALONE ) {
			/* NOTHING TO DO, trigger event and return */
			/* signal is send before save */
			$e = Signal::trigger(self::SIGNAL_RUNACTIVITY_POST, $this, $actInst);
			if ( $e->hasError() ) {
				throw new Exception($e->getError());
			}
			return $this;
		}
		else {
			/* TRANSLATE : */
			$this->translateActivity($actInstId);
		}

		/* signal is send before save */
		$e = Signal::trigger(self::SIGNAL_RUNACTIVITY_POST, $this, $actInst);
		if ( $e->hasError() ) {
			throw new Exception($e->getError());
		}

		/* save process and activity */
		$this->factory->getDao($actInst)->save($actInst);
		$this->factory->getDao($procInst)->save($procInst);

		return $this;
	}

	/**
	 *
	 * @param integer $activityOrId
	 * @param integer $procInstId
	 *        	Process instance Id
	 * @throws Exception
	 */
	public function runStandalone($activityOrId, $procInstId)
	{
		/* load activity */
		if ( is_object($activityOrId) ) {
			$activity = $activityOrId;
		}
		else {
			$activity = Loader::get()->loadFromId($activityOrId, Wf\Activity\Standalone::$classId);
		}
		unset($activityOrId);

		/* Load instance of process */
		if ( !$this->instance ) {
			$this->instance = Loader::get()->loadFromId($procInstId, Wf\Instance::$classId);
		}
		$procInst = $this->instance;

		/* Start activity = Create an activity instance of standalone activity */
		$actInst = Wf\Instance\Activity::start($activity, $procInst);

		return $this->runActivity($actInst);
	}

	/**
	 * Start a process from his id and return an instance with the start activity instanciate
	 *
	 * @param integer $activityId
	 *        	Id of activity instance to translate
	 * @return Workflow
	 */
	public function translateActivity($actInstId, $selectedNextTrans = null)
	{
		/* get the instance of process */
		if ( !isset($this->instance) ) {
			throw (new Exception('$this->instance is not set'));
		}
		$procInst = $this->instance;

		$e = Signal::trigger(self::SIGNAL_TRANSLATE_PRE, $this);
		if ( $e->hasError() ) {
			throw new Exception($e->getError());
		}

		$actInstDao = $this->factory->getDao(Wf\Instance\Activity::$classId);
		$procInstDao = $this->factory->getDao(Wf\Instance::$classId);

		/* get list of candidates */
		$selectedNextTrans = $this->getNextTransitions();
		$selectedNextAct = $procInst->getNextActivities();

		$nexts = $procInstDao->getNextCandidates($actInstId, $selectedNextAct, $selectedNextTrans);

		if ( count($nexts) == 0 ) {
			throw new Exception("None next activities for activity instance " . $actInstId);
		}

		/* instanciate candidates activities and start it */
		foreach( $nexts as $next ) {
			$nextActivity = new Wf\Activity($next);

			$this->nextStatus = $next['nextStatus'];
			$this->nextActivity = Wf\Instance\Activity::start($nextActivity, $procInst);

			if($this->lastActivity){
				$this->nextActivity->setParent($this->lastActivity);
			}

			/* run automaticaly end activity or automatic activity */
			if ( $this->nextActivity->getType() == 'end' || $this->nextActivity->isAutomatic() == true ) {
				$this->runActivity($this->nextActivity);
			}
			$actInstDao->save($this->nextActivity);
		}

		/* signal is send before save */
		$e = Signal::trigger(self::SIGNAL_TRANSLATE_POST, $this);
		if ( $e->hasError() ) {
			throw new Exception($e->getError());
		}

		return $this;
	}

	/**
	 * Close the current process instance
	 * 
	 * @return Workflow
	 */
	public function complete()
	{
		$this->instance->setStatus(Wf\Instance::STATUS_COMPLETED)->setEnded(new \DateTime());

		/* signal is send before save */
		$e = Signal::trigger(self::SIGNAL_COMPLET_POST, $this);
		if ( $e->hasError() ) {
			throw new Exception($e->getError());
		}
		$this->factory->getDao($this->instance)->save($this->instance);
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getNextTransitions()
	{
		return $this->nextTransitions;
	}

	/**
	 *
	 * @param array $array
	 * @return Workflow
	 */
	public function setNextTransition($transition)
	{
		$this->nextTransitions[] = "'$transition'";
		return $this;
	}

	/**
	 *
	 * @param string $processId
	 * @return array
	 */
	public function export($processId)
	{
		$procDao = $this->factory->getDao(Wf\Process::$classId);
		
		$return = array(
			'process' => array(),
			'activities' => array(),
			'transitions' => array(),
		);

		/* Load the process definition */
		if ( !$this->process ) {
			$this->process = new Wf\Process();
			$procDao->loadFromId($this->process, $processId);
		}
		$process = $this->process->getArrayCopy();
		$return['process'] = $process;
		
		/* load activities */
		$activities = $this->factory->getList(Wf\Activity\Activity::$classId);
		$activityDao = $this->factory->getDao(Wf\Activity\Activity::$classId);
		$activities->setOption('asapp', true);
		$activities->load($activityDao->toSys('processId') . '=:processId', array(
			':processId' => $processId
		));
		foreach( $activities as $item ) {
			$return['activities'][$item['uid']] = $item;
		}
		
		/* load transitions */
		$transitions = $procDao->getTransitions($processId);
		$transDao = $this->factory->getDao(Wf\Transition::$classId);
		foreach( $transitions as $item ) {
			$item = $transDao->getTranslator()->toApp($item);
			$return['transitions'][$item['uid']] = $item;
		}
		
		return $return;
	}

	/**
	 *
	 * @param string json $data
	 * @param string $version
	 * @param string $name
	 * @throws Exception
	 * @return \Workflow\Model\Wf\Process
	 */
	public function import($data, $version = null, $name = null)
	{
		$data = iconv('ISO-8859-1', 'ISO-8859-1//TRANSLIT', $data);
		$data = json_decode($data, true);

		if ( !isset($data['process']) ) {
			throw new Exception('Bad import data format');
		}

		$process = new Wf\Process();
		$process->hydrate($data['process']);
		$process->newUid();
		$process->isActive(false);
		$process->setOwner(People\CurrentUser::get());

		if ( $version ) {
			$process->setVersion($version);
			$process->setNormalizedName(null);
		}

		if ( $name ) {
			$process->setName($name);
			$process->setNormalizedName(null);
		}

		/* reinit normalizedName */
		$process->getNormalizedName();

		try {
			$this->factory->getDao($process)->save($process);
		}
		catch( \Exception $e ) {
			$nName = $process->getNormalizedName();
			throw new Exception('this process can not be saved. The name-version ' . $nName . ' is probably yet in used.' . $e->getMessage());
		}

		/* import activities */
		if ( isset($data['activities']) ) {
			$activityRegistry = array();
			foreach( $data['activities'] as $item ) {
				$activity = Wf\Activity::factory($item['type'], false);
				$activity->hydrate($item);

				$activityRegistry[$activity->getUid()] = $activity;/* registry is indexed with original uid */
				$activity->newUid();
				$activity->setProcess($process, true);
				$this->factory->getDao($activity)->save($activity);
			}
		}

		/* import transitions */
		if ( isset($data['transitions']) ) {
			foreach( $data['transitions'] as $item ) {
				$parent = null;
				$child = null;
				$transition = new Wf\Transition();
				$transition->hydrate($item);
				/* registry is indexed with original uid */
				$transition->newUid();
				$transition->processId = $process->getId();
				
				if ( isset($activityRegistry[$transition->parentUid]) ) {
					$parent = $activityRegistry[$transition->parentUid];
					$transition->setParent($parent);
					$parent->transition[] = $transition;
				}
				if ( isset($activityRegistry[$transition->childUid]) ) {
					$child = $activityRegistry[$transition->childUid];
					$transition->setChild($child);
				}
				
				$this->factory->getDao($transition->cid)->insert($transition);
			}
		}

		return $process;
	}

	/**
	 *
	 * @param integer $processId
	 * @param integer $activityId
	 * @return void
	 * @throw Exception
	 */
	public function getCode($processId, $activityId)
	{
		throw new Exception('Method not implemented');
	}
}
