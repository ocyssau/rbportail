<?php
namespace Ldap\Controller;

use Zend\View\Model\ViewModel;
use Zend\Ldap\Exception\LdapException;
use Zend\Ldap\Ldap;

/**
 */
class IndexController extends \Application\Controller\AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		$view = new ViewModel();

		/**/
		$ldap = $this->getEvent()->getApplication()->getServiceManager()->get('LdapService');
		$ldap->bind();

		/**/
		$dn = $ldap->getBaseDn();
		$attributes = array(
			'name',
			'dn',
			'mail'
		);

		$entries = $ldap->searchEntries("mailnickname=l_miaille", $dn, Ldap::SEARCH_SCOPE_SUB, $attributes);
		var_dump($entries);
		return $view;
	}
}

