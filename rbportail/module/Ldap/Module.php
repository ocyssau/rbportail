<?php
namespace Ldap;

/**
 * 
 */
class Module
{

	/**
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'LdapService' => function ($sm) {
					$config = $sm->get('Configuration');

					$authOptions = $config['rbp']['auth'];
					if($authOptions['adapter'] == 'ldap'){
						$ldap = new \Zend\Ldap\Ldap($authOptions['options']['server1']);
					}
					else{
						$ldap = new \Zend\Ldap\Ldap($authOptions['options']['server1']);
					}

					return $ldap;
				},
			),
		);
	}

	/**
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
				)
			)
		);
	}
}
