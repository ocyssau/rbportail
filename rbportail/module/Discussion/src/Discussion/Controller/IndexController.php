<?php

namespace Discussion\Controller;

use Zend\View\Model\ViewModel;

//models and Dao
use Discussion\Model\Comment;
use Application\Dao\Factory as DaoFactory;

class IndexController extends \Application\Controller\AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$view = new ViewModel();
		
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}
		
		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}
		
		$request = $this->getRequest();
		$discussionId = $request->getQuery('discussionId');
		$parentId = $request->getQuery('parentId');
		$body = $request->getQuery('body');
		
		$model = Comment::init(uniqid());
		$model->hydrate(array(
			'discussionId'=>$discussionId,
			'parentId'=>$parentId,
			'body'=>$body,
			));
		
		$dao = DaoFactory::get()->getDao($model);
		$dao->save($model);
		
		$view->object = $comment;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$request = $this->getRequest();
		$id = $request->getQuery('commentid');
		
		$dao = DaoFactory::get()->getDao(Comment::$classId);
		$dao->deleteFromId($id);
		
		$this->redirect()->toRoute('wucomment', array(
			'id' => $id,
			'layout'=>'fragment'
		));
	}
}

