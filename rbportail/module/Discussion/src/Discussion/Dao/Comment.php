<?php
//%LICENCE_HEADER%
//%LICENCE_HEADER%
namespace Discussion\Dao;

use Wumanager\Dao\Any;

/** SQL_SCRIPT>>
 CREATE TABLE discussion_comment(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
 `name` VARCHAR(255) NULL DEFAULT NULL,
 `discussionUid` VARCHAR(255) NOT NULL,
 `ownerId` INT(11) NULL,
 `ownerUid` VARCHAR(64) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updated` datetime NOT NULL,
 `body` TEXT NOT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_ALTER>>
 ALTER TABLE discussion_comment ADD UNIQUE (uid);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UID` (`uid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussionUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_OWNERID` (`ownerId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTID` (`parentId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UPDATED` (`updated` ASC);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/
class Comment extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'discussion_comment';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'cid' => 'cid',
		'uid' => 'uid',
		'name' => 'name',
		'body' => 'body',
		'discussionUid' => 'discussionUid',
		'ownerId' => 'ownerId',
		'ownerUid' => 'ownerUid',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'updated' => 'updated'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	}

	/**
	 * @param \Wumanager\Model\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		(!$parentId = $mapped->getParent(true)) ? $parentId = null : null;
		(!$parentUid = $mapped->getParentUid()) ? $parentUid = null : null;
		
		return array(
			':id' => $mapped->getId(),
			':cid' => $mapped->cid,
			':uid' => $mapped->getUid(),
			':name' => $mapped->getName(),
			':body' => $mapped->getBody(),
			':discussionUid' => $mapped->getDiscussionUid(),
			':ownerId' => $mapped->ownerId,
			':ownerUid' => $mapped->ownerUid,
			':parentId' => $parentId,
			':parentUid' => $parentUid,
			':updated' => $mapped->getUpdated(self::DATE_FORMAT)
		);
	}

	/**
	 * Recursive function
	 * @return Comment
	 */
	public function deleteFromId($id, $withChilds = true, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		if ( !$this->deleteFromIdStmt ) {
			$table = $this->_table;
			$sql = 'DELETE FROM ' . $table . ' WHERE `id`=:id';
			$this->deleteFromIdStmt = $this->connexion->prepare($sql);
			$sql = 'SELECT `id` FROM ' . $table . ' WHERE `parentId`=:id';
			$this->selectToDeleteFromIdStmt = $this->connexion->prepare($sql);
		}
		
		try {
			$bind = array(
				':id' => $id
			);
			$this->selectToDeleteFromIdStmt->execute($bind);
			$result = $this->selectToDeleteFromIdStmt->fetchAll(\PDO::FETCH_COLUMN);
			
			if ( $withChilds ) {
				foreach( $result as $childId ) {
					$this->deleteFromId($childId, true, false);
				}
			}
			
			$this->deleteFromIdStmt->execute($bind);
			
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}
}

