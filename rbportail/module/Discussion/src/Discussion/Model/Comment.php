<?php
//%LICENCE_HEADER%

namespace Discussion\Model;

use Application\Model\Any;

/**
 * 
 *
 */
class Comment extends Any
{
	static $classId = '568be4fc7a0a8';

	/**
	 * @var string
	 */
	protected $body;

	/**
	 * @var string
	 */
	protected $discussionUid = null;

	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		$this->ownedHydrate($properties);
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['discussionUid'])) ? $this->discussionUid=$properties['discussionUid'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		(isset($properties['updated'])) ? $this->setUpdated( new \DateTime($properties['updated']) ) : null;
		(isset($properties['body'])) ? $this->body=$properties['body'] : null;
		return $this;
	}

	/**
	 * @param string
	 */
	public function setBody( $string )
	{
		$this->body = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * Set the discussion id.
	 *
	 * @param string
	 */
	public function setDiscussionUid( $discussionUid )
	{
		$this->discussionUid = $discussionUid;
		return $this;
	}

	/**
	 * Get the discussion id.
	 *
	 * @return string
	 */
	public function getDiscussionUid()
	{
		return $this->discussionUid;
	}

}
