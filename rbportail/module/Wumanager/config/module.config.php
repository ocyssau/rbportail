<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'workunit-index' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/index[/:action]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Index',
						'action' => 'index',
					),
				),
			),
		    'workunit' => array(
		        'type' => 'Segment',
		        'options' => array(
		            'route' => '/wumanager/workunit/[:action[/][:id]]',
		            'defaults' => array(
		                'controller' => 'Wumanager\Controller\Workunit',
		                'action' => 'index',
		            ),
		        ),
		    ),
			'wuprocess' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/process/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Process',
						'action' => 'index',
					),
				),
			),
			'deliverable' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/deliverable/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Deliverable',
						'action' => 'index',
					),
				),
			),
			'wutype' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/wutype/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Wutype',
						'action' => 'index',
					),
				),
			),
			'wutypelink' => array(
			    'type' => 'Segment',
			    'options' => array(
			        'route' => '/wumanager/wutypelink/[:action[/][:id]]',
			        'defaults' => array(
			            'controller' => 'Wumanager\Controller\Wutypelink',
			            'action' => 'index',
			        ),
			    ),
			),
			'wuuser' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/user/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\User',
						'action' => 'index',
					),
				),
			),
			'discussion' => array(
					'type' => 'Segment',
					'options' => array(
							'route' => '/wumanager/discussion/[:action[/][:id]]',
							'defaults' => array(
									'controller' => 'Wumanager\Controller\Comment',
									'action' => 'index',
							),
					),
			),
			'notifier' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/notifier/[:action]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Notifier',
						'action' => 'index',
					),
				),
			),
			'notification' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/wumanager/notification/:action[/:referenceId]',
					'defaults' => array(
						'controller' => 'Wumanager\Controller\Notification',
					),
				),
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'wumanager' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/wumanager',
					'defaults' => array(
						'__NAMESPACE__' => 'Wumanager\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Wumanager\Controller\Index' => 'Wumanager\Controller\IndexController',
			'Wumanager\Controller\Workunit'=>'Wumanager\Controller\WorkunitController',
			'Wumanager\Controller\Process'=>'Wumanager\Controller\ProcessController',
			'Wumanager\Controller\Deliverable'=>'Wumanager\Controller\DeliverableController',
			'Wumanager\Controller\Wutype'=>'Wumanager\Controller\WutypeController',
			'Wumanager\Controller\Wutypelink'=>'Wumanager\Controller\WutypelinkController',
			'Wumanager\Controller\User'=>'Wumanager\Controller\UserController',
			'Wumanager\Controller\Comment'=>'Wumanager\Controller\CommentController',
			'Wumanager\Controller\Notifier'=>'Wumanager\Controller\NotifierController',
			'Wumanager\Controller\Notification'=>'Wumanager\Controller\NotificationController',
		),
	),
	'view_manager' => array(
		'template_map' => array(
			'layout/wumanager'   => __DIR__ . '/../view/layout/layout.phtml',
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
			__DIR__ . '/../../../data/Process/ProcessDef',
		),
	),
);
