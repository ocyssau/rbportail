<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Wumanager;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Wumanager\Controller\WorkunitController;

/**
 *
 *
 */
class Module implements AutoloaderProviderInterface
{

	/**
	 *
	 * @param \Zend\Mvc\MvcEvent $e
	 * @throws \Exception
	 */
	public function onBootstrap($e)
	{
	    $config = $e->getApplication()->getServiceManager()->get('Configuration');
	    \Wumanager\Service\Workflow\Code::$processPath = realpath($config['rbp']['workflow']['process']['path']);

	    /** @var Event $eventManager */
	    $eventManager = $e->getTarget()->getEventManager();
	    $mailNotification = new \Wumanager\Callback\MailNotification();
	    
	    /***/
	    $sm = $e->getApplication()->getServiceManager();
	    $mailService = $sm->get('Mail');
	    $mailNotification->mailService = $mailService;
	    
	    $eventManager->attach('wu.edit.post', array(
	    	new \Wumanager\Callback\Notifier(),
	    	'editPost'
	    ));
	    $eventManager->attach(WorkunitController::SIGNAL_POST_UPDATE, array(
	    	$mailNotification,
	    	'editPost'
	    ));
	    $eventManager->attach(WorkunitController::SIGNAL_POST_CREATE, array(
	    	$mailNotification,
	    	'onEvent'
	    ));
	    $eventManager->attach(WorkunitController::SIGNAL_POST_DELETE, array(
	    	$mailNotification,
	    	'onEvent'
	    ));
	    $eventManager->attach(WorkunitController::SIGNAL_POST_DELIVERY, array(
	    	$mailNotification,
	    	'onEvent'
	    ));
	    $eventManager->attach(WorkunitController::SIGNAL_POST_COPY, array(
	    	$mailNotification,
	    	'onEvent'
	    ));
	    
	    return $this;
	}
	
	/**
	 *
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\ModuleManager\Feature.AutoloaderProviderInterface::getAutoloaderConfig()
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					'ProcessDef'=>realpath('./data/Process/ProcessDef'),
				),
			),
		);
	}
}
