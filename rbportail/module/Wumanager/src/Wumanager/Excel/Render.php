namespace Wumanager\Excel;

require_once 'Spreadsheet/Excel/Writer.php';

/**
 * 
 * @param Render $excelRender
 * @return array
 */
function excel_render($excelRender)
{
	$format = [];
	
	/* The body lines format */
	$format['body'] = $excelRender->workbook->addFormat(array(
		'Size' => 10,
		'bold' => 0,
		'Align' => 'left'
	));

	/* The title lines format */
	$format['title'] = $excelRender->workbook->addFormat(array(
		'Size' => 12,
		'bold' => 1,
		'Align' => 'center'
	));

	$excelRender->beginLine = 0;
	$excelRender->beginCol = 0;

	return $format;
}

/*
 * !brief Generate a excel file from a recordset
 *
 */
class Render
{

	/* @var integer */
	public $beginLine;

	/* @var integer */
	public $beginCol;

	/* @var string */
	public $title;

	/**
	 *
	 * @var \PDOStatement
	 */
	public $stmt;

	/**
	 *
	 * @var \Spreadsheet_Excel_Writer
	 */
	public $workbook;

	/**
	 *
	 * @var \Spreadsheet_Excel_Writer_Worksheet
	 */
	public $worksheet;

	/**
	 *
	 * @param \PDOStatement $stmt
	 * @param string $title
	 */
	function __construct($stmt, $title)
	{
		$this->stmt = $stmt;
		$this->beginLine = 0;
		$this->beginCol = 0;
		$this->title = $title;

		/* Creating a workbook */
		$this->workbook = new \Spreadsheet_Excel_Writer();

		/* Creating a worksheet */
		$this->worksheet = $this->workbook->addWorksheet($this->title . '_sheet');
	}

	/**
	 */
	function render()
	{
		$format = excel_render($this);

		/* sending HTTP headers */
		$this->workbook->send($this->title . '_content.xls');

		/* Write title line */
		$ncols = $this->stmt->columnCount();
		$col = $this->beginCol;
		for ($i = 0; $i < $ncols; $i++) {
			$cmeta = $this->stmt->getColumnMeta($i); // get meta-data about column
			if ( $cmeta ) {
				$fname = htmlspecialchars($cmeta['name']);
			}
			else {
				$fname = 'Field ' . ($i + 1);
			}
			$this->worksheet->write($this->beginLine, $col, $fname, $format['title']);
			$col++;
		}

		/* Write body */
		$lin = $this->beginLine + 1;
		while( $rowInArray = $this->stmt->fetch(\PDO::FETCH_ASSOC) ) { // Returns array containing current row, or false if EOF
			$col = $this->beginCol;
			foreach( $rowInArray as $val ) {
				$this->worksheet->write($lin, $col, $val, $format['body']);
				$col++;
			}
			$lin++;
		}

		/* Let's send the file */
		$this->workbook->close();
	}
} /* End of class */

