<?php
namespace Wumanager\Excel;

use PhpOffice\PhpSpreadsheet\Style;

/*
 * !brief Generate a excel file from a recordset
 *
 */
class RenderPhpOffice
{

	/**/
	public $beginRow;

	/**/
	public $beginCol;

	/**/
	public $title;

	/**
	 *
	 * @var \PDOStatement
	 */
	public $stmt;

	/**
	 *
	 * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
	 */
	public $spreadsheet;

	/**
	 *
	 * @param \PDOStatement $stmt
	 * @param string $title
	 */
	public function __construct($stmt, $title)
	{
		$this->stmt = $stmt;
		$this->beginRow = 1;
		$this->beginCol = 1;
		$this->title = $title;

		/* Creating a workbook */
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadsheet->getProperties()
			->setTitle($title)
			->setSubject('From Wumanager')
			->setDescription('Export of workunits from WuManager.')
			->setKeywords('office PhpSpreadsheet php')
			->setCategory('Wumanager Workunit');

		$this->spreadsheet = $spreadsheet;
	}

	/**
	 *
	 * @param \Rbplm\People\User $user
	 */
	public function setOwner($user)
	{
		$this->spreadsheet->getProperties()
			->setCreator($user->getName())
			->setLastModifiedBy($user->getName());
		return $this;
	}

	/**
	 *
	 */
	public function getHeaderStyle()
	{
		return array(
			'font' => [
				'name' => 'Calibri',
				'size' => 12,
				'bold' => 1
			],
			'borders' => [
				'allborders' => [
					'style' => Style\Border::BORDER_THIN
				]
			],
			'alignment' => [
				'horizontal' => 'center'
			]
		);
	}

	/**
	 *
	 */
	public function getBodyStyle()
	{
		return array(
			'font' => [
				'name' => 'Calibri',
				'size' => 10,
				'bold' => 0
			],
			'borders' => [
				'allborders' => [
					'style' => Style\Border::BORDER_THIN
				]
			],
			'alignment' => [
				'horizontal' => 'left'
			]
		);
	}

	/**
	 */
	public function render()
	{
		/* */
		$activeSheet = $this->spreadsheet->getActiveSheet();
		$activeSheet->setTitle('From Ranchbe');
		
		/* Write header line */
		$ncols = $this->stmt->columnCount();
		$nrows = $this->stmt->rowCount();

		$firstCol = $this->beginCol;
		$lastCol = $firstCol + $ncols;

		$firstBodyRow = $this->beginRow + 1;
		$lastBodyRow = $firstBodyRow + $nrows;
		
		$activeSheet->getStyleByColumnAndRow($firstCol, $this->beginRow, $lastCol, $this->beginRow)->applyFromArray($this->getHeaderStyle());
		$activeSheet->getStyleByColumnAndRow($firstCol, $firstBodyRow, $lastCol, $lastBodyRow)->applyFromArray($this->getBodyStyle());
		
		$col = $this->beginCol;
		
		/* write headers from column name extract from metadata of PDO */
		for ($i = 0; $i < $ncols; $i++) {
			/* get meta-data about column */
			$cmeta = $this->stmt->getColumnMeta($i);
			if ( $cmeta ) {
				$fname = htmlspecialchars($cmeta['name']);
			}
			else {
				$fname = 'Field ' . ($i + 1);
			}

			$activeSheet->setCellValueByColumnAndRow($col, $this->beginRow, $fname);
			$col++;
		}
		
		/* Write body */
		$row = $firstBodyRow;
		/* Returns array containing current row, or false if EOF */
		while( $rowInArray = $this->stmt->fetch(\PDO::FETCH_ASSOC) ) {
			$col = $this->beginCol;
			foreach( $rowInArray as $val ) {
				$activeSheet->setCellValueByColumnAndRow($col, $row, $val);
				$col++;
			}
			$row++;
		}

		return $this;
	}

	/**
	 * Redirect output to a client’s web browser (Xls)
	 */
	public function download($fileName = 'excel')
	{
		$spreadsheet = $this->spreadsheet;
		
		/* for xls */
		//header('Content-Type: application/vnd.ms-excel');
		/* for xlsx */
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $fileName . '"');
		//header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header('Cache-Control: max-age=0');
		/* If you're serving to IE 9, then the following may be needed */
		header('Cache-Control: max-age=1');
		/* If you're serving to IE over SSL, then the following may be needed */
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); /* Date in the past */
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); /* always modified */
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		die();
	}
} /* End of class */
