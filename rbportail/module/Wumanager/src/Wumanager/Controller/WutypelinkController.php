<?php

namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

use Wumanager\Model;
use Wumanager\Model\Workunit\LinkTypeContext;
use Wumanager\Model\Workunit\Type as WuType;
use Wumanager\Form;
use Application\Model\Exception;

//models and Dao
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Application\Form\StdFilterForm;


/**
 * 
 *
 */
class WutypelinkController extends AbstractController
{

	/**
	 *
	 * @param boolean $init
	 */
	public function _getModel($init = false)
	{
		if($init){
			$model = LinkTypeContext::init();
		}
		else{
			$model = new LinkTypeContext();
		}
		return $model;
	}


	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		//get Acls for application from SESSION, see dispatch method
		$appAcl = People\CurrentUser::get()->appacl;

		if($appAcl->hasRight($appAcl::RIGHT_ADMIN)){
			return $this->manage();
		}
		elseif($appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->manage();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 *
	 */
	public function manage()
	{
		$view = new ViewModel();
		$view->setTemplate('wumanager/wutypelink/index');
		$request = $this->getRequest();

		//get Acls for application
		$appAcl = People\CurrentUser::get()->appacl;
		$userId = People\CurrentUser::get()->getId();

		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->key = 'CONCAT(type.name,type.uid)';
		$filter->passThrough=true;
		$filter->prepare();

		$bind = array();
		$cid = LinkTypeContext::$classId;
		$factory = DaoFactory::get();
		$list = $factory->getList($cid);
		$table = $factory->getTable($cid);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql  = "SELECT
		link.id,
		context.id as ctx_id, context.uid as ctx_uid, context.name as ctx_name,
		type.id as typeid, type.uid as type_uid, type.name as type_name,
		CONCAT(type.uid, ' - ',type.name) AS type_nname
		FROM link_workunit_type_context AS link
		JOIN workunit_type AS type ON link.parentId = type.id
		JOIN workunit_context AS context ON link.childId = context.id";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->myList = $list;

		$view->myHeaders = array(
			'#'=>'id',
			'Context Name'=>'ctx_name',
			'Type'=>'type_nname',
		);

		$view->filter = $filter;
		return $view;
	}

	/**
	 *
	 */
	public function getAction()
	{
		$view =  new ViewModel();

		$id = $this->params()->fromRoute('id');
		$params = $this->getRequest()->getQuery();
		( isset($params['typedim']) ) ? $typeDim = strtolower($params['typedim']) : $typeDim = null;
		( isset($params['context']) ) ? $context = strtolower($params['context']) : $context = null;

		$cid = WuType::$classId;
		$list = DaoFactory::get()->getList($cid);

		$filter = array();

		$sql = "SELECT type.*, context.name AS context, context.uid AS contextUid, context.id AS contextId FROM workunit_type AS type";
		$sql .= " INNER JOIN link_workunit_type_context AS link ON link.parentId=type.id";
		$sql .= " INNER JOIN workunit_context AS context ON context.id=link.childId";

		if($typeDim=='2d'){
			$filter[] = "type.dimension = 2";
		}
		elseif($typeDim=='3d'){
			$filter[] = "type.dimension = 3";
		}
		if($context){
			$filter[] = "context.id = $context";
		}

		if($filter){
			$sql .= " WHERE " . implode(' AND ', $filter);
		}

		$sql .= " ORDER BY uid ASC";

		$list->loadFromSql($sql);

		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
			echo json_encode($list->toArray(), JSON_FORCE_OBJECT);
			die;
		}
		else{
			$view->list = $list;
			return $view;
		}
	}

	/**
	 * AJAX method
	 *
	 */
	public function addAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$contextId = $request->getQuery('contextid');
		$typeId = $request->getQuery('typeid');

		if($contextId=="" || $typeId==""){
			throw new Exception('context and type must be set');
		}

		$context = new Model\Workunit\Context();
		$type = new Model\Workunit\Type();
		DaoFactory::get()->getDao($context)->loadFromId($context, $contextId);
		DaoFactory::get()->getDao($type)->loadFromId($type, $typeId);

		//init model
		$link = $this->_getModel(true);
		$link->setparent($type);
		$link->setChild($context);
		DaoFactory::get()->getDao($link)->insert($link);

		//init view
		$view->link = $link;
		$view->context = $context;
		$view->type = $type;
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');

		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();

		//load model
		$Model = $this->_getModel(false);
		DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);

		$form = new Form\WutypeForm();
		$form->bind($Model);

		if ($request->isPost()) {
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				//save
				DaoFactory::get()->getDao($Model)->save($Model);
				return $this->redirect()->toRoute('wutype');
			}
		}

		$view->title = 'Edit Type ' . $Model->getUid();
		$view->setTemplate($form->template);
		$view->model = $Model;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
		    return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();

		$contextId = $request->getQuery('contextId');
		$typeId = $request->getQuery('typeId');
		if(!$contextId || !$typeId){
			throw new Exception('Id must be set');
		}

		$link = $this->_getModel(false);
		$Dao = DaoFactory::get()->getDao($link);
		$connexion = $Dao->getConnexion();
		$connexion->beginTransaction();

		try{
			$Dao->delete('parentId='.$typeId.' AND childId='.$contextId);
			$connexion->commit();
		}
		catch(\Exception $e){
			$connexion->rollBack();
			throw $e;
		}

		if ($request->isXmlHttpRequest()) {
		    $view->setTerminal(true);
		    $view->parentId = $typeId;
		    $view->childId = $contextId;
		    return $view;
		}
		else{
			return $this->redirect()->toRoute('wutypelink');
		}
	}
}
