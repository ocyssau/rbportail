<?php
namespace Wumanager\Controller;

use Zend\View\Model\ViewModel;
use Wumanager\Model\Wumanager;
use Application\Dao as ApplicationDao;
use Wumanager\Form\SetUserSiteForm;

/**
 * 
 *
 */
class UserController extends AbstractController
{

	/**
	 * 
	 */
	public function displayAction()
	{
		return $this->indexAction();
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$view = new ViewModel();
		
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}
		
		$resourceId = Wumanager::$id;
		
		$view->id = uniqid();
		$view->title = 'User Sites Affectation';
		$view->module = 'wumanager';
		$view->headers = array(
			'Site','User'
		);
		
		$dao = new ApplicationDao\Acl\Acl();
		$items = $dao->getRoleFromResourceId($resourceId);
		$view->items = $items;
		
		$form = new SetUserSiteForm($acl);
		$form->bind($acl);
		$view->form = $form;

		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$request = $this->getRequest();
		$projectId = $request->getQuery('projectid');
		$roleId = $request->getQuery('roleid');
		$userId = $request->getQuery('userid');

		$dao = new ApplicationDao\Acl\Acl();
		$dao->acl = $acl;
		$dao->insert($userId,$roleId,$projectId,$acl);
		
		$this->redirect()->toRoute('wuuser', array(
			'id' => $projectId,
			'layout'=>'fragment'
		));
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}
		
		$request = $this->getRequest();
		$projectId = $request->getQuery('projectid');
		$roleId = $request->getQuery('roleid');
		$userId = $request->getQuery('userid');
		
		$dao = new ApplicationDao\Acl\Acl();
		$dao->delete($userId,$roleId,$projectId);

		$this->redirect()->toRoute('wuuser', array(
			'id' => $projectId,
			'layout'=>'fragment'
		));
	}
}
