<?php
namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Filter\Op;
use Wumanager\Model;
use Wumanager\Form;
use Wumanager\Model\Workunit;
use Discussion\Model\Comment;

/**
 * 
 *
 */
class WorkunitController extends AbstractController
{

	/**
	 * Signals emited by this controller
	 */
	const SIGNAL_POST_CREATE = 'wu.create.post';
	const SIGNAL_POST_UPDATE = 'wu.update.post';
	const SIGNAL_POST_DELIVERY = 'wu.delivery.post';
	const SIGNAL_POST_COPY = 'wu.copy.post';
	const SIGNAL_POST_DELETE = 'wu.delete.post';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
	 */
	public function onDispatch(\Zend\Mvc\MvcEvent $e)
	{
		parent::onDispatch($e);
	}

	/**
	 * Display one workunit in disabled form
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$id = $this->params()->fromRoute('id');
		$view = new ViewModel();
		
		/* load model */
		$model = $this->_getModel(false);
		$type = $model->getType();
		$factory = DaoFactory::get();
		$factory->getDao($model)->loadFromId($model, $id);
		
		$factory->getDao($model)->loadDeliverables($model);
		$factory->getDao($type)->loadFromId($type, $model->getType(true));
		
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			if ( !$acl->getAcl()->hasRole($model->getSite(true)) ) {
				return $this->notauthorized();
			}
		}
		
		$typeDim = $type->dimension . 'd';
		$form = new Form\WorkunitCreateForm($typeDim);
		$form->add(array(
			'type' => 'Wumanager\Form\DeliveryForm'
		));
		$form->bind($model);
		$form->get('delivery')->bind($model);
		
		foreach( $form->get('workunit')->getElements() as $element ) {
			$element->setAttribute('disabled', 1);
			$element->setAttribute('placeholder', '');
		}
		foreach( $form->get('workunit')
			->get('deliverables')
			->getFieldsets() as $fieldset ) {
			foreach( $fieldset->getElements() as $element ) {
				$element->setAttribute('disabled', 1);
			}
		}
		foreach( $form->get('delivery')->getElements() as $element ) {
			$element->setAttribute('disabled', 1);
		}
		
		$discussionUid = $id;
		$discussion = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussionUid', Op::EQUAL);
		$discussion->load($filter->__toString());
		
		$view->title = 'Edit Workunit';
		$view->id = $id;
		$view->form = $form;
		$view->discussion = $discussion;
		return $view;
	}

	/**
	 *
	 * @param boolean $init
	 */
	public function _getModel($init = false)
	{
		if ( $init ) {
			$model = Workunit::init();
		}
		else {
			$model = new Workunit();
		}
		return $model;
	}

	/**
	 * @return multitype:\Application\Form\ProjetForm
	 */
	public function addAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		/* switch page */
		$view = new ViewModel();
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		$page = (int)$request->getPost('page', null);
		$typeDim = $request->getPost('typeDimension', null);
		$typeId = (int)$request->getPost('typeId', null);
		
		/* init model */
		$model = $this->_getModel(true);
		$model->typeDimension = $typeDim;
		$model->setType($typeId);
		$model->setOpendate(new \DateTime());
		
		/* switch type */
		if ( $typeDim ) {
			$form = new Form\WorkunitCreateForm($typeDim);
			$form->bind($model);
			
			if ( $typeId ) {
				$type = $model->getType();
				$factory->getDao($type)->loadFromId($type, $typeId);
			}
		}
		elseif ( $typeDim == null ) {
			$form = new Form\WorkunitCreateFormPage1();
		}
		
		if ( $request->isPost() && $page ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* save workitem */
				$factory->getDao($model)->save($model);
				/* save Deliverables */
				foreach( $model->getDeliverables() as $deliverable ) {
					if ( $deliverable->getUid() == '' ) {
						$deliverable->newUid();
					}
					$deliverable->setParent($model);
					$factory->getDao($deliverable)->save($deliverable);
				}
				/** @var \Zend\EventManager $eventManager */
				$eventManager = $this->getEvent()
					->getApplication()
					->getEventManager();
					$eventManager->trigger(self::SIGNAL_POST_CREATE, $model, array());
				return $this->redirect()->toRoute('wumanager');
			}
		}
		
		$view->setTemplate($form->template);
		$view->workunit = $model;
		$view->title = 'Add new Workunit';
		$view->form = $form;
		
		return $view;
	}

	/**
	 * @return multitype:\Application\Form\ProjetForm
	 */
	public function editAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		
		$view = new ViewModel();
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		/* delete deleted deliverables */
		$deleted = $request->getPost('deleted', array());
		if ( $deleted ) {
			/* delete deleted deliverables */
			foreach( $deleted as $uid ) {
				$factory->getDao(Model\Deliverable::$classId)->delete($uid);
			}
		}
		
		/* load model */
		$model = $this->_getModel(false);
		$wuDao = $factory->getDao($model);
		$type = $model->getType();
		$wuDao->loadFromId($model, $id);
		$wuDao->loadDeliverables($model);
		$factory->getDao($type)->loadFromId($type, $model->getType(true));
		$model->hashBefore = \Wumanager\Model\Workunit\Hasher::hash($model);
		
		$typeDim = $type->dimension . 'd';
		$form = new Form\WorkunitCreateForm($typeDim);
		$form->bind($model);
		
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* save added deliverables */
				foreach( $model->getDeliverables() as $deliverable ) {
					if ( $deliverable->getUid() == '' ) {
						$deliverable->newUid();
					}
					$deliverable->setParent($model);
					$factory->getDao($deliverable)->save($deliverable);
				}
				
				/* save workunit */
				$factory->getDao($model)->save($model);
				/** @var \Zend\EventManager $eventManager */
				$eventManager = $this->getEvent()
					->getApplication()
					->getEventManager();
					$eventManager->trigger(self::SIGNAL_POST_UPDATE, $model, array());
				return $this->redirect()->toRoute('wumanager');
			}
		}
		
		/**/
		$discussionUid = $id;
		$discussion = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussionUid', Op::EQUAL);
		$discussion->load($filter->__toString());
		
		/**/
		$view->id = $id;
		$view->discussion = $discussion;
		$view->title = 'Edit Workunit ' . $model->getName();
		$view->setTemplate($form->template);
		$view->workunit = $model;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function copyAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		
		/**/
		$view = new ViewModel();
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		/* load model */
		$model = $this->_getModel(false);
		$wuDao = $factory->getDao($model);
		$type = $model->getType();
		$wuDao->loadFromId($model, $id);
		$wuDao->loadDeliverables($model);
		$factory->getDao($type)->loadFromId($type, $model->getType(true));
		
		/* Init Form */
		$form = new Form\WorkunitCopyForm();
		$form->bind($model);
		
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* save workunit */
				$wuCopy = clone ($model);
				$wuCopy->setStatus('init');
				$wuCopy->newUid();
				$wuCopy->setId(null);
				$factory->getDao($wuCopy)->save($wuCopy);
				
				/* save added deliverables */
				foreach( $model->getDeliverables() as $deliverable ) {
					$deliverableCopy = clone ($deliverable);
					$deliverableCopy->newUid();
					$deliverableCopy->setId(null);
					$deliverableCopy->setParent($wuCopy);
					$factory->getDao($deliverableCopy)->save($deliverableCopy);
				}
				/** @var \Zend\EventManager $eventManager */
				$eventManager = $this->getEvent()
					->getApplication()
					->getEventManager();
					$eventManager->trigger(self::SIGNAL_POST_COPY, $model, array());
				return $this->redirect()->toRoute('wumanager');
			}
		}
		
		/**/
		$view->id = $id;
		$view->title = 'Copy Workunit ' . $model->getName();
		$view->setTemplate($form->template);
		$view->workunit = $model;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		
		$model = $this->_getModel(false);
		$factory = DaoFactory::get();
		$dao = $factory->getDao($model);
		$connexion = $dao->getConnexion();
		$connexion->beginTransaction();
		
		/** @var \Zend\EventManager $eventManager */
		$eventManager = $this->getEvent()
			->getApplication()
			->getEventManager();
		
		try {
			$dao->loadFromId($model, $id);
			$uid = $model->getUid();
			
			//delete deliverables, BY TRIGGER OR CONSTRAINT
			//$dao->deleteDeliverables($id);
			
			//delete process instance, BY TRIGGER OR CONSTRAINT
			//$dao->deleteWfFromWuid($id);
			
			//delete Workunit
			$dao->delete($uid, $withchild = true, $withTrans = false);
			
			//@todo delete roles
			//$dao = $factory->getDao(Acl\Acl::$classId);
			//$dao->deleteAll("`resourceId`=$id");
			
			$connexion->commit();
			$eventManager->trigger(self::SIGNAL_POST_DELETE, $model, array());
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw $e;
		}
		
		return $this->redirect()->toRoute('wumanager');
	}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function billtoAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		$bill = new Model\Bill();
		try {
			DaoFactory::get()->getDao($bill)->load($bill, 'parentId=' . $id);
		}
		catch( \Exception $e ) {
			$bill = Model\Bill::init();
			$model = $this->_getModel(false);
			$factory->getDao($model)->loadFromId($model, $id);
			$bill->setParent($model);
		}
		
		$form = new Form\BilltoForm();
		$form->bind($bill);
		
		if ( $request->isPost() ) {
			$form->prepare();
			$form->setInputFilter($form->prepareFilters());
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$form->save($bill);
				$factory->getDao($bill)->save($bill);
				return $this->redirect()->toRoute('wumanager');
			}
		}
		
		$view = new ViewModel();
		$view->title = 'Set the Bill';
		$view->form = $form;
		return $view;
	}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function deletebillAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		$bill = new Model\Bill();
		$dao = $factory->getDao($bill);
		
		try {
			//$dao->deleteFromId($bill, $id);
			$dao->deleteFromFilter("id=:id", array(
				':id' => $id
			));
		}
		catch( \Exception $e ) {
			throw new \Exception("Bill is not founded");
		}
		
		return $this->redirect()->toRoute('wumanager');
	}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function deliveryAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}
		
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		
		$model = $this->_getModel(false);
		$factory = DaoFactory::get();
		$factory->getDao($model)->loadFromId($model, $id);
		
		$form = new Form\DeliveryForm();
		$form->bind($model);
		
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$factory->getDao($model)->save($model);
				/** @var \Zend\EventManager $eventManager */
				$eventManager = $this->getEvent()
					->getApplication()
					->getEventManager();
				$eventManager->trigger(self::SIGNAL_POST_DELIVERY, $model, array());
				return $this->redirect()->toRoute('wumanager');
			}
		}
		
		$view = new ViewModel();
		$view->setTemplate($form->template);
		$view->title = 'Set Delivery';
		$view->form = $form;
		return $view;
	}

	/**
	 * Ajax method
	 * @return \Zend\View\Model\ViewModel
	 */
	public function statusAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		
		$view = new ViewModel();
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		
		$id = $request->getQuery('id', null);
		$status = $request->getQuery('status', '');
		
		//load model
		$model = $this->_getModel(false);
		$factory->getDao($model)->loadFromId($model, $id);
		
		$model->setStatus($status);
		$factory->getDao($model)->saveStatus($model);
		
		/* disable layout */
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		$view->return = json_encode($model);
		return $view;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function gethistoryAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		
		$view = new ViewModel();
		$request = $this->getRequest();
		
		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();
		
		$workunitId = $this->params()->fromRoute('id');
		
		//$filter = $this->_getFilter('workflow/instance/filter');
		//$filter->get('stdfilter-id')->setValue($processId);
		//$filter->prepare();
		
		//search from header search area :
		$list = DaoFactory::get()->getList(1);
		
		//$paginator = new PaginatorForm();
		//$paginator->setMaxLimit($list->countAll(""));
		//$paginator->setData($request->getPost());
		//$paginator->setData($request->getQuery());
		//$paginator->prepare()->bindToView($view);
		
		$sql = "SELECT
		inst.name as iname,
		inst.id as iid,
		inst.started as istarted,
		inst.ended as iended,
		inst.ownerId as iownerId,
		inst.status as istatus,
		act.title as aname,
		acti.id as aid,
		acti.started as astarted,
		acti.ended as aended,
		acti.ownerId as aownerId,
		acti.status as astatus,
		acti.comment as acomment,
		acti.attributes as aattributes
		FROM workunit_process AS lnk
		JOIN wf_instance_activity AS acti ON lnk.childId = acti.instanceId
		JOIN wf_activity AS act ON acti.activityId = act.id
		JOIN wf_instance AS inst ON inst.id = acti.instanceId";
		$sql .= " WHERE lnk.parentId = :parentId";
		$sql .= " ORDER BY iid ASC, astarted ASC";
		
		$bind = array(
			':parentId' => $workunitId
		);
		$list->loadFromSql($sql, $bind);
		
		/*
		 if($filter->where){
		 $sql .= ' WHERE ' . $filter->where;
		 $bind = array_merge($bind, $filter->bind);
		 }
		 $sql .= $paginator->toSql();
		 */
		
		$view->list = $list;
		$view->headers1 = array(
			'#' => 'iid',
			'Name' => 'iname',
			'Owner' => 'iownerId',
			'Started' => 'istarted',
			'Ended' => 'iended',
			'Status' => 'istatus'
		);
		
		$view->headers2 = array(
			'#' => 'aid',
			'Name' => 'aname',
			'Comment' => 'acomment',
			'Owner' => 'aownerId',
			'Started' => 'astarted',
			'Ended' => 'aended',
			'Status' => 'astatus',
			'Attributes' => 'aattributes'
		);
		
		$view->filter = $filter;
		$view->paginator = $paginator;
		
		return $view;
	}
}

