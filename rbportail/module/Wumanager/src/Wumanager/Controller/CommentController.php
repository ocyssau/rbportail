<?php
namespace Wumanager\Controller;

use Zend\View\Model\ViewModel;

//models and Dao
use Discussion\Model\Comment;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Filter\Op;

class CommentController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_CONSULT)){
			return $this->notauthorized();
		}

		$view = new ViewModel();

		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$discussionUid = $request->getQuery('discussionUid');

		$list = DaoFactory::get()->getList(Comment::$classId);
		$filter = DaoFactory::get()->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussionUid', Op::EQUAL);
		$list->load($filter->__toString());

		$view->list = $list;

		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_CONSULT)){
			return $this->notauthorized();
		}

		$view = new ViewModel();

		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$request = $this->getRequest();
		$discussionUid = $request->getQuery('discussionUid');
		$parentId = $request->getQuery('parentId');
		$body = $request->getQuery('body');

		$model = Comment::init();
		$model->hydrate(array(
			'discussionUid'=>$discussionUid,
			'parentId'=>$parentId,
			'body'=>$body,
		));

		$dao = DaoFactory::get()->getDao($model->cid);
		$dao->save($model);

		$view->object = $model;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$id = $request->getQuery('commentId');

		$dao = DaoFactory::get()->getDao(Comment::$classId);
		$dao->deleteFromId($id);
		$view->data = array('id'=>$id);
		return $view;
	}
}
