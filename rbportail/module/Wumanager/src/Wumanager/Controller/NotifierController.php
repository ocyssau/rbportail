<?php
namespace Wumanager\Controller;

use Zend\View\Model\ViewModel;
use Application\Dao\Factory as DaoFactory;
use Application\Model\People;

/**
 *
 *
 */
class NotifierController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = "wumanager_notifier";

	/**
	 */
	public function indexAction()
	{
		$view = new ViewModel();
		$request = $this->getRequest();

		/* disable layout */
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$sql = "SELECT workunitId, message FROM workunit_marker AS mark WHERE userId=:userId LIMIT 1000";
		$bind = array(':userId'=>People\CurrentUser::get()->getId());
		$list = DaoFactory::get()->getList('wumarker5ab4g');
		$list->loadFromSql($sql, $bind);
		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$view = new ViewModel();
		$request = $this->getRequest();

		/* disable layout */
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$wuId = $request->getQuery('workunitId', null);

		if(!$wuId){
			throw new \Exception('workunitId query parameter is not setted');
		}

		$bind = array(
			':userId'=>People\CurrentUser::get()->getId(),
			':wuId'=>$wuId
		);
		$dao = DaoFactory::get()->getDao(\Wumanager\Model\Workunit\Marker::$classId);
		$dao->deleteFromFilter('workunitId=:wuId AND userId=:userId LIMIT 10', $bind, false);

		$view->list=array(
			'userId'=>People\CurrentUser::get()->getId(),
			'wuId'=>$wuId,
		);

		return $view;
	}

}
