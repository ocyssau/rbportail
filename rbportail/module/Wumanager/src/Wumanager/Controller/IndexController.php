<?php
namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Wumanager\Model;
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Wumanager\Form\WorkunitFilterForm;

/**
 * 
 *
 */
class IndexController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = "wumanager_index";

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{

		$acl = $this->getAcl();

		if($acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->manage();
		}
		elseif($acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->manage();
		}
		elseif($acl->hasRight($acl::RIGHT_CONSULT)){
			return $this->consult();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function consultAction()
	{
		$acl = $this->getAcl();

		if($acl->hasRight($acl::RIGHT_CONSULT)){
			return $this->consult();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 *
	 */
	public function consult()
	{
		$view = new ViewModel();
		$view->setTemplate('wumanager/index/consult');
		$request = $this->getRequest();

		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();

		$filter = $this->_getFilter('wumanager/index/filter');
		$filter->prepare();
		$filter->saveToSession('wumanager/index/filter');

		/* search from header search area : */
		$bind = array();
		$list = DaoFactory::get()->getList(Model\Workunit::$classId);

		$paginator = new PaginatorForm($this->pageId);
		$paginator->load($request);
		$paginator->setMaxLimit($list->countAll(''));
		$paginator->prepare()->bindToView($view)->save();

		/*
		$sql = "SELECT
		wu.*,
		site.name AS site,
		CONCAT(wutype.uid, '-', wutype.name) AS type,
		deliv.title AS deliverableTitle,
		deliv.name AS deliverableName,
		discus.body AS comment
		FROM workunit AS wu";
		$sql .= " JOIN site ON site.uid = wu.siteUid";
		$sql .= " JOIN workunit_type AS wutype ON wutype.id = wu.typeId";
		$sql .= " JOIN link_workunit_type_context AS lcontext ON lcontext.parentId = wutype.id";
		$sql .= " JOIN workunit_context AS wucontext ON lcontext.childId = wucontext.id";
		$sql .= " LEFT OUTER JOIN deliverable AS deliv ON deliv.parentId = wu.id";
		$sql .= " LEFT OUTER JOIN acl AS acl ON acl.roleId=site.uid";
		$sql .= "
		LEFT OUTER JOIN(
		SELECT id,body,discussionUid FROM discussion_comment as t1
		JOIN (SELECT MAX(id) as maxid FROM discussion_comment GROUP BY discussionUid) as t2 ON t2.maxid=t1.id
		) AS discus ON discus.discussionUid=wu.id";
		*/

		$sql = "SELECT
		wu.*,
		site.name AS site,
		CONCAT(wutype.uid, '-', wutype.name) AS type,
		deliv.title AS deliverableTitle,
		deliv.name AS deliverableName,
		discus.body AS comment
		FROM workunit AS wu";
		$sql .= " LEFT OUTER JOIN site ON site.uid = wu.siteUid";
		$sql .= " JOIN workunit_type AS wutype ON wutype.id = wu.typeId";
		$sql .= " JOIN link_workunit_type_context AS lcontext ON lcontext.parentId = wutype.id";
		$sql .= " JOIN workunit_context AS wucontext ON lcontext.childId = wucontext.id";
		$sql .= " LEFT OUTER JOIN deliverable AS deliv ON deliv.parentId = wu.id";
		$sql .= " LEFT OUTER JOIN acl AS acl ON acl.roleId=site.uid";
		$sql .= "
		LEFT OUTER JOIN(
		SELECT id,body,discussionUid FROM discussion_comment as t1
		JOIN (SELECT MAX(id) as maxid FROM discussion_comment GROUP BY discussionUid) as t2 ON t2.maxid=t1.id
		) AS discus ON discus.discussionUid=wu.id";

		$sql .= " WHERE (acl.userId=:userlogin OR wu.ownerId=:userId) AND wu.visibility=:visibility";

		$bind[':userlogin']=$userLogin;
		$bind[':userId']=$userId;
		$bind[':visibility']='public';

		if($filter->where){
			$sql .= ' AND ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}

		$sql .= " GROUP BY wu.id, site.name, deliv.title, deliv.name, discus.body";
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->myWorkunits = $list;
		$view->myWuHeaders = $this->_getHeader();
		$view->filter = $filter;
		$view->statusSelect = $this->_getStatusSelect();
		return $view;
	}

	/**
	 *
	 */
	public function manage()
	{
		$view = new ViewModel();
		$view->setTemplate('wumanager/index/manage');
		$request = $this->getRequest();
		$bind = [];

		$acl = $this->getAcl();
		if($acl->hasRight($acl::RIGHT_ADMIN)){
			$view->mode = 'admin';
		}

		$filter = $this->_getFilter('wumanager/index/filter');
		$filter->prepare();
		$filter->saveToSession('wumanager/index/filter');

		$cid = Model\Workunit::$classId;
		$list = DaoFactory::get()->getList($cid);

		$paginator = new PaginatorForm($this->pageId);
		$paginator->load($request);
		$paginator->setMaxLimit($list->countAll(''));
		$paginator->prepare()->bindToView($view)->save();

		$sql = "SELECT
		wu.*,
		site.name AS site,
		CONCAT(wutype.uid, '-', wutype.name) AS type,
		deliv.title AS deliverableTitle,
		deliv.name AS deliverableName,
		discus.body AS comment
		FROM workunit AS wu";
		$sql .= " LEFT OUTER JOIN site ON site.uid = wu.siteUid";
		$sql .= " JOIN workunit_type AS wutype ON wutype.id = wu.typeId";
		$sql .= " JOIN link_workunit_type_context AS lcontext ON lcontext.parentId = wutype.id";
		$sql .= " JOIN workunit_context AS wucontext ON lcontext.childId = wucontext.id";
		$sql .= " LEFT OUTER JOIN deliverable AS deliv ON deliv.parentId = wu.id";
		$sql .= "
		LEFT OUTER JOIN(
		SELECT id,body,discussionUid FROM discussion_comment as t1
		JOIN (SELECT MAX(id) as maxid FROM discussion_comment GROUP BY discussionUid) as t2 ON t2.maxid=t1.id
		) AS discus ON discus.discussionUid=wu.id";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = $filter->bind;
		}

		$sql .= " GROUP BY wu.id, site.name, deliv.title,deliv.name, discus.body";
		$sql .= $paginator->toSql();
		
		
		$list->loadFromSql($sql, $bind);
		$view->myWorkunits = $list;
		$view->myWuHeaders = $this->_getHeader();
		$view->statusSelect = $this->_getStatusSelect();
		$view->filter = $filter;
		return $view;
	}

	/**
	 */
	public function exportAction()
	{
		$view = new ViewModel();
		$view->setTemplate('wumanager/index/excel');
		//$request = $this->getRequest();

		$filter = $this->_getFilter('wumanager/index/filter');
		$filter->prepare();

		$cid = Model\Workunit::$classId;
		$list = DaoFactory::get()->getList($cid);
		$bind = [];
		
		$sql = "SELECT
		wu.*,
		site.name AS site,
		CONCAT(wutype.uid, '-', wutype.name) AS type,
		deliv.title AS deliverableTitle,
		deliv.name AS deliverableName
		FROM workunit AS wu";
		$sql .= " JOIN site ON site.uid = wu.siteUid";
		$sql .= " JOIN workunit_type AS wutype ON wutype.id = wu.typeId";
		$sql .= " JOIN link_workunit_type_context AS lcontext ON lcontext.parentId = wutype.id";
		$sql .= " JOIN workunit_context AS wucontext ON lcontext.childId = wucontext.id";
		$sql .= " LEFT OUTER JOIN deliverable AS deliv ON deliv.parentId = wu.id";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = $filter->bind;
		}

		$sql .= " GROUP BY wu.id";
		$list->loadFromSql($sql, $bind);

		$view->list = $list;
		$view->title = 'Workunits';
		return $view;
	}

	/**
	 * Return a select element to select the status
	 */
	private function _getStatusSelect()
	{
		$element = new \Zend\Form\Element\Button('status');
		return $element;
	}

	/**
	 * Return a select element to select the status
	 */
	private function _getHeader()
	{
		return array(
			'id'=>array('label'=>'#', 'sysName'=>'id', 'sortable'=>true),
			'name'=>array('label'=>'Reference Wu', 'sysName'=>'name', 'sortable'=>true),
			'title'=>array('label'=>'Title', 'sysName'=>'title', 'sortable'=>true),
			'site'=>array('label'=>'Site', 'sysName'=>'site', 'sortable'=>true),
			'status'=>array('label'=>'Status', 'sysName'=>'status', 'sortable'=>true),
			'type'=>array('label'=>'Type', 'sysName'=>'type', 'sortable'=>true),
			'comment'=>array('label'=>'Last Comment', 'sysName'=>'comment', 'sortable'=>false),
			'sitedeliveryDate'=>array('label'=>'Production Site Delivery Date', 'sysName'=>'sitedeliveryDate', 'sortable'=>true),
			'deliverableTitle'=>array('label'=>'Deliverable Title', 'sysName'=>'deliverableTitle', 'sortable'=>true),
			'deliverableName'=>array('label'=>'Deliverable Ref', 'sysName'=>'deliverableName', 'sortable'=>true),
			'planDate'=>array('label'=>'Plan Date', 'sysName'=>'planDate', 'sortable'=>true),
		);
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new WorkunitFilterForm();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = "CONCAT_WS('',wu.id,wu.name,wu.title,site.name,wutype.uid,wutype.name,wu.comment,wu.status,deliv.title,deliv.name)";
		$filter->key2 = "lcontext.childUid";
		return $filter;
	}
}
