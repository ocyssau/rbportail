<?php

namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

use Wumanager\Dao;
use Wumanager\Model\Workunit\Type as WuType;
use Wumanager\Form;

//models and Dao
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Application\Form\StdFilterForm;

/**
 * 
 *
 */
class WutypeController extends AbstractController
{

	/**
	 *
	 * @param boolean $init
	 */
	public function _getModel($init = false)
	{
		if($init){
			$model = WuType::init();
		}
		else{
			$model = new WuType();
		}
		return $model;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		//get Acls for application from SESSION, see dispatch method
		$appAcl = People\CurrentUser::get()->appacl;

		if($appAcl->hasRight($appAcl::RIGHT_ADMIN)){
			return $this->manage();
		}
		elseif($appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->manage();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 *
	 */
	public function manage()
	{
		$view = new ViewModel();
		$view->setTemplate('wumanager/wutype/index');
		$request = $this->getRequest();

		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->key = 'CONCAT(type.name,type.uid)';
		$filter->passThrough=true;
		$filter->prepare();

		$bind = array();
		$cid = WuType::$classId;
		$factory = DaoFactory::get();
		$list = $factory->getList($cid);
		$table = $factory->getTable($cid);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql  = " SELECT
		type.id,
		type.uid,
		type.name,
		type.tuProduction,
		type.tuManagement,
		type.tuManagement,
		type.tuProductionTlse,
		type.tuManagementTlse,
		type.amount,
		type.salesRate,
		type.costSolintep,
		type.costGeometric,
		type.newCostGeometric
		FROM $table AS type";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->myList = $list;

		$view->myHeaders = array(
			'#'=>'id',
			'Uid'=>'uid',
			'Name'=>'name',
			'OFF WU Time'=>'tuProduction',
			'OFF Management Time'=>'tuManagement',
			'TLSE Production Time'=>'tuProductionTlse',
			'TLSE Management Time'=>'tuManagementTlse',
			'Amount'=>'amount',
			'Sales Rate'=>'salesRate',
			'Cost Solintep'=>'costSolintep',
			'Cost Geom'=>'costGeometric',
			'New Cost Geom'=>'newCostGeometric'
		);

		$view->filter = $filter;
		return $view;
	}

	/**
	 *
	 */
	public function getAction()
	{
		$view =  new ViewModel();

		$params = $this->getRequest()->getQuery();
		( isset($params['typedim']) ) ? $typeDim = strtolower($params['typedim']) : $typeDim = null;
		( isset($params['context']) ) ? $context = strtolower($params['context']) : $context = null;

		$cid = WuType::$classId;
		$list = DaoFactory::get()->getList($cid);

		$filter = array();

		$sql = "SELECT type.*, context.name AS context, context.uid AS contextUid, context.id AS contextId FROM workunit_type AS type";
		$sql .= " INNER JOIN link_workunit_type_context AS link ON link.parentId=type.id";
		$sql .= " INNER JOIN workunit_context AS context ON context.id=link.childId";

		if($typeDim=='2d'){
			$filter[] = "type.dimension = 2";
		}
		elseif($typeDim=='3d'){
			$filter[] = "type.dimension = 3";
		}
		if($context){
			$filter[] = "context.id = $context";
		}

		if($filter){
			$sql .= " WHERE " . implode(' AND ', $filter);
		}

		$sql .= " ORDER BY uid ASC";

		$list->loadFromSql($sql);

		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
			echo json_encode($list->toArray(), JSON_FORCE_OBJECT);
			die;
		}
		else{
			$view->list = $list;
			return $view;
		}
	}

	/**
	 *
	 */
	public function addAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		//init model
		$model = $this->_getModel(true);
		//init view
		$view = new ViewModel();
		$request = $this->getRequest();

		$form = new Form\WutypeForm();
		$form->bind($model);

		if ($request->isPost()) {
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				//save
				DaoFactory::get()->getDao($model)->save($model);
				return $this->redirect()->toRoute('wutype');
			}
		}

		$view->title = 'Create New Type';
		$view->setTemplate($form->template);
		$view->model = $model;
		$view->linkedContext = array();
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');

		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();

		//load model
		$model = $this->_getModel(false);
		DaoFactory::get()->getDao($model)->loadFromId($model, $id);

		$linkDao = new Dao\Workunit\LinkTypeContext();
		$linkedContext = $linkDao->getChildren($id)->fetchAll();

		$form = new Form\WutypeForm();
		$form->bind($model);

		if ($request->isPost()) {
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				//save
				DaoFactory::get()->getDao($model)->save($model);
				return $this->redirect()->toRoute('wutype');
			}
		}

		$view->title = 'Edit Type ' . $model->getUid();
		$view->setTemplate($form->template);
		$view->model = $model;
		$view->linkedContext = $linkedContext;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$id = $this->params()->fromRoute('id');

		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$model = $this->_getModel(false);
		$dao = DaoFactory::get()->getDao($model);

		$dao->loadFromId($model, $id);
		$uid = $model->getUid();
		$dao->delete($uid, /*$withchild*/ true, /*$withtrans*/ true);

		return $this->redirect()->toRoute('wutype');
	}
}
