<?php
namespace Wumanager\Controller;

use Zend\View\Model\ViewModel;
use Application\Dao\Factory as DaoFactory;
use Wumanager\Model\Notification;

/**
 */
class NotificationController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = "wumanager_notification";

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		return array();
	}

	/**
	 * @return \Wumanager\Model\Workunit
	 */
	protected function _newReference()
	{
		return new \Wumanager\Model\Workunit();
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		
		$view = new ViewModel();
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		/**/
		$referenceId = $this->params()->fromRoute('referenceId', null);
		
		/* set helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Notification\Notification::$classId);
		$owner = \Application\Model\People\CurrentUser::get();
		
		$form = new \Wumanager\Form\Notification\EditForm($view);
		$collection = new \Wumanager\Form\Notification\Collection();
		$form->get('mail')->setValue($owner->getMail());
		
		if ( $referenceId ) {
			try {
				/* load reference */
				$reference = $this->_newReference();
				$factory->getDao($reference->cid)->loadFromId($reference, $referenceId);
				$referenceUid = $reference->getUid();
				
				/* get existing notifications */
				$stmt = $dao->getFromReferenceUidAndOwnerUid($referenceUid, $owner->getLogin());
				$notifications = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$view->title = 'My Notifications for '.$reference->getName();
			}
			catch( \PDOException $e ) {
				throw $e;
			}
		}
		else {
			try {
				/* get existing notifications */
				$stmt = $dao->getFromReferenceUidAndOwnerUid('all', $owner->getLogin());
				$notifications = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$view->title = 'My Notifications for All';
			}
			catch( \PDOException $e ) {
				throw $e;
			}
		}
		
		foreach( $notifications as $properties ) {
			$notification = new Notification\Notification();
			$notification->newUid();
			$notification->hydrate($properties);
			$collection->add($notification);
		}
		
		$view->form = $form;
		$view->collection = $collection;
		$view->currentUser = $owner;
		$view->referenceId = $referenceId;
		
		return $view;
	}

	/**
	 * HTTP POST REQUEST
	 *
	 */
	public function addAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		
		/**/
		$view = new ViewModel();
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		if ( !$request->isPost() ) {
			throw new \Exception('You must use http post method only');
		}
		
		/**/
		$referenceId = $this->params()->fromRoute('referenceId', null);
		$mail = $request->getPost('mail');
		$event = $request->getPost('event');
		
		/* set helpers */
		$factory = DaoFactory::get();
		$currentUser = \Application\Model\People\CurrentUser::get();
		$ownerUid = $currentUser->getUid();
		
		/* build notification */
		$notification = Notification\Notification::init()->newUid();
		$notification->setOwner($currentUser);
		$notification->mail = $mail;
		$notification->setEvent($event);
		
		/* load reference */
		if ( $referenceId ) {
			$reference = $this->_newReference();
			$factory->getDao($reference->cid)->loadFromId($reference, $referenceId);
			$referenceUid = $reference->getUid();
			$notification->setReference($reference);
			$notification->setName($event . '-' . $currentUser->getUid() . '-' . referenceUid);
		}
		else {
			$notification->setName($event . '-' . $currentUser->getUid() . '-all');
			$notification->referenceId = 0;
			$notification->referenceUid = 'all';
			$notification->referenceCid = \Wumanager\Model\Workunit::$classId;
		}
		
		$notification->dao = $factory->getDao(Notification\Notification::$classId);
		$notification->dao->save($notification);
		
		$view->notification = $notification;
		return $view;
	}

	/**
	 * HTTP GET REQUEST
	 *
	 */
	public function deleteAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		
		/**/
		$view = new ViewModel();
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		if ( !$request->isGet() ) {
			throw new \Exception('You must use http get method only');
		}
		
		/**/
		$referenceId = $this->params()->fromRoute('referenceId', null);
		$notificationId = $request->getQuery('id');
		
		/* set helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Notification\Notification::$classId);
		$dao->deleteFromId($notificationId, false);
		
		$view->notificationId = $notificationId;
		return $view;
	}
}
