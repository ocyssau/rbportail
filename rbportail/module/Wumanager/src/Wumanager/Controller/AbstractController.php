<?php
namespace Wumanager\Controller;

use Application\Model\People;
use Wumanager\Model\Acl\Wumanager as Acl;

abstract class AbstractController extends \Application\Controller\AbstractController
{
	/**
	 *
	 * @var string
	 */
	public $application='wumanager';

	/**
	 * @return Acl
	 */
	public function getAcl()
	{
		$user = People\CurrentUser::get();
		if(!isset($user->wumacl)){
			$acl = new Acl($user->appacl);
			$acl->loadRole($user);
			$user->wumacl = $acl;
		}
		return $user->wumacl;
	}
}
