<?php
namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Filter\Op;
use Wumanager\Form;
use Wumanager\Model\Workunit;
use Workflow\Model\Wf;
use Zend\Mvc\Exception\RuntimeException as Exception;

/**
 * 
 *
 */
class ProcessController extends AbstractController
{

	/**
	 * Display one workunit in disabled form
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function startAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		$workunitId = $this->params()->fromRoute('id', null);

		/* disable layout */
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$link = Workunit\LinkProcess::init();
		$link->parentId = $workunitId;

		$workunit = new Workunit();
		$workunit->dao = DaoFactory::get()->getDao($workunit);
		$workunit->dao->loadFromId($workunit, $workunitId);

		/* get runnings process */
		$runningProcess = DaoFactory::get()->getDao(Workunit\LinkProcess::$classId)->getProcess($workunitId, 'running');

		$form = new Form\StartProcessForm();
		$form->bind($link);
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					/* only one process by WU */
					if ( count($runningProcess) > 0 ) {
						throw new Exception("Some processes are currently running on this WU. You must close this process before.");
					}

					/* instanciate Workflow service */
					$workflow = $this->getEvent()
						->getApplication()
						->getServiceManager()
						->get('Workflow')
						->connect($this);
					$workflow->workunit = $workunit;

					/* start process */
					$processId = $link->childId;
					$startActInst = $workflow->startProcess($processId)->startInstance;

					/* translate to next activities */
					$workflow->translateActivity($startActInst->getId());

					/* create link between process instance and workunit object */
					$link->childId = $workflow->instance->getId();
					$link->attributes = array(
						'processId' => $processId
					);
					DaoFactory::get()->getDao($link)->insert($link);
				}
				catch( \Exception $e ) {
					throw $e;
				}
				return $this->redirect()->toRoute('wumanager');
			}
		}

		$view->title = 'Edit Workunit';
		$view->form = $form;
		$view->runningProcess = $runningProcess;
		$view->workunit = $workunit;
		return $view;
	}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function stopAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$piId = $this->params()->fromRoute('id');
		$workflow = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow')
			->connect($this);
		$workflow->deleteInstance($piId);

		return $this->redirect()->toRoute('wumanager');
	}

	/**
	 * @return \Zend\View\Model\ViewModel
	 */
	public function runactivityAction()
	{
		$request = $this->getRequest();

		/* Check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_WFPUSH) ) {
			return $this->notauthorized();
		}

		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		$actInstId = $this->params()->fromQuery('aid');
		$redirectTo = $this->getRequest()->getQuery('redirectTo', 'wumanager');

		/* Instanciate Workflow service */
		$workflow = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow')
			->connect($this);

		/* RUN ACTIVITY: */
		$workflow->init();
		$workflow->runActivity($actInstId)->lastActivity;
		
		if ( $redirectTo ) {
			return $this->redirect()->toRoute($redirectTo);
		}
		else {
			return $view;
		}
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function runstandaloneAction()
	{
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$activityId = $this->params()->fromQuery('aid');
		$procInstId = $this->params()->fromQuery('pid');
		$redirectTo = $this->getRequest()->getQuery('redirectTo', 'wumanager');

		/* instanciate Workflow service */
		$workflow = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow')
			->connect($this);

		/* RUN ACTIVITY: */
		$workflow->runStandalone($activityId, $procInstId)->lastActivity;

		if ( $redirectTo ) {
			return $this->redirect()->toRoute($redirectTo);
		}
		else {
			return $view;
		}
	}

	/**
	 * ajax method
	 */
	public function getnextactivityAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}

		$workunitId = $this->params()->fromRoute('id');
		$view = new ViewModel();
		$factory = DaoFactory::get();

		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		/* @var \Wumanager\Dao\Workunit\LinkProcess $dao */
		$dao = $factory->getDao(Workunit\LinkProcess::$classId);

		/* get next activities */
		$stmt = $dao->getNextactivities($workunitId);
		$view->activities = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		/* get standalone activities */
		$stmt = $dao->getStandaloneActivities($workunitId);
		$view->standalones = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		return $view;
	}

	/**
	 * ajax method
	 */
	public function deletelastAction()
	{
		$factory = DaoFactory::get();

		/* check authorization */
		$acl = $this->getAcl();
		if ( !$acl->hasRight($acl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = new ViewModel();

		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		/* instance process Id */
		$instProcessId = (int)$this->params()->fromRoute('id');

		/* get the running activity */
		$runningActInstance = new Wf\Instance\Activity();
		$actIDao = $factory->getDao($runningActInstance->cid);
		$filter = $factory->getFilter($runningActInstance->cid)->setOption('asapp', true);
		$filter->andfind($instProcessId, 'instanceId', Op::OP_EQUAL);
		$filter->andfind('running', 'status', Op::OP_EQUAL);
		$actIDao->load($runningActInstance, $filter);

		$iaId = $runningActInstance->getId();

		$stmt = $actIDao->getPrevious($iaId);
		$res0 = $stmt->fetch(\PDO::FETCH_ASSOC);
		$previousActInstId = $res0['id'];
		$previousStatus = $res0['previousTransitionName'];
		$processInstanceId = $res0['instanceId'];

		/* get workunit attached to processInstance */
		$linkStmt = $factory->getDao(\Wumanager\Model\Workunit\LinkProcess::$classId)->getParent($processInstanceId);
		$workunitId = (int)$linkStmt->fetchColumn(1);

		/* put previous instance activity in running mode */
		$actIDao->setFromId($previousActInstId, $actIDao->toSys('status'), Wf\Instance\Activity::STATUS_RUNNING);

		/* delete activity instance */
		$actIDao->deleteFromId($iaId);
		/* Update status on workunit */
		$wuDao = $factory->getDao(Workunit::$classId);
		$wuDao->setFromId($workunitId, $wuDao->toSys('status'), $previousStatus);

		return $this->redirect()->toRoute('wumanager');
	}
}
