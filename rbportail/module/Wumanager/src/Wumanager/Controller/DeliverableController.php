<?php

namespace Wumanager\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Wumanager\Form;
use Wumanager\Model;
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;

/**
 * 
 *
 */
class DeliverableController extends AbstractController
{
	public function indexAction()
	{
		return new ViewModel();
	}

	/**
	 *
	 */
	protected function _getForm($type='2d')
	{
		if($type=='2d'){
			$form = new Form\Deliverable2dFieldset();
		}
		elseif($type=='3d'){
			$form = new Form\Deliverable3dFieldset();
		}
		return $form;
	}

	/**
	 *
	 * @param boolean $init
	 */
	protected function _getModel($init = false)
	{
		if($init){
			$model = Model\Deliverable::init();
		}
		else{
			$model = new Model\Deliverable();
		}
		return $model;
	}

	/**
	 *
	 */
	public function addAction()
	{
		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$request = $this->getRequest();
		$type = $request->getQuery('type');

		$form = $this-> _getForm($type);

		if ($request->isPost()) {
			$form->prepare();
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$model = $this->_getModel(true);
				$model->setOwner(People\CurrentUser::get());
				$model->setUpdateBy(People\CurrentUser::get());
				$model->setUpdated(new \DateTime());

				//save project
				$form->save($model);

				return $this->redirect()->toRoute('wumanager');
			}
		}

		$view = new ViewModel();
		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}

	public function editAction()
	{
		return new ViewModel();
	}

	public function deleteAction()
	{
		/* check authorization */
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();

		$uid = $this->params()->fromRoute('id', $request->getQuery('uid', null));

		$model = $this->_getModel(false);
		$dao = DaoFactory::get()->getDao($model);
		$dao->loadFromUid($model, $uid);
		$dao->delete($uid, $withchild=true);

		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
			$view->return = json_encode($model);
		}
		return $view;
	}
}

