<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wumanager\Model;
use Application\Dao\Factory as DaoFactory;

/**
 *
 */
class WorkunitCreateFormPage1 extends Form
{

	/**
	 * 
	 * @var \Zend\InputFilter\InputFilter
	 */
	protected $inputFilter;

	/**
	 * 
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('workunit_create_page1');
		$this->setAttribute('method', 'post');
		
		$this->template = 'wumanager/workunit/createform/workunitcreateformpage1.phtml';
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'typeId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select',
				'placeholder' => 'Type'
			),
			'options' => array(
				'label' => 'Type',
				'value_options' => array(),
				'empty_option' => 'Please, select a type'
			)
		));
		
		$this->add(array(
			'name' => 'context',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select',
				'placeholder' => 'Program'
			),
			'options' => array(
				'label' => 'Program',
				'value_options' => $this->_getProgramOptions(),
				'empty_option' => 'Please, select the program'
			)
		));
		
		$this->add(array(
			'name' => 'typeDimension',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select',
				'placeholder' => 'Type Dimension'
			),
			'options' => array(
				'label' => 'Dimension',
				'value_options' => array(
					'2d' => '2D',
					'3d' => '3D'
				),
				'empty_option' => 'Please, select a dimension'
			)
		));
		
		$this->add(array(
			'name' => 'next',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Next',
				'id' => 'nextbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function prepareFilters()
	{
		if ( !$this->inputFilter ) {
			$inputFilter = new InputFilter();
			$factory = new InputFactory();
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'id',
				'required' => true,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'type',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			)));
			
			$this->inputFilter = $inputFilter;
		}
		
		return $this->inputFilter;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->getId());
		$this->get('type')->setValue($object->getType(true));
		$this->get('typeDimension')->setValue($object->typeDimension);
	}

	/**
	 *
	 */
	protected function _getProgramOptions()
	{
		$ret = array();
		$list = DaoFactory::get()->getList(Model\Workunit\Context::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['id']] = $item['name'];
		}
		return $ret;
	}
}
