<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Application\Dao\Factory as DaoFactory;

class StartProcessForm extends Form implements InputFilterProviderInterface
{

	public function __construct()
	{
		parent::__construct('startProcess');

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'childId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select'
			),
			'options' => array(
				'label' => 'Process',
				'value_options' => $this->_getProcessOptions(),
				'empty_option' => 'Please, select a process'
			)
		));

		$this->add(array(
			'name' => 'parentId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Start',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'childId' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'parentId' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 */
	protected function _getProcessOptions()
	{
		$ret=array();
		$list = DaoFactory::get()->getList(\Workflow\Model\Wf\Process::$classId);
		foreach($list->load('isActive=1') as $item){
			$ret[$item['id']] = $item['name'].' '.$item['version'];
		}
		return $ret;
	}
}
