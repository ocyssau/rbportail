<?php
namespace Wumanager\Form;

use Wumanager\Model;
use Application\Dao\Factory as DaoFactory;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 * @author ocyssau
 *
 */
class WutypeForm extends Form implements InputFilterProviderInterface
{
	
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct('Wutype');
		$this->template = 'wumanager/wutype/createform.phtml';

		$this
			->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
		    'name' => 'id',
		    'type'  => 'Hidden',
		));

		$this->add(array(
		    'name' => 'name',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'text',
		        'placeholder' => 'Name',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'Name',
		    ),
		));

		$this->add(array(
		    'name' => 'uid',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'text',
		        'placeholder' => 'Uid',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'Uid',
		    ),
		));

		$this->add(array(
		    'name' => 'tuProduction',
		    'type'  => 'Zend\Form\Element\Number',
		    'attributes' => array(
		        'type'  => 'number',
		    	'step'=> '0.01',
		    	'min'=> '0',
		        'placeholder' => 'tuProduction Time',
		    ),
		    'options' => array(
		        'label' => 'tuProduction Time',
		        'format' => 'd-m-Y',
		    ),
		));

		$this->add(array(
		    'name' => 'tuManagement',
		    'type'  => 'Zend\Form\Element\Number',
		    'attributes' => array(
		        'type'  => 'number',
		    	'step'=> '0.01',
		    	'min'=> '0',
		    	'placeholder' => 'tuManagement Time',
		    ),
		    'options' => array(
		        'label' => 'tuManagement Time',
		        'format' => 'd-m-Y',
		    ),
		));

		$this->add(array(
		    'name' => 'tuProductionTlse',
		    'type'  => 'Zend\Form\Element\Number',
		    'attributes' => array(
		        'type'  => 'number',
		    	'step'=> '0.01',
		    	'min'=> '0',
		        'placeholder' => 'tuProductionTlse Time',
		    ),
		    'options' => array(
		        'label' => 'tuProductionTlse Time',
		        'format' => 'd-m-Y',
		    ),
		));

		$this->add(array(
		    'name' => 'tuManagementTlse',
		    'type'  => 'Zend\Form\Element\Number',
		    'attributes' => array(
		        'type'  => 'number',
		    	'step'=> '0.01',
		    	'min'=> '0',
		        'placeholder' => 'tuManagementTlse Time',
		    ),
		    'options' => array(
		        'label' => 'tuManagementTlse Time',
		        'format' => 'd-m-Y',
		    ),
		));

		$this->add(array(
		    'name' => 'amount',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'number',
		    	'step'=> '0.01',
		    	'min'=> '0',
		        'placeholder' => 'Amount In Euro',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'Amount In Euro',
		    ),
		));

		$this->add(array(
		    'name' => 'salesRate',
		    'type'  => 'Zend\Form\Element',
		    'attributes' => array(
		        'type'  => 'text',
		        'placeholder' => 'salesRate',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'salesRate',
		    ),
		));
		
		$this->add(array(
			'name' => 'costSolintep',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '0.01',
				'min'=> '0',
				'placeholder' => 'Cost Solintep',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Cost Of Solintep',
			),
		));
		
		$this->add(array(
			'name' => 'costGeometric',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '0.01',
				'min'=> '0',
				'placeholder' => 'Cost Geometric',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Cost Of Geometric',
			),
		));
		
		$this->add(array(
			'name' => 'newCostGeometric',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '0.01',
				'min'=> '0',
				'placeholder' => 'New Cost Geometric',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'New Cost Of Geometric',
			),
		));
		
		$this->add(array(
		    'name' => 'context',
		    'type'  => 'Zend\Form\Element\Select',
		    'attributes' => array(
		        'type'  => 'select',
		        'placeholder' => 'Context',
		        'class'=>'form-control'
		    ),
		    'options' => array(
		        'label' => 'Context',
		    	'value_options'=> $this->_getContextOptions(),
		    	'empty_option'=>'Please, select a context'
		    ),
		));

		$this->add(array(
			'name' => 'dimension',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
				'placeholder' => 'Dimension',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Dimension',
				'value_options'=> array('2'=>'2D', '3'=>'3D'),
				'empty_option'=>'Please, select a dimension'
			),
		));


		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
	    return array(
	    	'id' => array(
	    	    'required' => false,
	    	),
	    	'uid' => array(
	    		'required' => true,
	    	),
	    	'name' => array(
	    		'required' => true,
	    	),
	    	'tuProduction' => array(
	    	    'required' => false,
	    	),
	    	'tuManagement' => array(
	    	    'required' => false,
	    	),
	    	'tuProductionTlse' => array(
	    	    'required' => false,
	    	),
	    	'tuManagementTlse' => array(
	    	    'required' => false,
	    	),
	    	'context'=>array('required' => false),
	    	'salesRate'=>array('required' => false),
	    	'amount'=>array('required' => false),
	    	'costSolintep'=>array('required' => false),
	    	'costGeometric'=>array('required' => false),
	    );
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
	    $ret = array();
	    $list = DaoFactory::get()->getList(Model\Workunit\Context::$classId);
	    foreach($list->load() as $item){
	        $ret[$item['id']] = $item['name'];
	    }
	    return $ret;
	}
}
