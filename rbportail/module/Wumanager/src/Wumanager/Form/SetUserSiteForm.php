<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Wumanager\Model\Acl\Wumanager as Acl;
use Wumanager\Model;
use Wumanager\Dao;

class SetUserSiteForm extends Form
{
	protected $inputFilter;

	public function __construct($acl)
	{
		// we want to ignore the name passed
		parent::__construct('setUserSite');
		$this->setAttribute('method', 'post');
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'resourceId',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'user',
			'type'  => 'Text',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'User',
				'value_options'=> $this->_getUserOptions(),
				'empty_option'=>'Please, choose an user'
			)
		));
		
		$this->add(array(
			'name' => 'role',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
			),
			'options' => array(
				'label' => 'Site',
				'value_options'=> $this->_getRoleOptions($acl),
				'empty_option'=>'Please, choose a role'
			)
		));
		
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Save',
						'id' => 'submitbutton',
				),
		));
	}
	
	
	/**
	 * @param Acl
	 * @return Array
	 */
	protected function _getRoleOptions($acl)
	{
		$ret = array();
		foreach($acl->roles as $id=>$def){
			$ret[$id]=$def[0];
		}
		return $ret;
	}
	
	/**
	 *
	 */
	protected function _getUserOptions()
	{
		return array(
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($object, $flags = 17)
	{
		$this->get('resourceId')->setValue($object->getMainResource()->getResourceId());
	}
}
