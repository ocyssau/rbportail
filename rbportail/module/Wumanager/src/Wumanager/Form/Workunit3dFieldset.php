<?php
namespace Wumanager\Form;

/**
 * 
 *
 */
class Workunit3dFieldset extends Workunit2dFieldset
{

	/**
	 * @param string $name
	 */
	public function __construct()
	{
		$this->type = '3d';
		parent::__construct();
	}
}
