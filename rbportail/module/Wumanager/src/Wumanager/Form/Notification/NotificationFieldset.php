<?php
namespace Wumanager\Form\Notification;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Wumanager\Model\Notification;

/**
 *
 *
 */
class NotificationFieldset extends Fieldset implements InputFilterProviderInterface
{

	/** @var array */
	protected $eventSet;

	/**
	 *
	 * @param string $name
	 * @param array $events
	 */
	public function __construct($name, $events = null)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);
		
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new Notification\Notification());
		
		if ( $events ) {
			$this->eventSet = $events;
		}
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceCid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceUid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'ownerUid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Add hidden fields */
		$this->add(array(
			'name' => 'ownerId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		/* Event */
		$this->add(array(
			'name' => 'events',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker form-control',
				'multiple' => 'multiple',
			),
			'options' => array(
				'label' => 'Events',
				'value_options' => $this->_getEvents()
			)
		));
		
		/* Mail */
		$this->add(array(
			'name' => 'mail',
			'type' => 'Zend\Form\Element\Email',
			'attributes' => array(
				'placeholder' => 'user@mail',
				'class' => 'form-control',
			),
			'options' => array(
				'label' => 'Mail',
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'referenceId' => array(
				'required' => false
			),
			'referenceCid' => array(
				'required' => false
			),
			'referenceUid' => array(
				'required' => false
			),
			'ownerId' => array(
				'required' => false
			),
			'ownerUid' => array(
				'required' => false
			),
			'events' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToNull'
					)
				)
			),
			'mail' => array(
				'required' => true,
			)
		);
	}

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		if ( !$this->eventSet ) {
			$this->eventSet = array(
				Notification\Notification::SIGNAL_POST_CREATE=>'On Create',
				Notification\Notification::SIGNAL_POST_UPDATE=>'On Update',
				Notification\Notification::SIGNAL_POST_DELETE=>'On Delete',
			);
		}
		return $this->eventSet;
	}
} /* End of class */
