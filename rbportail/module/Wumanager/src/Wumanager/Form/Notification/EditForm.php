<?php
namespace Wumanager\Form\Notification;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Wumanager\Controller\WorkunitController;


/**
 * 
 *
 */
class EditForm extends Form implements InputFilterProviderInterface
{

	/** @var string */
	public $template;

	/** @var InputFilter */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('notificationsForm');
		
		$this->template = 'wumanager/notification/editform';
		$this->view = $view;
		
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());
		
		/* Set notifications collection */
		/*
		$notificationFieldset = new \Wumanager\Form\Notification\NotificationFieldset('notificationFieldset');
		$notificationFieldset->setUseAsBaseFieldset(true);
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'notifications',
			'options' => array(
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => $notificationFieldset,
				'use_as_base_fieldset'
			)
		));
		*/
		
		/* Event */
		$this->add(array(
			'name' => 'event',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker form-control',
				'multiple' => false,
			),
			'options' => array(
				'label' => 'Event',
				'value_options' => $this->_getEvents()
			)
		));
		
		/* Mail */
		$this->add(array(
			'name' => 'mail',
			'type' => 'Zend\Form\Element\Email',
			'attributes' => array(
				'placeholder' => 'user@mail',
				'class' => 'form-control',
			),
			'options' => array(
				'label' => 'Mail',
			)
		));
		
		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));
		
		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'events' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToNull'
					)
				)
			),
			'mail' => array(
				'required' => true,
			)
		);
	}
	
	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		if ( !$this->eventSet ) {
			$this->eventSet = array(
				WorkunitController::SIGNAL_POST_CREATE=>'On Create',
				WorkunitController::SIGNAL_POST_UPDATE=>'On Update',
				WorkunitController::SIGNAL_POST_DELIVERY=>'On Delivery',
				WorkunitController::SIGNAL_POST_COPY=>'On Copy',
				WorkunitController::SIGNAL_POST_DELETE=>'On Delete',
			);
		}
		return $this->eventSet;
	}
}
