<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wumanager\Model\Bill;

/**
 *
 *
 */
class BilltoForm extends Form
{

	protected $inputFilter;

	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('workunit');
		$this->setAttribute('method', 'post');
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'wuid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Reference',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Reference'
			)
		));
		
		$this->add(array(
			'name' => 'emitDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Emit Date'
			),
			'options' => array(
				'label' => 'Emit Date',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'orderDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Order Date'
			),
			'options' => array(
				'label' => 'Order Date',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'orderId',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Order Id',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Order Id'
			)
		));
		
		$this->add(array(
			'name' => 'amount',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Amount In Euro',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Amount In Euro'
			)
		));
		
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
		
		$this->add(array(
			'name' => 'delete',
			'attributes' => array(
				'type' => 'button',
				'id' => 'deletebutton'
			),
			'options' => array(
				'label' => 'Delete'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function prepareFilters()
	{
		if ( !$this->inputFilter ) {
			$inputFilter = new InputFilter();
			$factory = new InputFactory();
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'id',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'wuid',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'name',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'orderId',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'orderDate',
				'required' => false
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'amount',
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Digits'
					)
				),
				'validators' => array(
					array(
						'name' => 'Digits',
						'options' => array()
					)
				)
			)));
			
			$this->inputFilter = $inputFilter;
		}
		
		return $this->inputFilter;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->getId());
		$this->get('wuid')->setValue($object->getParent(true));
		$this->get('name')->setValue($object->getName());
		$this->get('emitDate')->setValue($object->getEmitdate());
		$this->get('orderId')->setValue($object->getOrder());
		$this->get('orderDate')->setValue($object->getOrderdate());
		$this->get('amount')->setValue($object->getAmount());
	}

	/**
	 * @param Bill $bill
	 */
	public function save(Bill $bill)
	{
		$data = $this->getData();
		$properties = array();
		
		(isset($data['name'])) ? $properties['name'] = $data['name'] : null;
		(isset($data['orderId'])) ? $properties['orderId'] = $data['orderId'] : null;
		(isset($data['amount'])) ? $properties['amount'] = $data['amount'] : null;
		
		(isset($data['emitDate'])) ? $properties['emitDate'] = $data['emitDate'] : null;
		(isset($data['orderDate'])) ? $properties['orderDate'] = $data['orderDate'] : null;
		
		$properties['dateFormat'] = 'd-m-Y H:i:s';
		
		$bill->hydrate($properties);
	}
}
