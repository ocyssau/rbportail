<?php
namespace Wumanager\Form;

use Wumanager\Model\Deliverable;

/**
 * 
 * @author ocyssau
 *
 */
class Deliverable2dFieldset extends DeliverableFieldset
{

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('delivrable');

		$this->setObject(new Deliverable());
		$this->template = 'wumanager/deliverable/deliverable2dform.phtml';

		$this->add(array(
			'name' => 'indice',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Indice',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Indice',
			),
		));

		$this->add(array(
			'name' => 'codeFiliere',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Code Filiere',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Code Filiere',
			),
		));

		$this->add(array(
			'name' => 'ds',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Design Solution',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'DS/Parent Assy',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
	    return array_merge(parent::getInputFilterSpecification(), array());
	}
}
