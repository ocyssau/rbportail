<?php
namespace Wumanager\Form;

use Wumanager\Model\Deliverable;

/**
 * 
 * @author ocyssau
 *
 */
class Deliverable3dFieldset extends DeliverableFieldset
{
	protected $inputFilter;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/*  we want to ignore the name passed */
		parent::__construct('delivrable');
		$this->setObject(new Deliverable());
		$this->template = 'wumanager/deliverable/deliverable3dform.phtml';
	}
}
