<?php
namespace Wumanager\Form;

//models and Dao
use Wumanager\Model;
use Application\Dao\Factory as DaoFactory;
use Zend\Form\Form;

/**
 * 
 *
 */
class WorkunitFilterForm extends Form //implements InputFilterAwareInterface
{

	/**
	 * 
	 * @var string
	 */
	public $where;

	/**
	 * 
	 * @var string
	 */
	public $passThrough = true;

	/**
	 * 
	 * @var string
	 */
	public $key1;

	/**
	 * 
	 * @var string
	 */
	public $key2;

	/**
	 * 
	 * @var array
	 */
	public $bind = array();

	/**
	 * 
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('contextfilter');
		$this->setAttribute('id', 'contextfilter');
		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'form-inline');
		
		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type' => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => ''
			),
			'attributes' => array(
				'onChange' => '',
				'class' => 'form-control'
			)
		));
		
		$this->add(array(
			'name' => 'contextfilter-selectInput',
			'type' => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => '',
				'value_options' => $this->_getContextOptions(),
				'empty_option' => 'Please, select a context'
			),
			'attributes' => array(
				'onChange' => '',
				'class' => 'form-control'
			)
		));
		
		$this->add(array(
			'type' => 'Zend\Form\Element\Submit',
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Filter',
				'id' => 'stdfilter-submit',
				'class' => 'btn btn-default'
			)
		
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare()
	{
		parent::prepare();
		$where = array();
		$search = $this->get('stdfilter-searchInput')->getValue();
		if ( $search ) {
			$where[] = $this->key1 . " LIKE :search";
			if ( $this->passThrough ) {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = '%' . $search . '%';
			}
			else {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = $search;
			}
		}
		
		$search = $this->get('contextfilter-selectInput')->getValue();
		if ( $search == 'A350' ) {
			$where[] = $this->key2 . " LIKE 'a350%'";
		}
		else if ( $search == 'LEGACY' ) {
			$where[] = $this->key2 . " LIKE 'legacy%'";
		}
		else if ( $search ) {
			$where[] = $this->key2 . "=:contextfiltersearch";
			$this->bind[':contextfiltersearch'] = $search;
		}
		
		$this->where = implode(' AND ', $where);
		return $this;
	}

	/**
	 * 
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= '<div class="form-group">' . $view->formRow($this->get('contextfilter-selectInput')) . '</div>';
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-searchInput')) . '</div>';
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-submit')) . '</div>';
		$html .= $view->form()->closeTag();
		return $html;
	}

	/**
	 * @param string $key	Name of session key
	 */
	public function saveToSession($key)
	{
		$_SESSION[$key]['stdfilter-searchInput'] = $this->get('stdfilter-searchInput')->getValue();
		$_SESSION[$key]['contextfilter-selectInput'] = $this->get('contextfilter-selectInput')->getValue();
	}

	/**
	 * @param string $key	Name of session key
	 */
	public function loadFromSession($key)
	{
		isset($_SESSION[$key]) ? $data = $_SESSION[$key] : $data=null;
		$this->get('stdfilter-searchInput')->setValue($data['stdfilter-searchInput']);
		$this->get('contextfilter-selectInput')->setValue($data['contextfilter-selectInput']);
	}

	/**
	 *
	 */
	protected function _getContextOptionsAsRecordInGrid()
	{
		$ret = array();
		$list = DaoFactory::get()->getList(Model\Workunit\Context::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['id']] = $item['name'];
		}
		return $ret;
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
		$ret = array(
			'A350' => 'ALL A350',
			'LEGACY' => 'ALL LEGACY'
		);
		
		$list = DaoFactory::get()->getList(Model\Workunit\Context::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['uid']] = $item['name'];
		}
		
		return $ret;
	}
}

