<?php
namespace Wumanager\Form;

use Zend\View\Model\ViewModel;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

/*
 * @see http://framework.zend.com/manual/2.3/en/modules/zend.form.collections.html
 */
class DeliverableFieldset extends Fieldset implements InputFilterProviderInterface
{

	protected $inputFilter;

	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		parent::__construct($name);
		$this->setAttribute('method', 'post');
		$this->setHydrator(new Hydrator(false));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'id'
			)
		));

		$this->add(array(
			'name' => 'uid',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'uid'
			)
		));

		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Reference',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Reference'
			)
		));

		$this->add(array(
			'name' => 'title',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Description',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Description'
			)
		));

		$this->add(array(
			'name' => 'section',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Section',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Section'
			)
		));

		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => 'Comments',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Comments'
			)
		));

		$this->add(array(
			'name' => 'projeteur',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Projeteur',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Projeteur'
			)
		));

		$this->add(array(
			'name' => 'protectionCode',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Protection Code',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Protection Code'
			)
		));

		$this->add(array(
			'name' => 'beCode',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Be Code',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'BE Code'
			)
		));

		$this->add(array(
			'name' => 'functionalClass',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Functional Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Functional Class'
			)
		));

		$this->add(array(
			'name' => 'drawingType',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Drawing Type',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Drawing Type'
			)
		));

		$this->add(array(
			'name' => 'configItem',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Config Item',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'CI'
			)
		));

		$this->add(array(
			'name' => 'layout',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'requiered' => false,
				'type' => 'text',
				'placeholder' => 'Layout',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Layout'
			)
		));

		$this->add(array(
			'name' => 'mod',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Mod/Pm',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Mod/Pm'
			)
		));

		$this->add(array(
			'name' => 'team',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Team/Data Path',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Team/Data Path'
			)
		));

		$this->add(array(
			'name' => 'revision',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Revision',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Revision'
			)
		));

		$this->add(array(
			'name' => 'gildaStatus',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'disabled'=>1,
				'requiered' => false,
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Status Gilda'
			)
		));

		$this->add(array(
			'name' => 'officialDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'disabled'=>1,
				'requiered' => false,
				'type' => 'date',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Official Since',
				'format' => 'd-m-Y'
			)
		));

		$this->add(array(
			'name' => 'compilerDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'disabled'=>1,
				'requiered' => false,
				'type' => 'date',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Compiler Signature',
				'format' => 'd-m-Y'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			),
			'description' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 100
						)
					)
				)
			),
			'officialDate' => array(
				'required' => false,
				'filters' => array(
				),
				'validators' => array()
			),
			'compilerDate' => array(
				'required' => false,
				'filters' => array(
				),
				'validators' => array()
			)
		);
	}

	/**
	 */
	protected function _getTypeOptions()
	{
		$ret = array();
		return $ret;
	}

	/**
	 *
	 * @param ViewModel $view
	 */
	public function render(ViewModel $view)
	{
		$view->form = $this;
		$view->setTemplate($this->template);
		return $view;
	}
}
