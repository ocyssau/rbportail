<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 * @author ocyssau
 *
 */
class WorkunitCreateForm extends Form implements InputFilterProviderInterface
{

	public function __construct($tdim = '2d')
	{
		parent::__construct('workunitCreate');
		$this->template = 'wumanager/workunit/createform/workunitcreateform.phtml';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());
		
		$this->add(array(
			'type' => '\Wumanager\Form\Workunit' . $tdim . 'Fieldset',
			'options' => array(
				'use_as_base_fieldset' => true
			)
		));

		$this->add(array(
			'name' => 'typeDimension',
			'attributes' => array(
				'type' => 'hidden',
				'value' => '2d'
			)
		));

		$this->add(array(
			'name' => 'page',
			'attributes' => array(
				'type' => 'hidden',
				'value' => 2
			)
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'type' => array(
				'required' => false
			),
			'typeDimension' => array(
				'required' => false
			),
			'id' => array(
				'required' => false
			),
			'page' => array(
				'required' => false
			)
		);
	}
}
