<?php
namespace Wumanager\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Wumanager\Model\Site;
use Wumanager\Model\Workunit;
use Application\Dao\Factory as DaoFactory;

/**
 * 
 *
 */
class Workunit2dFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 * 
	 * @var string
	 */
	public $mode = 'create';
	
	/**
	 *
	 * @var string
	 */
	protected $type = '2d';

	/**
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('workunit');
		$this->setHydrator(new Hydrator(false));
		
		$this->template = 'wumanager/workunit/createform/workunitcreateform.phtml';
		$this->comments = $this->_getCommentsOptions();
		
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'deliverables',
			'options' => array(
				'label' => 'Please choose deliverable for this wu',
				'count' => 0,
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Wumanager\Form\Deliverable' . $this->type . 'Fieldset'
				)
			)
		));
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'typeId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'tos',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text'
			),
			'options' => array(
				'label' => 'Tos'
			)
		));
		
		$this->add(array(
			'name' => 'version',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'step' => '1',
				'min' => '0',
				'placeholder' => 'Issue',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Issue'
			)
		));
		
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'step' => '1',
				'min' => '0',
				'placeholder' => 'Number',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		
		$this->add(array(
			'name' => 'flt',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text'
			),
			'options' => array(
				'label' => 'Flt'
			)
		));
		
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Name'
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		
		$this->add(array(
			'name' => 'title',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Description'
			),
			'options' => array(
				'label' => 'Description'
			)
		));
		
		$this->add(array(
			'name' => 'siteUid',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select'
			),
			'options' => array(
				'label' => 'Site',
				'value_options' => $this->_getSiteOptions(),
				'empty_option' => 'Please, select a site'
			)
		));
		
		$this->add(array(
			'name' => 'processId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select'
			),
			'options' => array(
				'label' => 'Process',
				'value_options' => $this->_getProcessOptions(),
				'empty_option' => 'Please, select a process'
			)
		));
		
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => 'Comments'
			),
			'options' => array(
				'label' => 'Comments'
			)
		));
		
		$this->add(array(
			'name' => 'visibility',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select'
			),
			'options' => array(
				'label' => 'Visibility',
				'value_options' => array(
					'internal' => 'internal',
					'public' => 'public'
				),
				'empty_option' => 'Please, select a diffusion level'
			)
		));
		
		$this->add(array(
			'name' => 'openDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Open Date'
			),
			'options' => array(
				'label' => 'Open Date',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'planDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Plan Date'
			),
			'options' => array(
				'label' => 'Plan Date',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'addDeliverable',
			'type' => 'Zend\Form\Element\Button',
			'attributes' => array(
				'type' => 'button',
				'class' => 'btn btn-success add-btn add-2ddeliverable-btn'
			),
			'options' => array(
				'label' => 'Add Deliverable'
			)
		));
		
		$this->add(array(
			'name' => 'sitedeliveryDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Production Site Delivery Date'
			),
			'options' => array(
				'label' => 'Production Site Delivery Date',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'supplierBillingDate',
			'type' => 'Zend\Form\Element\Date',
			'attributes' => array(
				'type' => 'date',
				'placeholder' => 'Supplier Billing Date'
			),
			'options' => array(
				'label' => 'Supplier Billing',
				'format' => 'd-m-Y'
			)
		));
		
		$this->add(array(
			'name' => 'ataLeader',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Ata Leader Name'
			),
			'options' => array(
				'label' => 'Ata Leader'
			)
		));
		
		$this->add(array(
			'name' => 'quantity',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'number',
				'step' => '1',
				'min' => '0',
				'placeholder' => 'Quantity',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Quantity'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			),
			'name' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'type' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'tos' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'siteUid' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'processId' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'comment' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'description' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'openDate' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'planDate' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'sitedeliveryDate' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'supplierBillingDate' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'visibility' => array(
				'required' => true,
				'filters' => array(),
				'validators' => array()
			),
			'ataLeader' => array(
				'required' => false,
				'filters' => array(),
				'validators' => array()
			),
			'quantity' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 */
	protected function _getTosOptions()
	{
		$ret = array(
			'tos1',
			'tos2',
			'tos3'
		);
		return $ret;
	}

	/**
	 *
	 */
	protected function _getSiteOptions()
	{
		$ret = array();
		$list = DaoFactory::get()->getList(Site::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['uid']] = $item['name'];
		}
		return $ret;
	}

	/**
	 *
	 */
	protected function _getTypeOptions()
	{
		$ret = array();
		$list = DaoFactory::get()->getList(Workunit\Type::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['id']] = $item['uid'] . '-' . $item['name'];
		}
		return $ret;
	}

	/**
	 *
	 */
	protected function _getProcessOptions()
	{
		$ret = array();
		$list = DaoFactory::get()->getList(\Workflow\Model\Wf\Process::$classId);
		foreach( $list->load() as $item ) {
			$ret[$item['id']] = $item['uid'] . '-' . $item['name'];
		}
		return $ret;
	}

	/**
	 *
	 */
	protected function _getCommentsOptions()
	{
		$ret = array();
		return $ret;
	}
}
