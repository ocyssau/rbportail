<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 * 
 * @author ocyssau
 *
 */
class DeliveryForm extends Form implements InputFilterProviderInterface
{

	/**
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('delivery');
		$this->setHydrator(new Hydrator(false));

		/**/
		$this->template = 'wumanager/workunit/deliveryform.phtml';

		/** */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		
		/** */
		$this->add(array(
			'name' => 'deliveryMemo',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Delivery Note',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Delivery note',
			),
		));

		/** */
		$this->add(array(
			'name' => 'deliveryDate',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Delivery Date',
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => 'Delivery Date',
				'format' => 'd-m-Y',
			),
		));
		
		/** */
		$this->add(array(
			'name' => 'lastDeliveryDate',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Last Delivery Date',
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => 'Last Delivery Date',
				'format' => 'd-m-Y',
			),
		));

		/** */
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));

	}

	/**
	 * {@inheritDoc}
	 *
	 * @param bool $onlyBase
	 */
	public function populateValues($data, $onlyBase = false)
	{
		return parent::populateValues($data, $onlyBase);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
		    'id' => array(
		        'required' => false,
		    	'filters'  => array(
		    	    array('name' => 'Int'),
		    	),
		    ),
			'deliveryMemo' => array(
			    'required' => true,
			    'filters'  => array(
			        array('name' => 'StripTags'),
			        array('name' => 'StringTrim'),
			    ),
			    'validators' => array(
			    ),
			),
			'deliveryDate' => array(
			    'required' => true,
			    'filters'  => array(
			    ),
			    'validators' => array(
			    ),
			),
			'lastDeliveryDate' => array(
				'required' => true,
				'filters'  => array(
				),
				'validators' => array(
				),
			),
		);
	}
}
