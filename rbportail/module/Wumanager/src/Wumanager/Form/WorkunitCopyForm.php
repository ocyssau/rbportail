<?php
namespace Wumanager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class WorkunitCopyForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 * @param string $tdim
	 */
	public function __construct()
	{
		parent::__construct('workunitCopy');
		$this->template = 'wumanager/workunit/copyform.phtml';

		/* */
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* ID */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* NAME */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Name'
			),
			'options' => array(
				'label' => 'Name'
			)
		));

		/* TITLE */
		$this->add(array(
			'name' => 'title',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Description'
			),
			'options' => array(
				'label' => 'Description'
			)
		));

		/* QTY */
		$this->add(array(
			'name' => 'quantity',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'number',
				'step' => '1',
				'min' => '0',
				'placeholder' => 'Quantity',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Quantity'
			)
		));

		/* SUBMIT */
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Copy',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			),
			'name' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'title' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'quantity' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					)
				)
			)
		);
	}
}
