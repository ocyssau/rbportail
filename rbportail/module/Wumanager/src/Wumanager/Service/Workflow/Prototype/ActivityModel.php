<?php

namespace Wumanager\Service\Workflow\Prototype;

use Wumanager\Service\Workflow\Prototype\ActivityTrigger;

/**
 * Model class to trigger the activity
 * @author olivier
 *
 */
class ActivityModel extends ActivityTrigger
{
	public function trigger(){
		parent::trigger();
	}
}
