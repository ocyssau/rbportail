<?php
// %LICENCE_HEADER%
namespace Wumanager\Service\Workflow;

use Workflow\Model\Wf;
use Application\Model\Filesystem;
use Application\Dao\Factory as DaoFactory;
use Exception;
use Application\Model\Error;

/**
 * 
 *
 */
class Code
{

	/**
	 * Base path where to create folders for store acitivities scripts
	 *
	 * @var String
	 */
	public static $processPath = '';

	/**
	 * Base namespace for acitivities scripts
	 *
	 * @var string
	 */
	public static $processNamespace = 'ProcessDef';

	/**
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 *
	 * @var string
	 */
	protected $templateBasePath;

	/**
	 *
	 * @var Wf\Activity
	 */
	protected $activity;

	/**
	 *
	 * @var Wf\Process
	 */
	protected $process;

	/**
	 *
	 * @var string
	 */
	protected $modelClassName;

	/**
	 *
	 * @var string
	 */
	protected $modelClassFile;

	/**
	 *
	 * @var string
	 */
	protected $formClassName;

	/**
	 *
	 * @var string
	 */
	protected $formClassFile;

	/**
	 *
	 * @var string
	 */
	protected $templateFile;

	/**
	 *
	 * @var array
	 */
	protected $folders;

	/**
	 */
	public function __construct(Wf\Process $process)
	{
		$this->process = $process;
		$this->basePath = self::getBasePath($process);
		$this->templateBasePath = self::getTemplatePath($process);
		$this->graphBasePath = self::getGraphPath($process);

		/* ordered array: graph must be after view */
		$this->folders = array(
			'form' => $this->basePath . '',
			'model' => $this->basePath . '',
			'view' => $this->templateBasePath,
			'graph' => $this->graphBasePath
		);
	}

	/**
	 * Trigger
	 * Init and create folders.
	 *
	 * @param \Rbplm\Event $process
	 * @param Wf\Process $process
	 */
	public static function onSaveProcessPre($event, $worflow)
	{
		try {
			$process = $event->sender->process;
			$code = new self($process);
			$code->init();
		}
		catch( \Exception $e ) {
			$event->error = $e->getMessage();
		}
	}
	
	/**
	 * Trigger
	 * Save graph image
	 *
	 * @param \Rbplm\Event $process
	 * @param Wf\Process $process
	 */
	public static function onSaveProcessPost($event, $worflow)
	{
		try {
			$process = $event->sender->process;
			$code = new self($process);
			$code->saveGraph();
		}
		catch( \Exception $e ) {
			$event->error = $e->getMessage();
		}
	}

	/**
	 * Trigger
	 *
	 * @param Wf\Process $process
	 */
	public static function onDeleteProcess($event, $worflow)
	{
		$process = $event->sender->process;
		$code = new self($process);
		$code->delete();
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getBasePath(Wf\Process $process)
	{
		$processFolderName = 'PROC' . $process->getNormalizedName();
		$processPath = self::$processPath; /* where process definition is store */
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/' . $processFolderName;
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getTemplatePath(Wf\Process $process)
	{
		$processFolderName = 'PROC' . $process->getNormalizedName();
		$processPath = self::$processPath; // where process definition is store
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/view/' . $processFolderName;
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getGraphPath(Wf\Process $process)
	{
		$processFolderName = $process->getNormalizedName();

		/* put img in public folder */
		/* anti collision name */
		$publicImg = realpath('public/img/Graph568ce3c4d4590');
		if ( $publicImg == "" ) {
			throw new Exception('public/img/Graph568ce3c4d4590 is not existing :', Error::WARNING);
		}

		return $publicImg . '/' . $processFolderName;
	}

	/**
	 * Create directories to store the classes of the activities triggers.
	 */
	public function init()
	{
		if ( self::$processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		
		foreach( $this->folders as $path ) {
			if ( is_dir($path) ) {
				continue;
			}
			if ( !@mkdir($path, 0777, true) ) {
				throw new Exception('DIRECTORY_CREATION_FAILED :' . $path, Error::WARNING);
			}
		}
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string $type
	 * @return array | string
	 */
	public function delete()
	{
		foreach( $this->folders as $folder ) {
			/* Prevent a disaster */
			if ( trim($folder) == '/' || trim($folder) == '.' || trim($folder) == 'templates' || trim($folder) == 'templates/' ) {
				return false;
			}
			Filesystem\Filesystem::removeDirectory($folder, true);
		}
		return true;
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string $type
	 * @return array | string
	 */
	public function saveGraph()
	{
		$process = $this->process;
		$processId = $process->getId();
		$dao = DaoFactory::get()->getDao($process);
		$graphViz = new \Wumanager\Service\Workflow\GraphViz();

		/* Set name of graph */
		$graphViz->setPid($process->getNormalizedName());

		$interactiveFontColor = 'pink';
		$notinteractiveFontColor = 'black';
		$automaticColor = 'blue';
		$notautomaticColor = 'purple';
		$nodebgColor = 'white';
		$nodeEdgesColor = 'blue';

		$dao = DaoFactory::get()->getDao(Wf\Process::$classId);
		$filter = 'act.processId=:processId';
		$bind = array(
			':processId' => $processId
		);
		$list = $dao->getGraphWithTransitions($filter, $bind);

		foreach( $list as $activity ) {
			if ( $activity['isInteractive'] ) {
				$fontcolor = $interactiveFontColor;
			}
			else {
				$fontcolor = $notinteractiveFontColor;
			}

			$nodeId = $activity['uid'];
			$label = $activity['name'];
			$label = $activity['title'];
			$shape = Shape::getShapeFromType($activity['type']);
			$url = '#';
			$graphViz->addNode($nodeId, array(
				'URL' => $url,
				'label' => $label,
				'shape' => $shape,
				'fontcolor' => $fontcolor,
				'style' => 'filled',
				'fillcolor' => $nodebgColor,
				'color' => $nodeEdgesColor
			));

			$nodeParentId = $activity['parentUid'];
			if ( $nodeParentId ) {
				if ( $activity['isAutomatic'] ) {
					$transitionColor = $automaticColor;
				}
				else {
					$transitionColor = $notautomaticColor;
				}

				$graphViz->addEdge(array(
					$nodeParentId => $nodeId
				), array(
					'color' => $transitionColor,
					'taillabel' => $activity['status'],
					'decorate' => 1,
					'arrowhead' => 'open',
					'arrowtail' => 'none',
					'dir' => 'both'
				));
			}
		}

		$toDirectory = $this->folders['graph'];
		$graphViz->imageAndMap($toDirectory, $graphViz::$imageType);

		return true;
	}
}
