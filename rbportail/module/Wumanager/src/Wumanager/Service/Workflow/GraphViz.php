<?php 
//%LICENCE_HEADER%

namespace Wumanager\Service\Workflow;

class GraphViz extends \Workflow\Service\GraphViz
{
	public static $imageType = 'png';
	
	/**
	 */
	public function __construct()
	{
		$attributes = array(
			'bgcolor'=>'transparent',
		);
		
		parent::__construct($attributes);
	}
}
