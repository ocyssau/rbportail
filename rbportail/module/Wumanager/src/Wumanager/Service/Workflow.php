<?php
namespace Wumanager\Service;

use Wumanager\Model as Wumodel;
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;
use Workflow\Model\Wf;
use Application\Model\Signal;

/**
 *
 * @author olivier
 *
 */
class Workflow extends \Workflow\Service\Workflow
{

	/**
	 *
	 * @var \Zend\ServiceManager\ServiceManager
	 */
	public $serviceManager;

	/**
	 *
	 * @var Wumodel\Workunit
	 */
	public $workunit;

	/**
	 *
	 * @param \Application\Controller\AbstractController $controller
	 * @return Workflow
	 */
	public function connect()
	{
		/* connect events listner */
		Signal::connect($this, self::SIGNAL_RUNACTIVITY_PRE, array(
			$this,
			'onRunActivity'
		));
		Signal::connect($this, self::SIGNAL_TRANSLATE_POST, array(
			$this,
			'onTranslateActivity'
		));
		Signal::connect($this, self::SIGNAL_SAVEPROCESS_PRE, array(
			'Wumanager\Service\Workflow\Code',
			'onSaveProcessPre'
		));
		Signal::connect($this, self::SIGNAL_SAVEPROCESS_POST, array(
			'Wumanager\Service\Workflow\Code',
			'onSaveProcessPost'
		));
		Signal::connect($this, self::SIGNAL_DELETEPROCESS_POST, array(
			'Wumanager\Service\Workflow\Code',
			'onDeleteProcess'
		));
		Signal::connect($this, self::SIGNAL_SAVEACTIVITY_POST, array(
			'Wumanager\Service\Workflow\Activity\Code',
			'onSaveActivity'
		));
		Signal::connect($this, self::SIGNAL_DELETEACTIVITY_POST, array(
			'Wumanager\Service\Workflow\Activity\Code',
			'onDeleteActivity'
		));
		return $this;
	}
	
	/**
	 * To clear all internal state properties
	 * @return Workflow
	 */
	public function init()
	{
		$this->workunit = null;
		$this->lastActivity = null;
		$this->startInstance = null;
		$this->instance = null;
		$this->process = null;
		$this->nextTransitions = [];
		$this->nextStatus = null;
		$this->nextActivity = null;
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Event $e
	 * @param Workflow $workflow
	 */
	public function onRunActivity($e)
	{
		$actInst = $this->lastActivity;
		$factory = DaoFactory::get();

		$instanceOwner = $this->instance->getOwner(true);
		$currentUser = People\CurrentUser::get();
		$currentUserLogin = $currentUser->getLogin();
		if ( $currentUserLogin != $instanceOwner ) {
			/* check role */
			$roles = $actInst->getRoles();
			if ( $roles ) {
				$acl = $currentUser->wumacl;
				$ok = false;
				foreach( $roles as $role ) {
					if ( $currentUserLogin == $role ) {
						$ok = true;
					}
					if ( $acl->getAcl()->hasRole($role) ) {
						if ( $acl->getAcl()->inheritsRole($currentUserLogin, $role) ) {
							$ok = true;
						}
					}
				}

				if ( $ok == false ) {
					$e->error = 'Current user is not authorized to execute this activity';
					return $e;
				}
			}
		}

		$attr = $actInst->actAttributes;
		$callbackclass = $attr['callbackclass'];
		$callbackmethod = $attr['callbackmethod'];

		if ( $callbackclass == '' ) {
			$code = new \Wumanager\Service\Workflow\Activity\Code($this->instance, $this->lastActivity);
			$callbackclass = $code->getModelClass();
			$callbackmethod = 'trigger';
			$formName = $code->getFormClass();
			$template = $code->getTemplate();
		}

		/* get associated wu */
		$linkStmt = $factory->getDao(\Wumanager\Model\Workunit\LinkProcess::$classId)->getParent($this->instance->getId());
		$workunitId = (int)$linkStmt->fetchColumn(1);

		if(!isset($this->workunit)){
			$workunit = new \Wumanager\Model\Workunit();
			$workunit->dao = $factory->getDao($workunit);
			$workunit->dao->loadFromId($workunit, $workunitId);
			$this->workunit = $workunit;
		}
		else{
			$workunit = $this->workunit;
		}

		/* Set workunit progression */
		$progression = $actInst->getProgression();
		$workunit->setProgression($progression);

		if ( class_exists($callbackclass, true) ) {
			$model = new $callbackclass($this);
			$model->workunit = $workunit;
			$model->workunitDao = $workunit->dao;

			if ( $actInst->isInteractive() ) {
				if ( class_exists($formName, true) ) {
					$form = new $formName($this);
					return $model->runForm($form, $template);
				}
				else{
					throw new \Exception("Form class $formName NOT FOUND!");
				}
			}
			else {
				return $model->$callbackmethod();
			}
		}
		else{
			throw new \Exception("Callback class $callbackclass NOT FOUND!");
		}
	}

	/**
	 *
	 * @param \Rbplm\Event $e
	 */
	public function onTranslateActivity($e)
	{
		if ( isset($this->nextStatus) ) {
			$this->workunit->setStatus($this->nextStatus);
			$this->workunit->dao->updateFromArray($this->workunit->getId(), array(
				'status' => $this->nextStatus,
				'progression' => $this->workunit->getProgression()
			));
		}
	}

	/**
	 *
	 * @param integer $processId
	 * @param integer $activityId
	 * @return Workflow\Activity\Code
	 * @throw Exception
	 */
	public function getCode($processId, $activityId)
	{
		$factory = DaoFactory::get();

		/* Load the process definition */
		if(!$this->process){
			$procDao = $factory->getDao(Wf\Process::$classId);
			$this->process = new Wf\Process();
			$procDao->loadFromId($this->process, $processId);
		}

		/* Load the activity definition */
		if(!$this->activity){
			$actDao = $factory->getDao(Wf\Activity::$classId);
			$this->activity = new Wf\Activity();
			$actDao->loadFromId($this->activity, $activityId);
		}

		$code = new Workflow\Activity\Code($this->process, $this->activity);
		return $code;
	}

	/**
	 * 
	 * @param int $activityId Instance Activity Id
	 * @return Workflow
	 */
	public function deleteInstanceActivity($activityId)
	{
		$factory = DaoFactory::get();

		$stmt = $factory->getDao(Wf\Instance\Activity::$classId)->getPrevious($activityId);
		$res0 = $stmt->fetch(\PDO::FETCH_ASSOC);
		$previousStatus = $res0['previousTransitionName'];
		$processInstanceId = $res0['instanceId'];

		/* get workunit attached to processInstance */
		$linkStmt = $factory->getDao(\Wumanager\Model\Workunit\LinkProcess::$classId)->getParent($processInstanceId);
		$workunitId = (int)$linkStmt->fetchColumn(1);

		/* delete activity instance */
		$factory->getDao(Wf\Instance\Activity::$classId)->deleteFromId($activityId);

		/* Update status on workunit */
		$wuDao = $factory->getDao(Wumodel\Workunit::$classId);
		$wuDao->setFromId($workunitId, $wuDao->toSys('status'), $previousStatus);
		return $this;
	}
}
