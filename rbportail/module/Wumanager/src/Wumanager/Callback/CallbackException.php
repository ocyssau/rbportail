<?php
namespace Application\Callback;

/**
 * Exception for callback.
 * Throw when a callback failed.
 * 
 */
class CallbackException extends \Exception
{
}
