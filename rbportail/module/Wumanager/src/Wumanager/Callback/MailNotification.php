<?php
namespace Wumanager\Callback;

use Zend\EventManager\EventInterface;
use Application\Model\People;
use Wumanager\Model\Notification\Notification;
use Wumanager\Controller\WorkunitController;
use Application\Service\Mail as MailService;

/**
 * 
 *
 */
class MailNotification extends AbstractCallback
{

	/**
	 * 
	 */
	protected $factory;

	/**
	 * @param EventInterface $e
	 * @return MailNotification
	 */
	public function editPost(EventInterface $e)
	{
		$event = $e->getName();
		$workunit = $e->getTarget();
		$from = People\CurrentUser::get();

		/* Notify only if changed */
		if ( !isset($workunit->hashBefore) ) {
			throw new \Exception('hashBefore must be setted to Workunit before call onEdit event');
		}

		$hashBefore = $workunit->hashBefore;
		$hashAfter = \Wumanager\Model\Workunit\Hasher::hash($workunit);

		if ( $hashBefore == $hashAfter ) {
			return;
		}

		/* @var \PDOStatement $receipts */
		$receipts = $this->getFactory()
			->getDao(Notification::$classId)
			->getReceipts($event, $workunit->getUid());

		if ( $receipts->rowCount() == 0 ) {
			return;
		}
		else {
			$receipts = $receipts->fetchAll(\PDO::FETCH_ASSOC);
		}

		$subject = sprintf('Workunit %s has been edited by %s', $workunit->getName(), $from->getLogin());

		$htmlBody = '<p>' . $subject . '</p>';
		//$htmlBody .= '<p><a class="rb-popup btn btn-default" href="' . $wuDetailUrl . '">Get Details of document' . $workunit->getName() . '</a></p>';

		$this->send($receipts, $from, $subject, $htmlBody);
		return $this;
	}

	/**
	 * @param EventInterface $e
	 * @return MailNotification
	 */
	public function onEvent(EventInterface $e)
	{
		$event = $e->getName();
		$workunit = $e->getTarget();
		$from = People\CurrentUser::get();

		/* @var \PDOStatement $receipts */
		$receipts = $this->getFactory()
			->getDao(Notification::$classId)
			->getReceipts($event, $workunit->getUid());

		if ( $receipts->rowCount() == 0 ) {
			return;
		}
		else {
			$receipts = $receipts->fetchAll(\PDO::FETCH_ASSOC);
		}

		switch ($event) {
			case (WorkunitController::SIGNAL_POST_CREATE):
			case (WorkunitController::SIGNAL_POST_COPY):
				$subject = sprintf('Workunit %s has been created by %s', $workunit->getName(), $from->getLogin());
				$htmlBody = '<p>' . $subject . '</p>';
				break;
			case (WorkunitController::SIGNAL_POST_DELIVERY):
				$subject = sprintf('Workunit %s has been delivred by %s', $workunit->getName(), $from->getLogin());
				$htmlBody = '<p>' . $subject . '</p>';
				break;
			case (WorkunitController::SIGNAL_POST_DELETE):
				$subject = sprintf('Workunit %s has been deleted by %s', $workunit->getName(), $from->getLogin());
				$htmlBody = '<p>' . $subject . '</p>';
				break;
			default:
				break;
		}

		$this->send($receipts, $from, $subject, $htmlBody);
		return $this;
	}

	/**
	 * @return \Application\Dao\Factory
	 */
	protected function getFactory()
	{
		if ( !$this->factory ) {
			//throw new \Exception('$this->factory is not set');
			/** @var \Application\Dao\Factory $factory */
			$this->factory = \Application\Dao\Factory::get();
		}
		return $this->factory;
	}

	/**
	 * 
	 * @param array $sendTo
	 * @param People\User $fromUser
	 * @param string $subject
	 * @param string $body
	 * @return MailNotification
	 */
	protected function send($sendTo, $fromUser, $subject, $body)
	{
		$mailService = MailService::get();
		$toMail = null;
		
		$from = [];
		$from[0] = $fromUser->getLogin();
		$from[1] = $fromUser->getMail();
		if ( !$from[1] ) {
			$from[1] = $mailService->adminMail;
		}
		if ( !$from[0] ) {
			$from[0] = 'wumanager';
		}

		$message = MailService::messageFactory();
		$htmlPart = MailService::htmlPartFactory($body);
		$body = MailService::bodyFactory();
		$body->setParts(array(
			$htmlPart
		));
		$message->setBody($body);

		$message->addFrom($from[1], $from[0])
			->addReplyTo($from[1])
			->setSubject($subject);

		foreach( $sendTo as $contact ) {
			$toMail = $contact['mail'];
			$message->addTo($toMail, $toMail);
		}

		if ( $toMail ) {
			$mailService->send($message);
		}
		return $this;
	}
}
