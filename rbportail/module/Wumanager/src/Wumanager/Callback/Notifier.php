<?php
namespace Wumanager\Callback;

use Zend\EventManager\EventInterface;

/**
 * 
 *
 */
class Notifier extends AbstractCallback
{

	/**
	 * @param EventInterface $e
	 */
	public function editPost(EventInterface $e)
	{
		$event = $e->getName();
		$params = $e->getParams();
		$workunit = $e->getTarget();
		
		/* Notify only if changed */
		if ( !isset($workunit->hashBefore) ) {
			throw new \Exception('hashBefore must be setted to Workunit before call onEdit event');
		}
		
		$hashBefore = $workunit->hashBefore;
		$hashAfter = \Wumanager\Model\Workunit\Hasher::hash($workunit);
		
		if ( $hashBefore == $hashAfter ) {
			return;
		}
		
		/** @var \Application\Dao\Factory $factory */
		$factory = \Application\Dao\Factory::get();
		$markerDao = $factory->getDao(\Wumanager\Model\Workunit\Marker::$classId);
		$linkProcessDao = $factory->getDao(\Wumanager\Model\Workunit\LinkProcess::$classId);
		$markerDao = $factory->getDao(\Wumanager\Model\Workunit\Marker::$classId);
		
		/**/
		try {
			/* Get users involved in workflow */
			$workunitId = $workunit->getId();
			$stmt = $linkProcessDao->getActivityInstanceFromWorkunitId($workunitId, '1=1 GROUP BY instAct.ownerId');
			foreach( $stmt as $entry ) {
				$ownerId = $entry['ownerId'];
				if ( $ownerId ) {
					$marker = \Wumanager\Model\Workunit\Marker::init();
					$marker->open($workunitId, $ownerId, 'Edited');
					$markerDao->save($marker);
				}
			}
		}
		catch( \PDOException $e ) {
			if ( strpos($e->getMessage(), 'Integrity constraint violation') !== false ) {
				null;
			}
			else {
				throw $e;
			}
		}
	}
}
