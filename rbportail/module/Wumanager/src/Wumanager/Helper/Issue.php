<?php
//%LICENCE_HEADER%

namespace Wumanager\Helper;

class Issue
{
	/**
	 * @return string
	 */
	public static function extractFromName($name)
	{
		$a = explode('-', $name);
		return (int)$a[2];
	}
}
