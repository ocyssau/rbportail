<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao;

/** SQL_SCRIPT>>
CREATE TABLE bill(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`emitDate` datetime NULL,
	`orderId` varchar(255) NULL,
	`orderDate` datetime NULL,
	`amount` varchar(255) NULL,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE bill ADD UNIQUE (uid);

ALTER TABLE `bill` ADD INDEX `BILL_UID` (`uid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_NAME` (`name` ASC);

ALTER TABLE `bill` ADD INDEX `BILL_PARENTID` (`parentId` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_PARENTUID` (`parentUid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_CLASSID` (`cid` ASC);
ALTER TABLE `bill` ADD INDEX `BILL_OWNER` (`ownerId` ASC);


ALTER TABLE bill ADD `orderDate` datetime NULL after orderId;

<<*/

/** SQL_FKEY>>
<<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
<<*/

/** SQL_DROP>>
DROP TABLE bill;
<<*/


class Bill extends Any
{
	/**
	 * @var string
	 */
	public static $table = 'bill';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'emitDate'=>'emitDate',
		'orderId'=>'orderId',
		'orderDate'=>'orderDate',
		'amount'=>'amount',
	);

	public static $sysToAppFilter = array(
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':orderId'=>$mapped->getOrder(),
			':amount'=>$mapped->getAmount(),
			//DATES
			':emitDate'=>self::dateToSys($mapped->getEmitdate(self::DATE_FORMAT)),
			':orderDate'=>self::dateToSys($mapped->getOrderdate(self::DATE_FORMAT)),
		));
	}
}
