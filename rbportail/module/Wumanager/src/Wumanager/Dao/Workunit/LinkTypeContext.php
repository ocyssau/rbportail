<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao\Workunit;

use Application\Dao\Link;

/** SQL_ALTER>>
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_PARENTID` (`parentId` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_CHILDID` (`childId` ASC);
 ALTER TABLE `link_workunit_type_context` ADD INDEX `WORKUNIT_TYPE_CONTEXT_CHILDUID` (`childUid` ASC);
 ALTER TABLE `link_workunit_type_context` ADD UNIQUE INDEX `WORKUNIT_TYPE_CONTEXT_PARENTID_CHILDID` (`parentId` ASC, `childId` ASC);
 <<*/

/** SQL_SCRIPT>>
 CREATE TABLE link_workunit_type_context(
 `id` int NOT NULL AUTO_INCREMENT,
 `parentId` int NOT NULL,
 `parentUid` VARCHAR(255) NOT NULL,
 `childId` int NOT NULL,
 `childUid` VARCHAR(255) NOT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO link_workunit_type_context (parentId, parentUid, childId, childUid)
 VALUES
 (1, '3D01', 1, 'a350xwbs1112'),
 (2, '3D02', 2, 'a350xwbothersection'),
 (3, '3D03', 3, 'a350xwbhydraulic'),
 (4, '3D04', 1, 'a350xwbs1112'),
 (5, '3D05', 2, 'a350xwbothersection'),
 (6, '3D06', 3, 'a350xwbhydraulic'),
 (7, '3D07', 1, 'a350xwbs1112'),
 (7, '3D07', 2, 'a350xwbothersection'),
 (8, '3D08', 3, 'a350xwbhydraulic'),
 (9, '3D09', 1, 'a350xwbs1112'),
 (9, '3D09', 2, 'a350xwbothersection'),
 (10, '3D10', 3, 'a350xwbhydraulic'),
 (11, '2D01', 1, 'a350xwbs1112'),
 (11, '2D01', 2, 'a350xwbothersection'),
 (11, '2D01', 3, 'a350xwbhydraulic'),
 (12, '2D02', 1, 'a350xwbs1112'),
 (12, '2D02', 2, 'a350xwbothersection'),
 (12, '2D02', 3, 'a350xwbhydraulic'),
 (13, '2D03', 1, 'a350xwbs1112'),
 (13, '2D03', 2, 'a350xwbothersection'),
 (14, '2D04', 3, 'a350xwbhydraulic'),
 (15, '2D05', 3, 'a350xwbhydraulic'),
 (16, '2D06', 1, 'a350xwbs1112'),
 (16, '2D06', 2, 'a350xwbothersection'),
 (16, '2D06', 3, 'a350xwbhydraulic'),
 (17, '2D07', 1, 'a350xwbs1112'),
 (17, '2D07', 2, 'a350xwbothersection'),
 (18, '2D08', 3, 'a350xwbhydraulic'),
 (19, '2D09', 5, 'a320neo'),
 (20, '2D10', 5, 'a320neo'),
 (21, '2D11', 5, 'a320neo'),
 (22, '2D12', 5, 'a320neo'),
 (23, 'E0', 6 , 'legacy'),
 (24, 'E1-1', 6 , 'legacy'),
 (25, 'E1-2', 6 , 'legacy'),
 (26, 'E1-3', 6 , 'legacy'),
 (27, 'E2', 6 , 'legacy'),
 (28, 'L1', 6 , 'legacy'),
 (29, 'L2', 6 , 'legacy'),
 (30, 'L3', 6 , 'legacy'),
 (31, 'L4', 6 , 'legacy');
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/
class LinkTypeContext extends Link
{

	/**
	 * @var string
	 */
	public static $table = 'link_workunit_type_context';

	/**
	 * @var string
	 */
	public static $parentTable = 'workunit_type';

	/**
	 * @var string
	 */
	public static $childTable = 'workunit_context';

	/**
	 * @var string
	 */
	public static $parentForeignKey = 'id';

	/**
	 * @var string
	 */
	public static $childForeignKey = 'id';

	/**
	 * $mapped \Wumanager\Model\Workunit\LinkTypeContext
	 */
	public function insert($mapped, array $select = null)
	{
		$table = static::$table;
		
		if ( !$this->insertStmt ) {
			$sql = 'INSERT INTO ' . $table . ' (parentId, childId, parentUid, childUid)';
			$sql .= ' VALUES (:parentId, :childId, :parentUid, :childUid)';
			$this->insertStmt = $this->connexion->prepare($sql);
		}
		
		try {
			$this->connexion->beginTransaction();
			if ( is_array($mapped) ) {
				foreach( $mapped as $map ) {
					$this->insertStmt->execute($this->bind($map));
				}
			}
			else {
				$this->insertStmt->execute($this->bind($mapped));
			}
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * @param \Wumanager\Model\Workunit\LinkTypeContext $mapped
	 * @param array $select
	 * @throws \Exception
	 * @return Link
	 */
	public function update($mapped, array $select = null)
	{
		$table = static::$table;
		
		if ( !$this->updateStmt ) {
			$sql = 'UPDATE ' . $table . ' SET
			parentId=:parentId,
			childId=:childId,
			parentUid=:parentUid,
			childUid=:childUid,
			WHERE id=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}
		
		try {
			$this->connexion->beginTransaction();
			if ( is_array($mapped) ) {
				foreach( $mapped as $map ) {
					$bind = $this->bind($mapped);
					$bind[':id'] = $mapped->getId();
					$this->updateStmt->execute($bind);
				}
			}
			else {
				$bind = $this->bind($mapped);
				$bind[':id'] = $mapped->getId();
				$this->updateStmt->execute($bind);
			}
			$this->connexion->commit();
		}
		catch( \Exception $e ) {
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 */
	public function bind($mapped)
	{
		return array(
			':parentId' => (int)$mapped->parentId,
			':childId' => (int)$mapped->childId,
			':parentUid' => $mapped->parentUid,
			':childUid' => $mapped->childUid
		);
	}
}
