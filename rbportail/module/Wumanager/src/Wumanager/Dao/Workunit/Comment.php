<?php
//%LICENCE_HEADER%
//%LICENCE_HEADER%

namespace Wumanager\Dao\Workunit;

use Wumanager\Dao\Any;

/** SQL_SCRIPT>>
 CREATE TABLE workunit_comment(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
 `name` VARCHAR(255) NULL DEFAULT NULL,
 `ownerId` VARCHAR(255) NOT NULL,
 `parentId` int NOT NULL,
 `parentUid` varchar(255) NULL,
 `updated` datetime NOT NULL,
 `body` TEXT NOT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_ALTER>>
 ALTER TABLE workunit_comment ADD UNIQUE (uid);
 ALTER TABLE `workunit_comment` ADD INDEX `WORKUNIT_COMMENT_UID` (`uid` ASC);
 ALTER TABLE `workunit_comment` ADD INDEX `WORKUNIT_COMMENT_OWNERID` (`ownerId` ASC);
 ALTER TABLE `workunit_comment` ADD INDEX `WORKUNIT_COMMENT_PARENTID` (`parentId` ASC);
 ALTER TABLE `workunit_comment` ADD INDEX `WORKUNIT_COMMENT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `workunit_comment` ADD INDEX `WORKUNIT_COMMENT_UPDATED` (`updated` ASC);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class Comment extends Any
{
	
	/**
	 * @var string
	 */
	public static $table='workunit_comment';
	
	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'body'=>'body',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updated'=>'updated',
	);
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	}
	
	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
			':body'=>$mapped->getBody(),
			':ownerId'=>$mapped->getOwner(true),
			':parentId'=>$mapped->getParent(true),
			':parentUid'=>$mapped->getParentUid(),
			':updated'=>$mapped->getUpdated(self::DATE_FORMAT),
		);
	}
	
}

