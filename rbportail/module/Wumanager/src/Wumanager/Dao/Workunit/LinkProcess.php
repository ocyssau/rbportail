<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao\Workunit;

use Application\Dao\Link;

/** SQL_SCRIPT>>
 CREATE TABLE workunit_process(
 `uid` VARCHAR(64) NOT NULL,
 `parentId` INTEGER NOT NULL,
 `childId` INTEGER NOT NULL,
 `parentUid` VARCHAR(255) NULL,
 `childUid` VARCHAR(255) NULL,
 `name` VARCHAR(255) NULL,
 `lindex` INTEGER DEFAULT 0,
 `attributes` TEXT NULL,
 PRIMARY KEY (`parentId`, `childId`)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE workunit_process ADD UNIQUE (uid);

 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_UID` (`uid` ASC);
 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_NAME` (`name` ASC);
 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_LINDEX` (`lindex` ASC);
 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_CHILDUID` (`childUid` ASC);
 ALTER TABLE `workunit_process` ADD INDEX `WUPROCESS_PARENTID` (`parentId` ASC);
 ALTER TABLE `workunit_process` ADD UNIQ `WUPROCESS_CHILDID` (`childId` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE workunit_process CASCADE;
 <<*/

/**
 *
 * @author olivier
 *
 */
class LinkProcess extends Link
{

	/**
	 * @var \PDOStatement
	 */
	protected $getActivitiesStmt;

	/**
	 * @var string
	 */
	protected $getActivitiesFilter;

	/**
	 * @var \PDOStatement
	 */
	protected $getNextactivitiesStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $getStandaloneActivitiesStmt;

	/**
	 * @var string
	 */
	public static $table = 'workunit_process';

	/**
	 * @var string
	 */
	public static $parentTable = 'workunit';

	/**
	 * @var string
	 */
	public static $childTable = 'wf_process';

	/**
	 * @var string
	 */
	public static $parentForeignKey = 'id';

	/**
	 * @var string
	 */
	public static $childForeignKey = 'id';

	/**
	 * Getter for runningActivities.
	 * Return also properties of activity object as :
	 * type, isComment, isInteractive, isAutomatic
	 *
	 * @param integer $processInstanceId
	 * @return array
	 */
	public function getProcess($wuId, $status)
	{
		$sql = "SELECT inst.*, lnk.name as lname, lnk.lindex, lnk.attributes as lattributes
		FROM workunit_process AS lnk
		JOIN wf_instance AS inst ON inst.id=lnk.childId
		WHERE lnk.parentId=:workunitId AND inst.status=:status";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':workunitId' => $wuId,
			':status' => $status
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Getter for activity instances of this process instance.
	 * Return a PDOStatement
	 *
	 * @param integer $wuId
	 * @param string $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getActivityInstanceFromWorkunitId($wuId, $filter = '1=1', $bind = array())
	{
		if ( !$this->getActivitiesStmt || $this->getActivitiesFilter != $filter ) {
			$select = 'instAct.ownerId';
			$sql = "SELECT $select FROM workunit_process AS lnk";
			$sql .= " JOIN wf_instance_activity AS instAct ON lnk.childId=instAct.instanceId";
			$sql .= " WHERE lnk.parentId=:workunitId";
			if ( $filter ) {
				$sql .= ' AND ' . $filter;
			}
			$this->getActivitiesStmt = $this->connexion->prepare($sql);
			$this->getActivitiesFilter = $filter;
		}

		$bind[':workunitId'] = $wuId;
		$this->getActivitiesStmt->execute($bind);
		return $this->getActivitiesStmt;
	}

	/**
	 * Get instances
	 * Return a PDOStatement
	 *
	 * @param integer $workunitId
	 * @return \PDOStatement
	 */
	public function getNextactivities($workunitId)
	{
		if ( !$this->getNextactivitiesStmt ) {
			/* get instances */
			$sql = "SELECT
				actInst.name, actInst.id, actInst.activityId, actInst.instanceId,
				proc.normalizedName, proc.name as procName, proc.version as procVersion,
				act.attributes, act.title as label, act.type as type
				FROM workunit_process AS lnk
				JOIN wf_instance_activity AS actInst ON lnk.childId = actInst.instanceId
				JOIN wf_activity AS act ON actInst.activityId = act.id
				JOIN wf_instance AS inst ON inst.id=actInst.instanceId
				JOIN wf_process AS proc ON proc.id=inst.processId";
			$sql .= " WHERE lnk.parentId = :parentId AND actInst.status='running' AND inst.status='running'";
			$sql .= " ORDER BY procName ASC";
			$this->getNextactivitiesStmt = $this->connexion->prepare($sql);
		}
		$bind = array(
			':parentId' => $workunitId
		);
		
		$this->getNextactivitiesStmt->execute($bind);
		return $this->getNextactivitiesStmt;
	}
	
	/**
	 * Get standalone activities
	 * Return a PDOStatement
	 *
	 * @param integer $workunitId
	 * @return \PDOStatement
	 */
	public function getStandaloneActivities($workunitId)
	{
		if ( !$this->getStandaloneActivitiesStmt ) {
			$sql = "SELECT inst.id as procId,
				act.attributes, act.title as label, act.type as type, act.id as activityId, act.name as name,
				proc.normalizedName, proc.name as procName, proc.version as procVersion
				FROM workunit_process AS lnk
				JOIN wf_instance AS inst ON lnk.childId = inst.id
				JOIN wf_activity AS act ON inst.processId = act.processId
				JOIN wf_process AS proc ON proc.id = inst.processId";
			$sql .= " WHERE lnk.parentId = :parentId AND act.type='standalone' AND inst.status='running'";
			$sql .= " ORDER BY procName ASC";
			$this->getStandaloneActivitiesStmt = $this->connexion->prepare($sql);
		}
		$bind = array(
			':parentId' => $workunitId
		);
		
		$this->getStandaloneActivitiesStmt->execute($bind);
		return $this->getStandaloneActivitiesStmt;
	}
	
}

