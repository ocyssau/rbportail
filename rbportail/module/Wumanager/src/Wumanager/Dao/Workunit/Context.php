<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao\Workunit;

use Wumanager\Dao\Any;

/** SQL_ALTER>>
 ALTER TABLE workunit_context ADD UNIQUE (uid);
 ALTER TABLE `workunit_context` ADD INDEX `WORKUNIT_CONTEXT_UID` (`uid` ASC);
 ALTER TABLE `workunit_context` ADD INDEX `WORKUNIT_CONTEXT_NAME` (`name` ASC);
 <<*/

/** SQL_SCRIPT>>
CREATE TABLE workunit_context(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL DEFAULT 301,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
INSERT INTO workunit_context (id, uid, name)
					VALUES 
					(1, 'a350xwbs1112', 'A350XWB S11/12'),
					(2, 'a350xwbothersection', 'A350XWB OTHER SECTIONS'),
					(3, 'a350xwbhydraulic', 'A350XWB HYDRAULIC'),
					(4, 'a350xwb', 'A350XWB'),
					(5, 'a320neo', 'A320NEO'),
					(6, 'legacy', 'LEGACY');
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class Context extends Any
{
	
	/**
	 * @var string
	 */
	public static $table='workunit_context';
	
	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
	);
	
	/**
	 * @param \Wumanager\Model\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
		);
	}
}

