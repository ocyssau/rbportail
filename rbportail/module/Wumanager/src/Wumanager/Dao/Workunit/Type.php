<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao\Workunit;

use Wumanager\Dao\Any;

/** SQL_ALTER>>
 ALTER TABLE workunit_type ADD UNIQUE (uid);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_UID` (`uid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_NAME` (`name` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_PARENTID` (`parentId` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_CLASSID` (`cid` ASC);
 ALTER TABLE `workunit_type` ADD INDEX `WORKUNIT_TYPE_OWNER` (`ownerId` ASC);
 
 ALTER TABLE `workunit_type`
 CHANGE COLUMN `tuProduction` `tuProduction` DECIMAL(10,3) NULL DEFAULT NULL ,
 CHANGE COLUMN `tuProductionTlse` `tuProductionTlse` DECIMAL(10,3) NULL DEFAULT NULL ,
 CHANGE COLUMN `tuManagement` `tuManagement` DECIMAL(10,3) NULL DEFAULT NULL ,
 CHANGE COLUMN `tuManagementTlse` `tuManagementTlse` DECIMAL(10,3) NULL DEFAULT NULL ,
 CHANGE COLUMN `salesRate` `salesRate` DECIMAL(10,3) NULL DEFAULT NULL ;
 
 ALTER TABLE `workunit_type`
 ADD COLUMN `newCostGeometric` float DEFAULT NULL;
 
 <<*/
/** SQL_SCRIPT>>
 CREATE TABLE `workunit_type` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` varchar(255)  NOT NULL,
 `cid` int(11) NOT NULL DEFAULT '301',
 `name` varchar(255)  NOT NULL,
 `title` varchar(255)  DEFAULT '2',
 `dimension` tinyint(4) DEFAULT '2',
 `ownerId` varchar(255)  DEFAULT NULL,
 `parentId` int(11) DEFAULT NULL,
 `parentUid` varchar(255)  DEFAULT NULL,
 `updateById` varchar(255)  DEFAULT NULL,
 `updated` datetime DEFAULT NULL,
 `context` varchar(255)  DEFAULT NULL,
 `amount` float DEFAULT NULL,
 `tuProduction` decimal(10,0) DEFAULT NULL,
 `tuProductionTlse` decimal(10,0) DEFAULT NULL,
 `tuManagement` decimal(10,0) DEFAULT NULL,
 `tuManagementTlse` decimal(10,0) DEFAULT NULL,
 `salesRate` varchar(255)  DEFAULT NULL,
 `costSolintep` float DEFAULT NULL,
 `costGeometric` float DEFAULT NULL,
 `newCostGeometric` float DEFAULT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO workunit_type (id, uid, name, dimension, amount, tuProduction, tuManagement, salesRate)
 VALUES
 (1, '3D01', 'Design Solution Creation',3 ,2485, null, null, null),
 (2, '3D02', 'Design Solution Creation',3 , 2015, null, null, null),
 (3, '3D03', 'Design Solution Creation',3 , 7600, null, null, null),
 (4, '3D04', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)',3 , 1920, null, null, null),
 (5, '3D05', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)',3 , 1320, null, null, null),
 (6, '3D06', 'Design Solution Development (COM) / DS Creation issue de (hydraulic)',3 , 2200, null, null, null),
 (7, '3D07', 'Design Solution Modification',3 , 280, null, null, null),
 (8, '3D08', 'Design Solution Modification',3 , 1320, null, null, null),
 (9, '3D09', 'Design Solution Modification & Application of STELIA Aerospace Cheklist for 1st time',3 , 450, null, null, null),
 (10, '3D10', 'DQN implementation',3 , 560, null, null, null),
 
 (11, '2D01', 'Elementary part or sub-assembly drawing set creation',2 , 310, null, null, null),
 (12, '2D02', 'Complex elementary part drawing set creation',2 , 495, null, null, null),
 (13, '2D03', 'Assembly drawing set creation',2 , 1010, null, null, null),
 (14, '2D04', 'Assembly drawing set creation',2 , 3230, null, null, null),
 (15, '2D05', 'Assembly drawing set creation resulting from',2 , 1890, null, null, null),
 (16, '2D06', 'Elementary par drawing set modification',2 , 125, null, null, null),
 (17, '2D07', 'Assembly drawing set modification',2 , 245, null, null, null),
 (18, '2D08', 'Assembly drawing set modification',2 , 550, null, null, null),
 (19, '2D09', 'Elementary part or sub-assembly drawing set creation',2 , 720, null, null, null),
 (20, '2D10', 'Assembly drawing set creation',2 , 1800, null, null, null),
 (21, '2D11', 'Elementary par drawing set modification',2 , 360, null, null, null),
 (22, '2D12', 'Assembly drawing set creation',2 , 730, null, null, null),
 -- For Legacy programs
 (23, 'E0', 'Modification of an existing design solution',3 , 160, null, null, null),
 (24, 'E1-1', 'Pre-study',3 , 1100, null, null, null),
 (25, 'E1-2', 'Design solution maturity B development',3 , 1100, null, null, null),
 (26, 'E1-3', 'Design solution maturity C development',3 , 1100, null, null, null),
 (27, 'E2', 'Complete design solution development',3 , 2740, null, null, null),
 (28, 'L1', 'Elementary and equipped part drawing set modification',2 , 160, null, null, null),
 (29, 'L2', 'Installation drawing set modification',2 , 350, null, null, null),
 (30, 'L3', 'Creation of elementary part drawing set',2 , 280, null, null, null),
 (31, 'L4', 'Creation of installation drawing set',2 , 710, null, null, null);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/
class Type extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'workunit_type';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'context' => 'context',
		'tuProduction' => 'tuProduction',
		'tuManagement' => 'tuManagement',
		'tuProductionTlse' => 'tuProductionTlse',
		'tuManagementTlse' => 'tuManagementTlse',
		'amount' => 'amount',
		'salesRate' => 'salesRate',
		'dimension' => 'dimension',
		'costSolintep' => 'costSolintep',
		'costGeometric' => 'costGeometric',
		'newCostGeometric' => 'newCostGeometric'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':context' => $mapped->context,
			':tuProduction' => $mapped->tuProduction,
			':tuManagement' => $mapped->tuManagement,
			':tuProductionTlse' => $mapped->tuProductionTlse,
			':tuManagementTlse' => $mapped->tuManagementTlse,
			':amount' => $mapped->amount,
			':salesRate' => $mapped->salesRate,
			':dimension' => $mapped->dimension,
			':costSolintep' => $mapped->costSolintep,
			':costGeometric' => $mapped->costGeometric,
			':newCostGeometric' => $mapped->newCostGeometric
		));
	}
}

