<?php
// %LICENCE_HEADER%
// %LICENCE_HEADER%
namespace Wumanager\Dao\Workunit;

use Wumanager\Dao\Any;

/**
 * SQL_SCRIPT>>
CREATE TABLE workunit_marker(
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` VARCHAR(255) NOT NULL,
  `cid` CHAR(16) NOT NULL DEFAULT 'wumarker5ab4g',
  `workunitId` int NOT NULL,
  `userId` VARCHAR(255) NOT NULL,
  `message` TEXT NULL,
  PRIMARY KEY (`id`)
);
<< */

/**
 * SQL_ALTER>>
ALTER TABLE `workunit_marker` ADD UNIQUE (`uid`);
ALTER TABLE `workunit_marker` ADD UNIQUE `WORKUNIT_MARKER_U_WUSUER` (`workunitId`, `userId`);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_UID` (`uid` ASC);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_USERID` (`userId` ASC);
ALTER TABLE `workunit_marker` ADD INDEX `WORKUNIT_MARKER_WUID` (`workunitId` ASC);
<< */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */
class Marker extends Any
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'workunit_marker';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'cid' => 'cid',
		'uid' => 'uid',
		'workunitId' => 'workunitId',
		'userId' => 'userId',
		'message' => 'message'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	}

	/**
	 *
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id' => $mapped->getId(),
			':cid' => $mapped->cid,
			':uid' => $mapped->getUid(),
			':workunitId' => $mapped->workunitId,
			':userId' => $mapped->userId,
			':message' => $mapped->getMessage()
		);
	}
}

