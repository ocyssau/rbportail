<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao;

/** SQL_SCRIPT>>
CREATE TABLE bill_note(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`billId` int NOT NULL,
	`emitDate` datetime NULL,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>

ALTER TABLE bill_note ADD UNIQUE (uid);

ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_UID` (`uid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_NAME` (`name` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_PARENTID` (`parentId` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_PARENTUID` (`parentUid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_CLASSID` (`cid` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_OWNER` (`ownerId` ASC);
ALTER TABLE `bill_note` ADD INDEX `BILL_NOTE_BILLID` (`billId` ASC);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE bill_note;
 <<*/

class BillNote extends Any
{
	/**
	 * @var string
	 */
	public static $table = 'bill_note';
	
	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'emitDate'=>'emitDate',
		'billId'=>'billId',
	);
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	} //End of function
	
	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':emitDate'=>$mapped->getEmitdate(self::DATE_FORMAT),
			':billId'=>$mapped->getBillId(),
		));
	}
}
