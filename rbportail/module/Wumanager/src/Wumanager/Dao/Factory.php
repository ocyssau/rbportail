<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao;

use Wumanager\Model;
use Wumanager\Model\Any as AnyObject;
use Application\Model\Link as Link;

use Application\Dao\Connexion;

use Wumanager\Dao\Exception As Exception;
use Wumanager\Dao\Error As Error;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{
    const GENERIC = 200;
    
    /**
     * Registry of instanciated DAO.
     *
     * @var array
     */
    private static $_registry = array();
    
    /**
     * Associate for each Component Class ID a DAO CLASS
     * ClassId 200 return the super class DAO Component/Component
     * @var array
     */
    private static $_map = array(
    	300=>array('Workunit', 'workunit'),
    	301=>array('Workunit\Type', 'workunit_type'),
    	302=>array('Status', 'wumanager_status'),
    	303=>array('Site', 'site'),
    	304=>array('Deliverable', 'deliverable'),
    	305=>array('Billnote', 'bill_note'),
    	306=>array('Bill', 'bill'),
    	307=>array('Workunit\Context', 'workunit_context'),
    	308=>array('Workunit\LinkTypeContext', 'link_workunit_type_context'),
    	309=>array('Workunit\LinkProcess', 'workunit_process'),
    );
    
    /**
     * Return a dao object for the mapped object.
     * If $ComponentOrId = 200, return the supre class dao Component\Component
     *
	 * @param Any|integer $ComponentOrId
     * @return \Application\Dao\Dao
     */
    public static function getDao($ComponentOrId)
    {
    	if($ComponentOrId instanceof AnyObject || $ComponentOrId instanceof Link ){
    		$id = $ComponentOrId->cid;
    	}
    	else{
    		$id = $ComponentOrId;
    	}

        if(!isset(self::$_registry[$id])){
        	$daoClass = __NAMESPACE__ . '\\' . self::$_map[$id][0];
            self::$_registry[$id] = new $daoClass();
        }
        return self::$_registry[$id];
    }
    
    /**
     * Return a dao object for the mapped object.
     *
     * @param Integer
     * @return \Application\Dao\DaoList
     */
    public static function getList($classId)
    {
    	$id = $classId;
		$table = self::$_map[$id][1];
		return new \Application\Dao\DaoList($table);
    }
}
