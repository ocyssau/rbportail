<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao;

/** SQL_SCRIPT>>
 CREATE TABLE site(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` int NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO site (id, uid, cid, name) VALUES(1, 'sierblagnac', 303, 'SIER-BLAGNAC');
 INSERT INTO site (id, uid, cid, name) VALUES(2, 'solintep', 303, 'SOLINTEP');
 INSERT INTO site (id, uid, cid, name) VALUES(3, 'sierstnaz', 303, 'SIER-STNAZAIRE');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE site ADD UNIQUE (uid);
 
 ALTER TABLE `site` ADD INDEX `SITE_CLASSID` (`cid` ASC);
 ALTER TABLE `site` ADD INDEX `SITE_NAME` (`name` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE site CASCADE;
 <<*/
class Site extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'site';

	/**
	 * @var array
	 */
	public static $sysToApp = array();

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array());
	}
}
