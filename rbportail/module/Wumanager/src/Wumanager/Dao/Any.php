<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao;

use Application\Dao\Dao;

/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW anywu AS
	SELECT id, uid, cid, name,  ownerId, parentId, parentUid, updateById, updated FROM workunit
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM deliverable
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM bill
	UNION
	SELECT id, uid, cid, name, ownerId, parentId, parentUid, updateById, updated FROM bill_note;
 <<*/

/** SQL_DROP>>
 <<*/

class Any extends Dao
{
	/**
	 * @var string
	 */
	public static $table='any';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'title'=>'title',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updateById'=>'updateById',
		'updated'=>'updated',
	);

	/**
	 * Constructor
	 * @param Connexion
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	}

	/**
	 * @param \Wumanager\Model\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
			':title'=>$mapped->getTitle(),
			':ownerId'=>$mapped->getOwner(true),
			':parentId'=>$mapped->getParent(true),
			':parentUid'=>$mapped->getParentUid(),
			':updateById'=>$mapped->getUpdateBy(true),
			':updated'=>$mapped->getUpdated(self::DATE_FORMAT),
		);
	}
}
