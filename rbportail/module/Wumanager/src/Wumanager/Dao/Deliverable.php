<?php
//%LICENCE_HEADER%

namespace Wumanager\Dao;

/** SQL_SCRIPT>>
CREATE TABLE deliverable(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
		`status` varchar(255) NULL,
		`section` varchar(255) NULL,
		`circuit` varchar(255) NULL,
		`comment` varchar(255) NULL,
		`projeteur` varchar(255) NULL,
		`indice` varchar(255) NULL,
		`codeFiliere` varchar(255) NULL,
		`codeMer` varchar(255) NULL,
		`map` varchar(255) NULL,
		`ds` varchar(255) NULL,
		`ci` varchar(255) NULL,
		`msn` varchar(255) NULL,
		`protectionCode` varchar(255) NULL,
		`beCode` varchar(255) NULL,
		`functionalClass` varchar(255) NULL,
		`drawingType` varchar(255) NULL,
		`configItem` varchar(255) NULL,
		`layout` varchar(255) NULL,
		`mod` varchar(128) NULL,
		`team` varchar(128) NULL,
		`revision` varchar(128) NULL,
		`gildaStatus` varchar(255) NULL,
		`officialDate` DATE NULL,
		`compilerDate` DATE NULL,
		`quantity` int(11) NOT NULL DEFAULT 1,
PRIMARY KEY (`id`)
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
ALTER TABLE deliverable ADD UNIQUE (uid);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_UID` (`uid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_NAME` (`name` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PARENTID` (`parentId` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PARENTUID` (`parentUid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CLASSID` (`cid` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_OWNER` (`ownerId` ASC);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_DS` (`ds` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CI` (`ci` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_PROJETEUR` (`projeteur` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CODEF` (`codeFiliere` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CODEM` (`codeMer` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_CIRCUIT` (`circuit` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_SECTION` (`section` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_STATUS` (`status` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_MSN` (`msn` ASC);

ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_MOD` (`mod` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_TEAM` (`team` ASC);
ALTER TABLE `deliverable` ADD INDEX `DELIVERABLE_REVISION` (`revision` ASC);
ALTER TABLE `deliverable` ADD INDEX `I_GILDASTATUS` (`gildaStatus` ASC);

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE deliverable;
 <<*/

class Deliverable extends Any
{
	/**
	 * @var string
	 */
	public static $table = 'deliverable';

	public static $sysToAppFilter = array(
		'attributes'=>'json',
		'officialDate'=>'date',
		'compilerDate'=>'date',
	);

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'status'=>'status',
		'section'=>'section',
		'circuit'=>'circuit',
		'comment'=>'comment',
		'projeteur'=>'projeteur',
		'indice'=>'indice',
		'codeFiliere'=>'codeFiliere',
		'codeMer'=>'codeMer',
		'map'=>'map',
		'ds'=>'ds',
		'ci'=>'ci',
		'msn'=>'msn',
		'protectionCode'=>'protectionCode',
		'beCode'=>'beCode',
		'functionalClass'=>'functionalClass',
		'drawingType'=>'drawingType',
		'configItem'=>'configItem',
		'layout'=>'layout',
		'mod'=>'mod',
		'team'=>'team',
		'revision'=>'revision',
		'gildaStatus'=>'gildaStatus',
		'officialDate'=>'officialDate',
		'compilerDate'=>'compilerDate',
		'quantity'=>'quantity',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			/* common */
			':status'=>$mapped->getStatus(),
			':name'=>$mapped->getName(),
			':section'=>$mapped->section,
			':circuit'=>$mapped->circuit,
			':comment'=>$mapped->comment,
			':projeteur'=>$mapped->getProjeteur(),
			':mod'=>$mapped->mod,
			':team'=>$mapped->team,
			':revision'=>$mapped->revision,
			':gildaStatus'=>$mapped->gildaStatus,
			':quantity'=>$mapped->quantity,

			/* DATE */
			':officialDate'=>self::dateToSys($mapped->getOfficialDate(self::DATE_FORMAT)),
			':compilerDate'=>self::dateToSys($mapped->getCompilerDate(self::DATE_FORMAT)),

			/* 2d */
			':indice'=>$mapped->indice,
			':codeFiliere'=>$mapped->codeFiliere,
			':codeMer'=>$mapped->codeMer,
			':map'=>$mapped->map,
			':ds'=>$mapped->ds,
			/* 3d */
			':ci'=>$mapped->ci,
			':msn'=>$mapped->msn,
			':protectionCode'=>$mapped->protectionCode,
			':beCode'=>$mapped->beCode,
			':functionalClass'=>$mapped->functionalClass,
			':drawingType'=>$mapped->drawingType,
			':configItem'=>$mapped->configItem,
			':layout'=>$mapped->layout,

		));
	}
}
