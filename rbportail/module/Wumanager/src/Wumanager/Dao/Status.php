<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao;

/** SQL_SCRIPT>>
 CREATE TABLE wumanager_status(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` int NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `ownerId` varchar(255) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updateById` varchar(255) NULL,
 `updated` datetime NULL,
 `progression` varchar(255) NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE wumanager_status ADD UNIQUE (uid);
 
 ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_UID` (`uid` ASC);
 ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_NAME` (`name` ASC);
 ALTER TABLE `wumanager_status` ADD INDEX `WUMSTATUS_PARENTID` (`parentId` ASC);
 ALTER TABLE `wumanager_status` ADD INDEX `STATUS_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `wumanager_status` ADD INDEX `STATUS_CLASSID` (`cid` ASC);
 ALTER TABLE `wumanager_status` ADD INDEX `STATUS_OWNER` (`ownerId` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE wumanager_status CASCADE;
 <<*/

/**
 * 
 *
 */
class Status extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'wumanager_status';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'progression' => 'progression'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':progression' => $mapped->getProgression()
		));
	}
}

