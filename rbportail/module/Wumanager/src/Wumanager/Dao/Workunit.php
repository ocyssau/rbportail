<?php
//%LICENCE_HEADER%
namespace Wumanager\Dao;

use Wumanager\Model;
use Application\Dao\Factory as DaoFactory;

/** SQL_SCRIPT>>
 CREATE TABLE workunit(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` int NOT NULL,
 `version` int NOT NULL DEFAULT 1,
 `name` VARCHAR(255) NOT NULL,
 `flt` VARCHAR(128) NOT NULL,
 `number` varchar(64) DEFAULT NULL,
 `title` VARCHAR(255) NOT NULL,
 `ownerId` varchar(255) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updateById` varchar(255) NULL,
 `updated` datetime NULL,
 `typeId` int NULL,
 `siteUid` varchar(255) NULL,
 `processId` INT NULL,
 `progression` float NULL,
 `tos` varchar(255) NULL,
 `openDate` datetime NULL,
 `planDate` datetime NULL,
 `deliveryDate` datetime NULL,
 `lastDeliveryDate` datetime DEFAULT NULL,
 `supplierBillingDate` datetime NULL,
 `sitedeliveryDate` datetime NULL,
 `deliveryMemo` varchar(255) NULL,
 `status` varchar(255) NULL,
 `comment` varchar(512) NULL,
 `visibility` varchar(32) NULL,
 `ataLeader` varchar(255) NULL,
 `quantity` int(11) NOT NULL DEFAULT 1,
 PRIMARY KEY (`id`),
 KEY `WORKUNIT_UID` (`uid`),
 KEY `WORKUNIT_NAME` (`name`),
 KEY `WORKUNIT_PARENTID` (`parentId`),
 KEY `WORKUNIT_PARENTUID` (`parentUid`),
 KEY `WORKUNIT_CLASSID` (`cid`),
 KEY `WORKUNIT_OWNER` (`ownerId`),
 KEY `WORKUNIT_SITE` (`siteUid`),
 KEY `WORKUNIT_TYPE` (`typeId`),
 KEY `WORKUNIT_TOS` (`tos`),
 KEY `WORKUNIT_ODATE` (`openDate`),
 KEY `WORKUNIT_PDATE` (`planDate`),
 KEY `WORKUNIT_DDATE` (`deliveryDate`),
 KEY `WORKUNIT_LDDATE` (`lastDelivery`),
 KEY `WORKUNIT_DM` (`deliveryMemo`),
 KEY `progression` (`progression`),
 KEY `visibility` (`visibility`),
 KEY `WORKUNIT_VERSION` (`version`),
 KEY `WORKUNIT_NUMBER` (`number`),
 KEY `WORKUNIT_FLT` (`flt`)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE workunit;
 <<*/
class Workunit extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'workunit';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'flt' => 'flt',
		'number' => 'number',
		'version' => 'version',
		'typeId' => 'typeId',
		'siteUid' => 'siteUid',
		'processId' => 'processId',
		'openDate' => 'openDate',
		'planDate' => 'planDate',
		'deliveryDate' => 'deliveryDate',
		'lastDeliveryDate' => 'lastDeliveryDate',
		'sitedeliveryDate' => 'sitedeliveryDate',
		'deliveryMemo' => 'deliveryMemo',
		'tos' => 'tos',
		'status' => 'status',
		'comment' => 'comment',
		'visibility' => 'visibility',
		'progression' => 'progression',
		'supplierBillingDate' => 'supplierBillingDate',
		'ataLeader' => 'ataLeader',
		'quantity' => 'quantity'
	);

	/**
	 * Constructor
	 * @param Connexion
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(Any::$sysToApp, self::$sysToApp);
	}

	/**
	 * @param \Rbplm\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array_merge(Any::bind($mapped), array(
			':flt' => $mapped->getFlt(),
			':number' => $mapped->getNumber(),
			':version' => $mapped->getVersion(),
			':typeId' => $mapped->getType(true),
			':siteUid' => $mapped->getSite(true),
			':processId' => $mapped->getProcess(true),
			':openDate' => $mapped->getOpendate(self::DATE_FORMAT),
			':planDate' => $mapped->getPlandate(self::DATE_FORMAT),
			':deliveryDate' => $mapped->getDeliverydate(self::DATE_FORMAT),
			':lastDeliveryDate' => $mapped->getLastDeliverydate(self::DATE_FORMAT),
			':sitedeliveryDate' => $mapped->getSitedeliverydate(self::DATE_FORMAT),
			':deliveryMemo' => $mapped->getDeliverymemo(),
			':tos' => $mapped->getTos(),
			':status' => $mapped->getStatus(),
			':comment' => $mapped->getComment(),
			':visibility' => $mapped->getVisibility(),
			':progression' => $mapped->getProgression(),
			':supplierBillingDate' => $mapped->getSupplierBillingDate(self::DATE_FORMAT),
			':ataLeader' => $mapped->getAtaLeader(),
			':quantity' => $mapped->getQuantity()
		));
	}

	/**
	 * Load a workunit from his name and his version. If version is let blank, load the last version.
	 *
	 *
	 * @var \Wumanager\Model\Workunit $mapped
	 * @var string $name
	 * @var integer $versionId
	 * @return Workunit
	 */
	public function loadFromNameAndVersion(\Wumanager\Model\Workunit $mapped, $name, $versionId = null)
	{
		if ( $versionId ) {
			$filter = 'obj.name=:name AND obj.version=:versionId';
			$bind = array(
				':name' => $name,
				':versionId' => $versionId
			);
		}
		else {
			$filter = 'obj.name=:name ORDER BY version DESC LIMIT 1';
			$bind = array(
				':name' => $name
			);
		}
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 * @param \Wumanager\Model\Workunit $mapped
	 * @param string $filter
	 * @return Workunit
	 */
	public function loadDeliverables($mapped, $filter = null)
	{
		$list = DaoFactory::get()->getList(Model\Deliverable::$classId);
		$parentId = $mapped->getId();
		if ( $filter ) {
			$filter = "$filter AND parentId='$parentId' ORDER BY `id` ASC";
		}
		else {
			$filter = "parentId='$parentId' ORDER BY `id` ASC";
		}
		$list->load($filter);
		
		$deliverables = [];
		foreach( $list as $item ) {
			$child = Model\Factory::get($item['cid']);
			$child->hydrate($item);
			$deliverables[] = $child;
		}
		$mapped->setDeliverables($deliverables);
		return $mapped;
	}

	/**
	 * Delete all deliverable from workunit id
	 * @param integer $workunitId
	 * @return Workunit
	 */
	public function deleteDeliverables($workunitId)
	{
		if ( !$this->deleteDeliverables ) {
			$sql = "DELETE FROM deliverable";
			$sql .= " WHERE parentId = :workunitId";
			$this->deleteDeliverables = $this->connexion->prepare($sql);
		}
		
		$bind = array(
			':workunitId' => $workunitId
		);
		$this->deleteDeliverables->execute($bind);
		return $this;
	}

	/**
	 * @param \Wumanager\Model\Workunit $mapped
	 * @return Workunit
	 */
	public function saveStatus($mapped)
	{
		$set = [];
		if ( !$this->saveStatusStmt ) {
			$set[] = '`status`=:status';
			$sql = 'UPDATE ' . static::$table . ' SET ' . implode(',', $set) . ' WHERE `id`=:id;';
			$this->saveStatusStmt = $this->connexion->prepare($sql);
		}
		$bind = array(
			':id' => $mapped->getId(),
			':status' => $mapped->getStatus()
		);
		$this->saveStatusStmt->execute($bind);
		return $this;
	}

	/**
	 * Delete wf-wu association, instance and acitivties instances linked to workunit.
	 * @param integer $workunitId
	 * @return Workunit
	 */
	public function deleteWfFromWuid($workunitId)
	{
		if ( !$this->deleteWfFromWuid ) {
			$sql = "DELETE inst.*, lnk.*, actInst.* FROM wf_instance AS inst";
			$sql .= " LEFT JOIN workunit_process AS lnk ON lnk.childId = inst.id";
			$sql .= " LEFT JOIN wf_instance_activity AS actInst ON actInst.instanceId = inst.id";
			$sql .= " WHERE lnk.parentId = :workunitId";
			
			$this->deleteWfFromWuid = $this->connexion->prepare($sql);
		}
		
		$bind = array(
			':workunitId' => $workunitId
		);
		$this->deleteWfFromWuid->execute($bind);
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return integer
	 */
	public function getLastVersion($name)
	{
		if ( !$this->getlastversionStmt ) {
			$table = $this->_table;
			$sql = "SELECT MAX(version) FROM $table WHERE name=:name GROUP BY name";
			$this->getlastversionStmt = $this->connexion->prepare($sql);
		}
		$this->getlastversionStmt->execute(array(
			':name' => $name
		));
		$lastVersionId = $this->getlastversionStmt->fetchColumn(0);
		return $lastVersionId;
	}
}
