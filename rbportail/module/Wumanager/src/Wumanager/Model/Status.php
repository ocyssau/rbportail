<?php
//%LICENCE_HEADER%

namespace Wumanager\Model;

use Application\Model\People;
use Application\Model\People\User;
use DateTime;

class Status extends Any
{
	static $classId = 302;
	
	protected $progression;
	
	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);
		(isset($properties['progression'])) ? $this->progression=$properties['progression'] : null;
		return $this;
	} //End of function
	
	/**
	 * @return string
	 */
	public function getProgression()
	{
		return $this->progression;
	} //End of function
	
	/**
	 * @param string
	 */
	public function setProgression( $string )
	{
		$this->progression = $string;
		return $this;
	} //End of function
}
