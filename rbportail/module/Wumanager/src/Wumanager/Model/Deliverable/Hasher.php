<?php
// %LICENCE_HEADER%
namespace Wumanager\Model\Deliverable;

/**
 *
 */
class Hasher
{

	/**
	 *
	 * @param \Wumanager\Model\Deliverable $any
	 * @return string
	 */
	public static function hash($any)
	{
		$involved = array(
			'name',
			'section',
			'circuit',
			'projeteur',
			'protectionCode',
			'beCode',
			'functionalClass',
			'drawingType',
			'configItem',
			'layout',
			'mod',
			'team',
			'indice',
			'codeFiliere',
			'codeMer',
			'map',
			'ds',
			'quantity'
		);
		$datas = $any->getArrayCopy();

		$str = '';
		foreach( $involved as $p ) {
			$str .= $datas[$p];
		}
		return md5($str);
	}
}

