<?php
//%LICENCE_HEADER%
namespace Wumanager\Model;

use DateTime;
use Zend\Db\Sql\Ddl\Column\Decimal;

/**
 *
 *
 */
class Bill extends Any
{

	/**
	 * 
	 * @var integer
	 */
	static $classId = 306;

	/**
	 * 
	 * @var \DateTime
	 */
	protected $emitDate;
	
	/**
	 * 
	 * @var \DateTime
	 */
	protected $orderDate;

	/**
	 */
	protected $order;

	/**
	 * 
	 * @var integer
	 */
	protected $orderId;

	/**
	 * 
	 * @var Decimal
	 */
	protected $amount;

	/**
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		
		(isset($properties['orderId'])) ? $this->order = $properties['orderId'] : null;
		(isset($properties['amount'])) ? $this->amount = $properties['amount'] : null;
		
		//date
		(isset($properties['orderDate'])) ? $this->setOrderdate($properties['orderDate']) : null;
		(isset($properties['emitDate'])) ? $this->setEmitDate($properties['emitDate']) : null;
		
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getEmitdate($format = null)
	{
		if ( $this->emitDate instanceof DateTime ) {
			if ( $format ) {
				return $this->emitDate->format($format);
			}
			else {
				return $this->emitDate;
			}
		}
		else
			return null;
	}

	/**
	 * @param DateTime|string
	 */
	public function setEmitdate($date)
	{
		if ( !$date or substr($date, 0, 10) == '0000-00-00' ) {
			$this->emitDate = null;
		}
		elseif ( $date instanceof DateTime ) {
			$this->emitDate = $date;
		}
		else {
			$this->emitDate = new DateTime($date);
		}
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getOrderdate($format = null)
	{
		if ( $this->orderDate instanceof DateTime ) {
			if ( $format ) {
				return $this->orderDate->format($format);
			}
			else {
				return $this->orderDate;
			}
		}
		else
			return null;
	}

	/**
	 * @param DateTime
	 */
	public function setOrderdate($date)
	{
		if ( !$date or substr($date, 0, 10) == '0000-00-00' ) {
			$this->orderDate = null;
		}
		elseif ( $date instanceof DateTime ) {
			$this->orderDate = $date;
		}
		else {
			$this->orderDate = new DateTime($date);
		}
		return $this;
	}

	/**
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * @param string
	 */
	public function setOrderId($string)
	{
		$this->orderId = $string;
		return $this;
	}

	/**
	 * @return decimal
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param decimal
	 */
	public function setAmount($real)
	{
		$this->amount = $real;
		return $this;
	}
}
