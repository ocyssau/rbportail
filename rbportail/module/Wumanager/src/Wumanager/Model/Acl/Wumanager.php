<?php
namespace Wumanager\Model\Acl;

use Wumanager\Dao;
use Wumanager\Model;

use Application\Dao\Factory as DaoFactory;
use Application\Model\Acl\Resource;
use Application\Model\Acl\Role;
use Application\Model\People\User;
use Application\Model\Acl\NoRoleException;

final class Wumanager extends \Application\Model\Acl\Acl
{
	static $classId = 700;

	const RIGHT_WFPUSH = 512;

	const ROLE_MANAGER = 8;
	const ROLE_CUSTOMER = 32;
	const ROLE_SOLINTEP = 'solintep';
	const ROLE_SIERBLAGNAC = 'sierblagnac';
	const ROLE_SIERSTNAZAIRE = 'sierstnaz';
	const ROLE_GEOMETRIC = 'geometric';

	/**
	 *
	 */
	public function __construct($parentAcl)
	{
		$this->rules = array(
			self::ROLE_COLLABORATOR=>array(self::RIGHT_CONSULT,self::RIGHT_WFPUSH),
			self::ROLE_CUSTOMER=>self::RIGHT_CONSULT,
			self::ROLE_MANAGER=>array(
				self::RIGHT_MANAGE,
				self::RIGHT_CONSULT,
				self::RIGHT_UPDATE,
				self::RIGHT_DELETE
			),
			self::ROLE_ADMIN=>self::RIGHT_ADMIN,
		);

		$this->roles = array(
			//self::ROLE_GUEST=>array('GUEST',array()),
			self::ROLE_CUSTOMER=>array('CUSTOMER',array()),
			self::ROLE_COLLABORATOR=>array('COLLABORATOR',array(self::ROLE_CUSTOMER)),
			self::ROLE_SOLINTEP=>array('SOLINTEP',array(self::ROLE_COLLABORATOR)),
			self::ROLE_SIERBLAGNAC=>array('SIER-BLAGNAC',array(self::ROLE_COLLABORATOR)),
			self::ROLE_SIERSTNAZAIRE=>array('SIER-STNAZAIRE',array(self::ROLE_COLLABORATOR)),
			self::ROLE_GEOMETRIC=>array('GEOMETRIC',array(self::ROLE_COLLABORATOR)),
			self::ROLE_MANAGER=>array('MANAGER', array(self::ROLE_CUSTOMER, self::ROLE_COLLABORATOR, self::ROLE_SOLINTEP, self::ROLE_SIERBLAGNAC, self::ROLE_SIERSTNAZAIRE, self::ROLE_GEOMETRIC)),
			self::ROLE_ADMIN=>array('ADMINISTRATEUR',array(self::ROLE_MANAGER)),
		);

		$ressourceId = Model\Wumanager::$id;
		$this->acl = new \Zend\Permissions\Acl\Acl();
		$this->parentAcl = $parentAcl;
		$this->mainResource = new Resource( $ressourceId );

		$this->acl->addResource($parentAcl->getMainResource());
		$this->acl->addResource($this->mainResource, $parentAcl->getMainResource());

		foreach($this->roles as $id=>$def){
			$this->acl->addRole(new Role($id),$def[1]);
		}

		foreach($this->rules as $roleName=>$privileges){
			$this->acl->allow( $roleName, $this->mainResource, $privileges );
		}
	}

	/**
	 *
	 */
	public static function getRolesMap(){
		if(!self::$rolesMap){
			$ret = array();
			$list = DaoFactory::get()->getList(\Wumanager\Model\Site::$classId)->load();
			foreach($list as $item){
				$ret[$item['uid']] = $item['name'];
			}
			$ret[self::ROLE_ADMIN] = 'ADMIN';
			$ret[self::ROLE_MANAGER] = 'MANAGER';

			self::$rolesMap = $ret;
		}
		return self::$rolesMap;
	}

	/**
	 * @param User $user
	 * @param string $resourceId
	 */
	public function loadRole(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();

		$parentAcl = $this->parentAcl;

		if($userId=='administrateur' || $parentAcl->hasRight($parentAcl::RIGHT_ADMIN)){
			$roles[] = self::ROLE_ADMIN;
		}

		if(!$this->acl->hasRole($userId)){
			$stmt = $this->getDao()->getRole("userId='$userId' AND resourceId='$resourceId'", 'roleId');
			while($role = $stmt->fetch(\PDO::FETCH_OBJ)){
				$roles[$role->roleId] = $role->roleId;
			}

			if(count($roles)==0){
				throw new NoRoleException("User $userId has no role define for the Wumanager module");
			}
			else{
				$this->acl->addRole($userId, $roles);
			}
		}

		return $roles;
	}

}
