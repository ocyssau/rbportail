<?php
namespace Wumanager\Model\Notification;

use Wumanager\Dao\Any;

/** SQL_SCRIPT>>
 CREATE TABLE `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(128) NOT NULL,
 `mail` VARCHAR (128) NOT NULL,
 `referenceId` INT(11) NULL,
 `referenceUid` VARCHAR (128) NULL,
 `referenceCid` VARCHAR (128) NULL,
 `ownerId` VARCHAR (128) NULL,
 `ownerUid` VARCHAR (128) NULL,
 `event` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 KEY `INDEX_notifications_2` (`referenceUid`),
 KEY `INDEX_notifications_3` (`event`),
 UNIQUE KEY `UNIQ_notifications_1` (`uid`),
 UNIQUE KEY `UNIQ_notifications_2` (`mail`,`event`,`referenceUid`,`ownerUid`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE `notifications`;
 <<*/
class NotificationDao extends Any
{

	/**
	 * @var string
	 */
	public static $table = 'notifications';

	/**
	 * @var string
	 */
	public static $vtable = 'notifications';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'mail' => 'mail',
		'ownerId' => 'ownerId',
		'ownerUid' => 'ownerUid',
		'referenceUid' => 'referenceUid',
		'referenceCid' => 'referenceCid',
		'referenceId' => 'referenceId',
		'event' => 'event',
		'condition' => 'condition'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'condition' => 'json',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	}

	/**
	 * @param \Wumanager\Model\Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id' => $mapped->getId(),
			':uid' => $mapped->getUid(),
			':mail' => $mapped->mail,
			':ownerId' => $mapped->ownerId,
			':ownerUid' => $mapped->ownerUid,
			':referenceId' => $mapped->referenceId,
			':referenceUid' => $mapped->referenceUid,
			':referenceCid' => $mapped->referenceCid,
			':event' => $mapped->getEvent(),
			':condition' => json_encode($mapped->getCondition())
		);
	}

	/**
	 * @param string $uid referenceUid
	 * @param string $ownerUid
	 * @return \PDOStatement
	 */
	public function getFromReferenceUidAndOwnerUid($uid, $ownerUid)
	{
		$filter = 'referenceUid=:uid AND ownerUid=:ownerUid';
		$bind = array(
			':uid' => $uid,
			':ownerUid' => $ownerUid
		);
		
		$table = static::$table;
		$select = $this->getSelectAsApp();
		$selectStr = implode(',', $select);
		$sql = "SELECT $selectStr FROM $table WHERE $filter";
		
		$conn = $this->connexion;
		$stmt = $conn->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
	
	/**
	 * @param string $event 	Event name 
	 * @param string $uid 		Uid of the reference object
	 * @return \PDOStatement
	 */
	public function getReceipts($event, $uid)
	{
		$filter = "event=:event AND (referenceUid=:uid OR referenceUid='all')";
		$bind = array(
			':event' => $event,
			':uid' => $uid,
		);
		
		$table = static::$table;
		$selectStr = 'DISTINCT `mail`, `condition`';
		$sql = "SELECT $selectStr FROM $table WHERE $filter";
		
		$conn = $this->connexion;
		$stmt = $conn->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
}
