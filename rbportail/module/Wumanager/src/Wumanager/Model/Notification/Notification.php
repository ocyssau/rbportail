<?php
namespace Wumanager\Model\Notification;

use Rbplm\People;

/**
 * definition of the notification = event to notifiy to user for reference object "$reference"
 * Recorded in db and edited by users.
 *
 */
class Notification extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped, \Rbplm\Owned;

	/**
	 *
	 * @var string
	 */
	static $classId = '479v169a42a96';

	/**
	 *
	 * @var Condition
	 */
	protected $condition;

	/**
	 *
	 * @var array
	 */
	protected $event;

	/**
	 * Reference object to listen
	 * 
	 * @var \Rbplm\Any
	 */
	protected $reference;

	/**
	 * = 'all' if refer to all objects.
	 * else, uid of the object to listen
	 * 
	 * @var string
	 */
	public $referenceUid = 'all';

	/**
	 * Class id of the reference object to listen
	 *
	 * @var string
	 */
	public $referenceCid;

	/**
	 * Id of the reference object to listen
	 * = 0, if refer to all objects
	 *
	 * @var integer
	 */
	public $referenceId = 0;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}
		$this->cid = static::$classId;
	}

	/**
	 *
	 * @param string $name
	 * @return \Rbplm\Any
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		if ( !$name ) {
			$name = $obj->getUid();
		}
		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Notification
	 */
	public function hydrate(array $properties)
	{
		// ANY
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		// MAPPED
		$this->mappedHydrate($properties);
		// OWNED
		$this->ownedHydrate($properties);
		// NOTIFICATION
		(isset($properties['event'])) ? $this->event = $properties['event'] : null;
		(isset($properties['reference'])) ? $this->reference = $properties['reference'] : null;
		(isset($properties['referenceId'])) ? $this->referenceId = $properties['referenceId'] : null;
		(isset($properties['referenceUid'])) ? $this->referenceUid = $properties['referenceUid'] : null;
		(isset($properties['referenceCid'])) ? $this->referenceCid = $properties['referenceCid'] : null;
		(isset($properties['mail'])) ? $this->mail = $properties['mail'] : null;
		
		if ( isset($properties['condition']) ) {
			$this->condition = new Condition($properties['condition']);
		}
		return $this;
	}

	/**
	 *
	 * @return Condition
	 */
	public function getCondition()
	{
		return $this->condition;
	}

	/**
	 *
	 * @param Condition $condition
	 * @return Notification
	 */
	public function setCondition(Condition $condition)
	{
		$this->condition = $condition;
		return $this;
	}

	/**
	 *
	 * @return Notification
	 */
	public function unsetCondition()
	{
		$this->condition = false;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getEvent()
	{
		return $this->event;
	}

	/**
	 *
	 * @return Notification
	 */
	public function setEvent($string)
	{
		$this->event = $string;
		return $this;
	}

	/**
	 *
	 * @param [OPTIONAL] boolean $asId
	 * @return \Wumanager\Model\Any
	 */
	public function getReference($asId = false)
	{
		if ( $asId ) {
			return $this->referenceUid;
		}
		return $this->reference;
	}

	/**
	 *
	 * @param \Wumanager\Model\Any $reference
	 * @return Notification
	 */
	public function setReference(\Wumanager\Model\Any $reference)
	{
		$this->reference = $reference;
		$this->referenceId = $reference->getId();
		$this->referenceUid = $reference->getUid();
		return $this;
	}
}

