<?php
// %LICENCE_HEADER%
namespace Wumanager\Model\Workunit;

use Rbplm\Dao\MappedInterface;
use Application\Model\People;


/**
 *
 */
class Marker extends \Rbplm\Any implements MappedInterface
{
	use \Rbplm\Mapped;

	static $classId = 'wumarker5ab4g';

	/**
	 *
	 * @var \Wumanager\Model\Workunit
	 */
	protected $workunit;

	/**
	 *
	 * @var integer
	 */
	public $workunitId;

	/**
	 *
	 * @var string
	 */
	public $workunitUid;

	/**
	 * @var People\User
	 */
	protected $user;

	/**
	 * @var string
	 */
	public $userId;

	/**
	 * @var string
	 */
	public $userUid;

	/**
	 *
	 * @var string
	 */
	protected $message;

	/**
	 * @return Marker
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->setId(null);
		$obj->setUid(uniqid());
		$obj->saved = false;
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['userId'])) ? $this->userId=$properties['userId'] : null;
		(isset($properties['userUid'])) ? $this->userUid=$properties['userUid'] : null;
		(isset($properties['workunitId'])) ? $this->workunitId=$properties['workunitId'] : null;
		(isset($properties['workunitUid'])) ? $this->workunitUid=$properties['workunitUid'] : null;
		(isset($properties['message'])) ? $this->message=$properties['message'] : null;
		return $this;
	}

	/**
	 * @param integer $workunitId
	 * @param string $userId
	 * @param string $message
	 */
	public function open($workunitId, $userId, $message)
	{
		$this->workunitId = $workunitId;
		$this->userId = $userId;
		$this->message = $message;
		return $this;
	}

	/**
	 * @param boolean $asId
	 * @return People\User
	 */
	public function getUser($asId=false)
	{
		if($asId){
			return $this->userId;
		}
		return $this->user;
	}

	/**
	 * @param boolean $asId
	 * @return \Wumanager\Model\Workunit
	 */
	public function getWorkunit($asId=false)
	{
		if($asId){
			return $this->workunitId;
		}
		return $this->workunit;
	}

	/**
	 * @param People\User $user
	 * @return Marker
	 */
	public function setUser($user)
	{
		$this->user = $user;
		$this->userId = $user->getId;
		$this->userUid = $user->getUid;
		return $this;
	}

	/**
	 * @param \Wumanager\Model\Workunit $wu
	 * @return Marker
	 */
	public function setWorkunit($wu)
	{
		$this->workunit = $wu;
		$this->workunitId = $wu->getId;
		$this->workunitUid = $wu->getUid;
		return $this;
	}

	/**
	 * @param string $string
	 * @return Marker
	 */
	public function setMessage($string)
	{
		$this->message = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

}
