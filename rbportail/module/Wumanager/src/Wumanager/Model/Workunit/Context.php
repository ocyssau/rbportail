<?php
//%LICENCE_HEADER%

namespace Wumanager\Model\Workunit;

use Wumanager\Model\Any;

class Context extends Any
{
	static $classId = 307;
	
	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);
		return $this;
	}
	
}
