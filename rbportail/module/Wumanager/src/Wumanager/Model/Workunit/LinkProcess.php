<?php
//%LICENCE_HEADER%
namespace Wumanager\Model\Workunit;

/**
 * 
 *
 */
class LinkProcess extends \Application\Model\Link
{

	/**
	 * @var string
	 */
	public static $classId = 309;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return LinkProcess
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['childId'])) ? $this->childId = $properties['childId'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		return $this;
	}
}
