<?php
// %LICENCE_HEADER%
namespace Wumanager\Model\Workunit;

/**
 *
 */
class Hasher
{

	/**
	 *
	 * @param \Wumanager\Model\Workunit $any
	 * @return string
	 */
	public static function hash($any)
	{
		$involved = array(
			'name',
			'deliveryMemo',
			'tos',
			'ataLeader',
			'issue',
			'quantity',
			'deliveryDate',
			'sitedeliveryDate',
			'planDate',
			'supplierBillingDate',
			'siteUid'
		);
		$datas = $any->getArrayCopy();

		$str = '';
		foreach( $involved as $p ) {
			$str .= $datas[$p];
		}
		return md5($str);
	}
}
