<?php
// %LICENCE_HEADER%
namespace Wumanager\Model\Workunit;

use Wumanager\Model\Any;

/**
 * 
 *
 */
class Type extends Any
{

	static $classId = 301;

	/** @var integer */
	public $context;

	/** @var float */	
	public $tuProduction;

	/** @var float */	
	public $tuManagement;

	/** @var float */	
	public $tuProductionTlse;

	/** @var float */	
	public $tuManagementTlse;

	/** @var float */	
	public $amount;

	/** @var float */	
	public $salesRate;

	/** @var integer */
	public $dimension;

	/** @var float */
	public $costSolintep;

	/** @var float */
	public $costGeometric;
	
	/** @var float */
	public $newCostGeometric;
	
	/**
	 *
	 * @param array $properties        	
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['context'])) ? $this->context = $properties['context'] : null;
		(isset($properties['tuProduction'])) ? $this->tuProduction = $properties['tuProduction'] : null;
		(isset($properties['tuManagement'])) ? $this->tuManagement = $properties['tuManagement'] : null;
		(isset($properties['tuProductionTlse'])) ? $this->tuProductionTlse = $properties['tuProductionTlse'] : null;
		(isset($properties['tuManagementTlse'])) ? $this->tuManagementTlse = $properties['tuManagementTlse'] : null;
		(isset($properties['amount'])) ? $this->amount = $properties['amount'] : null;
		(isset($properties['salesRate'])) ? $this->salesRate = $properties['salesRate'] : null;
		(isset($properties['dimension'])) ? $this->dimension = $properties['dimension'] : null;
		(isset($properties['costSolintep'])) ? $this->costSolintep = $properties['costSolintep'] : null;
		(isset($properties['costGeometric'])) ? $this->costGeometric = $properties['costGeometric'] : null;
		(isset($properties['newCostGeometric'])) ? $this->newCostGeometric= $properties['newCostGeometric'] : null;
		return $this;
	}
}
