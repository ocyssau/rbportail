<?php
//%LICENCE_HEADER%

namespace Wumanager\Model;

use DateTime;

/**
 *
 *
 */
class Deliverable extends Any
{
	/**
	 *
	 * @var string
	 */
	static $classId = 304;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 *
	 * @var integer
	 */
	public $quantity=1;
	
	/**
	 *
	 * @var string
	 */
	public $section;
	
	/**
	 *
	 * @var string
	 */
	public $circuit;
	
	/**
	 *
	 * @var string
	 */
	public $comment;
	
	/**
	 *
	 * @var string
	 */
	public $projeteur;
	
	/**
	 *
	 * @var string
	 */
	public $protectionCode;
	
	/**
	 *
	 * @var string
	 */
	public $beCode;
	
	/**
	 *
	 * @var string
	 */
	public $functionalClass;
	
	/**
	 *
	 * @var string
	 */
	public $drawingType;
	
	/**
	 *
	 * @var string
	 */
	public $configItem;
	
	/**
	 *
	 * @var string
	 */
	public $layout;
	
	/**
	 *
	 * @var string
	 */
	public $mod;
	
	/**
	 *
	 * @var string
	 */
	public $team;
	
	/**
	 *
	 * @var string
	 */
	public $revision;
	
	/**
	 *
	 * @var string
	 */
	public $gildaStatus;
	
	/**
	 *
	 * @var \DateTime
	 */
	public $officialDate;
	
	/**
	 *
	 * @var \DateTime
	 */
	public $compilerDate;
	
	/**
	 *
	 * @var string
	 */
	public $indice;
	
	/**
	 *
	 * @var string
	 */
	public $codeFiliere;
	
	/**
	 *
	 * @var string
	 */
	public $codeMer;
	
	/**
	 *
	 * @var string
	 */
	public $map;
	
	/**
	 *
	 * @var string
	 */
	public $ds;
	
	/**
	 *
	 * @var string
	 */
	public $ci;
	
	/**
	 *
	 * @var string
	 */
	public $msn;
	
	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);

		//common
		(isset($properties['status'])) ? $this->status = $properties['status'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['description'])) ? $this->title = $properties['description'] : null;
		(isset($properties['status'])) ? $this->setStatus = $properties['status'] : null;
		(isset($properties['section'])) ? $this->section = $properties['section'] : null;
		(isset($properties['circuit'])) ? $this->circuit = $properties['circuit'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		(isset($properties['projeteur'])) ? $this->projeteur = $properties['projeteur'] : null;
		(isset($properties['protectionCode'])) ? $this->protectionCode = $properties['protectionCode'] : null;
		(isset($properties['beCode'])) ? $this->beCode = $properties['beCode'] : null;
		(isset($properties['functionalClass'])) ? $this->functionalClass = $properties['functionalClass'] : null;
		(isset($properties['drawingType'])) ? $this->drawingType = $properties['drawingType'] : null;
		(isset($properties['configItem'])) ? $this->configItem = $properties['configItem'] : null;
		(isset($properties['layout'])) ? $this->layout = $properties['layout'] : null;
		(isset($properties['mod'])) ? $this->mod = $properties['mod'] : null;
		(isset($properties['team'])) ? $this->team = $properties['team'] : null;
		(isset($properties['revision'])) ? $this->revision = $properties['revision'] : null;
		(isset($properties['gildaStatus'])) ? $this->gildaStatus = $properties['gildaStatus'] : null;

		//date
		(isset($properties['officialDate'])) ? $this->setOfficialDate($properties['officialDate']) : null;
		(isset($properties['compilerDate'])) ? $this->setCompilerDate($properties['compilerDate']) : null;
		
		//3d
		(isset($properties['ci'])) ? $this->ci = $properties['ci'] : null;
		(isset($properties['msn'])) ? $this->msn = $properties['msn'] : null;
		
		//2d
		(isset($properties['indice'])) ? $this->indice = $properties['indice'] : null;
		(isset($properties['codeFiliere'])) ? $this->codeFiliere = $properties['codeFiliere'] : null;
		(isset($properties['codeMer'])) ? $this->codeMer = $properties['codeMer'] : null;
		(isset($properties['map'])) ? $this->map = $properties['map'] : null;
		(isset($properties['ds'])) ? $this->ds = $properties['ds'] : null;
		(isset($properties['quantity'])) ? $this->quantity = (int)$properties['quantity'] : null;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setOfficialDate($date)
	{
		if($date=='0000-00-00' OR !$date){
			$this->officialDate = null;
		}
		elseif ( $date instanceof DateTime ) {
			$this->officialDate = $date;
		}
		else{
			$this->officialDate = new DateTime($date);
		}
		return $this;
	}

	/**
	 *
	 * @return DateTime | NULL
	 */
	public function getOfficialDate($format = null)
	{
		if ( $this->officialDate instanceof DateTime ) {
			if ( $format ) {
				return $this->officialDate->format($format);
			}
			else {
				return $this->officialDate;
			}
		}
		else return null;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setCompilerDate($date)
	{
		if($date=='0000-00-00' OR !$date){
			$this->compilerDate = null;
		}
		elseif ( $date instanceof DateTime ) {
			$this->compilerDate = $date;
		}
		else{
			$this->compilerDate = new DateTime($date);
		}
		return $this;
	}

	/**
	 *
	 * @return DateTime | NULL
	 */
	public function getCompilerDate($format = null)
	{
		if ( $this->compilerDate instanceof DateTime ) {
			if ( $format ) {
				return $this->compilerDate->format($format);
			}
			else {
				return $this->compilerDate;
			}
		}
		else return null;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string
	 */
	public function setStatus( $string )
	{
		$this->status = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getProjeteur()
	{
		return $this->projeteur;
	}

	/**
	 * @param string
	 */
	public function setProjeteur( $string )
	{
		$this->projeteur = $string;
		return $this;
	}

}
