<?php
//%LICENCE_HEADER%

namespace Wumanager\Model;

/**
 * @brief Define a package of constants to set error code in Rbplm.
 *
 */
Class Error{
    const ERROR = E_USER_ERROR;
    const WARNING = E_USER_WARNING;
    const NOTICE = E_USER_NOTICE;
}
