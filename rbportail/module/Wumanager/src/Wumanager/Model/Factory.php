<?php
//%LICENCE_HEADER%
namespace Wumanager\Model;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{

	/**
	 * Registry of instanciated DAO.
	 *
	 * @var array
	 */
	private static $_registry = array();

	/**
	 * Assiciate for each Component Class ID a DAO CLASS
	 * ClassId 200 return the super class DAO Component/Component
	 * @var array
	 */
	private static $_map = array(
		300 => array(
			'Workunit',
			'workunit'
		),
		301 => array(
			'Workunit\Type',
			'workunit_type'
		),
		302 => array(
			'Status',
			'wumanager_status'
		),
		303 => array(
			'Site',
			'site'
		),
		304 => array(
			'Deliverable',
			'deliverable'
		),
		305 => array(
			'Billnote',
			'bill_note'
		),
		306 => array(
			'Bill',
			'bill'
		),
		307 => array(
			'Context',
			'workunit_context'
		),
		308 => array(
			'Workunit\LinkTypeContext',
			'link_workunit_type_context'
		),
		309 => array(
			'Workunit\LinkProcess',
			'workunit_process'
		)
	);

	/**
	 * Return a instance of class specified by id.
	 * 
	 * @param string $classId
	 * @return \Application\Dao\Dao
	 */
	public static function get($classId)
	{
		$class = __NAMESPACE__ . '\\' . self::$_map[$classId][0];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @param string $classId
	 * @return \Application\Dao\Dao
	 */
	public static function getNew($classId)
	{
		$class = __NAMESPACE__ . '\\' . self::$_map[$id][0];
		return $class::init();
	}
}
