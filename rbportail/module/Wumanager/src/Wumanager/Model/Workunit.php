<?php
//%LICENCE_HEADER%
namespace Wumanager\Model;

use DateTime;
use Wumanager\Model\Workunit\Type;
use Workflow\Model\Wf\Process;

/**
 * 
 *
 */
class Workunit extends Any
{

	/**
	 * 
	 * @var string
	 */
	static $classId = 300;

	/**
	 * @var array
	 */
	protected $deliverables = array();

	/**
	 * @var Workunit\Type
	 */
	protected $type;

	/**
	 * @var integer
	 */
	protected $typeId;

	/**
	 * @var DateTime
	 */
	protected $openDate;

	/**
	 * @var DateTime
	 */
	protected $planDate;

	/**
	 * @var DateTime
	 */
	protected $deliveryDate;

	/**
	 * @var DateTime
	 */
	protected $lastDeliveryDate;

	/**
	 * @var DateTime
	 */
	protected $sitedeliveryDate;

	/**
	 *
	 * @var DateTime
	 */
	protected $supplierBillingDate;

	/**
	 * @var string
	 */
	protected $tos;

	protected $tosId;

	/**
	 * @var string
	 */
	protected $flt;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var string
	 */
	protected $issue;

	/**
	 * @var string
	 */
	protected $number;

	/**
	 * @var string
	 */
	protected $deliveryMemo;

	/**
	 * @var Site
	 */
	protected $site;

	protected $siteUid;
	
	/**
	 * @var Process
	 */
	protected $process;
	
	/**
	 * 
	 * @var integer
	 */
	public $processId;

	/**
	 * @var string
	 */
	protected $comment;

	/**
	 * Set the value of visibility :
	 * private
	 * internal
	 * public
	 *
	 * @var string
	 */
	protected $visibility;

	/**
	 * Progression of wu
	 * @var float
	 */
	protected $progression = 0;

	/**
	 * Ata Leader Full Name
	 * @var string
	 */
	protected $ataLeader;

	/**
	 *
	 * @var integer
	 */
	protected $quantity = 1;

	/**
	 *
	 * @var integer
	 */
	protected $version = 1;

	/**
	 *
	 * @var integer
	 */
	protected $iteration = 1;

	/**
	 * @param array $properties
	 * @return Workunit
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		
		/* literals */
		(isset($properties['flt'])) ? $this->flt = $properties['flt'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['deliveryMemo'])) ? $this->deliveryMemo = $properties['deliveryMemo'] : null;
		(isset($properties['tos'])) ? $this->tos = $properties['tos'] : null;
		(isset($properties['status'])) ? $this->status = $properties['status'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		(isset($properties['visibility'])) ? $this->visibility = $properties['visibility'] : null;
		(isset($properties['progression'])) ? $this->progression = $properties['progression'] : null;
		(isset($properties['ataLeader'])) ? $this->ataLeader = $properties['ataLeader'] : null;
		(isset($properties['issue'])) ? $this->issue = $properties['issue'] : null;
		(isset($properties['quantity'])) ? $this->quantity = (int)$properties['quantity'] : null;
		(isset($properties['iteration'])) ? $this->iteration = (int)$properties['iteration'] : null;
		(isset($properties['version'])) ? $this->version = (int)$properties['version'] : null;
		
		/* dates */
		if ( isset($properties['deliveryDate']) ) {
			if ( $properties['deliveryDate'] != null ) {
				if ( $properties['deliveryDate'] instanceof \DateTime ) {
					$this->setDeliverydate($properties['deliveryDate']);
				}
				else {
					$this->setDeliverydate(new DateTime($properties['deliveryDate']));
				}
			}
			else {
				$this->setDeliverydate(null);
			}
		}
		if ( isset($properties['lastDeliveryDate']) ) {
			if ( $properties['lastDeliveryDate'] != null ) {
				if ( $properties['lastDeliveryDate'] instanceof \DateTime ) {
					$this->setLastDeliverydate($properties['lastDeliveryDate']);
				}
				else {
					$this->setLastDeliverydate(new DateTime($properties['lastDeliveryDate']));
				}
			}
			else {
				$this->setLastDeliverydate(null);
			}
		}
		if ( isset($properties['sitedeliveryDate']) ) {
			if ( $properties['sitedeliveryDate'] != null ) {
				if ( $properties['sitedeliveryDate'] instanceof \DateTime ) {
					$this->setSitedeliverydate($properties['sitedeliveryDate']);
				}
				else {
					$this->setSitedeliverydate(new DateTime($properties['sitedeliveryDate']));
				}
			}
			else {
				$this->setSitedeliverydate(null);
			}
		}
		if ( isset($properties['planDate']) ) {
			if ( $properties['planDate'] != null ) {
				if ( $properties['planDate'] instanceof \DateTime ) {
					$this->setPlandate($properties['planDate']);
				}
				else {
					$this->setPlandate(new DateTime($properties['planDate']));
				}
			}
			else {
				$this->setPlandate(null);
			}
		}
		if ( isset($properties['openDate']) ) {
			if ( $properties['openDate'] != null ) {
				if ( $properties['openDate'] instanceof \DateTime ) {
					$this->setOpendate($properties['openDate']);
				}
				else {
					$this->setOpendate(new DateTime($properties['openDate']));
				}
			}
			else {
				$this->setOpendate(null);
			}
		}
		if ( isset($properties['supplierBillingDate']) ) {
			if ( $properties['supplierBillingDate'] != null ) {
				if ( $properties['supplierBillingDate'] instanceof \DateTime ) {
					$this->setSupplierBillingDate($properties['supplierBillingDate']);
				}
				else {
					$this->setSupplierBillingDate(new DateTime($properties['supplierBillingDate']));
				}
			}
			else {
				$this->setSupplierBillingDate(null);
			}
		}
		
		/* one to one */
		(isset($properties['typeId'])) ? $this->typeId = $properties['typeId'] : null;
		(isset($properties['siteUid'])) ? $this->siteUid = $properties['siteUid'] : null;
		(isset($properties['processId'])) ? $this->processId = $properties['processId'] : null;
		
		/* one to many */
		(isset($properties['deliverables'])) ? $this->setDeliverables($properties['deliverables']) : null;
		
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @param string $string
	 * @return Workunit
	 */
	public function setComment($string)
	{
		$this->comment = $string;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getDeliverydate($format = null)
	{
		if ( $this->deliveryDate && $format ) {
			return $this->deliveryDate->format($format);
		}
		elseif ( $this->deliveryDate ) {
			return $this->deliveryDate;
		}
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setDeliverydate($date)
	{
		$this->deliveryDate = $date;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getLastDeliverydate($format = null)
	{
		if ( $this->lastDeliveryDate && $format ) {
			return $this->lastDeliveryDate->format($format);
		}
		elseif ( $this->lastDeliveryDate ) {
			return $this->lastDeliveryDate;
		}
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setLastDeliverydate($date)
	{
		$this->lastDeliveryDate = $date;
		return $this;
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setSitedeliverydate($date)
	{
		$this->sitedeliveryDate = $date;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getSitedeliverydate($format = null)
	{
		if ( $this->sitedeliveryDate && $format ) {
			return $this->sitedeliveryDate->format($format);
		}
		elseif ( $this->sitedeliveryDate ) {
			return $this->sitedeliveryDate;
		}
	}

	/**
	 * @return string
	 */
	public function getDeliverymemo()
	{
		return $this->deliveryMemo;
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setDeliverymemo($string)
	{
		$this->deliveryMemo = $string;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getOpendate($format = null)
	{
		if ( $this->openDate && $format ) {
			return $this->openDate->format($format);
		}
		elseif ( $this->openDate ) {
			return $this->openDate;
		}
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setOpendate($date)
	{
		$this->openDate = $date;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getPlandate($format = null)
	{
		if ( $this->planDate && $format ) {
			return $this->planDate->format($format);
		}
		elseif ( $this->planDate ) {
			return $this->planDate;
		}
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setPlandate($date)
	{
		$this->planDate = $date;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getSupplierBillingDate($format = null)
	{
		if ( $this->supplierBillingDate && $format ) {
			return $this->supplierBillingDate->format($format);
		}
		elseif ( $this->supplierBillingDate ) {
			return $this->supplierBillingDate;
		}
	}

	/**
	 * @param DateTime
	 * @return Workunit
	 */
	public function setSupplierBillingDate($date)
	{
		$this->supplierBillingDate = $date;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSite($asId = false)
	{
		if ( $asId ) {
			return $this->siteUid;
		}
		else {
			return $this->site;
		}
	}

	/**
	 * @param Site|int
	 * @return Workunit
	 */
	public function setSite($any)
	{
		if ( $any instanceof Site ) {
			$this->site = $any;
			$this->siteUid = $any->getUid();
		}
		else {
			$this->siteUid = $any;
		}
		return $this;
	}

	/**
	 * @return Process
	 */
	public function getProcess($asId = false)
	{
		if ( $asId ) {
			return $this->processId;
		}
		else {
			return $this->process;
		}
	}

	/**
	 * @param Site|int
	 * @return Workunit
	 */
	public function setProcess($any)
	{
		if ( $any instanceof Process ) {
			$this->process = $any;
			$this->processId = $any->getId();
		}
		else {
			$this->processId = $any;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setStatus($string)
	{
		$this->status = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setNumber($string)
	{
		$this->number = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIssue()
	{
		return $this->issue;
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setIssue($string)
	{
		$this->issue = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFlt()
	{
		return $this->flt;
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setFlt($string)
	{
		$this->flt = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTos($asId = false)
	{
		if ( $asId ) {
			return $this->tos;
		}
		else {
			return $this->tos;
		}
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setTos($string)
	{
		$this->tos = $string;
		return $this;
	}

	/**
	 * @return Type
	 */
	public function getType($asId = false)
	{
		if ( $asId ) {
			return $this->typeId;
		}
		else {
			if ( !$this->type ) {
				$this->type = new Type();
			}
			return $this->type;
		}
	}

	/**
	 * @param string
	 * @return Workunit
	 */
	public function setType($any)
	{
		if ( $any instanceof Type ) {
			$this->type = $any;
			$this->typeId = $any->getId();
		}
		else {
			$this->typeId = $any;
		}
		return $this;
	}

	/**
	 * @param array
	 * @return Workunit
	 */
	public function setDeliverables($array)
	{
		$this->deliverables = $array;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getDeliverables()
	{
		if ( !$this->deliverables ) {
			$this->deliverables = array();
		}
		return $this->deliverables;
	}

	/**
	 * @param float $float
	 * @return Workunit
	 */
	public function setProgression($float)
	{
		$this->progression = $float;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getProgression()
	{
		return $this->progression;
	}

	/**
	 * @param string $string
	 * @return Workunit
	 */
	public function setVisibility($string)
	{
		$this->visibility = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVisibility()
	{
		return $this->visibility;
	}

	/**
	 * @param string $string
	 * @return Workunit
	 */
	public function setAtaLeader($string)
	{
		$this->ataLeader = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAtaLeader()
	{
		return $this->ataLeader;
	}

	/**
	 * @return string
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param integer
	 * @return Workunit
	 */
	public function setQuantity($int)
	{
		$this->quantity = (int)$int;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVersion()
	{
		return $this->version;
	}

	/**
	 * @param integer
	 * @return Workunit
	 */
	public function setVersion($int)
	{
		$this->version = (int)$int;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIteration()
	{
		return $this->iteration;
	}

	/**
	 * @param integer
	 * @return Workunit
	 */
	public function setIteration($int)
	{
		$this->iteration = (int)$int;
		return $this;
	}
}
