<?php 
//%LICENCE_HEADER%

namespace Wumanager\Model\Wf\Activity;

use Workflow\Model\Wf;
use Application\Model\Filesystem;
use Zend\Code\Reflection;
use Zend\Code\Generator;

/**
 * 
 *
 */
class Code
{
	/**
	 * @var string
	 */
	protected $basePath;
	
	/**
	 * @var string
	 */
	protected $templateBasePath;
	
	/**
	 * 
	 * @var string
	 */
	protected $namespace;
	
	/**
	 * @var string
	 */
	protected $modelClassName;
	
	/**
	 * @var string
	 */
	protected $modelClassFile;
	
	/**
	 * @var string
	 */
	protected $formClassName;
	
	/**
	 * @var string
	 */
	protected $formClassFile;
	
	/**
	 * @var string
	 */
	protected $templateFile;
	
	/**
	 * @var \Workflow\Model\Wf\Activity
	 */
	protected $activity;
	
	/**
	 * @var \Workflow\Model\Wf\Activity
	 */
	protected $process;
	
	/**
	 */
	public function __construct(Wf\Instance $instance, Wf\Activity $activity)
	{
		$this->instance = $instance;
		$this->activity = $activity;
		$this->basePath = \Wumanager\Model\Wf\Code::getBasePath($instance);
		$this->templateBasePath = \Wumanager\Model\Wf\Code::getTemplatePath($instance);
		
		$this->modelClassFile = $this->basePath . '/' . $activity->getNormalizedName() . '.php';
		$this->templateFile = $this->templateBasePath . '/' . $activity->getNormalizedName() . '.phtml';
		
		$modelClassName = substr($this->modelClassFile, strlen(\Wumanager\Model\Wf\Code::$processPath), -4);
		$this->namespace = str_replace('/', '\\', trim( dirname($modelClassName), '/'));
		$this->modelClassName = str_replace('/', '\\', $modelClassName);
	}
	
	/**
	 */
	public function generateModelCode()
	{
		$classFile = $this->modelClassFile;
		
		if(!is_file($classFile)){
			$activityName = $this->activity->getNormalizedName();
			$processName = $this->instance->getNormalizedName();
			$currentUserName = '';
			$namespace = $this->namespace;
			
		    touch($classFile);
		    
			$class = Generator\ClassGenerator::fromReflection(
			    new Reflection\ClassReflection('Workflow\Prototype\ActivityModel')
			);
			$docblock = new Generator\DocBlockGenerator();
			$docblock->setShortDescription('A class for activity '.$activityName);
			$date = new \DateTime();
			
			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('package')->setDescription($processName));
			
			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('generated')->setDescription($date->format(\DateTime::ISO8601)));
			
			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('author')->setDescription($currentUserName));
			
			$class->setName($activityName)
					->setDocblock($docblock)
					->setExtendedClass('Workflow\ActivityTrigger')
					->addUse('Application\Dao')
					->setNamespaceName($namespace);
			
			$file = new Generator\FileGenerator();
			file_put_contents($classFile, $file->setClass($class)->generate());
		}
		
	}
	
	/**
	 * @return string
	 */
	public function getModelFile()
	{
		return $this->modelClassFile;
	}
	
	/**
	 * @return string
	 */
	public function getModelClass()
	{
	    return $this->modelClassName;
	}
	
	/**
	 * @return string
	 */
	public function getFormFile()
	{
	    return $this->formClassFile;
	}
	
	/**
	 * @return string
	 */
	public function getFormClass()
	{
	    return $this->formClassName;
	}
	
	/**
	 * @return string
	 */
	public function getTemplateFile()
	{
	    return $this->templateFile;
	}
	
	/**
	 * @return string
	 */
	public function getTemplate()
	{
	    return $this->templateFile;
	}
	
	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string	$type
	 * @return array | string
	 */
	public function getFolders()
	{
	    if( !$this->folders ){
	        $this->folders = array(
	            'form' => dirname($this->formClassFile),
	            'model' => dirname($this->modelClassFile),
	            'view' => dirname($this->templateFile),
	        );
	    }
        return $this->folders;
	}
	
	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string	$type
	 * @return array | string
	 */
	public function deleteFolders()
	{
	    $folders = $this->getFolders();
	    foreach($folders as $folder){
	        // Prevent a disaster
	        if (trim($folder) == '/' || trim($folder) == '.' || trim($folder) == 'templates' || trim($folder) == 'templates/'){
	            return false;
	        }
	        Filesystem\Filesystem::removeDirectory($folder, true);
	    }
	    return true;
	}
}
