<?php
namespace Wumanager\Model\Wf\Prototype;

/**
 * Model class to trigger the activity
 * @author olivier
 *
 */
class ActivityModel
{

	public function trigger()
	{
		echo "trigger for activity";
	}
}
