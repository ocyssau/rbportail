<?php
namespace Wumanager\Model\Wf\Prototype;

use Zend\Form\Form;

/**
 * 
 * @author ocyssau
 *
 */
class ActivityForm extends Form
{

	/**
	 * 
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('activity-act1');
		$this->setAttribute('method', 'post');
		
		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type' => 'hidden',
				'value' => '4dcb6a55582ce745a1826edd6126cc0a'
			)
		));
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Reference',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Reference'
			)
		));
		
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
	}
}
