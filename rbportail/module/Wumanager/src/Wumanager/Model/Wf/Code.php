<?php
//%LICENCE_HEADER%
namespace Wumanager\Model\Wf;

use Application\Model\Filesystem;
use Exception;
use Application\Model\Error;
use Workflow\Model\Wf\Process;
use Workflow\Model\Wf\Activity;

/**
 * 
 * @author ocyssau
 *
 */
class Code
{

	/**
	 * Base path where to create folders for store acitivities scripts
	 *
	 * @var String
	 */
	public static $processPath = '';

	/**
	 * @var string
	 */
	protected $basePath;

	/**
	 * @var string
	 */
	protected $templateBasePath;

	/**
	 * @var \Workflow\Model\Wf\Activity
	 */
	protected $activity;

	/**
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process;

	/**
	 * @var string
	 */
	protected $modelClassName;

	/**
	 * @var string
	 */
	protected $modelClassFile;

	/**
	 * @var string
	 */
	protected $formClassName;

	/**
	 * @var string
	 */
	protected $formClassFile;

	/**
	 * @var string
	 */
	protected $templateFile;

	/**
	 */
	public function __construct(Process $process, Activity $activity = null)
	{
		$this->process = $process;
		$this->activity = $activity;
		
		$processFolderName = 'PROC' . $process->getUid();
		$processPath = self::$processPath; //where process definition is store
		$this->basePath = $processPath . '/ProcessDef/' . $processFolderName;
		$this->templateBasePath = $processPath . '/view';
		$this->baseNamespace = 'ProcessDef\\' . $processFolderName;
	}

	/**
	 * @param Process $process
	 * @return string
	 */
	public static function getBasePath(Process $process)
	{
		$processFolderName = 'PROC' . $process->getNormalizedName();
		$processPath = self::$processPath; //where process definition is store
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/ProcessDef/' . $processFolderName;
	}

	/**
	 * @param Process $process
	 * @return string
	 */
	public static function getTemplatePath(Process $process)
	{
		$processFolderName = 'PROC' . $process->getNormalizedName();
		$processPath = self::$processPath; //where process definition is store
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/ProcessDef/view/' . $processFolderName;
	}

	/**
	 * Create directories to store the classes of the activities triggers.
	 */
	public function initProcess($process)
	{
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		
		$this->folders = array(
			'form' => $this->basePath . '/Form',
			'model' => $this->basePath . '/Model',
			'view' => $this->templateBasePath . '/' . $processFolderName
		);
		
		foreach( $this->folders as $path ) {
			if ( is_dir($path) ) {
				continue;
			}
			if ( !mkdir($path, 0755, true) ) {
				throw new Exception('DIRECTORY_CREATION_FAILED :' . $path, Error::WARNING);
			}
		}
	}

	/**
	 */
	public function initActivity($processUid, $activity)
	{
		$processPath = self::$processPath;
		
		$this->modelClassName = $this->baseNamespace . '\\Model\\' . $activityUid;
		$this->modelClassFile = $processPath . '/' . str_replace('\\', '/', $this->modelClassName) . '.php';
		
		$this->formClassName = $this->baseNamespace . '\\Form\\' . $activityUid;
		$this->formClassFile = $processPath . '/' . str_replace('\\', '/', $this->formClassName) . '.php';
		
		$this->template = 'PROC' . $processUid . '/' . $activityUid;
		$this->templateFile = $this->folders['form'] . '/' . $activityUid . '.phtml';
	}

	/**
	 */
	public function buildActivityModel($activity)
	{}

	/**
	 * @return array
	 */
	public function getFiles()
	{
		return array(
			'form' => $this->formClassFile,
			'model' => $this->modelClassFile,
			'template' => $this->templateFile
		);
	}

	/**
	 * Create file need by process files definition.
	 * @return void
	 */
	public function initFile($type)
	{
		$files = $this->getFiles();
		$file = $files[$type];
		if ( !is_file($file) ) {
			touch($file);
		}
		return $file;
	}

	/**
	 * @return string
	 */
	public function getModelFile()
	{
		return $this->modelClassFile;
	}

	/**
	 * @return string
	 */
	public function getModelClass()
	{
		return $this->modelClassName;
	}

	/**
	 * @return string
	 */
	public function getFormFile()
	{
		return $this->formClassFile;
	}

	/**
	 * @return string
	 */
	public function getFormClass()
	{
		return $this->formClassName;
	}

	/**
	 * @return string
	 */
	public function getTemplateFile()
	{
		return $this->templateFile;
	}

	/**
	 * @return string
	 */
	public function getTemplate()
	{
		return $this->templateFile;
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string	$type
	 * @return array | string
	 */
	public function getFolders()
	{
		if ( !$this->folders ) {
			$this->folders = array(
				'form' => dirname($this->formClassFile),
				'model' => dirname($this->modelClassFile),
				'view' => dirname($this->templateFile)
			);
		}
		return $this->folders;
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string	$type
	 * @return array | string
	 */
	public function deleteFolders()
	{
		$folders = $this->getFolders();
		foreach( $folders as $folder ) {
			// Prevent a disaster
			if ( trim($folder) == '/' || trim($folder) == '.' || trim($folder) == 'templates' || trim($folder) == 'templates/' ) {
				return false;
			}
			Filesystem\Filesystem::removeDirectory($folder, true);
		}
		return true;
	}
}
