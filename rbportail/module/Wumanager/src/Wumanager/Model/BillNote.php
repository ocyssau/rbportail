<?php
//%LICENCE_HEADER%

namespace Wumanager\Model;

use DateTime;

class BillNote extends Any
{
	static $classId = 305;
	
	protected $emitDate;
	protected $bill;
	protected $billId;
	
	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);
		(isset($properties['billId'])) ? $this->billId=$properties['billId'] : null;
		
		if(isset($properties['emitDate'])){
			if($properties['emitDate'] != null){
				$this->setEmitdate( new DateTime($properties['emitDate']) );
			}
			else{
				$this->setEmitdate( null );
			}
		}
		
		return $this;
	} //End of function
	
	/**
	 * @return DateTime
	 */
	public function getEmitdate()
	{
		return $this->emitDate;
	} //End of function
	
	/**
	 * @param DateTime
	 */
	public function setEmitdate( $date )
	{
		$this->emitDate = $date;
		return $this;
	} //End of function
	
	
	/**
	 * @return string
	 */
	public function getBill()
	{
		return $this->bill;
	} //End of function
	
	/**
	 * @param string
	 */
	public function setBillId( $string )
	{
		$this->billId = $string;
		return $this;
	} //End of function
}
