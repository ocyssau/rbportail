<?php
namespace Wumanager\Model;

/**
 * 
 *
 */
class Wumanager
{
	/**
	 * @var integer
	 */
	public static $classId = 2;
	public static $id = 'wumanager';
	public static $version = '08oct2015';
	
	/**
	 * 
	 * @return string
	 */
	public static function getCopyright()
	{
		$copyright = '&copy; 2015 - '.date('Y').' By Olivier CYSSAU pour SIER - All rights reserved.';
		return $copyright;
	}
	
	/**
	 * 
	 * @return string
	 */
	public static function getVersion()
	{
		return self::$version;
	}
	
	/**
	 * 
	 * @return string
	 */
	public static function getId()
	{
		return self::$id;
	}
}
