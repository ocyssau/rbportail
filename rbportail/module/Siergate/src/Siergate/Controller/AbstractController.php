<?php
namespace Siergate\Controller;

use Application\Model\People;
use Siergate\Model\Acl\Siergate as Acl;
use Siergate\Model\Acl\Project as ProjectAcl;

abstract class AbstractController extends \Application\Controller\AbstractController
{
	public $application='siergate';

	/**
	 * @return Acl
	 */
	public function getAcl()
	{
		$User = People\CurrentUser::get();
		if(!isset($User->siergateacl)){
			$acl = new Acl($User->appacl);
			$acl->loadRole($User);
			$User->siergateacl = $acl;
		}
		return $User->siergateacl;
	}

	/**
	 * @return Acl
	 */
	public function getProjectAcl($projectId)
	{
		$User = People\CurrentUser::get();
		if(!isset($User->projectacl)){
			$acl = new ProjectAcl($projectId, $this->getAcl());
			$acl->loadRole($User);
			$User->projectacl = $acl;
		}
		return $User->projectacl;
	}

}
