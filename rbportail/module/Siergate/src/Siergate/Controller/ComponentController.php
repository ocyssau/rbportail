<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Application\Dao\Factory as DaoFactory;
use Siergate\Model;
use Siergate\Model\Component;
use Application\Model\People;
use Siergate\Model\Acl\Siergate as AclSiergate;
use Application\Model\Filesystem;
use Application\Model\Filesystem\File;

class ComponentController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$component = new Model\Component();
		$view = new ViewModel();
		return $view;
	}

	/**
	 * Ajax method
	 * Return a json array as:
	 *
	 * {
	 * 'id':'',
	 * 'updated':'',
	 * 'updatebyid':'',
	 * 'updatebyname':''
	 * }
	 *
	 */
	protected function _saveComponent($component, array $properties)
	{
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		//populate with commons properties
		$properties['name'] = $this->params()->fromQuery('name', null);
		$properties['index'] = $this->params()->fromQuery('index', null);
		$properties['parentId'] = $this->params()->fromQuery('parentid', null);
		$properties['parentUid'] = $this->params()->fromQuery('parentuid', null);
		
		$visibility = $this->params()->fromQuery('visibility', null);
		
		//convert visibility code to visibility name
		switch (strtolower($visibility)) {
			case 'private':
				$visibility = AclSiergate::VISIBILITY_PRIVATE;
				break;
			case 'internal':
				$visibility = AclSiergate::VISIBILITY_INTERNAL;
				break;
			case 'public':
				$visibility = AclSiergate::VISIBILITY_PUBLIC;
				break;
		}
		
		$properties['visibility'] = $visibility;
		$properties['attributes']['title'] = $this->params()->fromQuery('title', '{empty}');
		$properties['attributes']['xoffset'] = $this->params()->fromQuery('xoffset', null);
		$properties['attributes']['yoffset'] = $this->params()->fromQuery('yoffset', null);
		$properties['attributes']['zindex'] = $this->params()->fromQuery('zindex', null);
		$properties['attributes']['draggable'] = $this->params()->fromQuery('draggable', null);
		$properties['attributes']['resizable'] = $this->params()->fromQuery('resizable', null);
		$properties['attributes']['resizablex'] = $this->params()->fromQuery('resizablex', null);
		$properties['attributes']['resizabley'] = $this->params()->fromQuery('resizabley', null);
		$properties['attributes']['collapsed'] = $this->params()->fromQuery('collapsed', false);
		
		$component->hydrate($properties);
		
		if ( !$component->getId() ) {
			$component->setOwner(People\CurrentUser::get());
		}
		
		$component->setUpdateBy(People\CurrentUser::get());
		$dao = DaoFactory::get()->getDao($component);
		$dao->save($component);
		
		$view->uid = $component->getUid();
		$view->id = $component->getId();
		$view->updated = $component->getUpdated();
		$view->updatedByName = People\CurrentUser::get()->getId();
		$view->updatedById = People\CurrentUser::get()->getId();
		$view->component = $component;
		return $view;
	}

	/**
	 *
	 */
	public function saveboxAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Box();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'body' => $this->params()->fromQuery('body', '{empty}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savetabAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Tab();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'label' => $this->params()->fromQuery('label', '{empty}'),
				'notice' => $this->params()->fromQuery('notice', '{empty}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savestatusboxAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Statusbox();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'body' => $this->params()->fromQuery('body', '{empty}'),
				'label' => $this->params()->fromQuery('label', '{empty}'),
				'status' => $this->params()->fromQuery('status', ''),
				'notice' => $this->params()->fromQuery('notice', '{empty}'),
				'statuslist' => $this->params()->fromQuery('statuslist', '{}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savecheckboxAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Checkbox();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'notice' => $this->params()->fromQuery('notice', '{empty}'),
				'items' => $this->params()->fromQuery('items', '{}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savefilelistAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Filelist();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'notice' => $this->params()->fromQuery('notice', '{empty}')
				//'items'=>$this->params()->fromQuery('items', '{}'),
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savesimplegridAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Simplegrid();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'notice' => $this->params()->fromQuery('notice', '{empty}'),
				'items' => $this->params()->fromQuery('items', '{}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savejalonAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Jalon();
		$dao = DaoFactory::get()->getDao($component::$classId);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'body' => $this->params()->fromQuery('body', '{empty}'),
				'label' => $this->params()->fromQuery('label', '{empty}'),
				'status' => $this->params()->fromQuery('status', ''),
				'notice' => $this->params()->fromQuery('notice', '{empty}'),
				'statuslist' => $this->params()->fromQuery('statuslist', '{}'),
				'targetdate' => $this->params()->fromQuery('targetdate', '')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savetinymceAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Tinymce();
		$dao = DaoFactory::get()->getDao($component::$classId);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'body' => $this->params()->fromQuery('body', '{empty}')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function saveindicatorAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		
		$component = new Component\Indicator();
		$dao = DaoFactory::get()->getDao($component);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'body' => $this->params()->fromQuery('body', '{empty}'),
				'label' => $this->params()->fromQuery('label', '{empty}'),
				'notice' => $this->params()->fromQuery('notice', '{empty}'),
				'value' => $this->params()->fromQuery('value', '')
			)
		);
		
		$view = $this->_saveComponent($component, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function savetabpanelAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		$notice = $this->params()->fromQuery('notice', '{empty}');
		$items = $this->params()->fromQuery('items', null);
		$items = json_decode($items);
		
		$Tabpanel = new Component\Tabpanel();
		$dao = DaoFactory::get()->getDao($Tabpanel);
		
		if ( $id ) {
			$dao->loadFromUid($Tabpanel, $uid);
			$Tabpanel->setUpdated(new \DateTime());
		}
		else {
			$Tabpanel = $Tabpanel::init();
		}
		
		$Tabpanel->setItems(array()); //reinit items
		foreach( $items as $item ) {
			$Tab = new Component\Tabpanel\Item($item->index, $item->notice, $item->label);
			$Tabpanel->addItem($Tab); //reinit items
		}
		
		//uid must be set from value set in GUI.
		$properties = array(
			'uid' => $uid,
			'attributes' => array(
				'notice' => $notice
			)
		);
		
		$view = $this->_saveComponent($Tabpanel, $properties);
		$view->setTemplate('siergate/component/save');
		return $view;
	}

	/**
	 * If $save is true, save new component in db
	 * @param string $Type
	 * @param boolean $save
	 */
	protected function _getComponent($Type)
	{
		$modelClass = 'Siergate\Model\Component\\' . $Type;
		$renderClass = 'Siergate\Model\Renderer\\' . $Type;
		$factory = DaoFactory::get();
		
		$withHeader = (int)$this->params()->fromQuery('withheader', 0);
		$id = (int)$this->params()->fromRoute('id', null);
		
		if ( $id ) {
			$component = new $modelClass();
			$factory->getDao($component)
				->setOption('withtrans', true)
				->loadFromId($component, $id);
		}
		else {
			$component = $modelClass::init();
			$request = $this->getRequest();
			$parentId = $request->getQuery('parentid', null);
			$parentUid = $request->getQuery('parentuid', null);
			$save = $request->getQuery('save', null);
			if ( $save && $parentId && $parentUid ) {
				$component->hydrate(array(
					'parentId' => $parentId,
					'parentUid' => $parentUid
				));
				$factory->getDao($component)
					->setOption('withtrans', true)
					->save($component);
			}
		}
		
		$renderer = new $renderClass($component, new ViewModel());
		$renderer->setMode($renderer::MODE_BUILD);
		$renderer->bind();
		
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		$view->addChild($renderer->render(), 'component');
		return $view;
	}

	/**
	 *
	 */
	public function getboxAction()
	{
		return $this->_getComponent('Box');
	}

	/**
	 *
	 */
	public function getindicatorAction()
	{
		return $this->_getComponent('Indicator');
	}

	/**
	 *
	 */
	public function getstatusboxAction()
	{
		return $this->_getComponent('Statusbox');
	}

	/**
	 *
	 */
	public function getjalonAction()
	{
		return $this->_getComponent('Jalon');
	}

	/**
	 *
	 */
	public function gettinymceAction()
	{
		return $this->_getComponent('Tinymce');
	}

	/**
	 *
	 */
	public function getjssorAction()
	{
		return $this->_getComponent('Jssor');
	}

	/**
	 *
	 */
	public function getsimplegridAction()
	{
		return $this->_getComponent('Simplegrid');
		
		$view = new ViewModel();
		$view->id = uniqid();
		$view->title = 'Liste Simple';
		$view->width = '100%';
		$view->height = null;
		
		$view->resizable = true;
		$view->draggable = true;
		$view->visibility = 'private';
		
		$view->headers = array(
			'titre1',
			'titre2',
			'titre3'
		);
		$view->items = array(
			array(
				'item1',
				'item2',
				'item3'
			),
			array(
				'item1',
				'item2',
				'item3'
			),
			array(
				'item1',
				'item2',
				'item3'
			)
		);
		
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		return $view;
	}

	/**
	 *
	 */
	public function getcheckboxAction()
	{
		return $this->_getComponent('Checkbox');
	}

	/**
	 *
	 */
	public function getfilelistAction()
	{
		return $this->_getComponent('Filelist');
	}

	/**
	 *
	 */
	public function gettabpanelAction()
	{
		return $this->_getComponent('Tabpanel');
	}

	/**
	 *
	 */
	public function gettabAction()
	{
		return $this->_getComponent('Tab');
	}

	/**
	 *
	 */
	public function getaccordionAction()
	{
		return $this->_getComponent('Accordion');
	}

	/**
	 * Delete a component from his uid
	 */
	public function deleteAction()
	{
		$uid = $this->params()->fromQuery('uid', null);
		$id = $this->params()->fromQuery('id', null);
		$cssCLass = $this->params()->fromQuery('cssclass', null);
		$factory = DaoFactory::get();
		
		if ( strstr($cssCLass, 'rbp-filelist') ) {
			$factory->getDao(Component\Filelist::$classId)->delete($uid);
		}
		else {
			$factory->getDao(Component\AbstractComponent::$classId)->delete($uid);
		}
		
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		return $view;
	}

	/**
	 * params{
	 *	    'space'=''
	 *	    'documentid' = '';
	 *	    'encode' = 'json';
	 *	    'files'={
	 *			'file1':{
	 *				'name':filename,
	 *				'data':{
	 *					'name':'',
	 *					'md5':$.md5(data),
	 *					'size':0,
	 *					'data':'',
	 *					'mimestype':'',
	 *					'encode':'base64'
	 *				}
	 *			}
	 *		}
	 *	}
	 *
	 */
	public function addfileAction()
	{
		$id = $this->params()->fromPost('id', null);
		$files = $this->params()->fromPost('files', null);
		//$files = json_decode($files, true); //as array
		
		$factory = DaoFactory::get();
		
		$component = new Component\Filelist();
		$factory->getDao(Component\Filelist::$classId)->loadFromId($component, $id);
		$Items = json_decode($component->getItems(), true); //as array
		
		$reposit = Filesystem\Reposit::initFromUid($component->getReposit());
		
		/*------------- parse the document from input -------------*/
		for ($i = 2, $index = 'file1'; isset($files[$index]); $index = 'file' . $i++) {
			$file1 = $files[$index];
			isset($file1['name']) ? $name = basename($file1['name']) : $name = uniqid();
			$ext = File::sGetExtension($name);
			$id = uniqid() . $ext;
			
			try {
				//Put data in Vault
				$data = base64_decode($file1['data']['data']);
				$toPath = $reposit->getPath() . '/' . $id;
				file_put_contents($toPath, $data);
				
				if ( is_file($toPath) ) {
					$Item = new Component\Filelist\Item();
					$Item->setFile($name, $toPath);
					$Items[] = $Item;
					
					$return['feedback'][] = array(
						'index' => $index,
						'fullpath' => $toPath,
						'item' => end($Items)
					);
				}
				$outfiles[] = $name;
			}
			catch( \Exception $e ) {
				$return['error'][] = array(
					'index' => $index,
					'name' => $name,
					'fullpath' => $toPath,
					'function' => __FUNCTION__,
					'code' => 5000,
					'message' => 'Error during file upload',
					'exception_code' => $e->getCode(),
					'exception_messsage' => $e->getMessage()
				);
				continue;
			}
		}
		$return['files'] = $outfiles;
		
		$component->setItems(json_encode($Items));
		$factory->getDao(Component\Filelist::$classId)->save($component);
		
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		$view->return = $return;
		
		return $view;
	}

	/**
	 * Delete a component from his uid
	 */
	public function delfileAction()
	{
		$id = $this->params()->fromQuery('componentid', null);
		$fileId = $this->params()->fromQuery('fileid', null);
		
		$factory = DaoFactory::get();
		
		$component = new Component\Filelist();
		$factory->getDao(Component\Filelist::$classId)->loadFromId($component, $id);
		$Items = json_decode($component->getItems(), true); //as array
		
		$reposit = Filesystem\Reposit::initFromUid($component->getReposit());
		
		foreach( $Items as $key => $item ) {
			if ( $item['id'] == $fileId ) {
				$file = $reposit->getPath() . '/' . basename($item['id']);
				if ( is_file($file) ) {
					unlink($file);
				}
				unset($Items[$key]);
			}
		}
		
		$component->setItems(json_encode($Items));
		$factory->getDao(Component\Filelist::$classId)->save($component);
		
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		return $view;
	}

	/**
	 * Delete a component from his uid
	 */
	public function getfileAction()
	{
		$id = $this->params()->fromQuery('componentid', null);
		$fileId = $this->params()->fromQuery('fileid', null);
		
		$factory = DaoFactory::get();
		
		$component = new Component\Filelist();
		$factory->getDao(Component\Filelist::$classId)->loadFromId($component, $id);
		$Items = json_decode($component->getItems(), true); //as array
		
		$reposit = Filesystem\Reposit::initFromUid($component->getReposit());
		
		foreach( $Items as $key => $item ) {
			if ( $item['id'] == $fileId ) {
				$filepath = $reposit->getPath() . '/' . basename($item['id']);
				if ( is_file($filepath) ) {
					$File = new File($filepath);
					$File->download();
					die();
				}
			}
		}
		
		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}
		
		return $view;
	}
}

