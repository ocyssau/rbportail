<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Application\Dao\Acl\Acl As DaoAcl;
use Siergate\Model\Acl;
use Siergate\Model\Component;
use Siergate\Model\Component\Siergate;
use Application\Model\People;
use Application\Model\People\User;

use Siergate\Controller\AbstractController;

//Forms
use Siergate\Form\SetProjectRoleForm;

class UserController extends AbstractController
{

	public function displayAction()
	{
		return $this->indexAction();
	}
	
	public function indexAction()
	{
		$view = new ViewModel();
		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$projectId = $this->params()->fromRoute('id', null);
		if($projectId == Component\Siergate::$id || $projectId==null){ //set role on module
			$acl = $this->getAcl();
			if(!$acl->hasRight($acl::RIGHT_MANAGE)){
				return $this->notauthorized();
			}
			$view->title = 'Roles For Siergate Module';
		}
		else{
			$view->projectMode = true;
			$acl = $this->getProjectAcl($projectId);
			if(!$acl->hasRight($acl::RIGHT_MANAGE)){
				return $this->notauthorized();
			}
			$view->title = 'Roles For Project';
		}
		$projectId = $acl->getMainResource()->getResourceId();
		$view->projectId = $projectId;

		$view->id = uniqid();
		$request = $this->getRequest();

		$view->headers=array(
			'Role','User'
		);

		$Dao = new DaoAcl();
		$items = $Dao->getRoleFromResourceId($projectId);
		$view->items = $items;

		$form = new SetProjectRoleForm($acl);
		$view->form = $form;

		$layout = $request->getQuery('layout', 'layout');
		$this->layout('layout/'.$layout);
		 
		return $view;
	}
	
	/**
	 * 
	 */
	public function addAction()
	{
		$request = $this->getRequest();
		$projectId = $request->getQuery('projectid');
		$roleId = $request->getQuery('roleid');
		$userId = $request->getQuery('userid');
		
		if($projectId == Siergate::$id){
			$acl = $this->getAcl();
		}
		else{
			$acl = $this->getProjectAcl($projectId);
		}
		
		if(!$acl->hasRight($acl::RIGHT_UPDATE)){
			return $this->notauthorized();
		}
		
		$Dao = new DaoAcl();
		$Dao->insert($userId,$roleId,$projectId,$acl);
		
		$this->redirect()->toRoute('sguser', array(
			'id' => $projectId,
			'layout'=>'fragment'
		));
	}
	
	/**
	 * 
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$projectId = $request->getQuery('projectid');
		$roleId = $request->getQuery('roleid');
		$userId = $request->getQuery('userid');
		
		$acl = $this->getProjectAcl($projectId);
		if(!$acl->hasRight($acl::RIGHT_DELETE)){
			return $this->notauthorized();
		}

		$Dao = new DaoAcl();
		$Dao->delete($userId,$roleId,$projectId);

		$this->redirect()->toRoute('sguser', array(
			'id' => $projectId,
			'layout'=>'fragment'
		));
	}
	
}
