<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Siergate\Dao;
use Siergate\Model;
use Siergate\Model\Component;
use Siergate\Model\Component\Project;
use Siergate\Model\Renderer;
use Siergate\Model\Acl;
use Siergate\Controller\AbstractController;

use Application\Model\People;
use Application\Dao\Acl\Acl as AclDao;
use Application\Dao\Factory as DaoFactory;
use Application\Dao\Loader;


//Forms
use Siergate\Form\ProjetForm;

class ProjectController extends AbstractController
{
	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
        return new ViewModel();
    }

    protected function getForm()
    {
    	$form = new ProjetForm();
    	return $form;
    }

    protected function getModel($init=false)
    {
    	if($init){
    		$model = Project::init();
    	}
    	else{
    		$model = new Project();
    	}
    	return $model;
    }

    /**
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function consultAction()
    {
    	$roleId = null;
    	$userId = People\CurrentUser::get()->getId();
    	$projectId = (int)$this->params()->fromRoute('id');
    	$visibility = Acl\Siergate::VISIBILITY_PUBLIC;

    	$acl = $this->getProjectAcl($projectId);
    	if(!$acl->hasRight($acl::RIGHT_CONSULT)){
    		return $this->notauthorized();
    	}

    	$project = new Project();

    	/* get role for user and project */
    	$dao = DaoFactory::get()->getDao(Acl\Siergate::$classId);

    	$role = $dao->getRole("userId='$userId' AND resourceId='$projectId'", 'roleId');
    	if($f = $role->fetch(\PDO::FETCH_OBJ)){
    		$roleId = $f->roleId;
    		$visibility = Acl\Siergate::convertRoleToVisibility($roleId);
    	}

    	DaoFactory::get()->getDao($project)->loadFromId($project, $projectId);
    	Loader::get()->loadChildren($project, "visibility >= $visibility");

    	$Renderer = new Renderer\Project($project, new ViewModel());
    	$Renderer->setMode($Renderer::MODE_VIEW);
    	$Renderer->bind();

    	$view = new ViewModel();
    	$view->addChild($Renderer->render(), 'project');

    	$this->layout('layout/consult');
    	return $view;
    }

    /**
     * @return multitype:\Application\Form\ProjetForm
     */
    public function addAction()
    {
    	$acl = $this->getAcl();
    	if(!$acl->hasRight($acl::RIGHT_MANAGE)){
    		return $this->notauthorized();
    	}

    	$form = $this->getForm();

    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$form->prepare();
    		$form->setInputFilter( $form->prepareFilters() );
    		$form->setData($request->getPost());
    		if ($form->isValid()) {
    			$Model = $this->getModel(true);
    			$Model->setOwner(People\CurrentUser::get());
    			$Model->setUpdateBy(People\CurrentUser::get());
    			$Model->setUpdated(new \DateTime());

    			//save project
    			$form->save($Model);

    			//add admin role to this project for currentUser
    			$dao = new AclDao();
    			$dao->insert(People\CurrentUser::get()->getLogin(), Acl\Siergate::ROLE_MANAGER, $Model->getId(), $acl);

    			return $this->redirect()->toRoute('index');
    		}
    	}
    	return array('form' => $form);
    }

    /**
     *
     * @return multitype:\Application\Form\ProjetForm
     */
    public function editAction()
    {
    	$id = $this->params()->fromRoute('id');

    	$acl = $this->getProjectAcl($id);
    	if(!$acl->hasRight($acl::RIGHT_UPDATE)){
    		return $this->notauthorized();
    	}

    	$request = $this->getRequest();

    	$form = $this->getForm();
    	$Model = $this->getModel(false);

    	DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);
    	$form->bind($Model);

    	if ($request->isPost()) {
    		$form->prepare();
    		$form->setInputFilter( $form->prepareFilters() );
    		$form->setData($request->getPost());
    		if ($form->isValid()) {
    			$form->save($Model);
    			$dao = DaoFactory::get()->getDao($Model)->save($Model);
    			return $this->redirect()->toRoute('index');
    		}
    	}

    	return array('form' => $form);
    }

    /**
     *
     */
    public function deleteAction()
    {
    	$projectUid = $this->params()->fromRoute('id');

    	$Model = $this->getModel(false);
    	$dao = DaoFactory::get()->getDao($Model);

    	$dao->loadFromUid($Model, $projectUid);
    	$projectId = $Model->getId();

    	$acl = $this->getProjectAcl($projectId);
    	if(!$acl->hasRight($acl::RIGHT_DELETE)){
    		return $this->notauthorized();
    	}

    	//delete project and all children component
    	$dao->delete($projectUid, $withchild=true);

    	//delete roles of the project
    	$dao = DaoFactory::get()->getDao(Acl\Siergate::$classId);
    	$dao->deleteAll("`resourceId`=$projectId");

    	return $this->redirect()->toRoute('index');
    }
}

