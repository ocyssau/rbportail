<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Siergate\Dao;
use Siergate\Model;
use Siergate\Model\Component;
use Siergate\Model\Component\Template;
use Siergate\Model\Renderer;

//Forms
use Siergate\Form\TemplateForm;

class TemplateController extends ProjectController
{
	protected function getForm()
	{
		$form = new TemplateForm();
		return $form;
	}
	
	protected function getModel($init=false)
	{
		if($init){
			$model = Template::init();
		}
		else{
			$model = new Template();
		}
		return $model;
	}
}
