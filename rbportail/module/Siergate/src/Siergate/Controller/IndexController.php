<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

use Application\Model\Acl;
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;

//models and Dao
use Siergate\Dao;
use Siergate\Model\Component;


use Siergate\Controller\AbstractController;

class IndexController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$acl = $this->getAcl();

		if($acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->manage();
		}
		elseif($acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->manage();
		}
		elseif($acl->hasRight($acl::RIGHT_CONSULT)){
			return $this->consult();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 *
	 */
	public function consult()
	{
		$view = new ViewModel();
		$view->setTemplate('siergate/index/consult');

		$userLogin = People\CurrentUser::get()->getLogin();
		$userId = People\CurrentUser::get()->getId();
		$search = $this->params()->fromQuery('search', null);

		$cid = Component\Project::$classId;
		$list = DaoFactory::get()->getList($cid);
		$sql = "SELECT comp.*, acl.userId, acl.roleId
				FROM component as comp
				LEFT OUTER JOIN acl AS acl ON acl.resourceId = comp.id
				WHERE cid=$cid AND (acl.userId='$userLogin' OR comp.ownerId='$userId')
				GROUP BY comp.id";

		if($search){
			$sql = $sql . " AND CONCAT(name,internalref,attributes) LIKE %$search%";
		}

		$list->loadFromSql($sql);
		$view->myProjects = $list;
		$view->myProjectHeaders = array('name','title');
		return $view;
	}

	/**
	 *
	 */
	public function manage()
	{
		$view = new ViewModel();
		$view->setTemplate('siergate/index/manage');

		$appAcl = People\CurrentUser::get()->appacl;
		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();
		$search = $this->params()->fromQuery('search', null);
		$factory = DaoFactory::get();

		//see all projects
		if($appAcl->hasRight($appAcl::RIGHT_ADMIN)){
			$list = $factory->getList(Component\Project::$classId);
			$filter = 'cid='.Component\Project::$classId;
			if($search){
				$filter = $filter . " AND CONCAT(name,internalref,attributes) LIKE '%$search%'";;
			}

			$list->load( $filter );
			$view->mode = 'admin';
			$view->myProjects = $list;
		}
		//see only my projects
		elseif($appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			$cid = Component\Project::$classId;
			$list = $factory->getList($cid);
			$sql = "SELECT comp.*, acl.userId AS aclUserId, acl.roleId AS aclRoleId
					FROM component as comp
					LEFT OUTER JOIN acl AS acl ON acl.resourceId = comp.id
					WHERE cid=$cid
					GROUP BY comp.id";
			//AND (acl.userId='$userLogin' OR comp.ownerId='$userId')

			if($search){
				$sql = $sql . " AND CONCAT(name,internalref,attributes) LIKE %$search%";
			}

			$list->loadFromSql($sql);

			$view->myProjects = $list;
		}
		else{
			return $this->notauthorized();
		}

		$view->myProjectHeaders = array('Name'=>'name', 'Title'=>'title', 'Affaire'=>'internalref');

		$view->templateHeaders = array('Name'=>'name', 'Title'=>'title');
		$templates = $factory->getList(Component\Template::$classId);
		$templates->load('cid='.Component\Template::$classId);
		$view->myTemplates = $templates;

		$this->layout()->filteroptions = array(
			'search'=>$search,
		);

		return $view;
	}
}
