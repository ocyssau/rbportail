<?php
namespace Siergate\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Siergate\Dao;
use Siergate\Model\Acl;
use Siergate\Model\Component\Project;
use Siergate\Model\Renderer;
use Siergate\Dao\Loader;
use Application\Dao\Factory as DaoFactory;
use Siergate\Controller\AbstractController;

/**
 *
 *
 */
class ProjectbuilderController extends AbstractController
{
    public function editAction()
    {
    	$projectId = (int)$this->params()->fromRoute('id');

    	$acl = $this->getProjectAcl($projectId);
    	if(!$acl->hasRight($acl::RIGHT_UPDATE)){
    		return $this->notauthorized();
    	}

    	$project = new Project();
    	DaoFactory::get()->getDao($project)->loadFromId($project, $projectId);
    	Loader::get()->loadChildren($project);

    	$renderer = new Renderer\Project($project, new ViewModel());
    	$renderer->setMode($renderer::MODE_BUILD);
    	$renderer->bind();

    	$view = new ViewModel();
    	$view->addChild($renderer->render(), 'project');
    	return $view;
    }
}
