<?php
namespace Siergate\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

/**
 * 
 * @author ocyssau
 *
 */
class SetProjectRoleForm extends Form
{
	protected $inputFilter;

	/**
	 * 
	 * @param \Application\Model\Acl\Acl $acl
	 */
	public function __construct(\Application\Model\Acl\Acl $acl)
	{
		/* we want to ignore the name passed */
		parent::__construct('projet');
		$this->setAttribute('method', 'post');
		
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
					'type'  => 'hidden',
				),
		));
		
		$this->add(array(
				'name' => 'user',
				'type'  => 'Text',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
					'label' => 'User',
					'value_options'=> $this->_getUserOptions(),
					'empty_option'=>'Please, choose an user'
				)
		));
		
		$this->add(array(
			'name' => 'role',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
			),
			'options' => array(
				'label' => 'Role',
				'value_options'=> $this->_getRoleOptions($acl),
				'empty_option'=>'Please, choose a role'
			)
		));
		
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Save',
						'id' => 'submitbutton',
				),
		));
	}
	
	/**
	 * @param Acl
	 * @return Array
	 */
	protected function _getRoleOptions($acl)
	{
		$ret = array();
		foreach($acl->roles as $id=>$def){
			$ret[$id]=$def[0];
		}
		return $ret;
	}
	
	/**
	 *
	 */
	protected function _getUserOptions()
	{
		return array(
			'user1',
			'user2',
			'user3'
		);
	}
	
	/**
	 * 
	 */
	public function prepareFilters(){
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
				
			$inputFilter->add($factory->createInput(array(
					'name'     => 'id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));

			$inputFilter->add($factory->createInput(array(
					'name'     => 'name',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'description',
					'required' => false,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
				
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->id);
		$this->get('name')->setValue($object->name);
		$this->get('description')->setValue($object->description);
	}
	
	/**
	 * 
	 * @param \Rbplm\Any $object
	 */
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['name'])) ? $object->name=$data['name'] : null;
		(isset($data['description'])) ? $object->description=$data['description'] : null;
	}

}
