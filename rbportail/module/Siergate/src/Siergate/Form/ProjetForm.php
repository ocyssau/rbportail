<?php
namespace Siergate\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Siergate\Model\Component\Project;
use Siergate\Model\Component\Template;
use Siergate\Dao\Loader;
use Application\Model\People;
use Application\Dao\Factory as DaoFactory;

/**
 * 
 *
 */
class ProjetForm extends Form
{
	protected $inputFilter;

	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('projet');
		$this->setAttribute('method', 'post');
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name',
				'type'  => 'Zend\Form\Element',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Name',
				),
		));

		$this->add(array(
			'name' => 'internalref',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Affaire',
			),
		));

		$this->add(array(
				'name' => 'description',
				'type'  => 'Zend\Form\Element',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Description',
				),
		));

		$this->add(array(
			'name' => 'template',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
			),
			'options' => array(
				'label' => 'From Template',
				'value_options'=> $this->_getTemplatesOptions(),
				'empty_option'=>'Please, select a template'
			),
		));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Save',
						'id' => 'submitbutton',
				),
		));
	}

	public function prepareFilters(){
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
					'name'     => 'id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));

			$inputFilter->add($factory->createInput(array(
					'name'     => 'name',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'template',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			)));

			$inputFilter->add($factory->createInput(array(
					'name'     => 'description',
					'required' => false,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->getId());
		$this->get('name')->setValue($object->getName());
		$this->get('internalref')->setValue($object->getInternalref());
		$this->get('description')->setValue($object->getTitle());
	}

	/**
	 * @param Project $project
	 */
	public function save(Project $project)
	{
		$factory = DaoFactory::get();
		$data = $this->getData();
		(isset($data['name'])) ? $project->setName($data['name']) : null;
		(isset($data['description'])) ? $project->setTitle($data['description']) : null;
		(isset($data['internalref'])) ? $project->setInternalref($data['internalref']) : null;

		if( isset($data['template']) && !empty($data['template']) ){
			$templateId=$data['template'];

			//load template
			$template = DaoFactory::get()->getDao(Template::$classId)->loadFromId(new Template(), $templateId);

			//load tree
			Loader::get()->loadChildren($template);

			//transfert children of template to project
			foreach($template->getChildren() as $child){
				$project->addChild($child);
			}

			$factory->getDao($project)->save($project);
			self::iterateCloneAndSaveComponent($project);
		}
		else{
			$factory->getDao($project)->save($project);
		}
		return $project;
	}

	/**
	 *
	 * @param \Siergate\Model\Component\AbstractComponent $parent
	 */
	protected static function iterateCloneAndSaveComponent($parent)
	{
		foreach ($parent->getChildren() as $child){
			$child->newUid(); //regen a uid and set id as null
			$child->setParent($parent, false); //re-affect parent with new uid
			$child->setOwner(People\CurrentUser::get());
			$child->setUpdateBy(People\CurrentUser::get());
			$child->setUpdated(new \DateTime());

			DaoFactory::get()->getDao($child)->save($child);
			self::iterateCloneAndSaveComponent($child);
		}
	}

	/**
	 *
	 */
	protected function _getTemplatesOptions()
	{
		$ret=array();
		$templates = DaoFactory::get()->getList(Template::$classId);
		$templates->load('cid='.Template::$classId .' OR cid='.Project::$classId);
		foreach($templates as $template){
			$ret[$template['id']]=$template['name'];
		}
		return $ret;
	}

}
