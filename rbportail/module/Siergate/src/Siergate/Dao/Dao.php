<?php
//%LICENCE_HEADER%

namespace Siergate\Dao;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
abstract class Dao extends \Application\Dao\Dao
{
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	} //End of function
} //End of class
