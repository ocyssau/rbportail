<?php
//%LICENCE_HEADER%

namespace Siergate\Dao\Component;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class Project extends Component
{
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel['internalref'] = 'internalref';
	} //End of function
	
	/**
	 * (non-PHPdoc)
	 * @see Siergate\Dao\Component.Component::bind()
	 */
	public function bind($mapped)
	{
		$array = parent::bind($mapped);
		$array['internalref'] = $mapped->getInternalref();
		return $array;
	}
} //End of class
