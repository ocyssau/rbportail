<?php
//%LICENCE_HEADER%

namespace Siergate\Dao\Component;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class Jalon extends Statusbox
{
    
    /**
     * (non-PHPdoc)
     * @see Siergate\Dao\Component.Statusbox::serialize()
     */
	protected function _serialize($mapped)
	{
		$attributes = Component::_serialize($mapped);
		$attributes = array_merge($attributes, array(
			'notice'=>$mapped->getNotice(),
			'status'=>$mapped->getStatus(),
			'label'=>$mapped->getLabel(),
			'statuslist'=>$mapped->getStatuslist(),
			'targetdate'=>$mapped->getTargetdate(),
		));
		return $attributes;
	}
} //End of class
