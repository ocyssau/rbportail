<?php
//%LICENCE_HEADER%

namespace Siergate\Dao\Component;

use Application\Model\Filesystem;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class Filelist extends Component
{
    
    /**
     * (non-PHPdoc)
     * @see Siergate\Dao\Component.Component::serialize()
     */
	protected function _serialize($mapped)
	{
		$attributes = Component::_serialize($mapped);
		$attributes = array_merge($attributes, array(
			'notice'=>$mapped->getNotice(),
			'items'=>$mapped->getItems(),
			'reposit'=>$mapped->getReposit(),
		));
		return $attributes;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Siergate\Dao.Dao::delete()
	 */
	public function delete($uid, $withChilds=true, $withTrans=true)
	{
		$Component = $this->loadFromUid(new \Siergate\Model\Component\Filelist(), $uid);
		
		//delete record in db
		parent::delete($uid, $withChilds, $withTrans);
		
		//delete reposit
		try{
			$reposit = Filesystem\Reposit::initFromUid( $Component->getReposit() );
			$reposit->delete();
		}
		catch(\Exception $e){
		}
	}

} //End of class
