<?php
//%LICENCE_HEADER%

namespace Siergate\Dao\Component;

use Siergate\Dao\Dao;

/** SQL_SCRIPT>>

CREATE TABLE component(
	`id` int NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(255) NOT NULL,
	`cid` int NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`internalref` VARCHAR(255) NOT NULL,
	`index` int NULL,
	`visibility` varchar(255) NULL,
	`ownerId` varchar(255) NULL,
	`parentId` int NULL,
	`parentUid` varchar(255) NULL,
	`updateById` varchar(255) NULL,
	`updated` datetime NULL,
	`attributes` text NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE attributes(
    id int NOT NULL,
    title varchar(255),
    body varchar(255),
    xoffset varchar(255),
    yoffset varchar(255),
    width varchar(255),
    height varchar(255),
    resizable boolean,
    resizablex boolean,
    resizabley boolean,
    draggable boolean,
    zindex integer,
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE component ADD UNIQUE (uid);

ALTER TABLE `component` ADD INDEX `COMPONENT_INDEX` (`index` ASC);
ALTER TABLE `component` ADD INDEX `COMPONENT_VISIBILITY` (`visibility` ASC);
ALTER TABLE `component` ADD INDEX `COMPONENT_PARENTID` (`parentId` ASC);
ALTER TABLE `component` ADD INDEX `COMPONENT_PARENTUID` (`parentUid` ASC);
ALTER TABLE `component` ADD INDEX `COMPONENT_CLASSID` (`cid` ASC);
ALTER TABLE `component` ADD INDEX `COMPONENT_INTERNALREF` (`internalref` ASC);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class Component extends Dao
{
	/**
	 * @var string
	 */
	public static $table='component';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
	    'id'=>'id',
	    'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'index'=>'index',
		'visibility'=>'visibility',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updateById'=>'updateById',
		'updated'=>'updated',
		'attributes'=>'attributes',
	);
	
	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	} //End of function
	
	/**
	 * get properties from mapped object to put in db.
	 * Return a array with properties of mapped object to save in db.
	 * 
	 * @param \Siergate\Model\Component\AbstractComponent $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
			':index'=>$mapped->getIndex(),
			':visibility'=>$mapped->getVisibility(),
			':ownerId'=>$mapped->getOwner(true),
			':parentId'=>$mapped->getParent(true),
			':parentUid'=>$mapped->getParentUid(),
			':updateById'=>$mapped->getUpdateBy(true),
			':updated'=>$mapped->getUpdated()->format(self::DATE_FORMAT),
			':attributes'=>json_encode($this->_serialize($mapped)),
		);
	}
	
	/**
	 * Serialize attributes
	 * 
	 * @param \Siergate\Model\Component\AbstractComponent $mapped
	 * @return array
	 */
	protected function _serialize($mapped)
	{
		$attributes = array(
			'title'=>$mapped->getTitle(),
			'body'=>$mapped->getBody(),
			'xoffset'=>$mapped->getXoffset(),
			'yoffset'=>$mapped->getYoffset(),
			'width'=>$mapped->getWidth(),
			'height'=>$mapped->getHeight(),
			'resizable'=>$mapped->isResizable(),
			'resizablex'=>$mapped->isResizablex(),
			'resizabley'=>$mapped->isResizabley(),
			'draggable'=>$mapped->isDraggable(),
			'zindex'=>$mapped->getZindex(),
			'collapsed'=>$mapped->isCollapsed()
			);
		return $attributes;
	}
	
} //End of class



