<?php
//%LICENCE_HEADER%

namespace Siergate\Dao\Component;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class Statusbox extends Component
{
    
    /**
     * (non-PHPdoc)
     * @see Siergate\Dao\Component.Component::serialize()
     */
	protected function _serialize($mapped)
	{
		$attributes = Component::_serialize($mapped);
		$attributes = array_merge($attributes, array(
			'notice'=>$mapped->getNotice(),
			'status'=>$mapped->getStatus(),
			'label'=>$mapped->getLabel(),
			'statuslist'=>$mapped->getStatuslist(),
		));
		return $attributes;
	}
} //End of class
