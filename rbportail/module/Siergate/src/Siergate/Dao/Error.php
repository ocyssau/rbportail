<?php
//%LICENCE_HEADER%

namespace Siergate\Dao;

/**
 * @brief Define a package of constants to set error code in Rbplm.
 *
 */
Class Error extends \Application\Dao\Error
{
}
