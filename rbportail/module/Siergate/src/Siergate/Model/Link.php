<?php
//%LICENCE_HEADER%

namespace Siergate\Model;

/**
 * 
 *
 */
class Link
{
	public static $classId = 442;
	
	public $id = null;
	public $uid = null;
	public $cid = null;
	
	public $name = null;
	public $index = 0;
	public $attributes = null;
	
	public $parentId = null;
	public $parentUid = null;
	
	public $childId = null;
	public $childUid = null;

	public $level = false;
	public $path = false;

	public $isLeaf = false;
	public $isSuppressed = false;
	public $isLoaded = false;
	
	/**
	 * @var Any
	 */
	public $parent = null;

	/**
	 * @var Any
	 */
	public $child = null;


	public function __construct()
	{
		$this->cid = static::$classId;
	}

	/**
	 *
	 */
	public static function init()
	{
		$class = get_called_class();
		$link = new $class();
		$link->uid = \Siergate\Model\Uuid::newUid();
		return $link;
	}
	
	/**
	 * @param string $name
	 * @return Any
	 */
	public function newUid()
	{
		$this->id = null;
		$this->uid = \Siergate\Model\Uuid::newUid();
		$this->isLoaded = false;
		$this->isSaved = false;
		$this->isSuppressed = false;
	}
	

	/**
	 * @param Any $parent
	 * @return Link
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
		$this->parentUid = $parent->getUid();
		$this->parentId = $parent->getId();
		return $this;
	}
	
	/**
	 * @return Any
	 */
	public function getParent($asId=false)
	{
		if($asId){
			return $this->parentId;
		}
		else{
			return $this->parent;
		}
	}

	/**
	 * @param Any $child
	 * @return Link
	 */
	public function setChild($child)
	{
		$this->child = $child;
		$this->childUid = $child->getUid();
		$this->childId = $child->getId();
		return $this;
	}

	/**
	 * @return Any
	 */
	public function getChild($asId=false)
	{
		if($asId){
			return $this->childId;
		}
		else{
			return $this->child;
		}
	}
	/**
	 * @param string
	 * @return string
	 */
	public function getAttribute($name)
	{
		return $this->attributes[$name];
	}

	/**
	 * @param array
	 * @return Link
	 */
	public function setAttributes($array)
	{
		$this->attributes = $array;
		return $this;
	}
	
	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Link
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['childId'])) ? $this->childId=$properties['childId'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['childUid'])) ? $this->childUid=$properties['childUid'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['isLeaf'])) ? $this->isLeaf=$properties['isLeaf'] : null;
		(isset($properties['isLoaded'])) ? $this->isLoaded=$properties['isLoaded'] : null;
		(isset($properties['isSuppressed'])) ? $this->isSuppressed=$properties['isSuppressed'] : null;
		(isset($properties['level'])) ? $this->level=$properties['level'] : null;
		(isset($properties['index'])) ? $this->index=$properties['index'] : null;
		(isset($properties['data'])) ? $this->data=json_decode($properties['data']) : null;
		return $this;
	}

	/**
	 * implement arrayObject interface
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		$return=array();
		foreach($this as $name=>$value){
			$return[$name] = $value;
		}
		return $return;
	}

	/**
	 * Implement arrayObject interface
	 * @param array $properties
	 */
	public function populate($properties)
	{
		return $this->hydrate( $properties );
	}
}
