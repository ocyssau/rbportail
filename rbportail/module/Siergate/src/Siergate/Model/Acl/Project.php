<?php
namespace Siergate\Model\Acl;

use Siergate\Dao;
use Application\Model\Acl\Acl;
use Application\Model\Acl\Role;
use Application\Model\Acl\Resource;
use Application\Model\People\User;
use Application\Model\Acl\NoRoleException;

final class Project extends Acl
{
    static $classId = 510;
	
    const ROLE_MANAGER = 8; //8
    const ROLE_CUSTOMER = 32; //32
    
	/**
	 * 
	 * @param string $ressourceId Id of project
	 * @param Acl\Acl $parentAcl	\Siergate\Model\Acl\Siergate
	 * 
	 */
	public function __construct($ressourceId, Siergate $parentAcl)
	{
		$this->acl = $parentAcl->getAcl();
		$this->parentAcl = $parentAcl;
		$this->mainResource = new Resource( $ressourceId );
		
		$this->rules = $this->parentAcl->rules;
		$this->roles = $this->parentAcl->roles;
		
		if(!$this->acl->hasRole(self::ROLE_CUSTOMER)){
			foreach($this->roles as $id=>$def){
				$this->acl->addRole(new Role($id),$def[1]);
			}
		}
		
		if(!$this->acl->hasResource($this->mainResource)){
			$this->acl->addResource($this->mainResource, $parentAcl->getMainResource());
			foreach($this->rules as $roleName=>$privileges){
				$this->acl->allow( $roleName, $this->mainResource, $privileges );
			}
		}
	}
	
	/**
	 * @param User $user
	 * @param string $resourceId
	 */
	public function loadRole(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();

		$parentAcl = $this->parentAcl;
		if($userId=='administrateur' || $parentAcl->hasRight($parentAcl::RIGHT_ADMIN)){
			$roles[] = self::ROLE_ADMIN;
		}

		$stmt = $this->getDao()->getRole("userId='$userId' AND resourceId='$resourceId'", 'roleId');
		while($role = $stmt->fetch(\PDO::FETCH_OBJ)){
			$this->acl->allow($role->roleId, $this->mainResource, $this->rules[$role->roleId]);
		}

		return $roles;
	}
	
}
