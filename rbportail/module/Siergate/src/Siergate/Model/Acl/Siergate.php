<?php
namespace Siergate\Model\Acl;

use Application\Model\Acl\Acl;
use Application\Model\Acl\Resource;
use Application\Model\Acl\Role;
use Application\Model\People\User;
use Application\Model\Acl\NoRoleException;
use Siergate\Model\Component;

/**
 * 
 *
 */
final class Siergate extends Acl
{
	static $classId = 505;

	const VISIBILITY_PRIVATE=10;
	const VISIBILITY_INTERNAL=20;
	const VISIBILITY_PUBLIC=40;
	
	const ROLE_MANAGER = 8; //8
	const ROLE_CUSTOMER = 32; //32

	/**
	 * @param string $ressourceId
	 */
	public function __construct($parentAcl)
	{
		$this->rules = array(
			self::ROLE_COLLABORATOR=>self::RIGHT_CONSULT,
			self::ROLE_CUSTOMER=>self::RIGHT_CONSULT,
			self::ROLE_MANAGER=>array(
				self::RIGHT_MANAGE,
				self::RIGHT_CONSULT,
				self::RIGHT_UPDATE,
				self::RIGHT_DELETE
			),
			self::ROLE_ADMIN=>null,
		);
		
		$this->roles = array(
			self::ROLE_CUSTOMER=>array('CUSTOMER',array()),
			self::ROLE_COLLABORATOR=>array('COLLABORATOR',array(self::ROLE_CUSTOMER)),
			self::ROLE_MANAGER=>array('MANAGER',array(self::ROLE_COLLABORATOR)),
			self::ROLE_ADMIN=>array('ADMINISTRATEUR',array(self::ROLE_MANAGER)),
		);
		
		$ressourceId = Component\Siergate::$id;
		$this->acl = new \Zend\Permissions\Acl\Acl();
		$this->parentAcl = $parentAcl;
		$this->mainResource = new Resource( $ressourceId );

		$this->acl->addResource($parentAcl->getMainResource());
		$this->acl->addResource($this->mainResource, $parentAcl->getMainResource());

		foreach($this->roles as $id=>$def){
			$this->acl->addRole(new Role($id),$def[1]);
		}
		
		foreach($this->rules as $roleName=>$privileges){
			$this->acl->allow( $roleName, $this->mainResource, $privileges );
		}
	}

	/**
	 * @param User $user
	 * @param string $resourceId
	 */
	public function loadRole(User $user, $resourceId=null)
	{
		if(self::$useLdap && $user->authFrom == 'ldap'){
			return $this->_loadRoleFromLdap($user, $resourceId);
		}
		elseif($user->memberof && $user->authFrom == 'db'){
			return $this->_loadRoleFromLocalDb($user, $resourceId);

			$userId = $user->getLogin();
			$this->userId = $userId;
			$roles = array(
				(int)$user->memberof,
			);
			$this->userRole = $roles;
			$this->acl->addRole($userId, $roles);
			return $roles;
		}
		else{
			return $this->_loadRoleFromLocalDb($user, $resourceId);
		}
	}

	/**
	 * Load a role from Db and add to Acl
	 * @param User $user
	 * @param string $resourceId
	 */
	protected function _loadRoleFromLdap(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();
		
		$parentAcl = $this->parentAcl;
		if($userId=='administrateur' || $parentAcl->hasRight($parentAcl::RIGHT_ADMIN)){
			$roles[] = self::ROLE_ADMIN;
		}

		$memberof = $user->memberof;

		if($memberof){
			if(in_array('RbportailAdmin', $memberof) || $userId=='administrateur' ){
				$roles[] = static::ROLE_ADMIN;
			}
			if(in_array('RbportailProjectManager', $memberof)){
				$roles[] = static::ROLE_MANAGER;
			}
			if(in_array('RbportailCollaborator', $memberof)){
				$roles[] = static::ROLE_COLLABORATOR;
			}
			if(in_array('Cloud', $memberof) || in_array('RbportailCustomer', $memberof)){
				$roles[] = static::ROLE_COLLABORATOR;
			}
			if(in_array('GG caomeca', $memberof) || in_array('RbportailCustomer', $memberof)){
				$roles[] = static::ROLE_COLLABORATOR;
			}
			if(count($roles)==0){
				throw new NoRoleException('User has no role define for this application');
			}
			else{
				$this->acl->addRole($userId, $roles);
			}
		}
		else{
			throw new \Exception( sprintf( 'A group must be set for user %s', $user->getLogin() ) );
		}
		$this->userRole = $roles;
		return $roles;
	}

	/**
	 * Load a role from Db and add to Acl
	 * @param User $user
	 * @param string $resourceId
	 */
	protected function _loadRoleFromLocalDb(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();

		$parentAcl = $this->parentAcl;
		if($userId=='administrateur' || $parentAcl->hasRight($parentAcl::RIGHT_ADMIN)){
			$roles[] = self::ROLE_ADMIN;
		}
		$stmt = $this->getDao()->getRole("userId='$userId' AND resourceId='$resourceId'", 'roleId');
		while($role = $stmt->fetch(\PDO::FETCH_OBJ)){
			$roles[] = $role->roleId;
		}
		if(count($roles)==0){
			throw new NoRoleException("User $userId has no role define for the Siergate module");
		}
		else{
			if(!$this->acl->hasRole($userId)){
				$this->acl->addRole($userId, $roles);
			}
		}
		$this->userRole = $roles;
		return $roles;
	}

	/**
	 */
	public static function convertRoleToVisibility($roleId)
	{
		switch($roleId){
			case self::ROLE_MANAGER:
				$visibility = 0;
				break;
			case self::ROLE_ADMIN:
				$visibility = 0;
				break;
			case self::ROLE_COLLABORATOR:
				$visibility = 20;
				break;
			case self::ROLE_CUSTOMER:
				$visibility = 40;
				break;
			case self::ROLE_GUEST:
				$visibility = 100;
				break;
			default:
				$visibility = 0;
				break;
		}
		return $visibility;
	}

}
