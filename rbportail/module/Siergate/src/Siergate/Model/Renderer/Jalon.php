<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Jalon extends Statusbox
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/jalon';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->targetDate = $this->component->getTargetdate();
		return $this;
	}
}
