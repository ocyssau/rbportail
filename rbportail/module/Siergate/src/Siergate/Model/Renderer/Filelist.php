<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Filelist extends AbstractRenderer
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/filelist';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->notice = $this->component->getNotice();
		$this->view->items = json_decode($this->component->getItems());
		
		//get pictures from files
		$images = array();
		foreach( $this->view->items as $item ) {
			//file is a image?
			if ( $item->isImage == true ) {
				$images[] = $item;
			}
		}
		$this->view->images = $images;
		$this->view->reposit = $this->component->getReposit();
		return $this;
	}
}
