<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Jssor extends Filelist
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/jssor';
		parent::__construct($component, $view);
	}
}
