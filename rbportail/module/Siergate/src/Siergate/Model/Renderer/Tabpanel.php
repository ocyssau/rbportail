<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 */
class Tabpanel extends AbstractRenderer
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/tabpanel';
		parent::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->tabs = $this->component->getChildren();
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Siergate\Model\Renderer.AbstractRenderer::render()
	 */
	public function render()
	{
		$mode = $this->mode;
		$i = 0;
		foreach( $this->component->getChildren() as $childComponent ) {
			$i++;
			$renderer = $childComponent->getRenderer();
			$renderer->setMode($mode);
			$renderer->bind();
			$childView = $renderer->render();
			$this->view->addChild($childView, 'children' . $i);
		}
		$this->view->mode = $mode;
		return $this->view;
	}
}
