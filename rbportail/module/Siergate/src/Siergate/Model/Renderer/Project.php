<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Project extends AbstractRenderer
{
	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		parent::__construct($component,$view);
		$this->template = 'component/project';
		$this->view->setTemplate($this->template);
	}
	
	
	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		$this->view->id = $this->component->getId();
		$this->view->uid = $this->component->getUid();
		$this->view->title = $this->component->getTitle();
		$this->view->body = $this->component->getBody();
		$this->view->width =$this->component->getWidth();
		$this->view->height = $this->component->getHeight();
		$this->view->visibility = self::visibilityToView($this->component->getVisibility());
		return $this;
	}
	
}
