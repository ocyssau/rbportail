<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Tinymce extends AbstractRenderer
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/tinymce';
		parent::__construct($component, $view);
	}
}
