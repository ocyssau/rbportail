<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Statusbox extends AbstractRenderer
{
	
	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/statusbox';
		AbstractRenderer::__construct($component,$view);
	}
	
	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->label = $this->component->getLabel();
		$this->view->status = $this->component->getStatus();
		$this->view->notice = $this->component->getNotice();
		$this->view->values = json_decode($this->component->getStatuslist());
		return $this;
	}
}
