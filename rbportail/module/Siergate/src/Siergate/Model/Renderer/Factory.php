<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

use Siergate\Model\Component\AbstractComponent;
use Zend\View\Model\ViewModel;

/**
 * 
 * @author ocyssau
 *
 */
class Factory
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 */
	public static function get($component)
	{
		if ( $component instanceof AbstractComponent ) {
			$className = get_class($component);
		}
		
		$parts = explode('\\', $className);
		$RendererClass = __NAMESPACE__ . '\\' . end($parts);
		
		return new $RendererClass($component, new ViewModel());
	}
}
