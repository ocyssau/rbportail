<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Box extends AbstractRenderer
{
	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/box';
		parent::__construct($component,$view);
	}
}
