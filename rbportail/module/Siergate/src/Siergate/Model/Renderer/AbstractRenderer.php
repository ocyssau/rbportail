<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Renderer;

use Siergate\Model\Component\AbstractComponent;
use Siergate\Model\Acl;
use Zend\View\Model\ViewModel;

abstract class AbstractRenderer
{
	
	const MODE_BUILD = 'build';
	const MODE_VIEW = 'view';
	
    /**
     * @var AbstractComponent
     */
	protected $component;
	
	/**
	 * @var ViewModel
	 */
	protected $view;
	
	/**
	 * @var array
	 */
	protected $children;
	
	/**
	 * @var string
	 */
	protected $template;
	
	/**
	 * @var string
	 */
	protected $html;
	
	/**
	 * value is one of const MODE_*
	 * @var string
	 */
	protected $mode;
		
	/**
	 * @param AbstractComponent $component
	 * @param ViewModel $view
	 */
	public function __construct(AbstractComponent $component, ViewModel $view)
	{
	    $this->component = $component;
	    $this->view = $view;
	    $this->view->setTemplate($this->template);
	}
	
	/**
	 * Get children component of current component
	 * Create a renderer for each child
	 * Render each child renderer = get children of children...
	 * 
	 * Add a template var named as children# where # is a incremental integer start from 1
	 * children# template var contain the html of the child
	 * 
	 * @return string
	 */
	public function render()
	{
		$mode = $this->mode;
		$i=0;
		foreach($this->component->getChildren() as $childComponent){
			$i++;
			$renderer = $childComponent->getRenderer();
			$renderer->setMode($mode);
			$renderer->bind();
			$childView = $renderer->render();
			$this->view->addChild($childView, 'children'.$i);
		}
		$this->view->mode = $mode;
		return $this->view;
	}
	
	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		$this->view->id = $this->component->getId();
		$this->view->uid = $this->component->getUid();
		$this->view->title = $this->component->getTitle();
		$this->view->body = $this->component->getBody();
		$this->view->width =$this->component->getWidth();
		$this->view->height = $this->component->getHeight();
		$this->view->resizable = $this->component->isResizable();
		$this->view->resizablex = $this->component->isResizablex();
		$this->view->resizabley = $this->component->isResizabley();
		$this->view->draggable = $this->component->isDraggable();
		$this->view->visibility = self::visibilityToView($this->component->getVisibility());
		$this->view->updated = $this->component->getUpdated()->format('Y-m-d H:i:s');
		$this->view->updateBy = $this->component->getUpdateBy(true);
		$this->view->collapsed = $this->component->isCollapsed();
		return $this;
	}
	
	/**
	 * Convert visibility code to visibility string name
	 * 
	 * @param integer $visibilityCode
	 * @return string
	 */
	public static function visibilityToView($visibility)
	{
	    switch(strtolower($visibility)){
	        case Acl\Siergate::VISIBILITY_PRIVATE:
	            return 'private';
	            break;
	        case Acl\Siergate::VISIBILITY_INTERNAL:
	            return 'internal';
	            break;
	        case Acl\Siergate::VISIBILITY_PUBLIC:
	            return 'public';
	            break;
	        default:
	            return $visibility;
	    }
	}
	
	/**
	 * @param string
	 * @return AbstractRenderer
	 */
	public function setTemplate($template)
	{
	    $this->template = $template;
	    return $this;
	}
	
	/**
	 * @param ViewModel
	 * @return AbstractRenderer
	 */
	public function setView($view)
	{
	    $this->view = $view;
	    return $this;
	}
	
	/**
	 * $string must be one const MODE_*
	 * @param string
	 * @return AbstractRenderer
	 */
	public function setMode($string)
	{
		$this->mode = $string;
		return $this;
	}
	
	/**
	 * @return ViewModel
	 */
	public function getView()
	{
	    return $this->view;
	}
	
	/**
	 * return one MODE_*
	 * @return string 
	 */
	public function getMode()
	{
		return $this->mode;
	}
	
	/**
	 * @param AbstractComponent
	 * @return AbstractRenderer
	 */
	public function setComponent($component)
	{
	    $this->component = $component;
	    return $this;
	}
	
	/**
	 * @return AbstractComponent
	 */
	public function getComponent()
	{
	    return $this->component;
	}
	
	/**
	 * @return String
	 */
	public function getHtml()
	{
	}

}
