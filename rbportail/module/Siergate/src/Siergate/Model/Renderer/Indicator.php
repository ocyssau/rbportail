<?php
//%LICENCE_HEADER%
namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Indicator extends AbstractRenderer
{

	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/indicator';
		parent::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->label = $this->component->getLabel();
		$this->view->notice = $this->component->getNotice();
		$this->view->value = $this->component->getValue();
		return $this;
	}
}
