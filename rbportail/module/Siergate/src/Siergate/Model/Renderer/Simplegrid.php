<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Simplegrid extends AbstractRenderer
{
	
	/**
	 * @param \Siergate\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/simplegrid';
		AbstractRenderer::__construct($component,$view);
	}
	
	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->notice = $this->component->getNotice();
		$this->view->items = json_decode($this->component->getItems());
		return $this;
	}
}
