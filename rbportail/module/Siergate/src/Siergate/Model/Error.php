<?php
//%LICENCE_HEADER%

namespace Siergate\Model;

/**
 * @brief Define a package of constants to set error code in Rbplm.
 *
 */
Class Error extends \Application\Model\Error
{
}
