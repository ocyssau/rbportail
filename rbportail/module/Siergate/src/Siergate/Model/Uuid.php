<?php
//%LICENCE_HEADER%
namespace Siergate\Model;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 * 
 * Note you need php-uuid installed.
 * @code
 * //On ubuntu, do simply
 * sudo apt-get install php5-uuid
 * @endcode
 *
 * @see http://fr.wikipedia.org/wiki/Universal_Unique_Identifier
 * @author Marius Karthaus
 * @author Eric COSNARD
 * @author Olivier CYSSAU
 *
 */
class Uuid implements \Rbplm\Uuid\GeneratorInterface
{
	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		return uniqid();
	}
	
	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		return $uuid;
	}
}
