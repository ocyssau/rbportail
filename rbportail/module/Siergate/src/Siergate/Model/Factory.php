<?php
//%LICENCE_HEADER%

namespace Siergate\Model;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{
    /**
     * Registry of instanciated DAO.
     *
     * @var array
     */
    private static $_registry = array();
    
    /**
     * Assiciate for each Component Class ID a DAO CLASS
     * ClassId 200 return the super class DAO Component/Component
     * @var array
     */
    private static $_map = array(
    	10=>array('Component\Siergate', 'component'),
    	90=>array('Component\Template', 'component'),
    	100=>array('Component\Project', 'component'),
    	101=>array('Component\Box', 'component'),
    	102=>array('Component\Indicator', 'component'),
    	103=>array('Component\Jalon', 'component'),
    	104=>array('Component\Statusbox', 'component'),
    	105=>array('Component\Tabpanel', 'component'),
    	106=>array('Component\Checkbox', 'component'),
    	107=>array('Component\Simplegrid', 'component'),
    	108=>array('Component\Tabpanel\Item', 'component'),
    	110=>array('Component\Tinymce', 'component'),
        111=>array('Component\Jssor', 'component'),
    	112=>array('Component\Tab', 'component'),
    	115=>array('Component\Filelist', 'component'),
    	116=>array('Component\Accordion', 'component'),
    	500=>array('Acl\Acl', 'acl'),
    	505=>array('\Siergate\Model\Acl\Siergate', 'acl'),
    	510=>array('Acl\Project', 'acl'),
    	550=>array('\Application\Model\People\User', 'user'),
    );
    
    /**
     * Return a instance of class specified by id.
     * 
	 * @param integer $classId
     * @return \Application\Dao\Dao
     */
    public static function get($classId)
    {
        	$class = __NAMESPACE__ . '\\' . self::$_map[$classId][0];
            return new $class();
    }
    
    /**
     * Return a initialized instance of class specified by id.
     *
     * @param integer $classId
     * @return \Application\Dao\Dao
     */
    public static function getNew($classId)
    {
        $class = __NAMESPACE__ . '\\' . self::$_map[$id][0];
        return $class::init();
    }
    
}
