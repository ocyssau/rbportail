<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model;

class Project extends AbstractComponent
{
	protected $description;
	protected $internalref;
	
	/**
	 * @var integer
	 */
	public static $classId = 100;
	
	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return AbstractComponent
	 */
	public function hydrate( array $properties )
	{
		parent::hydrate($properties);
		(isset($properties['internalref'])) ? $this->internalref=$properties['internalref'] : null;
		return $this;
	} //End of function
	
	/**
	 * @param string $string
	 */
	public function setDescription($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * $return string
	 */
	public function getDescription()
	{
		return $this->notice;
	}
	
	/**
	 * @param string $string
	 */
	public function setInternalref($string)
	{
		$this->internalref = $string;
		return $this;
	}
	
	/**
	 * $return string
	 */
	public function getInternalref()
	{
		return $this->internalref;
	}
	
}
