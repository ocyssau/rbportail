<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model;

class Tab extends AbstractComponent
{
	
	/**
	 * @var integer
	 */
	public static $classId = 112;
	
	/**
	 *
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * @var string
	 */
	protected $label='{empty}';
	
	/**
	 * @param string $string
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * $return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setLabel($string)
	{
		$this->label = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

}
