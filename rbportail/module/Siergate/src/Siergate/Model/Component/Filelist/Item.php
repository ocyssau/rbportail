<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component\Filelist;

use Application\Model\Filesystem;
use Application\Model\Filesystem\File;

class Item
{
	protected $file;
	protected $fullpath;
	
	public $id;
	public $name;
	public $size;
	public $mtime;
	public $isImage=false;
	
	/**
	 * 
	 * @param string $fullpath
	 */
	public function __construct($fullpath=null)
	{
		if(is_file($fullpath)){
			$this->setFile($fullpath);
		}
		$this->id = uniqid();
	}
	
	/**
	 * 
	 * @param string $fullpath
	 */
	public function setFile($name, $fullpath)
	{
		$ext = File::sGetExtension($name);
		
		$this->file = new File($fullpath);
		$this->id = $this->file->getName();
		$this->name = $name;
		$this->size = $this->file->getSize();
		$this->mtime = $this->file->getMtime()->format('Y-m-d');
		$this->fullpath = $fullpath;
		
		if($ext == '.jpg' || $ext == '.gif' || $ext == '.png' || $ext == '.tif' || $ext == '.tiff'){
			$this->isImage = true;
		}
	}
	
	/**
	 * @return string
	 */
	public function getFile()
	{
		if(!$this->file){
			$this->file = new File($this->fullpath);
		}
		return $this->file;
	}
	
}
