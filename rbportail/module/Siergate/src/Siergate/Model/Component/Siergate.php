<?php
namespace Siergate\Model\Component;

use Siergate\Model;

class Siergate
{
	/**
	 * @var integer
	 */
	public static $classId = 10;
	public static $id = 'siergate';
	public static $version = '0.3';
	public static $build = '07mai2015';
	
	public static function getCopyright()
	{
		$copyright = '&copy; 2015 - '.date('Y').' By Olivier CYSSAU pour SIER - All rights reserved.';
		return $copyright;
	}
	
	public static function getVersion()
	{
		return self::$version;
	}
	
	public static function getId()
	{
		return self::$id;
	}
}
