<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model;

class Indicator extends AbstractComponent
{
	/**
	 * 
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * @var string
	 */
	protected $label='{empty}';
	
	/**
	 * @var integer
	 */
	protected $value = 0;
	
	/**
	 * @var integer
	 */
	public static $classId = 102;
	
	/**
	 * @param string $string
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * @param integer $int
	 */
	public function setValue($int)
	{
		$this->value = (int)$int;
		return $this;
	}
	
	/**
	 * $return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	/**
	 * $return integer
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setLabel($string, $format=false)
	{
		$this->label = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}
	
	
}
