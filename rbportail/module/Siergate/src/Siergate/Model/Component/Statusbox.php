<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

/**
 * 
 * 
 * @author ocyssau
 *
 */
class Statusbox extends AbstractComponent
{
	/**
	 * @var integer
	 */
	public static $classId = 104;
	
	/**
	 * @var string
	 */
	protected $label='{empty}';
	
	/**
	 * @var string
	 */
	protected $status='LABEL 1';
	
	/**
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * Json string array
	 * @var string
	 */
	protected $statuslist = "[0:'LABEL 1']";
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setStatus($string)
	{
		$this->status = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * @param string json
	 * @return Jalon
	 */
	public function setStatuslist($json)
	{
		$this->statuslist = $json;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setLabel($string, $format=false)
	{
		$this->label = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}
	
	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	/**
	 * @return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	/**
	 * @return string json
	 */
	public function getStatuslist()
	{
		return $this->statuslist;
	}
}
