<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component\Tabpanel;

class Item
{
	public function __construct($index, $notice, $label)
	{
		$this->index = $index;
		$this->notice = $notice;
		$this->label = $label;
	}
}
