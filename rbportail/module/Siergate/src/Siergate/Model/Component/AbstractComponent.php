<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model;
use Siergate\Model\Renderer;
use Siergate\Model\Any;
use Application\Model\People;
use Application\Model\People\User;
use DateTime;

abstract class AbstractComponent extends Any
{
	
	static $classId = 117;
	
	/**
	 * @var string
	 */
	protected $index=1;
	
	/**
	 * @var boolean
	 */
	protected $visibility='private';
	
	/**
	 * @var string
	 */
	protected $title='{empty}';
	
	/**
	 * @var string
	 */
	protected $body='{empty}';
	
	/**
	 * @var string
	 */
	protected $xoffset;
	
	/**
	 * @var string
	 */
	protected $yoffset;
	
	/**
	 * @var string
	 */
	protected $width='100%';
	
	/**
	 * @var string
	 */
	protected $height=null;
	
	/**
	 * @var boolean
	 */
	protected $resizable=false;
	
	/**
	 * @var boolean
	 */
	protected $resizablex=false;
	
	/**
	 * @var boolean
	 */
	protected $resizabley=false;
	
	/**
	 * @var boolean
	 */
	protected $draggable=false;
	
	/**
	 * @var boolean
	 */
	protected $collapsed = false;
	
	/**
	 * @var integer
	 */
	protected $zindex=100;
	
	/**
	 * @var Renderer\AbstractRenderer
	 */
	protected $renderer;
	
	/**
	 * @param AbstractComponent $parent
	 */
	public function __construct($properties=null)
	{
		if($properties){
			foreach($properties as $name=>$value){
				$this->$name = $value;
			}
		}
		$this->cid = static::$classId;
	}
	
	/**
	 * @param string $name
	 * @return AbstractComponent
	 */
	public static function init($name="", $parent=null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();
		$obj->visibility = 'private';
		
		if( !$name ){
			$name = uniqid(get_class($obj));
		}
		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());
		
		if($parent){
			$obj->setParent($parent);
		}
		
		return $obj;
	}
	
	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return AbstractComponent
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		
		(isset($properties['ownerId'])) ? $this->ownerId=$properties['ownerId'] : null;
		(isset($properties['updateById'])) ? $this->updateById=$properties['updateById'] : null;
		(isset($properties['updateById'])) ? $this->setUpdated( DateTime::createFromFormat('Y-m-d H:i:s', $properties['updated']) ) : null;
		
		(isset($properties['index'])) ? $this->index=$properties['index'] : null;
		(isset($properties['visibility'])) ? $this->visibility=$properties['visibility'] : null;
		
		if( isset($properties['attributes']) && is_array($properties['attributes'])){
			foreach($properties['attributes'] as $name=>$value){
				if($value=='true') $value = true;
				elseif($value=='false') $value = false;
				else $value = $value; //@todo; filter user input
				$this->$name=$value;
			}
		}
		
		return $this;
	} //End of function
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setBody($string)
	{
		$this->body = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setTitle($string)
	{
		$this->title = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setWidth($string)
	{
		$this->width = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setHeight($string)
	{
		$this->height = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setXoffset($string)
	{
		$this->xoffset = $string;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setYoffset($string)
	{
		$this->yoffset = $string;
		return $this;
	}
	
	/**
	 * @param integer $int
	 * @return AbstractComponent
	 */
	public function setZindex($int)
	{
		return $this->zindex = $int;
		return $this;
	}
	
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizable($bool=null)
	{
		if(is_null($bool)){
			return $this->resizable;
		}
		$this->resizable = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizablex($bool=null)
	{
		if(is_null($bool)){
			return $this->resizablex;
		}
		$this->resizablex = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizabley($bool=null)
	{
		if(is_null($bool)){
			return $this->resizabley;
		}
		$this->resizabley = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isDraggable($bool=null)
	{
		if(is_null($bool)){
			return $this->draggable;
		}
		$this->draggable = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isSaved($bool=null)
	{
		if(is_null($bool)){
			return $this->saved;
		}
		$this->saved = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isCollapsed($bool=null)
	{
		if(is_null($bool)){
			return $this->collapsed;
		}
		$this->collapsed = (boolean)$bool;
		return $this;
	}
	
	/**
	 * @param integer $int
	 * @return AbstractComponent
	 */
	public function setIndex($int)
	{
		$this->index = $int;
		return $this;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setVisibility($string)
	{
		$this->visibility = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}
	
	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * @return integer
	 */
	public function getIndex()
	{
		return $this->index;
	}

	/**
	 * @return string
	 */
	public function getVisibility()
	{
		return $this->visibility;
	}
	
	/**
	 * @return string
	 */
	public function getWidth()
	{
		return $this->width;
	}
	
	/**
	 * @return string
	 */
	public function getXoffset()
	{
		return $this->xoffset;
	}
	
	/**
	 * @return string
	 */
	public function getYoffset()
	{
		return $this->yoffset;
	}

	/**
	 * @return string
	 */
	public function getZindex()
	{
		return $this->zindex;
	}
	
	/**
	 * @return string
	 */
	public function getHeight()
	{
		return $this->height;
	}
	
	/**
	 * @return Renderer\AbstractRenderer
	 */
	public function getRenderer()
	{
		if(!isset($this->renderer)){
			$this->renderer = Renderer\Factory::get($this);
		}
		return $this->renderer;
	}
}


