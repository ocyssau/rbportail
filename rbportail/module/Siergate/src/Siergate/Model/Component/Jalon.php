<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

class Jalon extends Statusbox
{
	
	/**
	 * @var integer
	 */
	public static $classId = 103;
	
	/**
	 * TargetDate is recorded as a simple string
	 * 
	 * @var string
	 */
	protected $targetdate;
	
	/**
	 * @param string $date
	 * @return Jalon
	 */
	public function setTargetdate($date)
	{
        $this->targetdate = $date;
		return $this;
	}
    
	/**
	 * @return string
	 */
	public function getTargetdate()
	{
		return $this->targetdate;
	}
	
}
