<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component\Checkbox;

class Item
{
	public function __construct($name, $checked, $notice)
	{
		$this->name = $name;
		$this->checked = (boolean)$checked;
		$this->notice = $notice;
	}
}
