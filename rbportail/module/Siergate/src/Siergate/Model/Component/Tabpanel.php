<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model;
use Siergate\Model\Component\Tabpanel\Item;

class Tabpanel extends AbstractComponent
{
	/**
	 * @var integer
	 */
	public static $classId = 105;
	
	/**
	 * Array of Item
	 * @var array
	 */
	protected $items = array();
	
	/**
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * @param 
	 * @return Tabpanel
	 */
	public function addItem(Item $item)
	{
		$this->items[]=$item;
	}
	
	/**
	 * Return a array of Item objects
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * Return a array of Item objects
	 * @return array
	 */
	public function setItems($array)
	{
		$this->items = $array;
		return $this;
	}
	
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	
}
