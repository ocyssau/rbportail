<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Siergate\Model\Component\Checkbox\Item;

/**
 * 
 * @author ocyssau
 *
 */
class Checkbox extends AbstractComponent
{
	/**
	 * @var integer
	 */
	public static $classId = 106;
	
	/**
	 * 
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * Json array
	 * @var string
	 */
	protected $items='';
	
	/**
	 * @param string $name
	 * @return AbstractComponent
	 */
	public static function init($name="", $parent=null)
	{
		$obj = parent::init($name, $parent);
		$obj->items = json_encode(array(
			new Item('{empty}', true, '{empty}'),
			new Item('{empty}', false, '{empty}'),
			new Item('{empty}', false, '{empty}'),
		));
		return $obj;
	}
	
	/**
	 * @param string $string
	 * @return Jalon
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * @param string json
	 * @return Jalon
	 */
	public function setItems($json)
	{
		$this->items = $json;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	/**
	 * @return string json
	 */
	public function getItems()
	{
		return $this->items;
	}
}
