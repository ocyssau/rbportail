<?php
//%LICENCE_HEADER%

namespace Siergate\Model\Component;

use Application\Model\Filesystem\Reposit;

/**
 * 
 * @author ocyssau
 *
 */
class Filelist extends AbstractComponent
{
	
	/**
	 * @var integer
	 */
	public static $classId = 115;
	
	/**
	 * @var string
	 */
	protected $notice='{empty}';
	
	/**
	 * Json array
	 * @var string
	 */
	protected $items='';
	
	/**
	 * Reposit uid
	 * @var string
	 */
	protected $reposit='';
	
	/**
	 * @param string $name
	 * @return AbstractComponent
	 */
	public static function init($name="", $parent=null)
	{
		$obj = parent::init($name, $parent);
		$reposit = Reposit::init();
		$obj->setReposit($reposit->getUid());
		$obj->items = json_encode(array());
		
		return $obj;
	}
	
	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setNotice($string)
	{
		$this->notice = $string;
		return $this;
	}
	
	/**
	 * @param string json
	 * @return AbstractComponent
	 */
	public function setItems($json)
	{
		$this->items = $json;
		return $this;
	}
	
	/**
	 * @param string
	 * @return AbstractComponent
	 */
	public function setReposit($string)
	{
		$this->reposit = $string;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNotice()
	{
		return $this->notice;
	}
	
	/**
	 * @return string json
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	/**
	 * @return string
	 */
	public function getReposit()
	{
		return $this->reposit;
	}
	
}
