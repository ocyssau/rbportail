<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Siergate;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface as Event;
use Application\Dao\Connexion;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Application\Model\Auth;
use Application\Model\People;
use Application\Dao;
use Application\Model\Filesystem;

class Module implements AutoloaderProviderInterface
{
	/**
	 * 
	 * @param Event $e
	 * @throws \Exception
	 */
	public function onBootstrap(Event $e)
	{
		$config = $e->getApplication()->getServiceManager()->get('Configuration');
		Filesystem\Reposit::$basePath = realpath($config['rbp']['path']['reposit']);
	}

	/**
	 * 
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\ModuleManager\Feature.AutoloaderProviderInterface::getAutoloaderConfig()
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					'Rbplm'=>realpath('./vendor/Rbplm'),
				),
			),
		);
	}
}
