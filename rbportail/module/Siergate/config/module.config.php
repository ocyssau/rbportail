<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'project' => array(
				'type' => 'Segment',
				'options' => array(
					'route'=> '/project[/:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Siergate\Controller\Project',
						'action' => 'index',
					),
				),
			),
			'projectbuilder' => array(
				'type' => 'Segment',
				'options' => array(
					'route'=> '/projectbuilder[/:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Siergate\Controller\Projectbuilder',
						'action' => 'edit'
					),
				),
			),
			'template' => array(
				'type' => 'Segment',
				'options' => array(
					'route'=> '/template[/:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Siergate\Controller\Template',
						'action' => 'index',
					),
				),
			),
			'component' => array(
				'type' => 'Segment',
				'options' => array(
					'route'=> '/component[/:action][/][:id]',
					'defaults' => array(
						'controller' => 'Siergate\Controller\Component',
						'action' => 'index',
					),
				),
			),
			'sguser' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/siergate/user[/:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Siergate\Controller\User',
						'action' => 'index',
					),
				),
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'siergate' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/siergate',
					'defaults' => array(
						'__NAMESPACE__' => 'Siergate\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Siergate\Controller\Index' => 'Siergate\Controller\IndexController',
			'Siergate\Controller\Project' => 'Siergate\Controller\ProjectController',
			'Siergate\Controller\Projectbuilder' => 'Siergate\Controller\ProjectbuilderController',
			'Siergate\Controller\Template' => 'Siergate\Controller\TemplateController',
			'Siergate\Controller\Component' => 'Siergate\Controller\ComponentController',
			'Siergate\Controller\User' => 'Siergate\Controller\UserController',
			),
	),
	'view_manager' => array(
		'template_map' => array(
			'layout/siergate'   => __DIR__ . '/../view/layout/layout.phtml',
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);
